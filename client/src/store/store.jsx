import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import localForage from 'localforage';
const intialState = {};
const middleware = [thunk];

const persistConfig = {
	key: 'root',
	storage: localForage,
	blacklist: ['checkout', 'orders', 'auth', 'contact', 'isSubmitted', 'alerts']
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
let store = createStore(
	persistedReducer,
	intialState,
	composeWithDevTools(applyMiddleware(...middleware))
);
let persistor = persistStore(store);

export { store, persistor };
