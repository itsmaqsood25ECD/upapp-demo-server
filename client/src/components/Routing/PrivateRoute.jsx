import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({
	component: Component,
	user,
	auth: { loading },
	...rest
}) => {
	return loading && user === null ? (
		<Fragment>Spinner</Fragment>
	) : (
			<Route
				{...rest}
				render={props =>
					user && user.role === "admin" || user.role == "agency" ? (
						<Component {...props} />
					) : (
							<Redirect to="/cms/admin/login" />
						)
				}
			/>
		);
};

PrivateRoute.propTypes = {
	user: PropTypes.object.isRequired,
	auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth,
	user: state.auth.user
});
export default connect(mapStateToProps)(PrivateRoute);
