import React, { useEffect, useState, Fragment } from "react";
import "react-phone-number-input/style.css";
import { connect } from "react-redux";
import SrollTop from "../ScrollTop";
import { Helmet } from "react-helmet";
import FileBase64 from "react-file-base64";
import MobileHome from "./MobileHome";
import { SliderPicker } from "react-color";
import {
  Typography,
  Row,
  Col,
  Form,
} from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';

const { Paragraph } = Typography;

const MobileAppPage = ({
  form: { getFieldDecorator, validateFieldsAndScroll },
  direction
}) => {
  const { t } = useTranslation();

  const [formData, setFormData] = useState({
    files: [],
    logo_img: "",
    primary_color: "#2c786c",
    secondry_color: "#2c786c",
    bg_color: "#fff"
  });

  const handleChangeComplete = (color, event) => {
    setFormData({
      ...formData,
      primary_color: color.hex
    });
  };

  const handleChangeCompleteSec = (color, event) => {
    setFormData({
      ...formData,
      secondry_color: color.hex
    });
  };

  // console.log("Primary Color : >>", localStorage.getItem("app_primary_color"));
  // console.log(
  //   "Secondery Color : >>",
  //   localStorage.getItem("app_secondry_color")
  // );

  const getFiles = files => {
    // console.log(files[0].base64);
    setFormData({ ...formData, logo_img: files[0].base64 });
  };

  return (
    <Fragment>
      <SrollTop />
      <Helmet></Helmet>
      <div
        style={{
          marginTop: "20px",
          minHeight: "580px",
          height: "100%",
          background: "#fff"
        }}
      >
        {direction === DIRECTIONS.LTR  && <Fragment>

          <Row>

<Col md={12} lg={12} xs={24}>
  <Form
    style={{
      boxShadow: "0px 0px 10px #ccc",
      padding: "40px 20px",
      background: "#fff",
      textAlign: "left",
      maxWidth: "450px",
      margin: "0px auto"
    }}
  >
    <Row gutter={16}>
      <Col xs={24} lg={24}>
        <p
          style={{
            fontSize: "18px",
            margin: "0px auto 60px auto",
            textAlign: "center"
          }}
        >
          {t("ecommercePage.mobielView.howyourbrand")}
        </p>
        <p>
          <b>{t("ecommercePage.mobielView.PickPrimaryColor")}</b>
        </p>
        <Form.Item>
          {getFieldDecorator("primary_color", {
            rules: [
              {
                required: true,
                message: "please enter Primary color"
              }
            ]
          })(
            <SliderPicker
              name="primary_color"
              onChangeComplete={e => handleChangeComplete(e)}
              color={formData.primary_color}
            />
          )}
        </Form.Item>
      </Col>
      <Col xs={24} lg={24}>
        <p>
          <b>{t("ecommercePage.mobielView.PickSeconderyColor")}</b>
        </p>
        <Form.Item>
          {getFieldDecorator("secondry_color", {
            rules: [
              {
                required: true,
                message: "please enter secondary Color"
              }
            ]
          })(
            <SliderPicker
              name="secondry_color"
              onChangeComplete={e => handleChangeCompleteSec(e)}
              color={formData.secondry_color}
            />
          )}
        </Form.Item>
      </Col>
      <Col xs={24} lg={24}>
        <p>
          <b>{t("ecommercePage.mobielView.UploadyourBrandLogo")}</b>
        </p>
        <Form.Item>
          {getFieldDecorator("logo_img", {
            rules: [
              {
                required: true,
                message: "Please Upload Your Brand logo"
              }
            ]
          })(
            <FileBase64
              name="logo_img"
              multiple={true}
              onDone={getFiles}
            />
          )}
        </Form.Item>
      </Col>
    </Row>
    <br />
    <hr />
    <br />
    <Row>
      <Col xs={24} lg={12}>
        <p>
          {" "}
          <b>{t("ecommercePage.mobielView.primary")} :</b> <Paragraph style={{fontSize:"18px"}} copyable>{formData && formData.primary_color}</Paragraph>
        </p>
      </Col>
      <Col xs={24} lg={12}>
        <p>
          {" "}
          <b>{t("ecommercePage.mobielView.secondery")} :</b>{" "}
          <Paragraph style={{fontSize:"18px"}} copyable>{formData && formData.secondry_color}</Paragraph>
        </p>
      </Col>
    </Row>
    <hr />
    <br />
    <br />
    <p style={{ marginTop: "30px" }}>
      <b>*{t("ecommercePage.mobielView.primary")}</b> - {t("ecommercePage.mobielView.PrimaryColor")}
     
    </p>
    <p>
      <b>*{t("ecommercePage.mobielView.secondery")}</b> - {t("ecommercePage.mobielView.SecondaryColor")}
     
    </p>
    <p>
      <b>*{t("ecommercePage.mobielView.BrandL")} </b> - {t("ecommercePage.mobielView.BrandLogo")}
    </p>
  </Form>

</Col>

<Col md={12} lg={12} xs={24} style={{ paddingTop: "60px" }}>
  <div class="smartphone-portrait">
    <MobileHome
      primary_color={formData && formData.primary_color}
      secondry_color={formData && formData.secondry_color}
      bg_color={formData && formData.bg_color}
      logo_img={formData && formData.logo_img}
    />
  </div>
</Col>

</Row>


        </Fragment>
}
{direction === DIRECTIONS.RTL  && <Fragment>

<Row>
<Col md={12} lg={12} xs={24} style={{ paddingTop: "60px" }}>
<div class="smartphone-portrait">
<MobileHome
primary_color={formData && formData.primary_color}
secondry_color={formData && formData.secondry_color}
bg_color={formData && formData.bg_color}
logo_img={formData && formData.logo_img}
/>
</div>
</Col>
<Col md={12} lg={12} xs={24}>
<Form
style={{
boxShadow: "0px 0px 10px #ccc",
padding: "40px 20px",
background: "#fff",
textAlign: "left",
maxWidth: "450px",
margin: "30px auto"
}}
className="lg-rtl"
>
<Row gutter={16}>
<Col xs={24} lg={24} className="lg-rtl">
<p
style={{
  fontSize: "18px",
  margin: "0px auto 60px auto",
  textAlign: "center"
}}
className="lg-rtl"
>
{t("ecommercePage.mobielView.howyourbrand")}
</p>
<p className="lg-rtl" > 
<b>{t("ecommercePage.mobielView.PickPrimaryColor")}</b>
</p>
<Form.Item>
{getFieldDecorator("primary_color", {
  rules: [
    {
      required: true,
      message: "please enter Primary color"
    }
  ]
})(
  <SliderPicker
    name="primary_color"
    onChangeComplete={e => handleChangeComplete(e)}
    color={formData.primary_color}
  />
)}
</Form.Item>
</Col>
<Col xs={24} lg={24} className="lg-rtl" >
<p>
<b>{t("ecommercePage.mobielView.PickSeconderyColor")}</b>
</p>
<Form.Item>
{getFieldDecorator("secondry_color", {
  rules: [
    {
      required: true,
      message: "please enter secondary Color"
    }
  ]
})(
  <SliderPicker
    name="secondry_color"
    onChangeComplete={e => handleChangeCompleteSec(e)}
    color={formData.secondry_color}
  />
)}
</Form.Item>
</Col>
<Col xs={24} lg={24} className="lg-rtl lg-float-right">
<p>
<b>{t("ecommercePage.mobielView.UploadyourBrandLogo")}</b>
</p>
<Form.Item>
{getFieldDecorator("logo_img", {
  rules: [
    {
      required: true,
      message: "Please Upload Your Brand logo"
    }
  ]
})(
  <FileBase64
    name="logo_img"
    multiple={true}
    onDone={getFiles}
  />
)}
</Form.Item>
</Col>
</Row>
<br />
<hr />
<br />
<Row>
<Col xs={24} lg={12} className="lg-rtl">
<p>
{" "}
<b>{t("ecommercePage.mobielView.primary")} :</b> {formData && formData.primary_color}
</p>
</Col>
<Col xs={24} lg={12} className="lg-rtl">
<p>
{" "}
<b>{t("ecommercePage.mobielView.secondery")} :</b>{" "}
{formData && formData.secondry_color}
</p>
</Col>
</Row>
<hr />
<br />
<br />
<p style={{ marginTop: "30px" }} className="lg-rtl">
<b>*{t("ecommercePage.mobielView.primary")}</b> - {t("ecommercePage.mobielView.PrimaryColor")}

</p>
<p className="lg-rtl">
<b>*{t("ecommercePage.mobielView.secondery")}</b> - {t("ecommercePage.mobielView.SecondaryColor")}

</p>
<p className="lg-rtl">
<b>*{t("ecommercePage.mobielView.BrandL")} </b> - {t("ecommercePage.mobielView.BrandLogo")}
</p>
</Form>
</Col>


</Row>


</Fragment>
}
      </div>
    </Fragment>
  );
};

const MAEC = Form.create({ name: "Demo_Request" })(MobileAppPage);
MAEC.propTypes = {};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, {})(withDirection(MAEC));
