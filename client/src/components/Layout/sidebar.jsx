import React, { Fragment, useState, useEffect } from 'react';
import { NavLink, Link, Redirect } from 'react-router-dom';
import { Menu, Layout, Icon, Skeleton } from 'antd';
import { logout } from '../../actions/auth.action';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrivateRoute from '../Routing/PrivateRoute';
import Dashboard from '../CMS/Dashboard/Dashboard';
import CreateCategory from '../CMS/CategoryManagement/Create';
import ListCategory from '../CMS/CategoryManagement/List';
import EditCategory from '../CMS/CategoryManagement/Edit';
import CreateJob from '../CMS/CareerManagement/Create';
import ListJob from '../CMS/CareerManagement/List';
import EditJob from '../CMS/CareerManagement/Edit';
import CreateApp from '../CMS/AppManagement/Create';
import ListApp from '../CMS/AppManagement/List';
import EditApp from '../CMS/AppManagement/Edit';
import CouponManagement from '../CMS/CouponManagement/ListView';
import SubscriptionManagement from '../CMS/SubscriptionManagement/ListView';
import ListCustomer from '../CMS/CustomerManagement/List';
import FAQs from '../CMS/FAQs';
import ActiveOrders from '../CMS/OrderManagement/ActiveOrders';
import PendingOrders from '../CMS/OrderManagement/PendingOrders';
import CancelledOrders from '../CMS/OrderManagement/CancelledOrders';
import PaymentManagement from '../CMS/PaymentManagement';
import ReviewManagement from '../CMS/ReviewManagement';
import ListUser from '../CMS/UserManagement/List';
import CreateCoupon from '../CMS/CouponManagement/Create';
import EditCoupon from '../CMS/CouponManagement/Edit';
import Invoice from '../CMS/components/invoice/Invoice';
import CallBackRequest from '../CMS/CallBackRequest/callBackRequest'
import DemoRequestManagement from '../CMS/CallBackRequest/DemoRequestManagement'
import CustomAppManagement from '../CMS/CallBackRequest/CustomAppManagement'
import UPappDreamManagement from '../CMS/CallBackRequest/UPappDreamManagement'
import logo from '../../assets/img/brand/upapp-white-logo.png';
import BlogCreate from '../CMS/Blog/Create'
import BlogEdit from '../CMS/Blog/Edit'
import BlogListView from '../CMS/Blog/ListView'

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const Sidebar = ({
	auth: { loading, isAuthenticated, loggedOut },
	user,
	logout,
	auth
}) => {
	const [sideTrigger, setSideTrigger] = useState({
		collapsed: false
	});

	const { collapsed } = sideTrigger;
	const toggle = () => {
		setSideTrigger({
			collapsed: !collapsed
		});
	};
	if (loggedOut === true) {
		return <Redirect to="/cms/admin/login"></Redirect>;
	}
	return loading && user === null ? (
		<Fragment></Fragment>
	) : (
			<Fragment>
				{user && user.role === 'admin' && (
					<Fragment>
						<Layout style={{ minHeight: '100Vh' }}>
							<Sider trigger={null} width="300" collapsible collapsed={collapsed}>
								<div className="adminDash-logo">
									<img src={logo} style={{ width: "120px" }}></img>
								</div>
								<Menu
									theme="dark"
									mode="inline"
									defaultSelectedKeys={['dashboard']}
								>
									<Menu.Item key="dashboard">
										<Icon type="dashboard" />
										<span>Dashboard</span>
										<NavLink to="/cms/admin/dashboard"></NavLink>
									</Menu.Item>
									<Menu.Item key="appManagement">
										<Icon type="android" />
										<span>App Management</span>
										<NavLink to="/cms/admin/app-management"></NavLink>
									</Menu.Item>
									<SubMenu
										key="sub1"
										title={
											<span>
												<Icon type="code-sandbox" />
												<span>Order</span>
											</span>
										}
									>
										<Menu.Item key="ActiveOrders">
											<Icon type="check" />
											<span>Active Orders</span>
											<NavLink to="/cms/admin/active-orders-management"></NavLink>
										</Menu.Item>
										<Menu.Item key="PendingOrders">
											<Icon type="exclamation" />
											<span>Pending Orders</span>
											<NavLink to="/cms/admin/pending-orders-management"></NavLink>
										</Menu.Item>
										<Menu.Item key="CancelledOrders">
											<Icon type="close-circle" />
											<span>Cancelled Orders</span>
											<NavLink to="/cms/admin/cancelled-orders-management"></NavLink>
										</Menu.Item>
									</SubMenu>

									<Menu.Item key="paymentManagement">
										<Icon type="credit-card" />
										<span>Payment Management</span>
										<NavLink to="/cms/admin/payment-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="couponManagement">
										<Icon type="percentage" />
										<span>Coupon Management</span>
										<NavLink to="/cms/admin/coupon-management"></NavLink>
									</Menu.Item>
									<SubMenu
										key="lead-management"
										title={
											<span>
												<Icon type="code-sandbox" />
												<span>Lead Management</span>
											</span>
										}
									>
									<Menu.Item key="callBackManagement">
										<span>Callback Management</span>
										<NavLink to="/cms/admin/callback-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="DemoRequest">
										<span>Demo Request</span>
										<NavLink to="/cms/admin/demo-request-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="customAppRequest">
										<span>Custom App Request</span>
										<NavLink to="/cms/admin/custom-app-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="upappDreamRequest">
										<span>UPapp Dream Request</span>
										<NavLink to="/cms/admin/upapp-dream-management"></NavLink>
									</Menu.Item>
									</SubMenu>

									<Menu.Item key="callBackManagement">
										<Icon type="phone" />
										<span>Subscription Management</span>
										<NavLink to="/cms/admin/subscription-management"></NavLink>
									</Menu.Item>

									<Menu.Item key="categoryManagement">
										<Icon type="unordered-list" />
										<span>Category Management</span>
										<NavLink to="/cms/admin/category-management"></NavLink>
									</Menu.Item>
									{/* Blog Started */}
									<SubMenu
										key="blog1"
										title={
											<span>
												<Icon type="code-sandbox" />
												<span>Blog</span>
											</span>
										}
									>
										<Menu.Item key="CreateBlog">
											<Icon type="check" />
											<span>Create Blog</span>
											<NavLink to="/cms/admin/create-blog"></NavLink>
										</Menu.Item>
										<Menu.Item key="ViewBlogs">
											<Icon type="exclamation" />
											<span>All Blogs</span>
											<NavLink to="/cms/admin/Blog-list"></NavLink>
										</Menu.Item>

									</SubMenu>
									{/* Blog End */}
									<Menu.Item key="UserManagement">
										<Icon type="user" />
										<span>User Management</span>
										<NavLink to="/cms/admin/user-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="CustomerManagement">
										<Icon type="usergroup-add" />
										<span>Customer Management</span>
										<NavLink to="/cms/admin/customer-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="careerManagement">
										<Icon type="audit" />
										<span>Career Management</span>
										<NavLink to="/cms/admin/career-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="reviewManagement">
										<Icon type="message" />
										<span>Review management</span>
										<NavLink to="/cms/admin/review-management"></NavLink>
									</Menu.Item>
									<Menu.Item key="faqs">
										<Icon type="question-circle" />
										<span>FAQ's</span>
										<NavLink to="/cms/admin/faqs"></NavLink>
									</Menu.Item>
								</Menu>
							</Sider>
							<Layout>
								<Header style={{ background: '#fff', padding: 0 }}>
									<Icon
										className="trigger"
										type={collapsed ? 'menu-unfold' : 'menu-fold'}
										onClick={toggle}
									/>
									{!loading && (
										<Fragment>
											{localStorage.token ? (
												<Link
													to="/cms/admin/login"
													onClick={logout}
													style={{ float: 'right', padding: '0px 15px' }}
												>
													<Icon type="logout" />
													Logout
												</Link>
											) : (
													<Fragment></Fragment>
												)}
										</Fragment>
									)}
								</Header>
								<Content
									style={{
										margin: '24px 16px',
										padding: 24,
										background: '#fff',
										minHeight: 280
									}}
								>
									<PrivateRoute
										path="/cms/admin/dashboard"
										component={Dashboard}
									/>
									<PrivateRoute
										path="/cms/admin/category-management"
										component={ListCategory}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/category-management/new-category"
										component={CreateCategory}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/edit-category/:id"
										component={EditCategory}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/career-management"
										component={ListJob}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/career-management/new-job"
										component={CreateJob}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/career-management/edit-job/:name"
										component={EditJob}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/callback-management"
										component={CallBackRequest}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/demo-request-management"
										component={DemoRequestManagement}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/custom-app-management"
										component={CustomAppManagement}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/upapp-dream-management"
										component={UPappDreamManagement}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/app-management"
										component={ListApp}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/app-management/new-app"
										component={CreateApp}
									/>
									<PrivateRoute
										path="/cms/admin/app-management/edit/:id"
										component={EditApp}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/coupon-management/new-coupon"
										component={CreateCoupon}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/edit-coupon/:coupon"
										component={EditCoupon}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/coupon-management"
										component={CouponManagement}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/subscription-management"
										component={SubscriptionManagement}
										exact
									/>

									<PrivateRoute
										path="/cms/admin/customer-management"
										component={ListCustomer}
									/>
									<PrivateRoute path="/cms/admin/faqs" component={FAQs} />
									<PrivateRoute
										path="/cms/admin/order-management/:invoice"
										component={Invoice}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/active-orders-management"
										component={ActiveOrders}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/pending-orders-management"
										component={PendingOrders}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/cancelled-orders-management"
										component={CancelledOrders}
										exact
									/>
									<PrivateRoute
										path="/cms/admin/payment-management"
										component={PaymentManagement}
									/>
									<PrivateRoute
										path="/cms/admin/review-management"
										component={ReviewManagement}
									/>
									<PrivateRoute
										path="/cms/admin/user-management"
										component={ListUser}
									/>
									<PrivateRoute
										path="/cms/admin/create-blog"
										component={BlogCreate}
									/>
									<PrivateRoute
										path="/cms/admin/edit-blog/:id"
										component={BlogEdit}
									/>
									<PrivateRoute
										path="/cms/admin/Blog-list"
										component={BlogListView}
									/>
								</Content>
							</Layout>
						</Layout>
					</Fragment>
				)}
				{user && user.role === 'agency' && (
					<Fragment>
						<Layout style={{ minHeight: '100Vh' }}>
							<Sider trigger={null} collapsible collapsed={collapsed}>
								<div className="adminDash-logo">
									<img src={logo} style={{ width: "120px" }}></img>
								</div>
								<Menu
									theme="dark"
									mode="inline"
									defaultSelectedKeys={['ViewBlogs']}
								>
									{/* Blog Started */}
									<SubMenu
										key="blog1"
										title={
											<span>
												<Icon type="code-sandbox" />
												<span>Blog</span>
											</span>
										}
									>
										<Menu.Item key="CreateBlog">
											<Icon type="check" />
											<span>Create Blog</span>
											<NavLink to="/cms/admin/create-blog"></NavLink>
										</Menu.Item>
										<Menu.Item key="ViewBlogs">
											<Icon type="exclamation" />
											<span>All Blogs</span>
											<NavLink to="/cms/admin/Blog-list"></NavLink>
										</Menu.Item>

									</SubMenu>
									{/* Blog End */}
								</Menu>
							</Sider>
							<Layout>
								<Header style={{ background: '#fff', padding: 0 }}>
									<Icon
										className="trigger"
										type={collapsed ? 'menu-unfold' : 'menu-fold'}
										onClick={toggle}
									/>
									{!loading && (
										<Fragment>
											{localStorage.token ? (
												<Link
													to="/cms/admin/login"
													onClick={logout}
													style={{ float: 'right', padding: '0px 15px' }}
												>
													<Icon type="logout" />
													Logout
												</Link>
											) : (
													<Fragment></Fragment>
												)}
										</Fragment>
									)}
								</Header>
								<Content
									style={{
										margin: '24px 16px',
										padding: 24,
										background: '#fff',
										minHeight: 280
									}}
								>
									<PrivateRoute
										path="/cms/admin/create-blog"
										component={BlogCreate}
									/>
									<PrivateRoute
										path="/cms/admin/edit-blog/:id"
										component={BlogEdit}
									/>
									<PrivateRoute
										path="/cms/admin/Blog-list"
										component={BlogListView}
									/>
								</Content>
							</Layout>
						</Layout>
					</Fragment>
				)}
			</Fragment>
		);
};

Sidebar.prototypes = {
	logout: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth,
	user: state.auth.user
});

export default connect(
	mapStateToProps,
	{ logout }
)(Sidebar);
