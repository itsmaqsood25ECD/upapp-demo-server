/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { NavLink, Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logout } from "../../actions/auth.action";
import RequestForDemo from "../Pages/RequestForDemo";
import "react-flags-select/css/react-flags-select.css";
import factoryIcon from "../../assets/img/Icons/factory.svg";
import supportIcon from "../../assets/img/Icons/support.svg";
import pricingIcon from "../../assets/img/Icons/priceing.svg";
import restaurantIcon from "../../assets/img/Icons/restaurant.svg";
import profileIcon from "../../assets/img/Icons/profile.svg";
import logoutIcon from "../../assets/img/Icons/logout.svg";
import loginIcon from "../../assets/img/Icons/login.svg";
import axios from "axios";
import { store } from "../../store/store";
import { loadApplication } from "../../actions/application.action";
import { GetCategory } from "../../actions/admin/category.management.action";
import UpappHeaderMenuData from './Header/HeaderMenu/UpappHeaderMenuData'
import { Layout, Menu, Dropdown, Icon, Skeleton, Form, Drawer, Button, Select, Switch} from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import chatbotIcon from '../../assets/img/HeaderIcon/cb2.svg'

// const { Text } = Typography;

const { Header } = Layout;
const { Option } = Select;

// let filteredApps;
const HeaderForm = ({ auth: { loading }, user, logout, direction }) => {

  const { t } = useTranslation();

  const [headerLink, setHeaderLink] = useState({
    visible: false,
  });
  const [state, setState] = useState({
    countryCode: "",
    CurrencyView: "",
    emVisible: true,
    language: ""
  });


  const { CurrencyView, countryCode, emVisible, language } = state;

  const fetchCountryData = async () => {

    try {
      console.log("This statement works")

      const response = await axios.get(
        `https://api.ipstack.com/check?access_key=32e8b62adfadea003abc9299f80931ea&format=1`
      );

      console.log("fetch response",response);

      if(response.status === 200){
        sessionStorage.setItem("current_country", response.data.currency.code)
        fetchCountryCurrencyData(response.data.currency.code);
  
      }else {
        sessionStorage.setItem("current_country", "USD")
        fetchCountryCurrencyData("USD");
      }

     
    }
    catch (error) {
      console.log("Error has been handled")
     }

  };

  const fetchCountryCurrencyData = async (code) => {

    try {
      const currexc = await axios.get(`https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${code},USD,AED,SGD,OMR,QAR,MYR,LKR,EGP,SAR,KWD,JOD,KYD,ZAR&source=USD&format=1`);
      const currexcSGD = await axios.get(`https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${code},USD,AED,SGD,OMR,QAR,MYR,LKR,EGP,SAR,KWD,JOD,KYD,ZAR&source=SGD&format=1`);

      console.log("currexc", currexc)

      sessionStorage.setItem("exchange_rate_response", JSON.stringify(currexc))
        sessionStorage.setItem("sgd_exchange_rate_response", JSON.stringify(currexcSGD))
        console.log("currx code", code);
        console.log("exchange_rate_response", sessionStorage.getItem("exchange_rate_response"))
        const curr = `USD${code}`;
        const sgdcurr = `SGD${code}`;

        sessionStorage.setItem("currency_rate", currexc.data.quotes[`${curr}`]);
        sessionStorage.setItem("sgd_currency_rate", currexcSGD.data.quotes[`${sgdcurr}`]);
        sessionStorage.setItem("currency_sign", code);
        settingCurrency(code);


      
      throw new Error('error')

    }
    catch (error) {
      console.log("Error has been handled")
      

    }
    finally {
      console.log("Everything has been handled")
    }


  };

  const settingCurrency = (code) => {
    if (sessionStorage.getItem("currency") === "KWD") {
      setState({
        ...state,
        CurrencyView: "KWD"
      });
      sessionStorage.setItem("currency", 'KWD');
    } else if (sessionStorage.getItem("currency") === "AED") {
      setState({
        ...state,
        CurrencyView: "AED"
      });
      sessionStorage.setItem("currency", "AED");
    } else if (sessionStorage.getItem("currency") === "SGD") {
      setState({
        ...state,
        CurrencyView: "SGD"
      });
      sessionStorage.setItem("currency", "SGD");
    } else if (sessionStorage.getItem("currency") === "OMR") {
      setState({
        ...state,
        CurrencyView: "OMR"
      });
      sessionStorage.setItem("currency", "OMR");
    } else if (sessionStorage.getItem("currency") === "QAR") {
      setState({
        ...state,
        CurrencyView: "QAR"
      });
      sessionStorage.setItem("currency", "QAR");
    } else if (sessionStorage.getItem("currency") === "BHD") {
      setState({
        ...state,
        CurrencyView: "BHD"
      });
      sessionStorage.setItem("currency", "BHD");
    } else if (sessionStorage.getItem("currency") === "MYR") {
      setState({
        ...state,
        CurrencyView: "MYR"
      });
      sessionStorage.setItem("currency", "MYR");
    } else if (sessionStorage.getItem("currency") === "LKR") {
      setState({
        ...state,
        CurrencyView: "LKR"
      });
      sessionStorage.setItem("currency", "LKR");
    } else if (sessionStorage.getItem("currency") === "EGP") {
      setState({
        ...state,
        CurrencyView: "EGP"
      });
      sessionStorage.setItem("currency", "EGP");
    } else if (sessionStorage.getItem("currency") === "SAR") {
      setState({
        ...state,
        CurrencyView: "SAR"
      });
      sessionStorage.setItem("currency", "SAR");
    } else if (sessionStorage.getItem("currency") === "JOD") {
      setState({
        ...state,
        CurrencyView: "JOD"
      });
      sessionStorage.setItem("currency", "JOD");
    } else if (sessionStorage.getItem("currency") === "KYD") {
      setState({
        ...state,
        CurrencyView: "KYD"
      });
      sessionStorage.setItem("currency", "KYD");
    } else if (sessionStorage.getItem("currency") === "ZAR") {
      setState({
        ...state,
        CurrencyView: "ZAR"
      });
      sessionStorage.setItem("currency", "ZAR");
    } else if (sessionStorage.getItem("currency") === "TRY") {
      setState({
        ...state,
        CurrencyView: "TRY"
      });
      sessionStorage.setItem("currency", "TRY");
      localStorage.setItem('lng',"TR")
      i18next.changeLanguage('TR')
    } else {
      setState({
        ...state,
        CurrencyView: code
      });
      sessionStorage.setItem("currency", code);
    }
  }

  useEffect(() => {
    store.dispatch(GetCategory());
    store.dispatch(loadApplication());

    console.log("user >>", user)

    if (sessionStorage.getItem("currency")) {
      setState({
        ...state,
        countryCode: sessionStorage.getItem("currency"),
        CountryView: sessionStorage.getItem("currency")
      });
    } else {

      fetchCountryData();

      // if (localStorage.getItem('lng')) {
      //   setState({
      //     ...state,
      //     language: localStorage.getItem('lng'),

      //   })
      // } else {
      //   localStorage.setItem('lng', 'EN')
      //   setState({
      //     ...state,
      //     language: 'EN',
      //   })
      // }

      if (localStorage.getItem('lng') === 'AR') {
        i18next.changeLanguage('AR')
      } else if (localStorage.getItem('lng') === 'EN') {
        i18next.changeLanguage('EN')
      } else if (localStorage.getItem('lng') === 'TR') {
        i18next.changeLanguage('TR')
      }
    }
  }, [countryCode, CurrencyView, user, logout]);

  const { visible } = headerLink;

  const onClose = () => {
    setHeaderLink({
      visible: false
    });
  };

  const logoutME = () => {
    logout()
    window.location.reload()
  };

  const showDrawer = () => {
    setHeaderLink({
      visible: true
    });
  };



  const myAccountMenu = (
    <Menu>
      <Menu.Item>
        <Link to="/my-profile">
          My Profile
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/my-orders">
          My Order
        </Link>
      </Menu.Item>
      <Menu.Item onClick={() => logoutME()}>
        Logout
      </Menu.Item>
    </Menu>
  );

  const countrychanged = (Code) => {
    const curr = JSON.parse(sessionStorage.getItem("exchange_rate_response"));
    const sgdcurr = JSON.parse(sessionStorage.getItem("sgd_exchange_rate_response"));
    console.log("Code", Code)
    console.log("curr country chnaged >", curr);
    console.log("curr.data.quotes[`USD${Code}`]", curr.data.quotes[`USD${Code}`])
    sessionStorage.setItem("country", Code);
    sessionStorage.setItem("currency", Code);
    sessionStorage.setItem("currency_sign", Code);
    sessionStorage.setItem("currency_rate", curr.data.quotes[`USD${Code}`]);
    sessionStorage.setItem("sgd_currency_rate", sgdcurr.data.quotes[`SGD${Code}`]);
    window.location.reload();
    setState({
      ...state,
      countryCode: Code
    });
  };

  const LanguageChanged = (lang) => {
    console.log(lang);
    localStorage.setItem('lng', lang)

    setState({
      ...state,
      Language: localStorage.getItem('lng')
    })
    window.location.reload()
  }

  const onLangSwitchChange = (checked) => {
    console.log(checked);

    if (checked) {
      localStorage.setItem('lng', "AR")
      setState({
        ...state,
        Language: localStorage.getItem('lng')
      })
    } else {
      localStorage.setItem('lng', "EN")
      setState({
        ...state,
        Language: localStorage.getItem('lng')
      })
    }
    window.location.reload()
  }

  const ProductMenu = (
    <Fragment>
      <UpappHeaderMenuData
        ecommerce={`${t('header.ecommerce')}`}
        booking={`${t('header.booking')}`}
        Facility={`${t('header.Facility')}`}
        Restaurant={`${t('header.Restaurant')}`}
        multivendor={`${t('header.multivendor')}`}
        UPappbot={`${t('header.UPappbot')}`}
      />
    </Fragment>
  );

  const generalHeader = (
    <Fragment>
      <Header className="responsive-header-menu">
        <Button
          type=""
          style={{
            float: "left",
            fontSize: "30px",
            border: "none",
            boxShadow: "none"
          }}
          onClick={showDrawer}
        >
          <Icon type="align-right" />
          {/* <Ionicons.IoMdMenu /> */}
        </Button>
        <NavLink className="brand-logo" to="/">
          <div className="logo" />
        </NavLink>

        <RequestForDemo
          demo={`${t('header.demo')}`}
          requestfordemo={`${t('header.requestfordemo')}`}
        />
        <Drawer
          className="header-drawer removing-border"
          title={<div>
            {/* <div
              key="n-country-selector"
              className="cc-selector"
              style={{ fontSize: '14px', color: "#000", width: "120px", float: "left" }}
            >
              Arabic <Switch onChange={onLangSwitchChange} checkedChildren="ON" unCheckedChildren="OFF" defaultChecked={localStorage.getItem('lng') === "EN" || null ? false : true} />
            </div> */}
            <Select
                defaultValue={language}
                // defaultValue="default"
                value={localStorage.getItem('lng')}
                onSelect={LanguageChanged}
                style={{ width: "100%", padding: "7px 0px" }}
              >
                <Option value="EN">English</Option>
                <Option value="AR">عربى</Option>
                <Option value="TR">Turkish</Option>
              </Select>
              <Select
              className="cc-selector float-right"
              defaultValue={CurrencyView}
              value={sessionStorage.getItem("currency")}
              onSelect={countrychanged}
              style={{ padding: "7px 0px",width:"100%",marginBottom:"10px" }}
            >
              <Option value={sessionStorage.getItem("current_country")}>
                {sessionStorage.getItem("current_country")}
              </Option>
              <Option value="USD">USD - United States Dollar</Option>
              <Option value="AED">AED - United Arab Emirates dirham</Option>
              <Option value="SGD">SGD - Singapore dollar</Option>
              <Option value="OMR">OMR - Omani rial</Option>
              <Option value="QAR">QAR - Qatari Riyal</Option>
              <Option value="MYR">MYR - Malaysian ringgit</Option>
              <Option value="LKR">LKR - Sri Lankan rupee</Option>
              <Option value="EGP">EGP - Egyptian pound</Option>
              <Option value="SAR">SAR - Saudi riyal</Option>
              <Option value="KWD">KWD - Kuwaiti dinar</Option>
              <Option value="JOD">JOD -Jordanian dinar</Option>
              <Option value="KYD">KYD - Cayman Islands dollar</Option>
              <Option value="ZAR">ZAR - South African rand</Option>
            </Select>
          </div>
          }
          placement={direction === DIRECTIONS.LTR ? 'left' : 'right'}
          closable={false}
          onClose={onClose}
          visible={visible}
        >
          
          <Menu
            className="drawer-v-Menu"
            theme="light"
            mode="vertical"
            defaultSelectedKeys={["0"]}
            onSelect={onClose}
          >
            <Menu.Item key="dr-factory">
              <NavLink to="/factory" className="mobile-menu-name-data">
                <img src={factoryIcon} className="m-v-menu-icon" />
                {t('header.thefactory')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-pricing">
              <NavLink to="/pricing" className="mobile-menu-name-data">
                <img src={pricingIcon} className="m-v-menu-icon" />
                {t('header.Pricing')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="/contactUs" className="mobile-menu-name-data">
                <img src={supportIcon} className="m-v-menu-icon" />
                {t('header.Support')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-products">
              {/* <NavLink to="/ecommerce-platform" className="mobile-menu-name-data"> */}
              <NavLink to="/eCommerce-Single-Vendor-Platform" className="mobile-menu-name-data">
                <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/eCommerce.svg"
                  className="m-v-menu-icon"
                />
                {t('header.ecommerce')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-products">
              {/* <NavLink to="/ecommerce-marketplace" className="mobile-menu-name-data"> */}
              <NavLink to="/eCommerce-Multi-Vendor-Platform" className="mobile-menu-name-data">
                <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/Booking+Icon.png"
                  className="m-v-menu-icon"
                />
                {t('header.multivendor')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-products">
              <NavLink to="/upappbot" className="mobile-menu-name-data">
                <img
                  src={chatbotIcon}
                  className="m-v-menu-icon"
                />
                {t('header.UPappbot')}
              </NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink activeClassName="is-active" to="/restaurant-application" className="mobile-menu-name-data">
                <img
                  src={restaurantIcon}
                  className="m-v-menu-icon"
                />
                {t('header.Restaurant')}
              </NavLink>
            </Menu.Item>
            {/* <Menu.Item>
              <NavLink
                activeClassName="is-active"
                to="/facility-management-application" className="mobile-menu-name-data"
              >
                <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/FaciltiyManagement.svg"
                  className="m-v-menu-icon"
                />
                {t('header.Facility')}
              </NavLink>
            </Menu.Item> */}
            <Menu.Item>
              <a href="https://studio.upappfactory.com/" target="_blank" activeClassName="is-active" className="mobile-menu-name-data">
              <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/FaciltiyManagement.svg"
                  className="m-v-menu-icon"
                />
                {t('header.studio')}
              </a>
            </Menu.Item>
            <Menu.Item>
              <Link to="/login" className="mobile-menu-name-data">
                <img src={loginIcon} className="m-v-menu-icon"/>
                {t('home.login')}
                </Link>
            </Menu.Item>
            <Menu.Item key="6" className="">
              <a class="" href="https://consultancy.upappfactory.com/" title="">
                <Button type="primary" size="large" className="uaf_hire_consultant_mobile_btn">
                  <span>Hire a Consultant</span>
                </Button>
              </a>
            </Menu.Item>

            {/* {user != null ? <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-left"
            >
              <Link
                to="/login">
                {t('home.login')}
              </Link>
            </Menu.Item> : ''} */}


          </Menu>
        </Drawer>
      </Header>
      {direction === DIRECTIONS.LTR && <Fragment>
        <Header className="header-comp">
          <NavLink to="/">
            <div className="logo" />
          </NavLink>
          <Menu
            className="U-Menu"
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={["0"]}
          >
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="/factory">
                {t('header.thefactory')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-pricing">
              <NavLink activeClassName="is-active" to="/pricing">
                {t('header.Pricing')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <NavLink activeClassName="is-active" to="/contactUs">
                {t('header.Support')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <Dropdown overlay={ProductMenu} placement="bottomCenter">
                <span>{t('header.Products')}</span>
              </Dropdown>
            </Menu.Item>
            <Menu.Item key="n-studio">
              <a href="https://studio.upappfactory.com/" target="_blank" activeClassName="is-active" >
              {t('header.studio')}
              </a>
            </Menu.Item>

            <Menu.Item className="menu-action-btn tab-request-btn float-right">
              <RequestForDemo
                demo={`${t('header.demo')}`}
                requestfordemo={`${t('header.requestfordemo')}`}
              />
            </Menu.Item>
            <Menu.Item key="6" className="tab-request-btn uaf_mainwebsite_linkbtn_main">
              <a class="" href="https://consultancy.upappfactory.com/" title="">
                <Button type="primary" size="large" className="uaf_mainwebsite_linkbtn">
                  <span>Hire a Consultant</span>
                </Button>
              </a>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px", }}
              className="d-guestLinks cc-selector float-right"
            >
              <Select
                defaultValue={language}
                // defaultValue="default"
                value={localStorage.getItem('lng')}
                onSelect={LanguageChanged}
                style={{ width: 112, padding: "0px 5px" }}
              >
                <Option value="EN">English</Option>
                <Option value="AR">عربى</Option>
                <Option value="TR">Turkish</Option>
              </Select>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector  float-right"
            >
              <Select
                defaultValue={CurrencyView}
                // defaultValue="default"
                value={sessionStorage.getItem("currency")}
                onSelect={countrychanged}
                style={{ width: 122, padding: "0px 15px" }}
              >

                <Option value={sessionStorage.getItem("current_country")}>
                  {sessionStorage.getItem("current_country")}
                </Option>
                <Option value="USD">USD - United States Dollar</Option>
                <Option value="AED">AED - United Arab Emirates dirham</Option>
                <Option value="SGD">SGD - Singapore dollar</Option>
                <Option value="OMR">OMR - Omani rial</Option>
                <Option value="QAR">QAR - Qatari Riyal</Option>
                <Option value="MYR">MYR - Malaysian ringgit</Option>
                <Option value="LKR">LKR - Sri Lankan rupee</Option>
                <Option value="EGP">EGP - Egyptian pound</Option>
                <Option value="SAR">SAR - Saudi riyal</Option>
                <Option value="KWD">KWD - Kuwaiti dinar</Option>
                <Option value="JOD">JOD -Jordanian dinar</Option>
                <Option value="KYD">KYD - Cayman Islands dollar</Option>
                <Option value="ZAR">ZAR - South African rand</Option>
              </Select>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-right"
            >
              <Link
                to="/login"
              // onClick={logout}
              >
                {/* Login */}
                {t('home.login')}
              </Link>
            </Menu.Item>

          </Menu>
        </Header>
      </Fragment>}
      {direction === DIRECTIONS.RTL && <Fragment>
        <Header className="header-comp float-right">
          <NavLink to="/">
            <div className="logo float-right" />
          </NavLink>
          <Menu
            className="U-Menu"
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={["0"]}
          >
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="/factory">
                {t('header.thefactory')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-pricing">
              <NavLink activeClassName="is-active" to="/pricing">
                {t('header.Pricing')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <NavLink activeClassName="is-active" to="/contactUs">
                {t('header.Support')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-products">
              <Dropdown overlay={ProductMenu} placement="bottomCenter">
                <span>{t('header.Products')}</span>
              </Dropdown>
            </Menu.Item>
            <Menu.Item key="n-studio">
              <NavLink activeClassName="is-active" href="https://studio.upappfactory.com/" target="_blank">
              ستوديو   
              </NavLink>
            </Menu.Item>
            <Menu.Item key="6" className="menu-action-btn tab-request-btn float-left">
              <RequestForDemo
                demo={`${t('header.demo')}`}
                requestfordemo={`${t('header.requestfordemo')}`}
              />
            </Menu.Item>

            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-left"
            >
              <Select
                defaultValue={CurrencyView}
                // defaultValue="default"
                value={sessionStorage.getItem("currency")}
                onSelect={countrychanged}
                style={{ width: 150, padding: "0px 15px" }}
              >

                <Option value={sessionStorage.getItem("current_country")}>
                  {sessionStorage.getItem("current_country")}
                </Option>
                <Option value="USD">USD - United States Dollar</Option>
                <Option value="AED">AED - United Arab Emirates dirham</Option>
                <Option value="SGD">SGD - Singapore dollar</Option>
                <Option value="OMR">OMR - Omani rial</Option>
                <Option value="QAR">QAR - Qatari Riyal</Option>
                <Option value="MYR">MYR - Malaysian ringgit</Option>
                <Option value="LKR">LKR - Sri Lankan rupee</Option>
                <Option value="EGP">EGP - Egyptian pound</Option>
                <Option value="SAR">SAR - Saudi riyal</Option>
                <Option value="KWD">KWD - Kuwaiti dinar</Option>
                <Option value="JOD">JOD -Jordanian dinar</Option>
                <Option value="KYD">KYD - Cayman Islands dollar</Option>
                <Option value="ZAR">ZAR - South African rand</Option>
              </Select>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-left"
            >
              <Link
                to="/login"
              // onClick={logout}
              >
                {/* Login */}
                {t('home.login')}
              </Link>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-left"
            >
              <Select
                defaultValue={language}
                // defaultValue="default"
                value={localStorage.getItem('lng')}
                onSelect={LanguageChanged}
                style={{ width: 150, padding: "0px 15px" }}
              >
                <Option value="EN">English</Option>
                <Option value="AR">عربى</Option>
                <Option value="TR">Turkish</Option>
              </Select>
            </Menu.Item>

          </Menu>
        </Header>
      </Fragment>}
    </Fragment>
  );

  const userHeader = (
    <Fragment>
      <Header className="responsive-header-menu">
        <Button
          type=""
          style={{
            float: "left",
            fontSize: "30px",
            border: "none",
            boxShadow: "none"
          }}
          onClick={showDrawer}
        >
          <Icon type="align-right" />
          {/* <Ionicons.IoMdMenu /> */}
        </Button>
        <NavLink className="brand-logo" to="/">
          <div className="logo" />
        </NavLink>

        <RequestForDemo
          demo={`${t('header.demo')}`}
          requestfordemo={`${t('header.requestfordemo')}`}
        />
        <Drawer
          className="header-drawer removing-border"
          title={<div>
            {/* <div
              key="n-country-selector"
              className="cc-selector"
              style={{ fontSize: '14px', color: "#000", width: "120px", float: "left" }}
            >
              Arabic <Switch onChange={onLangSwitchChange} checkedChildren="ON" unCheckedChildren="OFF" defaultChecked={localStorage.getItem('lng') === "EN" || null ? false : true} />
            </div> */}
           <Select
                defaultValue={language}
                // defaultValue="default"
                value={localStorage.getItem('lng')}
                onSelect={LanguageChanged}
                style={{ width: "100%", padding: "7px 0px" }}
              >
                <Option value="EN">English</Option>
                <Option value="AR">عربى</Option>
                <Option value="TR">Turkish</Option>
              </Select>
              <Select
              className="cc-selector float-right"
              defaultValue={CurrencyView}
              value={sessionStorage.getItem("currency")}
              onSelect={countrychanged}
              style={{ padding: "7px 0px",width:"100%",marginBottom:"10px" }}
            >
              <Option value={sessionStorage.getItem("current_country")}>
                {sessionStorage.getItem("current_country")}
              </Option>

              <Option value="USD">USD - United States Dollar</Option>
              <Option value="AED">AED - United Arab Emirates dirham</Option>
              <Option value="SGD">SGD - Singapore dollar</Option>
              <Option value="OMR">OMR - Omani rial</Option>
              <Option value="QAR">QAR - Qatari Riyal</Option>
              <Option value="MYR">MYR - Malaysian ringgit</Option>
              <Option value="LKR">LKR - Sri Lankan rupee</Option>
              <Option value="EGP">EGP - Egyptian pound</Option>
              <Option value="SAR">SAR - Saudi riyal</Option>
              <Option value="KWD">KWD - Kuwaiti dinar</Option>
              <Option value="JOD">JOD -Jordanian dinar</Option>
              <Option value="KYD">KYD - Cayman Islands dollar</Option>
              <Option value="ZAR">ZAR - South African rand</Option>
            </Select>
          </div>
          }
          placement={direction === DIRECTIONS.LTR ? 'left' : 'right'}
          closable={false}
          onClose={onClose}
          visible={visible}
        >
          <Menu
            className="drawer-v-Menu"
            theme="light"
            mode="vertical"
            defaultSelectedKeys={["0"]}
            onSelect={onClose}
          >
            <Menu.Item key="dr-factory">
              <NavLink to="/factory" className="mobile-menu-name-data">
                <img src={factoryIcon} className="m-v-menu-icon" />
                {t('header.thefactory')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-pricing">
              <NavLink to="/pricing" className="mobile-menu-name-data">
                <img src={pricingIcon} className="m-v-menu-icon" />
                {t('header.Pricing')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="/contactUs" className="mobile-menu-name-data">
                <img src={supportIcon} className="m-v-menu-icon" />
                {t('header.Support')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-products">
              {/* <NavLink to="/ecommerce-platform" className="mobile-menu-name-data"> */}
              <NavLink to="/eCommerce-Single-Vendor-Platform" className="mobile-menu-name-data">
                <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/eCommerce.svg"
                  className="m-v-menu-icon"
                />
                {t('header.ecommerce')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-products">
              {/* <NavLink to="/ecommerce-marketplace" className="mobile-menu-name-data"> */}
              <NavLink to="/eCommerce-Multi-Vendor-Platform" className="mobile-menu-name-data">
                <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/Booking+Icon.png"
                  className="m-v-menu-icon"
                />
                {t('header.multivendor')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-products">
              <NavLink to="/upappbot" className="mobile-menu-name-data">
                <img
                  src={chatbotIcon}
                  className="m-v-menu-icon"
                />
                {t('header.UPappbot')}
              </NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink activeClassName="is-active" to="/restaurant-application" className="mobile-menu-name-data">
                <img
                  src={restaurantIcon}
                  className="m-v-menu-icon"
                />
                {t('header.Restaurant')}
              </NavLink>
            </Menu.Item>
            <Menu.Item>
              {/* <NavLink
                activeClassName="is-active"
                to="/facility-management-application" className="mobile-menu-name-data"
              >
                <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/FaciltiyManagement.svg"
                  className="m-v-menu-icon"
                />
                {t('header.Facility')}
              </NavLink> */}
              <a href="https://studio.upappfactory.com/" target="_blank" activeClassName="is-active" className="mobile-menu-name-data">
              <img
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/FaciltiyManagement.svg"
                  className="m-v-menu-icon"
                />
                {t('header.studio')}
              </a>
            </Menu.Item>



            <Menu.Item>
              <NavLink activeClassName="is-active" to="/my-profile" className="mobile-menu-name-data">
                <img
                  src={profileIcon}
                  className="m-v-menu-icon"
                />
                {t('header.MyProfile')}
                {/* My Profile */}
              </NavLink>
            </Menu.Item>
            {/* <Menu.Item>
              <NavLink activeClassName="is-active" to="/my-orders" className="mobile-menu-name-data">
                <img
                  src={restaurantIcon}
                  className="m-v-menu-icon"
                />
                 My Order
              </NavLink>
            </Menu.Item> */}
            <Menu.Item onClick={() => logoutME()}>

              <a href="javascript:void(0)" className="mobile-menu-name-data">
              <img
                src={logoutIcon}
                className="m-v-menu-icon"
              />
                  {/* Logout */}
                  {t('header.Logout')}
              </a>
            </Menu.Item>
            <Menu.Item key="6" className="">
              <a class="" href="https://consultancy.upappfactory.com/" title="">
                <Button type="primary" size="large" className="uaf_hire_consultant_mobile_btn">
                  <span>Hire a Consultant</span>
                </Button>
              </a>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              className="cc-selector float-left"
              style={{ width: "100%" }}
            >
            </Menu.Item>


            {/* <Menu.Item
                key="n-country-selector"
                className="cc-selector float-left"
                style={{ width: "100%" }}
              >
              <Dropdown overlay={myAccountMenu}>
              <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
              My Account <Icon type="down" />
              </a>
              </Dropdown>

              </Menu.Item> */}
          </Menu>
        </Drawer>
      </Header>
      {direction === DIRECTIONS.LTR && <Fragment>
        <Header className="header-comp">
          <NavLink to="/">
            <div className="logo" />
          </NavLink>
          <Menu
            className="U-Menu"
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={["0"]}
          >
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="/factory">
                {t('header.thefactory')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-pricing">
              <NavLink activeClassName="is-active" to="/pricing">
                {t('header.Pricing')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <NavLink activeClassName="is-active" to="/contactUs">
                {t('header.Support')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <Dropdown overlay={ProductMenu} placement="bottomCenter">
                <span>{t('header.Products')}</span>
              </Dropdown>
            </Menu.Item>
            <Menu.Item key="n-studio">
              <a href="https://studio.upappfactory.com/" target="_blank" activeClassName="is-active" >
              {t('header.studio')}
              </a>
            </Menu.Item>
            <Menu.Item className="menu-action-btn tab-request-btn float-right">
              <RequestForDemo
                demo={`${t('header.demo')}`}
                requestfordemo={`${t('header.requestfordemo')}`}
              />
            </Menu.Item>
            <Menu.Item key="6" className="tab-request-btn uaf_mainwebsite_linkbtn_main">
              <a class="" href="https://consultancy.upappfactory.com/" title="">
                <Button type="primary" size="large" className="uaf_mainwebsite_linkbtn">
                  <span>Hire a Consultant</span>
                </Button>
              </a>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px", }}
              className="d-guestLinks cc-selector float-right"
            >
              <Select
                defaultValue={language}
                // defaultValue="default"
                value={localStorage.getItem('lng')}
                onSelect={LanguageChanged}
                style={{ width: 112, padding: "0px 5px" }}
              >
                <Option value="EN">English</Option>
                <Option value="AR">عربى</Option>
                <Option value="TR">Turkish</Option>
              </Select>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector-user"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector  float-right"
            >
              <Select
                defaultValue={CurrencyView}
                // defaultValue="default"
                value={sessionStorage.getItem("currency")}
                onSelect={countrychanged}
                style={{ width: 122, padding: "0px 15px" }}
              >

                <Option value={sessionStorage.getItem("current_country")}>
                  {sessionStorage.getItem("current_country")}
                </Option>
                <Option value="USD">USD - United States Dollar</Option>
                <Option value="AED">AED - United Arab Emirates dirham</Option>
                <Option value="SGD">SGD - Singapore dollar</Option>
                <Option value="OMR">OMR - Omani rial</Option>
                <Option value="QAR">QAR - Qatari Riyal</Option>
                <Option value="MYR">MYR - Malaysian ringgit</Option>
                <Option value="LKR">LKR - Sri Lankan rupee</Option>
                <Option value="EGP">EGP - Egyptian pound</Option>
                <Option value="SAR">SAR - Saudi riyal</Option>
                <Option value="KWD">KWD - Kuwaiti dinar</Option>
                <Option value="JOD">JOD -Jordanian dinar</Option>
                <Option value="KYD">KYD - Cayman Islands dollar</Option>
                <Option value="ZAR">ZAR - South African rand</Option>
              </Select>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector-user"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector  float-right"
            >
              <Dropdown overlay={myAccountMenu}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                  My Account <Icon type="down" />
                </a>
              </Dropdown>

            </Menu.Item>

          </Menu>
        </Header>
      </Fragment>}
      {direction === DIRECTIONS.RTL && <Fragment>
        <Header className="header-comp float-right">
          <NavLink to="/">
            <div className="logo float-right" />
          </NavLink>
          <Menu
            className="U-Menu"
            theme="light"
            mode="horizontal"
            defaultSelectedKeys={["0"]}
          >
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="/factory">
                {t('header.thefactory')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-pricing">
              <NavLink activeClassName="is-active" to="/pricing">
                {t('header.Pricing')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <NavLink activeClassName="is-active" to="/contactUs">
                {t('header.Support')}
              </NavLink>
            </Menu.Item>
            <Menu.Item key="n-studio">
              <a href="https://studio.upappfactory.com/" target="_blank" activeClassName="is-active" >
              {t('header.studio')}
              </a>
            </Menu.Item>
            <Menu.Item key="n-contactus">
              <Dropdown overlay={ProductMenu} placement="bottomCenter">
                <span>{t('header.Products')}</span>
              </Dropdown>
            </Menu.Item>
            <Menu.Item key="6" className="menu-action-btn tab-request-btn float-left">
              <RequestForDemo
                demo={`${t('header.demo')}`}
                requestfordemo={`${t('header.requestfordemo')}`}
              />
            </Menu.Item>

            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-left"
            >
              <Select
                defaultValue={CurrencyView}
                // defaultValue="default"
                value={sessionStorage.getItem("currency")}
                onSelect={countrychanged}
                style={{ width: 150, padding: "0px 15px" }}
              >

                <Option value={sessionStorage.getItem("current_country")}>
                  {sessionStorage.getItem("current_country")}
                </Option>
                <Option value="USD">USD - United States Dollar</Option>
                <Option value="AED">AED - United Arab Emirates dirham</Option>
                <Option value="SGD">SGD - Singapore dollar</Option>
                <Option value="OMR">OMR - Omani rial</Option>
                <Option value="QAR">QAR - Qatari Riyal</Option>
                <Option value="MYR">MYR - Malaysian ringgit</Option>
                <Option value="LKR">LKR - Sri Lankan rupee</Option>
                <Option value="EGP">EGP - Egyptian pound</Option>
                <Option value="SAR">SAR - Saudi riyal</Option>
                <Option value="KWD">KWD - Kuwaiti dinar</Option>
                <Option value="JOD">JOD -Jordanian dinar</Option>
                <Option value="KYD">KYD - Cayman Islands dollar</Option>
                <Option value="ZAR">ZAR - South African rand</Option>
              </Select>
            </Menu.Item>

            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector float-left"
            >
              <Select
                defaultValue={language}
                // defaultValue="default"
                value={localStorage.getItem('lng')}
                onSelect={LanguageChanged}
                style={{ width: 150, padding: "0px 15px" }}
              >
                <Option value="EN">English</Option>
                <Option value="AR">عربى</Option>
                <Option value="TR">Turkish</Option>
              </Select>
            </Menu.Item>
            <Menu.Item
              key="n-country-selector"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector  float-left"
            >
              <Dropdown overlay={myAccountMenu}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                  My Account <Icon type="down" />
                </a>
              </Dropdown>

            </Menu.Item>
          </Menu>
        </Header>
      </Fragment>}
    </Fragment>
  );


  if (user && user.role === "admin") {
    return <Redirect to="/cms/admin/dashboard"></Redirect>;
  } if (user && user.role === "agency") {
    return <Redirect to="/cms/admin/blog-list"></Redirect>;
  }
  return loading && user === null ? (
    <Fragment>
      <Skeleton
        title={true}
        paragraph={false}
        loading={loading}
        rows="1"
        active
      ></Skeleton>
    </Fragment>
  ) : (
      <Fragment>
        {console.log("user >", user)}
        {user == null ? generalHeader : ''}
        {user && user.role != "admin" ? userHeader : <Fragment> </Fragment>}
      </Fragment>
    );
};

const HeaderComp = Form.create({ name: "header_comp" })(HeaderForm);
HeaderComp.prototypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.auth.user
});

export default connect(mapStateToProps, { logout })(withDirection(HeaderComp));
