import React, { Component, Fragment } from 'react'
import { NavLink } from "react-router-dom";
import { Menu } from "antd";


// Icons Used
import BookingIcon from "../../../../assets/img/HeaderIcon/booking.svg";
import FacilitiesIcon from "../../../../assets/img/HeaderIcon/facilities.svg";
import FoodIcon from "../../../../assets/img/HeaderIcon/food.svg";
import Chatbot from "../../../../assets/img/HeaderIcon/cb1.svg";
import MoniterIcon from "../../../../assets/img/HeaderIcon/monitor.svg";
import '../../../../assets/css/upappheadermenudata.css'
const UpappHeaderMenuData = (props) => {
    return (
        <Menu className="Product-Menu">
            <Menu.Item>
                {/* <NavLink activeClassName="is-active" to="/ecommerce-platform"> */}
                <NavLink activeClassName="is-active" to="/eCommerce-Single-Vendor-Platform">
                    <div className="menu-list-name">
                        <img src={MoniterIcon} className="desktopMenuIconView" />
                        <span style={{ color: '#000000a6' }}>{props.ecommerce}</span>
                    </div>
                </NavLink>
            </Menu.Item>
            <Menu.Item>
                {/* <NavLink activeClassName="is-active" to="/ecommerce-marketplace"> */}
                <NavLink activeClassName="is-active" to="/eCommerce-Multi-Vendor-Platform">
                    <div className="menu-list-name">
                        <img src={BookingIcon} className="desktopMenuIconView" />
                        <span style={{ color: '#000000a6' }}>{props.multivendor}</span>
                    </div>
                </NavLink>
            </Menu.Item>
            {/* <Menu.Item>
                <NavLink activeClassName="is-active"
                    to="/facility-management-application">
                    <div className="menu-list-name">
                        <img src={FacilitiesIcon} className="desktopMenuIconView" />
                        <span style={{ color: '#000000a6' }}>{props.Facility}</span>
                    </div>
                </NavLink>
            </Menu.Item> */}
            <Menu.Item>
                <NavLink activeClassName="is-active" to="/restaurant-application">
                    <div className="menu-list-name">
                        <img src={FoodIcon} className="desktopMenuIconView" />
                        <span style={{ color: '#000000a6' }}>{props.Restaurant}</span>
                    </div>
                </NavLink>
            </Menu.Item>
            <Menu.Item>
                <NavLink activeClassName="is-active" to="/upappbot">
                    <div className="menu-list-name">
                        <img src={Chatbot} className="desktopMenuIconView" />
                        <span style={{ color: '#000000a6' }}>{props.UPappbot}</span>
                    </div>
                </NavLink>
            </Menu.Item>
        </Menu>
    )
}

export default UpappHeaderMenuData
