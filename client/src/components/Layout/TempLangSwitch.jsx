import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Layout,
  Row,
  Col,
  Typography,
  Select,
  Skeleton,

} from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';


const { Title, Text } = Typography;
const recaptchaRef = React.createRef();

const TempLangSwitch = ({
  direction
}) => {
  const { Title } = Typography;
  const { Option } = Select;

  const { t } = useTranslation();


  const [state, setState] = useState({

  });

  const {
    Language
  } = state;

  useEffect(() => {

    if(localStorage.getItem('lng') ){
			setState({
                ...state,
				Language: localStorage.getItem('lng')
			})
		} else{
			localStorage.setItem('lng', 'EN');
			setState({
          ...state,
          Language: 'EN'
			})
    }
    
    if(localStorage.getItem('lng') === 'AR'){
			// layoutDirection = 'rtl';
			i18next.changeLanguage('AR')
		  }else if(localStorage.getItem('lng') === 'EN'){
			// layoutDirection = 'ltr';
			i18next.changeLanguage('EN')
      }
      
  }, []);


// language change


  const LanguageChanged = (lang) => {
    console.log(lang);
    localStorage.setItem('lng', lang)
    
    setState({
        ...state,
        Language: localStorage.getItem('lng')
    })
    window.location.reload()
}


  // Google reCAPCHA


  // End Of Google reCAPCHA

  // Forms

  // Redirect if contact form is submitted

  // End Of Forms

  // Video Modal

 
  // End Of Video Modal

  return (
    <Fragment>
  <Row style={{height:"100Vh"}}>
				<Col lg={24} md={24} sm={24} xs={24}>
					<div
						className="footer-cont"
						style={{ margin: "45Vh auto", textAlign: "center" }}
					>
                        <Title>Switch Language</Title>
						<Select
							onSelect={LanguageChanged}
							defaultValue={localStorage.getItem('lng')}
							style={{ width:280, padding: "0px 15px" }}
						>
							<Option value="EN">English</Option>
							<Option value="AR">Arabic</Option>
						</Select>

						
					</div>
				</Col>
			</Row>
      
    </Fragment>
  );
};

TempLangSwitch.prototypes = {
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, { })(
  withDirection(TempLangSwitch)
);
