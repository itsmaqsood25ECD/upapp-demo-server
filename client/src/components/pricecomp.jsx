import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class pricecomp extends Component {
	render() {
		return (
			<>
				<div className="Price-card">
					<div className="price-container">
						<div className="Currency-Symbol">{this.props.currency}</div>
						<div className="price">{this.props.price}</div>
						<div className="duration">/{this.props.duration}</div>
					</div>
				</div>
				{/* <div className="learn-more-btn">
                                <Link to="/subscription">Learn More</Link>
                </div> */}
			</>
		);
	}
}
