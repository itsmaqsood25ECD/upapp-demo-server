import React from "react";
import "antd/dist/antd.css";
import { List, Card, Row, Col } from "antd";
import '../../../assets/css/restaurantwebdemo.css'

const Newdata = [
    {
        title: "Layout 1",
        // url: "https://ecomdemo1.upappfactory.app/",
        // img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layout1.png"
    },
    {
        title: "Layout 2",
        // url: "https://ecomdemo2.upappfactory.app/",
        // img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layout2.png"
    },
    {
        title: "Layout 3",
        // url: "https://ecomdemo3.upappfactory.app/",
        // img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layout3.png"
    }];



const { Meta } = Card;
let count;
class WebsiteDemo extends React.Component {
    state = {
        initLoading: true,
        loading: false,
        data: [],
        list: []
    };

    componentDidMount() {
        this.setState({
            initLoading: false,
            data: Newdata,
            list: []
        });
    }

    getData = () => {
        let count = 3;
        while (count > 0) {
            console.log(Newdata);
            count = count - 1;
        }
    };

    onLoadMore = () => {
        this.setState({
            loading: true,
            list: this.state.data.concat(
                [...new Array(count)].map(() => ({ loading: true, title: {} }))
            )
        });
        this.getData(res => {
            const data = this.state.data.concat();
            this.setState({
                data,
                list: data,
                loading: false
            });
        });
    };

    render() {
        return (
            // <div style={{ maxWidth: "1760px", margin: "0 auto" }}>
            //     <List
            //         grid={{
            //             gutter: 16, xs: 1, sm: 2, md: 2, lg: 3, xl: 3, xxl: 3
            //         }}
            //         itemLayout="horizontal"
            //         dataSource={Newdata}
            //         renderItem={item => (
            //             <List.Item className="image tran-4">
            //                 <Card
            //                     hoverable
            //                     className="store-card theme-card"
            //                     cover={
            //                         <a href={item.url} target="_blank">
            //                             <img alt="example" src={item.img} />
            //                         </a>
            //                     }
            //                 >
            //                     <Meta title={item.title} />
            //                 </Card>
            //             </List.Item>
            //         )}
            //     />
            // </ div>

            <div style={{ maxWidth: "1760px", margin: "0 auto" }}>
                <div className="main-demo">
                    {/* <List
                        grid={{
                            gutter: 16, xs: 1, sm: 2, md: 2, lg: 3, xl: 3, xxl: 3
                        }}
                        itemLayout="horizontal"
                        dataSource={Newdata}
                        renderItem={item => (
                            <List.Item className="image tran-4 layout2" >
                                <a href={item.url} target="_blank">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layouthead.jpg" width="100%" />
                                </a>
                                <Meta title={item.title} className="layoutName" />
                            </List.Item>
                        )}
                    /> */}

                    <Row gutter={[16, 16]}>
                        <Col span={8}>
                            <div className="image tran-4 layout1">
                                <a href="javascript:void(0)" target="_blank">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layouthead.jpg" width="100%" />
                                </a>
                            </div>
                            <h3>
                                <a href="javascript:void(0)" target="_blank">Layout 1</a>
                            </h3>
                        </Col>
                        <Col span={8}>
                            <div className="image tran-4 layout2">
                                <a href="javascript:void(0)" target="_blank">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layouthead.jpg" width="100%" />
                                </a>
                            </div>
                            <h3>
                                <a href="javascript:void(0)" target="_blank">Layout 2</a>
                            </h3>
                        </Col>
                        <Col span={8}>
                            <div className="image tran-4 layout3">
                                <a href="javascript:void(0)" target="_blank">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/WebsiteImages/Layouthead.jpg" width="100%" />
                                </a>
                            </div>
                            <h3>
                                <a href="javascript:void(0)" target="_blank">Layout 3</a>
                            </h3>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}


export default WebsiteDemo;
