import React, { Component, Fragment } from 'react'
import { Row, Col, Typography, List, Icon, Tooltip, Spin, Select } from 'antd';
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import { connect } from "react-redux";
const { Title } = Typography;


const EcommerceAppFeatureList = ({ direction }) => {

	const { t } = useTranslation();

	const upAppSpecialData = [

		{
			title: `${t('Pricing.ecommerce.COMPLIMENTARY.1.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.1.desc')}`
		},
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.2.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.2.desc')}`
		},
		// {

		// 	title: `${t('Pricing.ecommerce.COMPLIMENTARY.3.title')}`,
		// 	desc: `initial content management is provided with the first 100 product's upload`
		// },
		// {

		// 	title: `${t('Pricing.ecommerce.COMPLIMENTARY.4.title')}`,
		// 	desc:'Two 1 hour training sessions are provided to assist you with the initial promotions, offers, and marketing activities set up'
		// },
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.5.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.5.desc')}`
		},
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.14.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.14.desc')}`
		},
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.6.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.6.desc')}`
		},
		// {

		// 	title:`${t('Pricing.ecommerce.COMPLIMENTARY.7.title')}`,
		// 	desc: 'Automatic app system upgrade'
		// },
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.8.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.8.desc')}`
		},
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.10.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.10.desc')}`
		},
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.12.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.12.desc')}`
		},
		{

			title: `${t('Pricing.ecommerce.COMPLIMENTARY.13.title')}`,
			desc: `${t('Pricing.ecommerce.COMPLIMENTARY.13.desc')}`
		}
	];


	const UPappUserFeature = [
		{
			title: `${t('Pricing.ecommerce.USER.19.title')}`,
			desc: `${t('Pricing.ecommerce.USER.19.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.20.title')}`,
			desc: `${t('Pricing.ecommerce.USER.20.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.21.title')}`,
			desc: `${t('Pricing.ecommerce.USER.21.desc')}`
		},
		//   {
		//       title: 'Card Payment',
		// 	desc: 'Seamless card payment through payment gateway which you must provide to our team & we integrate'
		// },
		{
			title: `${t('Pricing.ecommerce.USER.4.title')}`,
			desc: `${t('Pricing.ecommerce.USER.4.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.3.title')}`,
			desc: `${t('Pricing.ecommerce.USER.3.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.22.title')}`,
			desc: `${t('Pricing.ecommerce.USER.22.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.23.title')}`,
			desc: `${t('Pricing.ecommerce.USER.23.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.24.title')}`,
			desc: `${t('Pricing.ecommerce.USER.24.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.25.title')}`,
			desc: `${t('Pricing.ecommerce.USER.25.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.26.title')}`,
			desc: `${t('Pricing.ecommerce.USER.27.title')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.27.title')}`,
			desc: `${t('Pricing.ecommerce.USER.27.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.10.title')}`,
			desc: `${t('Pricing.ecommerce.USER.10.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.28.title')}`,
			desc: `${t('Pricing.ecommerce.USER.28.title')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.29.title')}`,
			desc: `${t('Pricing.ecommerce.USER.29.title')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.17.title')}`,
			desc: `${t('Pricing.ecommerce.USER.17.desc')}`,
		}
	]
	const UPappAdminFeature = [
		{
			title: `${t('Pricing.ecommerce.ADMIN.18.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.18.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.19.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.19.desc')}`
		},
		// {
		// 	title: `${t('Pricing.ecommerce.ADMIN.20.title')}`,
		// 	desc: `${t('Pricing.ecommerce.ADMIN.20.desc')}`
		// },
		{
			title: `${t('Pricing.ecommerce.ADMIN.21.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.21.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.22.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.22.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.23.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.23.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.24.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.24.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.USER.4.title')}`,
			desc: `${t('Pricing.ecommerce.USER.4.desc')}`,
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.26.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.26.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.27.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.27.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.29.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.29.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.30.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.30.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.31.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.31.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.32.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.32.desc')}`
		},
		{
			title: `${t('Pricing.ecommerce.ADMIN.33.title')}`,
			desc: `${t('Pricing.ecommerce.ADMIN.33.desc')}`
		}
	]

	return (
		<Fragment>

			<Row
				gutter={16}
				style={{
					marginTop: '50px',
					marginBottom: '50px',
					marginLeft: '0px',
					marginRight: '0px'
				}}
				// type="flex"
				justify="center">
				<div style={{ margin: "50px auto", maxWidth: "1240px" }}>
					<Col xs={24} md={8} lg={8}>
						<div style={{ textAlign: 'center' }} className="feature-title-shape user-feature-title">
							{t('Pricing.ecommerce.UPAPPCOMPLIMENTARY')}
						</div>

						<List
							size="large"
							className="feature-lists"
							dataSource={upAppSpecialData}
							renderItem={item => (
								<List.Item className="subs-feature-list">
									<Icon className="green-6" type="check" /> {item.title}&nbsp;
									<Tooltip title={item.desc}>
										<Icon
											style={{
												fontSize: '12px',
												color: '#607D8B',
												cursor: 'pointer'
											}}
											type="info-circle"
										/>
									</Tooltip>
								</List.Item>
							)}
						/>
					</Col>
					<Col xs={24} md={8} lg={8} >
						<div style={{ textAlign: 'center' }} className="feature-title-shape user-feature-title">
							{t('Pricing.ecommerce.USERFEATURES')}
						</div>

						<List
							size="large"
							className="feature-lists"
							dataSource={UPappUserFeature}
							renderItem={item => (
								<List.Item className="subs-feature-list">
									<Icon className="green-6" type="check" /> {item.title}&nbsp;
									<Tooltip title={item.desc}>
										<Icon
											style={{
												fontSize: '12px',
												color: '#607D8B',
												cursor: 'pointer'
											}}
											type="info-circle"
										/>
									</Tooltip>
								</List.Item>
							)}
						/>
					</Col>
					<Col xs={24} md={8} lg={8} >
						<div style={{ textAlign: 'center' }} className="feature-title-shape admin-feature-title">
							{t('Pricing.ecommerce.ADMINFEATURES')}
						</div>
						<List
							size="large"
							className="feature-lists"
							dataSource={UPappAdminFeature}
							renderItem={item => (
								<List.Item className="subs-feature-list">
									<Icon className="green-6" type="check" /> {item.title}&nbsp;
									<Tooltip title={item.desc}>
										<Icon
											style={{
												fontSize: '12px',
												color: '#607D8B',
												cursor: 'pointer'
											}}
											type="info-circle"
										/>
									</Tooltip>
								</List.Item>
							)}
						/>
					</Col>
				</div>
			</Row>

		</Fragment>
	)
}
const mapStateToProps = state => ({
	direction: withDirectionPropTypes.direction,
});
export default connect(mapStateToProps, {
	// getLocation
})(withDirection(EcommerceAppFeatureList));


