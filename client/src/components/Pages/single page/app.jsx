/* eslint react/prop-types: 0 */
import React, { useEffect, Fragment, useState } from "react";
import {
  Row,
  Col,
  Typography,
  Icon,
  Form,
  Input,
  Spin,
  Button,
  Modal,
  Anchor
} from "antd";
import ReactGA from "react-ga";
import { requestForCustomisedApp } from "../../../actions/contact.action";
import ScrollTop from "../../ScrollTop";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import SSslider from "./SSslider";
import { getApplication } from "../../../actions/application.action";
import { requestDemo } from "../../../actions/contact.action";
import PackagePriceVerticleEcommerce from "../PackagePriceVerticleEcommerce";
import PackagePriceVerticleBooking from "../PackagePriceVerticleBooking";
import WebsiteThemes from "../WebsiteThemes";
import RequestForDemo from "../RequestForDemo";
import Walkthrugh from "../Walkthrugh";
import { Carousel } from "antd";
import { Helmet } from "react-helmet";
import OrderMangementIMG from "../../../assets/img/whatYouWillManage/web/OrderManagement@2x.png";
import ProductMangementIMG from "../../../assets/img/whatYouWillManage/web/ProductManagement@2x.png";
import RefundMangementIMG from "../../../assets/img/whatYouWillManage/web/RefundMangement@2x.png";
import CouponMangementIMG from "../../../assets/img/whatYouWillManage/web/CouponMangement@2x.png";
import EcommerceUpAppSpecial from "./EcommerceUpAppSpecial";
import BookingAppFeatureList from "./BookingAppFeatureList";
import BookingUpAppSpecial from "./BookingUpAppSpecial";
import EcommerceAppFeatureList from "./EcommerceAppFeatureList";
import "../../../assets/css/single-app.css";

import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import SmartInput from "react-phone-number-input/smart-input";
import ReCAPTCHA from "react-google-recaptcha";
import { setAlert } from "../../../actions/alert.action.jsx";
import MobileAppView from "../../MobileAppView/MobileAppPage";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import DirectionProvider from 'react-with-direction/dist/DirectionProvider';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

// ecommerce img

import AndroidIosAppIncluded from "../../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";
import SecureAGILE from "../../../assets/img/eCommerce Page/SecureAGILE.svg";
import MultiVendor from "../../../assets/img/eCommerce Page/MultiVendor.svg";
import ResponsiveWebsite from "../../../assets/img/eCommerce Page/ResponsiveWebsite.svg";

// Key Featuresss

// Key Benifits
import ReadyToLaunch from "../../../assets/img/eCommerce Page/KeyBenifits/ReadyToLaunch.svg";
import NotificationsSMSEmail from "../../../assets/img/eCommerce Page/KeyBenifits/NotificationsSMSEmail.svg";
import SecurityisParamount from "../../../assets/img/eCommerce Page/KeyBenifits/SecurityisParamount.svg";
import AcceptPaymentsOnline from "../../../assets/img/eCommerce Page/KeyBenifits/AcceptPaymentsOnline.svg";
import IntegrationAPI from "../../../assets/img/eCommerce Page/KeyBenifits/Integration&API.svg";

// End Key Benifits

//Business Benifits
import AutomaticAppSystemUpgrade from "../../../assets/img/eCommerce Page/Business Benifits/AutomaticAppSystemUpgrade.svg";
import FastestLoadingTime from "../../../assets/img/eCommerce Page/Business Benifits/FastestLoadingTime.svg";
import SocialMediaIntegration from "../../../assets/img/eCommerce Page/Business Benifits/SocialMediaIntegration.svg";
import FreeAppStoreFeeSpace from "../../../assets/img/eCommerce Page/Business Benifits/FreeAppStoreFee&Space.svg";
import ProductsPromotions from "../../../assets/img/eCommerce Page/Business Benifits/Products&Promotions.svg";

import UnlimitedTransactions from "../../../assets/img/eCommerce Page/Client Benifits/UnlimitedTransactions.svg";
import Reviews from "../../../assets/img/eCommerce Page/Client Benifits/Reviews.svg";

// end of ecommerce img

const { Link } = Anchor;
const { TextArea } = Input;
const { Title } = Typography;

const recaptchaRef = React.createRef();


const App = ({
  match,
  getApplication,
  setAlert,
  Application,
  loading,
  user,
  direction,
  requestForCustomisedApp,
  form: { getFieldDecorator, validateFields, setFieldsValue }
}) => {
  const [formData, setFormData] = useState({
    initLoading: true,
    data: [],
    list: [],
    visible: false
  });

  let layoutDirection;
  const { t } = useTranslation();


  if (localStorage.getItem('lng') === 'AR') {
    layoutDirection = 'rtl';
    // i18next.changeLanguage('AR')
  } else if (localStorage.getItem('lng') === 'EN') {
    layoutDirection = 'ltr';
    // i18next.changeLanguage('EN')
  } else {
    layoutDirection = 'ltr';
    // i18next.changeLanguage('EN')
  }

  const KeyBenifits = [
    {
      icon: ReadyToLaunch,
      name: `${t("ecommercePage.ReadyToLaunch")}`
    },
    {
      icon: NotificationsSMSEmail,
      name: `${t("ecommercePage.PushNotifications")}`
    },
    {
      icon: SecurityisParamount,
      name: `${t("ecommercePage.HighlySecurePlatform")}`
    },
    {
      icon: AcceptPaymentsOnline,
      name: `${t("ecommercePage.AcceptPaymentOnline")}`
    },
    // {
    //   icon: IntegrationAPI,
    //   name: `${t("ecommercePage.Integration&API")}`
    // },

    // {
    //   icon: AutomaticAppSystemUpgrade,
    //   name:`${t("ecommercePage.Automaticappsystemupgrade")}`
    // },

    {
      icon: FastestLoadingTime,
      name: `${t("ecommercePage.QuickLoadTime")}`
    },
    // {
    //   icon: SocialMediaIntegration,
    //   name: `${t("ecommercePage.SocialMediaIntegration")}` 
    // },
    {
      icon: FreeAppStoreFeeSpace,
      name: `${t("ecommercePage.Noadditionalservercost")}`
    },
    {
      icon: ProductsPromotions,
      name: `${t("ecommercePage.MultiplePromotions")}`
    },
    {
      icon: UnlimitedTransactions,
      name: `${t("ecommercePage.UnlimitedTransactions")}`
    },
    {
      icon: Reviews,
      name: `${t("ecommercePage.Reviews")}`
    }

  ];


  const [state, setState] = useState({
    country: "",
    currency: "",
    // Booking
    bookingMobileBDSPM: "",
    bookingMobileADSPM: "",
    bookingMobileBDSPY: "",
    bookingMobileADSPY: "",
    bookingWebsiteBDSPM: "",
    bookingWebsiteADSPM: "",
    bookingWebsiteBDSPY: "",
    bookingWebsiteADSPY: "",
    bookingWebAppBDSPM: "",
    bookingWebAppADSPM: "",
    bookingWebAppBDSPY: "",
    bookingWebAppADSPY: "",
    // Ecommcerce
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    newWebsiteBDSPM: "",
    newWebsiteADSPM: "",
    newWebsiteBDSPY: "",
    newWebsiteADSPY: "",
    newWebAppBDSPM: "",
    newWebAppADSPM: "",
    newWebAppBDSPY: "",
    newWebAppADSPY: "",
    country: sessionStorage.getItem("country"),
    visible: false,
    modalVisible: false,
    HumanVarification: false,
    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: "",
    // setup charge
    WSC: "",
    MSC: "",
    WMSC: ""
  });

  const {
    // Ecommerce
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    newWebsiteBDSPM,
    newWebsiteADSPM,
    newWebsiteBDSPY,
    newWebsiteADSPY,
    newWebAppBDSPM,
    newWebAppADSPM,
    newWebAppBDSPY,
    newWebAppADSPY,
    currency,
    HumanVarification,
    WFPP,
    WAFPP,
    MFPP,
    // setup charge
    WSC,
    MSC,
    WMSC
  } = state;

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const onFormChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!HumanVarification) {
        console.log("You are BOT");
        setAlert("Please Verify reCAPTCHA", "danger");
      } else if (!err) {
        ReactGA.event({
          category: "Contact",
          label: "custom_app_request",
          action: "Request Custom_App Submit",
          value: 10
        });

        requestForCustomisedApp(formData);
        setState({
          ...state,
          isSubmitted: true
        });
      }
    });
  };

  const onCapchaChange = e => {
    console.log(e);
    setState({
      ...state,
      HumanVarification: true
    });
  };

  const onExpired = () => {
    console.log("expired");
    setState({
      ...state,
      HumanVarification: false
    });
  };

  const showModal = () => {
    setState({
      ...state,
      modalVisible: true
    });
  };

  function success() {
    Modal.success({
      content: "Request Sent Successfully"
    });
  }

  const handleOk = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!HumanVarification) {
        console.log("You are BOT");
        setAlert("Please Verify reCAPTCHA", "danger");
      } else if (!err) {
        ReactGA.event({
          category: "Contact",
          label: "custom_app_request",
          action: "Request Custom_App Submit",
          value: 10
        });

        requestForCustomisedApp(formData);
        setFormData({
          ...formData,
          mobile: "",
          name: "",
          email: "",
          country: "",
          subject: "",
          description: ""
        });
        setState({
          ...state,
          modalVisible: false
        });

        success();
      }
    });
    console.log(e);
  };

  const handleCancel = e => {
    console.log(e);
    setState({
      ...state,
      modalVisible: false
    });
  };

  const showDrawer = () => {
    setState({
      visible: true
    });
  };

  const onClose = () => {
    setState({
      visible: false
    });
  };

  useEffect(() => {
    getApplication(match.params.id);

    setState({
      ...state,
      country: sessionStorage.getItem("country"),
      currency: sessionStorage.getItem("currency_sign"),
      // Website
      newWebsiteBDSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPM: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteBDSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      // Web App
      newWebAppBDSPM: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppBDSPY: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      // Mobile
      newMobileBDSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileBDSPY: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPY: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      //  Plan B

      WFPP: Math.round(59.9 * sessionStorage.getItem("currency_rate")),
      MFPP: Math.round(89.9 * sessionStorage.getItem("currency_rate")),
      WAFPP: Math.round(119.9 * sessionStorage.getItem("currency_rate")),
      // setup charge
      WSC: Math.round(500 * sessionStorage.getItem("currency_rate")),
      WMSC: Math.round(1200 * sessionStorage.getItem("currency_rate")),
      MSC: Math.round(800 * sessionStorage.getItem("currency_rate"))
    });

  }, [sessionStorage.getItem("currency_rate")]);
  return Application === null || loading === true ? (
    <Fragment>
      <Spin />
    </Fragment>
  ) : (
      <Fragment>
        <Helmet>
          <title>{`${Application.fields &&
            Application.fields.name} - UPappfactory`}</title>
          <meta
            name={`Buy our readymade Mobile Appointment App for your ${Application.fields &&
              Application.fields.name} and take your business online today with our unique monthly or yearly subscription pricing.`}
            content={Application.fields && Application.fields.desc}
          />
        </Helmet>
        <ScrollTop />
        <section className="UAF-container-fluid">
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ marginTop: "80px" }}
            className={
              Application.fields && Application.fields.type === "Ecommerce"
                ? "ecomm-app-bg"
                : "booking-app-bg"
            }
          >
            <Col
              lg={{ span: 14, order: 1 }}
              md={{ span: 14, order: 2 }}
              xs={{ span: 24, order: 2 }}
              className="s-feature-text-cont single-app-desc-top-title animated zoomIn faster"
            >

              {direction === DIRECTIONS.LTR && <Fragment>
                <Title level={1} className="single-app-name">
                  {Application.fields && Application.fields.name}
                </Title>
                <Title level={4} className="single-app-description light-text">
                  {Application.fields && Application.fields.desc}
                </Title>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <Title level={1} className="single-app-name">
                  {Application.fields && Application.fields.ar.name}
                </Title>
                <Title level={4} className="single-app-description light-text">
                  {Application.fields && Application.fields.ar.desc}
                </Title>
              </Fragment>
              }
              <br />
              <div className="app-packs">
                <span
                  style={{ fontWeight: "600", color: "#000", padding: "5px 0px" }}
                >
                  <Icon type="mobile" />
                &nbsp; {t("singleapp.UserApplication")}
                &nbsp;&nbsp;&nbsp;
              </span>
                <span
                  style={{ fontWeight: "600", color: "#000", padding: "5px" }}
                >
                  <Icon type="laptop" />
                &nbsp; {t("singleapp.AdminDashboard")}
                &nbsp;&nbsp;&nbsp;
              </span>
                <span
                  style={{ fontWeight: "600", color: "#000", padding: "5px" }}
                >
                  <Icon type="clock-circle" />
                &nbsp; {t("singleapp.BusinessDays")}
                </span>
              </div>
              {/* {Application.fields && Application.fields.type === 'Ecommerce' ? (
					<div className="download app-packs">
						<a className="download-app-button" href="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/upappfactoryecommerce.ipa">
							<img src={iosAppButton} alt=""/>
						</a>
					     <a className="download-app-button" href="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/upappfactoryecommerce.apk">
						 <img src={andoridAppButton} alt=""/>
						 </a>
					</div>) : ''
					} */}
            </Col>
            <Col
              lg={{ span: 10, order: 2 }}
              md={{ span: 10, order: 1 }}
              xs={{ span: 24, order: 1 }}
              style={{ textAlign: "center" }}
            >
              <img
                src={
                  Application.fields && Application.fields.images[0].featured.url
                }
                alt="this"
                className="s-feature-img "
                style={{ marginTop: "50px" }}
              />
            </Col>
          </Row>
        </section>
        {/* Ecommerce Point */}

        {Application.fields && Application.fields.type === "Ecommerce" ? (
          <section
            className="UAF-container-fluid"
            style={{
              background: "#fff",
              padding: 0,
              maxWidth: "1200px",
              margin: "80px auto 80px auto",
              textAlign: "center"
            }}
          >
            <p style={{ fontSize: "45px", marginBottom: "0px" }}>{t("ecommercePage.KeyFeatures")}</p>

            <Row type="flex" justify="center">
              {KeyBenifits.map(index => {
                return (
                  <Col xs={12} sm={6} lg={4}>
                    <div
                      style={{
                        width: "100%",
                        maxWidth: "180px",
                        height: "130px",
                        margin: "30px auto"
                      }}
                    >
                      <img
                        style={{ width: "100px", height: "100px" }}
                        src={index.icon}
                        alt=""
                      />
                      <p>{index.name}</p>
                    </div>
                  </Col>
                );
              })}
            </Row>
          </section>
        ) : (
            ""
          )}

        {Application.fields && Application.fields.type === "Ecommerce" ? (
          <Fragment>


            <section
              className="UAF-container-fluid"
              style={{
                background: "#fff",
                padding: 0,
                maxWidth: "1200px",
                margin: "0px auto 80px auto"
              }}
            >
              <Row type="flex">
                <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>

                  {direction === DIRECTIONS.LTR && <Fragment>
                    <div
                      className="tagline-heading sub-points lg-rtl"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-rtl" style={{}}>
                        {t("ecommercePage.readytolaunch")}
                        <br /> {t("ecommercePage.e-CommerceBusiness")}
                      </Title>
                      <Title
                        level={1}
                        className="text-demo-sub-text main lg-float-right lg-rtl"
                        style={{
                          fontSize: "18px",
                          lineHeight: "20px",
                          fontWeight: "400",
                          maxWidth: "480px"
                        }}
                      >
                        {t("ecommercePage.subtagline")}
                      </Title>
                      <Anchor>
                        <Link
                          href="/contactUs#GetInTouchForm"
                          title={
                            <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                              {t("General.GetStarted")}
                            </Button>
                          }
                        ></Link>
                      </Anchor>
                    </div>
                  </Fragment>}
                  {direction === DIRECTIONS.RTL && <Fragment>
                    <div
                      className="tagline-heading sub-points lg-ltr"
                      style={{ padding: "auto 15px", marginTop: "-50px" }}
                    >
                      <Title level={1} className="texty-demo lg-ltr" style={{}}>
                        {t("ecommercePage.layer3.title1")}
                        <br /> {t("ecommercePage.layer3.title2")}
                      </Title>
                      <Title
                        level={1}
                        className="text-demo-sub-text main lg-float-left lg-ltr"
                        style={{
                          fontSize: "18px",
                          lineHeight: "20px",
                          fontWeight: "400",
                          maxWidth: "480px"
                        }}
                      >
                        {t("ecommercePage.subtagline")}
                      </Title>
                      <Anchor>
                        <Link
                          href="/contactUs#GetInTouchForm"
                          title={
                            <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                              {t("General.GetStarted")}
                            </Button>
                          }
                        ></Link>
                      </Anchor>
                    </div>
                  </Fragment>}


                </Col>
                <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                  <div className="home-feature-img main">
                    <img
                      style={{ maxWidth: "200px" }}
                      src={SecureAGILE}
                      alt="image with animation for upapp"
                    />
                  </div>
                </Col>
              </Row>
            </section>

            <section
              className="UAF-container-fluid"
              style={{
                margin: "80px auto 0px auto"
              }}
            >
              <div
                style={{
                  maxWidth: "1200px",
                  margin: "0 auto",
                  textAlign: "center",
                  padding: "25px"
                }}
              >
                <Row type="flex">
                  <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
                    {direction === DIRECTIONS.LTR && <Fragment>
                      <div
                        className="tagline-heading sub-points"
                        style={{ padding: "auto 15px" }}
                      >
                        <Title level={1} className="texty-demo lg-ltr" style={{}}>
                          {t("ecommercePage.layer4.title1")}

                        </Title>
                        <Title
                          level={1}
                          style={{
                            fontSize: "18px",
                            lineHeight: "20px",
                            fontWeight: "400",
                            maxWidth: "480px"
                          }}
                          className="text-demo-sub-text lg-ltr uaf_Title_Desc1">
                          {t("ecommercePage.layer4.desc")}
                        </Title>

                        <Anchor>
                          <Link
                            href="/contactUs#GetInTouchForm"
                            title={
                              <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                                {t("General.GetStarted")}
                              </Button>
                            }
                          ></Link>
                        </Anchor>
                      </div>
                    </Fragment>
                    }
                    {direction === DIRECTIONS.RTL && <Fragment>
                      <div
                        className="tagline-heading sub-points"
                        style={{ padding: "auto 15px" }}
                      >
                        <Title level={1} className="texty-demo lg-rtl" style={{}}>
                          {t("ecommercePage.layer4.title1")}
                          <br />
                        </Title>
                        <Title
                          level={1}
                          style={{
                            fontSize: "18px",
                            lineHeight: "20px",
                            fontWeight: "400",
                            maxWidth: "480px"
                          }}
                          className="text-demo-sub-text lg-rtl uaf_Title_Desc1">
                          {t("ecommercePage.layer4.desc")}
                        </Title>

                        <Anchor>
                          <Link
                            href="/contactUs#GetInTouchForm"
                            title={
                              <Button size={"large"} className="slider-get-started-btn main lg-float-right">
                                {t("General.GetStarted")}
                              </Button>
                            }
                          ></Link>
                        </Anchor>
                      </div>
                    </Fragment>
                    }
                  </Col>
                  <Col md={{ span: 12, order: 3 }} lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 1 }}>
                    <div className="home-feature-img">
                      <img
                        style={{ maxWidth: "200px" }}
                        src={AndroidIosAppIncluded}
                        alt="image with animation for upapp"
                      />
                    </div>
                  </Col>
                </Row>
              </div>
            </section>

            <section
              className="UAF-container-fluid"
              style={{
                background: "#fff",
                padding: 0,
                maxWidth: "1200px",
                margin: "60px auto 80px auto"
              }}
            >
              <Row type="flex">
                <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                  {direction === DIRECTIONS.LTR && <Fragment>
                    <div
                      className="tagline-heading sub-points lg-rtl"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-rtl" style={{}}>
                        {t("ecommercePage.layer5.title1")} <br /> {t("ecommercePage.layer5.title2")}
                      </Title>
                      <Title
                        level={1}
                        className="text-demo-sub-text main lg-rtl lg-float-right"
                        style={{
                          fontSize: "18px",
                          lineHeight: "20px",
                          fontWeight: "400",
                          maxWidth: "480px"
                        }}
                      >
                        {t("ecommercePage.layer5.desc")}
                      </Title>

                      <Anchor>
                        <Link
                          href="/contactUs#GetInTouchForm"
                          title={
                            <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                              {t("General.GetStarted")}
                            </Button>
                          }
                        ></Link>
                      </Anchor>
                    </div>
                  </Fragment>
                  }
                  {direction === DIRECTIONS.RTL && <Fragment>
                    <div
                      className="tagline-heading sub-points lg-ltr"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-lte" style={{ marginTop: "-50px" }}>
                        {t("ecommercePage.layer5.title1")} <br /> {t("ecommercePage.layer5.title2")}
                      </Title>
                      <Title
                        level={1}
                        className="text-demo-sub-text main lg-ltr lg-float-left"
                        style={{
                          fontSize: "18px",
                          lineHeight: "20px",
                          fontWeight: "400",
                          maxWidth: "480px"
                        }}
                      >
                        {t("ecommercePage.layer5.desc")}
                      </Title>

                      <Anchor>
                        <Link
                          href="/contactUs#GetInTouchForm"
                          title={
                            <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                              {t("General.GetStarted")}
                            </Button>
                          }
                        ></Link>
                      </Anchor>
                    </div>
                  </Fragment>
                  }

                </Col>
                <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                  <div className="home-feature-img main">
                    <img
                      style={{ maxWidth: "200px" }}
                      src={ResponsiveWebsite}
                      alt="image with animation for upapp"
                    />
                  </div>
                </Col>
              </Row>
            </section>
          </Fragment>
        ) : (
            ""
          )}

        {/* end of ecommerce Points */}

        {/* App Screenshots */}
        <Row style={{ paddingTop: "50px", paddingBottom: "20px" }}>
          {Application.fields && Application.fields.type === "Ecommerce" ? (
            <Fragment>
              <div className="single-app-heading-text">
                <br />
                {t("singleapp.HowYourAppWillLookToYourCustomers")}
              </div>
              <SSslider
                screenshots={
                  Application.fields && Application.fields.screenshots
                }
              />
            </Fragment>
          ) : (
              ""
            )}

          {Application.fields && Application.fields.type === "Ecommerce" ? (
            <DirectionProvider direction={layoutDirection}>
              <MobileAppView />
            </DirectionProvider>
          ) : (
              <Fragment>
                <div className="single-app-heading-text">
                  <br />
                  {t("singleapp.HowYourAppWillLookToYourCustomers")}
                </div>
                <SSslider
                  screenshots={
                    Application.fields && Application.fields.images[0].screenshots
                  }
                />
              </Fragment>
            )}
        </Row>
        {/* Ends App Screenshots */}
        <div className="request-for-demo-single-page">
          <RequestForDemo />
        </div>

        {Application.fields && Application.fields.type === "Ecommerce" ? (
          <div
            style={{
              background: "#FAFFFF",
              paddingTop: "20px",
              paddingBottom: "20px"
            }}
          >
            <section className="UAF-container-fluid">
              <Row>
                <Col xs={24} md={24} style={{ textAlign: "center" }}>
                  <div
                    style={{
                      margin: "55px auto"
                    }}
                    className="single-app-heading-text"
                  >
                    {t("ecommercePage.ChhoseDemo")}
                  </div>
                </Col>
                <Col xs={24}>
                  <WebsiteThemes />
                </Col>
              </Row>
            </section>
          </div>
        ) : (
            ""
          )}

        <Row
          style={{
            background: "#FAFFFF",
            paddingTop: "20px",
            paddingBottom: "20px"
          }}
        >
          <Col xs={24} md={24} style={{ textAlign: "center" }}>
            <div
              style={{
                margin: "25px auto"
              }}
              className="single-app-heading-text"
            >
              {t("singleapp.WhatYouwillManage")}
            </div>
          </Col>

          <Col xs={24}>
            <Carousel
              className="what-you-will-manage-slider"
            // style={{ height: '450px' }}
            >
              <div>
                <div className="no-shadow">
                  <Row>
                    <Col lg={12} md={24} xs={24}>
                      <img
                        src={OrderMangementIMG}
                        className="adminSS-img"
                        alt=""
                      />
                    </Col>
                    <Col lg={12} md={24} xs={24}>
                      <img
                        src={CouponMangementIMG}
                        className="adminSS-img"
                        alt=""
                      />
                    </Col>
                  </Row>
                </div>
              </div>
              <div className="no-shadow">
                <Row>
                  <Col lg={12} md={24} xs={24}>
                    <img
                      src={RefundMangementIMG}
                      className="adminSS-img"
                      alt=""
                    />
                  </Col>
                  <Col lg={12} md={24} xs={24}>
                    <img
                      src={ProductMangementIMG}
                      className="adminSS-img"
                      alt=""
                    />
                  </Col>
                </Row>
              </div>
            </Carousel>
          </Col>
          <Col xs={24}>
            <div className="request-for-demo-single-page">
              <RequestForDemo />
            </div>
          </Col>
        </Row>
        <section className="UAF-container-fluid"

        >
          {/* Key Features */}
          {Application.fields && Application.fields.type === "Ecommerce" ? (
            <EcommerceUpAppSpecial />
          ) : (
              <BookingUpAppSpecial />
            )}
        </section>
        <div style={{ margin: "20px auto auto 20px" }}>
          {Application.fields && Application.fields.type === "Ecommerce" ? (

            <PackagePriceVerticleEcommerce
              // currency="OMR"
              currency={currency}
              MBDSPM={newMobileBDSPM}
              MADSPM={newMobileADSPM}
              MBDSPY={newMobileBDSPY}
              MADSPY={newMobileADSPY}
              WBDSPM={newWebsiteBDSPM}
              WADSPM={newWebsiteADSPM}
              WBDSPY={newWebsiteBDSPY}
              WADSPY={newWebsiteADSPY}
              WABDSPM={newWebAppBDSPM}
              WAADSPM={newWebAppADSPM}
              WABDSPY={newWebAppBDSPY}
              WAADSPY={newWebAppADSPY}
              WFPP={WFPP}
              WAFPP={WAFPP}
              MFPP={MFPP}
              WSC={WSC}
              MSC={MSC}
              WMSC={WMSC}
              USER={user}
            />
          ) : (

              <PackagePriceVerticleBooking
                // currency="OMR"
                currency={currency}
                MBDSPM={newMobileBDSPM}
                MADSPM={newMobileADSPM}
                MBDSPY={newMobileBDSPY}
                MADSPY={newMobileADSPY}
                WBDSPM={newWebsiteBDSPM}
                WADSPM={newWebsiteADSPM}
                WBDSPY={newWebsiteBDSPY}
                WADSPY={newWebsiteADSPY}
                WABDSPM={newWebAppBDSPM}
                WAADSPM={newWebAppADSPM}
                WABDSPY={newWebAppBDSPY}
                WAADSPY={newWebAppADSPY}
                WFPP={WFPP}
                WAFPP={WAFPP}
                MFPP={MFPP}
                WSC={WSC}
                MSC={MSC}
                WMSC={WMSC}
                duration="Mo"
                SLUG={Application && Application.fields && Application.fields.slug}
                ID={Application && Application.UID}
                PackType="Annually"
              />

            )

          }

        </div>

        {Application.fields && Application.fields.type === "Ecommerce" ? (
          <EcommerceAppFeatureList />
        ) : (
            <BookingAppFeatureList />
          )}

        <div
          style={{
            textAlign: "center",
            marginBottom: "50px",
            paddingTop: "30px"
          }}
        >
          <Walkthrugh />
        </div>

        <Modal
          title="Customize App Request"
          visible={state.modalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Form onSubmit={e => onSubmit(e)}>
            <Row gutter={16}>
              <Col xs={24} lg={12}>
                <Form.Item>
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "Please type your name"
                      }
                    ]
                  })(
                    <Input
                      placeholder="Name"
                      name="name"
                      onChange={e => onFormChange(e)}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={12}>
                <Form.Item>
                  {getFieldDecorator("email", {
                    rules: [
                      {
                        required: true,
                        message: "Please type your email"
                      }
                    ]
                  })(
                    <Input
                      placeholder="Email"
                      name="email"
                      onChange={e => onFormChange(e)}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={12}>
                <Form.Item>
                  {getFieldDecorator("country", {
                    rules: [
                      {
                        required: true,
                        message: "Please type your country"
                      }
                    ]
                  })(
                    <CountryDropdown
                      blacklist={[
                        "AF",
                        "AO",
                        "DJ",
                        "GQ",
                        "ER",
                        "GA",
                        "IR",
                        "KG",
                        "LY",
                        "MD",
                        "NP",
                        "NG",
                        "ST",
                        "SL",
                        "SD",
                        "SY",
                        "SR",
                        "TM",
                        "VE",
                        "ZW",
                        "IL"
                      ]}
                      name="country"
                      valueType="short"
                      style={{ marginTop: "12px" }}
                      onChange={val => selectCountry(val)}
                    />
                  )}
                </Form.Item>
              </Col>

              <Col xs={24} lg={12}>
                <Form.Item>
                  {getFieldDecorator("mobile", {
                    rules: [
                      {
                        required: true,
                        message: "Please input your mobile number!"
                      }
                    ]
                  })(
                    <PhoneInput
                      style={{ marginTop: "10px" }}
                      name="mobile"
                      inputComponent={SmartInput}
                      placeholder="Enter phone number"
                      onChange={e => onPhoneChange(e)}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={24}>
                <Form.Item>
                  {getFieldDecorator("subject", {
                    rules: [
                      {
                        required: true,
                        message: "Please enter Subject"
                      }
                    ]
                  })(
                    <Input
                      placeholder="Subject"
                      name="subject"
                      onChange={e => onFormChange(e)}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={24}>
                <Form.Item>
                  {getFieldDecorator("description", {
                    rules: [
                      {
                        required: true,
                        message: "Please Enter Description"
                      }
                    ]
                  })(
                    <TextArea
                      name="description"
                      onChange={e => onFormChange(e)}
                      placeholder="Message"
                      autosize={{ minRows: 2, maxRows: 6 }}
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={24}>
                <Form.Item>
                  <ReCAPTCHA
                    ref={recaptchaRef}
                    onExpired={onExpired}
                    sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
                    onChange={e => onCapchaChange(e)}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Modal>
      </Fragment>
    );
};

const RFAC = Form.create({ name: "Request_for_app_customize" })(App);

RFAC.propTypes = {
  requestForCustomisedApp: PropTypes.func.isRequired,
  getApplication: PropTypes.func.isRequired,
  Application: PropTypes.object.isRequired,
  getLocation: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  match: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  requestDemo: PropTypes.func.isRequired,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  Application: state.application.Application,
  loading: state.application.loading,
  user: state.auth.user
});

export default connect(mapStateToProps, {
  getApplication,
  requestDemo,
  requestForCustomisedApp,
  setAlert
})(withDirection(RFAC));
