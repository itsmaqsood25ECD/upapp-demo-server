import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import { Button, Icon } from "antd";

export default class SSsliderFacility extends Component {
  render() {
    function NextArrow(props) {
      const { onClick } = props;
      return (
        <Button
          type="primary"
          onClick={onClick}
          className="ss-next-btn"
          shape="circle"
        >
          <Icon type="arrow-right" />
        </Button>
      );
    }

    function PrevArrow(props) {
      const { onClick } = props;
      return (
        <Button
          type="primary"
          onClick={onClick}
          className="ss-prev-btn"
          shape="circle"
        >
          <Icon type="arrow-left" />
        </Button>
      );
    }

    const ArrowLeft = props => (
      <Button
        type="primary"
        onClick={this.props.onClick}
        className="ss-prev-btn"
        shape="circle"
      >
        <Icon type="arrow-left" />
      </Button>
    );
    const ArrowRight = props => (
      <Button
        type="primary"
        onClick={this.props.onClick}
        className="ss-next-btn"
        shape="circle"
      >
        <Icon type="arrow-right" />
      </Button>
    );

    var settings = {
      prevArrow: <PrevArrow />,
      nextArrow: <NextArrow />,
      dots: false,
      infinite: true,
      centerMode: true,
      speed: 200,
      swipeToSlide: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      lazyLoad: true,
      initialSlide: 0,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    const SS = this.props.screenshots;
    return (
      <div className="screenshot-slider facility-slider">
        <Slider {...settings}>
          {SS &&
            SS.map((data, i) => {
              return (
                <div key={i}>
                  <div className="screenshots-container facility-slider-container">
                    <div className="screenshot-title">{data.name}</div>
                    <img src={data.img} alt="" />
                  </div>
                </div>
              );
            })}
        </Slider>
      </div>
    );
  }
}
