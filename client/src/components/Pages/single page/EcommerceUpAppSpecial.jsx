import React, { Component, Fragment } from "react";
import { Row, Col, Typography, List } from "antd";
import PaymentGatewayIntegrationIcon from "../../../assets/img/Icons/UpAppSpecial/PaymentGatewayIntegration.svg";
import ServerIcon from "../../../assets/img/Icons/UpAppSpecial/server.svg";
import EnglishArabicIcon from "../../../assets/img/Icons/UpAppSpecial/EnglishandArabic.svg";
import contentMangementIcon from "../../../assets/img/Icons/UpAppSpecial/contentMangement.svg";
import SPMarketingIcon from "../../../assets/img/Icons/UpAppSpecial/SPmarketing.svg";
import AppAndplaystoreIcon from "../../../assets/img/Icons/UpAppSpecial/AppAndplaystore.svg";
import TrainingIcon from "../../../assets/img/Icons/UpAppSpecial/Training.svg";
import SupportIcon from "../../../assets/img/Icons/UpAppSpecial/support.svg";
import TechnicalUpgradesIcon from "../../../assets/img/Icons/UpAppSpecial/TechnicalUpgrades.svg";
import AnalyticsToolIcon from "../../../assets/img/Icons/UpAppSpecial/AnalyticsTool.svg";
import SocialMediaIntegrationsIcon from "../../../assets/img/Icons/UpAppSpecial/SocialMediaIntegration.svg";
import SSLIcon from "../../../assets/img/Icons/UpAppSpecial/ssl.svg";
import CDNIcon from "../../../assets/img/Icons/UpAppSpecial/cdn.svg";
import MaliciousAppScanningIcon from "../../../assets/img/Icons/UpAppSpecial/MaliciousAppScanning.svg";
import UnlimitedTransactionsIcon from "../../../assets/img/Icons/UpAppSpecial/unlimitedTransaction.svg";
// feature Images
import NativeMobileApplication from "../../../assets/img/Icons/Featured/NativeMobileApp.png";
import MultiWishlistOption from "../../../assets/img/Icons/Featured/MultiWishlistOption.png";
import multiCurrency from "../../../assets/img/Icons/Featured/multiCurrency.png";
import GoogleAnalytics from "../../../assets/img/Icons/Featured/GoogleAnalytics.png";
import BulkImportProducts from "../../../assets/img/Icons/Featured/BulkImportProducts.png";
import CrossSellProduct from "../../../assets/img/Icons/Featured/CrossSellProductPages.png";
import CrossBrowser from "../../../assets/img/Icons/Featured/CrossBrowser.png";
import MultiLanguageSupport from "../../../assets/img/Icons/Featured/MultiLanguageSupport.png";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';

import { useTranslation} from 'react-i18next';
import i18next from 'i18next';
import { connect } from "react-redux";


const { Title, Text } = Typography;

const EcommerceUpAppSpecial = ({direction}) => {

  const { t } = useTranslation();

  const upAppSpecialData = [
    {
      icon: PaymentGatewayIntegrationIcon,
      title: `${t("ecommerceSpecialFeature.1.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.1.desc")}`
    },
    {
      icon: ServerIcon,
      title: `${t("ecommerceSpecialFeature.2.title")}`,
      desc: `${t("ecommerceSpecialFeature.2.desc")}`
    },
    // {
    //   icon: contentMangementIcon,
    //   title: `${t("ecommerceSpecialFeature.3.title")}`,
    //   desc: `${t("ecommerceSpecialFeature.3.desc")}`
    // },
    // {
    //   icon: SPMarketingIcon,
    //   title: `${t("ecommerceSpecialFeature.4.title")}`,
    //   desc:
    //   `${t("ecommerceSpecialFeature.4.desc")}`
    // },
    {
      icon: AppAndplaystoreIcon,
      title: `${t("ecommerceSpecialFeature.5.title")}`,
      desc: `${t("ecommerceSpecialFeature.5.desc")}`
    },
    {
      icon: TrainingIcon,
      title: `${t("ecommerceSpecialFeature.6.title")}`,
      desc: `${t("ecommerceSpecialFeature.6.desc")}`
    },
    {
      icon: SupportIcon,
      title: `${t("ecommerceSpecialFeature.7.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.7.desc")}`
    },
    // {
    //   icon: TechnicalUpgradesIcon,
    //   title: `${t("ecommerceSpecialFeature.8.title")}`,
    //   desc: `${t("ecommerceSpecialFeature.8.desc")}`
    // },
    {
      icon: AnalyticsToolIcon,
      title: `${t("ecommerceSpecialFeature.9.title")}`,
      desc: `${t("ecommerceSpecialFeature.9.desc")}`
    },
    // {
    //   icon: SocialMediaIntegrationsIcon,
    //   title: `${t("ecommerceSpecialFeature.10.title")}`,
    //   desc: `${t("ecommerceSpecialFeature.10.desc")}`
    // },
    {
      icon: SSLIcon,
      title: `${t("ecommerceSpecialFeature.11.title")}`,
      desc: `${t("ecommerceSpecialFeature.11.desc")}`
    },
    {
      icon: CDNIcon,
      title: `${t("ecommerceSpecialFeature.12.title")}`,
      desc: `${t("ecommerceSpecialFeature.12.desc")}`
    },
    {
      icon: MaliciousAppScanningIcon,
      title: `${t("ecommerceSpecialFeature.13.title")}`,
      desc: `${t("ecommerceSpecialFeature.13.desc")}`
    },
    {
      icon: UnlimitedTransactionsIcon,
      title: `${t("ecommerceSpecialFeature.14.title")}`,
      desc: `${t("ecommerceSpecialFeature.14.desc")}`
    }
  ];
  
  const featureDetails = [
    {
      img: NativeMobileApplication,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.1.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.1.desc")}`
    },
    {
      img: MultiWishlistOption,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.2.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.2.desc")}`
    },
    {
      img: multiCurrency,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.3.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.3.desc")}`
    },
    {
      img: GoogleAnalytics,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.4.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.4.desc")}`
    },
    {
      img: BulkImportProducts,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.5.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.5.desc")}`
    },
    {
      img: CrossSellProduct,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.6.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.6.desc")}`
    },
    {
      img: CrossBrowser,
      title: `${t("ecommerceSpecialFeature.AmazingFeature.7.title")}`,
      desc:
      `${t("ecommerceSpecialFeature.AmazingFeature.7.desc")}`
    }
  ];
    return (
      <Fragment>
        <section style={{ padding: "20px 15px 30px 15px" }}>
          <div
            style={{
              margin: "50px auto 50px auto"
            }}
            className="single-app-heading-text"
          >
            {" "}
            {t("ecommerceSpecialFeature.WhatWillYouGetFromUs")} {" "}
          </div>
          <List
            size="middle"
            className="featureList"
            grid={{
              gutter: 16,
              xs: 1,
              sm: 2,
              md: 2,
              lg: 3,
              xl: 3,
              xxl: 3
            }}
            dataSource={upAppSpecialData}
            renderItem={item => (
              <List.Item className="featuer-list-card">
                {direction === DIRECTIONS.LTR  && <Fragment>

                <Row className="up-card-hover" gutter={16}>
                  <Col span={6}>
                    <div className="upapp-feature-icons">
                      <img
                        style={{
                          width: "35px"
                        }}
                        src={item.icon}
                      ></img>
                    </div>
                  </Col>
                  <Col span={16}>
                    <Title className="fontSize-16 fw-700">{item.title}</Title>
                    <p className="light-text">{item.desc}</p>
                  </Col>
                </Row>
             </Fragment>
             }
              {direction === DIRECTIONS.RTL  && <Fragment>

<Row className="up-card-hover" gutter={16}>
 
  <Col span={16} className="lg-rtl">
    <Title className="fontSize-16 fw-700">{item.title}</Title>
    <p className="light-text">{item.desc}</p>
  </Col>
  <Col span={6}>
    <div className="upapp-feature-icons">
      <img
        style={{
          width: "35px"
        }}
        src={item.icon}
      ></img>
    </div>
  </Col>
</Row>
</Fragment>
}
              </List.Item>
            )}
          />
        </section>

        <section
          style={{ background: "#FFFFFF", padding: "20px 15px 50px 15px" }}
        >
          <div
            style={{
              textAlign: "center",
              fontSize: "30px",
              margin: "50px auto 50px auto"
            }}
          >
            {" "}
            {t("ecommerceSpecialFeature.AmazingFeature.AF")}
          </div>
          <List
            size="middle"
            className="featureList"
            grid={{
              gutter: 16,
              xs: 1,
              sm: 2,
              md: 2,
              lg: 4,
              xl: 4,
              xxl: 4
            }}
            dataSource={featureDetails}
            renderItem={item => (
              <List.Item
                className="Amazing-feature-list"
                style={{ textAlign: "center" }}
              >
                <div className="feature-img-eUpApp-container">
                  <img className="feature-img-eUpApp" src={item.img}></img>
                </div>
                <div className="featureList-container">
                  <Title className="fontSize-16 fw-700">{item.title}</Title>
                  {/* <p className="light-text">
                                        {item.desc}
                                    </p> */}
                </div>

                <div class="blog-card spring-fever">
         
                  {/* <!-- /.title-content --> */}
                  <div class="card-info">
                    <p style={{ fontSize: "16px", fontWeight: "600" }}>
                      {item.title}
                    </p>
                    {item.desc}
                  </div>
                  {/* <!-- overlays --> */}
                  {/* <div class="gradient-overlay"></div> */}
                  <div class="color-overlay"></div>
                </div>
                {/* <!-- /.blog-card --> */}
              </List.Item>
            )}
          />
        </section>
      </Fragment>
    );
}

EcommerceUpAppSpecial.propTypes = {
  direction: withDirectionPropTypes.direction,
};
const mapStateToProps = state => ({

});


export default connect(mapStateToProps, {
})(withDirection(EcommerceUpAppSpecial));