import React, { Component, Fragment } from 'react'
import { NavLink } from "react-router-dom"
import { Button, Typography, Row, Col } from "antd"
import '../../assets/css/clientele-section.css'
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import PropTypes from "prop-types";
import { connect } from "react-redux";


const { Title, Text } = Typography

const ClienteleSection = ({ direction }) => {

    const { t } = useTranslation();

    return (
        <Fragment>
            <div className="UAF-container-fluid"
                style={{
                    // background: "#333",
                    // padding: "20px auto",
                    maxWidth: "1200px", margin: "100px auto 100px auto", textAlign: "center"
                }}>
                <Title level={1} style={{ color: "#333", fontWeight: "400", textAlign: "center" }} >
                    <span style={{ color: "#00BCD4", fontWeight: "800" }}>{t('Home.layer7.Ideas')} </span>
                    {t('Home.layer7.webaught')} <br />
                    {t('Home.layer7.reality')}
                </Title>
                <p style={{ textAlign: "center", color: "#333", fontSize: "16px" }}>
                    {t('Home.layer7.1')} <br /> {t('Home.layer7.2')}
                </p>
                {/* <Row gutter={[16, 16]}>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }} xxl={{ span: 6 }} className="potr-padd">
                        <div className="port-section">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/alhilal.jpg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Al-Hilaal-final-logo.png" style={{ width: "100px" }} />
                                <p>AL-HILAL</p>
                            </div>
                        </div>
                        <div className="port-section hover-port mob-port">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/tutionmela.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over1">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/TuitionMelaLogo.png" style={{ width: "80px" }} />
                                <p>TUITION MELA</p>
                            </div>
                        </div>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }} xxl={{ span: 6 }} className="potr-padd padd-top">
                        <div className="port-section hover-port mob-port">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/burger.jpg" />
                            <div className="overlay"></div>
                            <div className="letter-over1">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/buger_garage.png" style={{ width: "80px" }} />
                                <p>BURGER GARAGE</p>
                            </div>
                        </div>
                        <div className="port-section">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/comex.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Comex+3%402x.png" style={{ width: "100px" }} />
                                <p>COMEX</p>
                            </div>
                        </div>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }} xxl={{ span: 6 }} className="potr-padd padd-top padd-port-top">
                        <div className="port-section">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/salam.jpg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/SalamPropertiesLogo.png" style={{ width: "100px" }} />
                                <p>SALAM PROPERTIES</p>
                            </div>
                        </div>
                        <div className="port-section hover-port mob-port">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/scanzone.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over1">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/ScanZone.png" style={{ width: "80px", margin: "10px auto" }} />
                                <p>SCANZONE</p>
                            </div>
                        </div>
                    </Col>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 6 }} lg={{ span: 6 }} xl={{ span: 6 }} xxl={{ span: 6 }} className="potr-padd padd-top">
                        <div className="port-section hover-port mob-port">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/fursahh.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over1">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Fursahh_Logo_Final.png" style={{ width: "80px", margin: "10px auto" }} />
                                <p>FURSAHH</p>
                            </div>
                        </div>
                        <div className="port-section">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/Sunbleza+2.jpg" />
                                <div className="overlay"></div>
                                <div className="letter-over">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/sunbeleza.png" style={{ width: "100px" }} />
                                    <p>SUNBELEZA</p>
                                </div>
                            </div>
                        <div className="port-section hover-port mob-port">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/sunbeleza.jpeg" height="144px" />
                            <div className="overlay"></div>
                            <div className="letter-over1">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/sunbeleza.png" style={{ width: "80px", margin: "10px auto" }} />
                                <p>SUNBELEZA</p>
                            </div>
                        </div>
                        <div className="port-section hover-port mob-port">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/petbery.jpeg"/>
                                <div className="overlay"></div>
                                <div className="letter-over1">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/petbery.png" style={{ width: "80px", margin:"10px auto" }} />
                                    <p>PETBERY</p>
                                </div>
                            </div>
                        <div className="port-section hover-port mob-port">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/crowneplaza.jpg" />
                            <div className="overlay"></div>
                            <div className="letter-over1">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/crowneplaza.png" style={{ width: "110px", margin: "10px auto" }} />
                                <p>CROWNE PLAZA</p>
                            </div>
                        </div>
                    </Col>
                </Row> */}

                <Row gutter={[16, 16]}>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 9 }} lg={{ span: 9 }} xl={{ span: 9 }} xxl={{ span: 9 }}>
                        <div className="cs_img_main cs_img_container_size1">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/1.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Al-Hilaal-final-logo.png" className="logo1" />
                                <p>AL-HILAL</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} xxl={{ span: 8 }}>
                        <div className="cs_img_main cs_img_container_size2">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/2.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Fursahh_Logo_Final.png" className="logo2" />
                                <p>FURSAHH</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 7 }} lg={{ span: 7 }} xl={{ span: 7 }} xxl={{ span: 7 }}>
                        <div className="cs_img_main cs_img_container_size3">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/3.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/buger_garage.png" className="logo3" />
                                <p>BURGER GARAGE</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 7 }} lg={{ span: 7 }} xl={{ span: 7 }} xxl={{ span: 7 }}>
                        <div className="cs_img_main cs_img_container_size3">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/4.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/SalamPropertiesLogo.png" className="logo4" />
                                <p>SALAM PROPERTIES</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 9 }} lg={{ span: 9 }} xl={{ span: 9 }} xxl={{ span: 9 }}>
                        <div className="cs_img_main cs_img_container_size1">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/5.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/crowneplaza.png" className="logo5" />
                                <p>CROWNE PLAZA</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} xxl={{ span: 8 }}>
                        <div className="cs_img_main cs_img_container_size2">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/6.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/sunbeleza.png" className="logo6" />
                                <p>SUNBELEZA</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} xxl={{ span: 8 }}>
                        <div className="cs_img_main cs_img_container_size2">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/7.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/TuitionMelaLogo.png" className="logo7" />
                                <p>TUITION MELA</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 7 }} lg={{ span: 7 }} xl={{ span: 7 }} xxl={{ span: 7 }}>
                        <div className="cs_img_main cs_img_container_size3">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/8.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/ScanZone.png" className="logo8" />
                                <p>SCANZONE</p>
                            </div>
                        </div>
                    </Col>
                    <Col className="cs_img_container" xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 9 }} lg={{ span: 9 }} xl={{ span: 9 }} xxl={{ span: 9 }}>
                        <div className="cs_img_main cs_img_container_size1">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/clientele-section-backgrounds/9.jpeg" />
                            <div className="overlay"></div>
                            <div className="letter-over">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Comex+3%402x.png" className="logo9" />
                                <p>COMEX</p>
                            </div>
                        </div>
                    </Col>
                </Row>

                <NavLink to="/clientele">
                    <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                            background: "#4FBFDD",
                            color: "#fff",
                            textAlign: "center"
                        }}
                        className="slider-get-started-btn"
                    >
                        {t('Home.layer7.checkout')}
                    </Button>
                </NavLink>
            </div>
        </Fragment>
    )

}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, {})(withDirection(ClienteleSection));
