import React from "react";
import { Typography, Row, Col, Icon, Button, Collapse, Anchor } from "antd";
import ScrollTop from "../../ScrollTop";
import { Helmet } from "react-helmet";
import WebsitePagePricing from '../WebsitePagePricing'

//css
import '../../../assets/css/UPappweb.css'

//Consts
const { Title, Text } = Typography;
const { Panel } = Collapse;
const { Link } = Anchor;
const keyFeatures = [
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/1.png",
        name: "Fully Customizable Layouts​"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/2.png",
        name: "Very High Reliability"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/3.png",
        name: "Advanced User Management"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/4.png",
        name: "High Responsive Design"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/5.png",
        name: "Supports Unlimited Content Types"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/6.png",
        name: "Swift Multilingual Approach"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/7.png",
        name: "Huge Dedicated Community"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/8.png",
        name: "Well defined Configuration Management"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/9.png",
        name: "Greater Scalability Can be integrated to almost Everything"
    }
]
const keySteps = [
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/ks1.svg",
        name: "John shared his business scenario with UPapp factory​",
        num: "1"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/ks2.svg",
        name: "We suggested John with 5 varied industry specific themes​",
        num: "2"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/ks3.svg",
        name: "John selected the best design and shared content and artefacts with us​",
        num: "3"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/ks4.svg",
        name: "John requested for our help to create content and provide high resolution images. We helped him at a very nominal additional cost​​",
        num: "4"
    },
    {
        icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/ks5.svg",
        name: "In just few weeks, website went live and John is minting money",
        num: "5"
    },
]
const websiteBuilders = [
    {
        Key: "1",
        title: "Appearance",
        desc: "Cookie-Cutter Website Template lacks professional look & feel"
    },
    {
        Key: "2",
        title: "Scalability​",
        desc: "Limited set of features with zero room for customization, hampering the scalability of business​"
    },
    {
        Key: "3",
        title: "Mobile Responsiveness​",
        desc: "Websites are optimized to display correctly on mobile devices, not fully mobile responsive​"
    },
    {
        Key: "4",
        title: "SEO Friendliness",
        desc: "Not at all SEO friendly with limitations on SEO best practices implementation​"
    },
    {
        Key: "5",
        title: "Flexibility",
        desc: "If not satisfied with service provider, website can't be reused and needs to be dumped"
    }
]
const Upappwebsite = [
    {
        Key: "1",
        title: "Appearance",
        desc: "Highly Customizable Web Layouts makes businesses stand out of the competition​"
    },
    {
        Key: "2",
        title: "Scalability​",
        desc: "Highly scalable and Easy to customize so that the website can evolve with evolving business needs."
    },
    {
        Key: "3",
        title: "Mobile Responsiveness​",
        desc: "Fully mobile responsive websites built with fluid layouts, media queries and scripts​​"
    },
    {
        Key: "4",
        title: "SEO Friendliness",
        desc: "SEO friendly websites that has potential to rank higher on Google search list​​"
    },
    {
        Key: "5",
        title: "Flexibility",
        desc: "We design and develop a masterpiece and let you be the owner of the website with complete flexibility"
    }
]
const UPappweb = props => {
    return (
        <div style={{ marginTop: "80px" }}>
            <Helmet>
                <title>Build AI Chatbots for your Business with UPapp | Automate Customer Chats‎</title>
                <meta name="description" content="Automate your customer service with ChatBot and never miss a chance to sell or help your customers. Send updates, provide customer support and more." />
                <meta name="keywords" content="Chatbots Online, Chatbot for business, Chatbot for Website, AI Chatbot, AI Chatbot Builder, Chatbot in UAE, Chatbot in Oman" />
            </Helmet>
            {/* ============================================
                            Start Banner Section
                ============================================ */}

            <section className="uaf_wp_bannersection" >
                <Row type="flex" className="UAF-container-fluid lg-rtl" >
                    <Col sm={{ span: 24, order: 2 }} md={{ span: 9, order: 1 }} lg={{ span: 10, order: 1 }} xs={{ span: 24, order: 2 }} ></Col>
                    <Col sm={{ span: 24, order: 2 }} md={{ span: 15, order: 1 }} lg={{ span: 14, order: 1 }} xs={{ span: 24, order: 2 }}>
                        <div className="uaf_wp_slider_content" >
                            <h1 className="uaf_wp_slider_content_title">
                                Do you have a business in mind? Interested in taking your business to a larger audience?
                            </h1>
                            <p className="uaf_wp_slider_content_text">
                                Then don’t wait any further, we have pocket friendly website development plan for you to provide your business the most lucid and fluid digital web presence.
                            </p>
                            <div className="uaf_wp_slider_content_btn_outer">
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button className="uaf_wp_slider_content_btn">
                                            Get Started
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </div>
                    </Col>
                </Row>
            </section>
            {/* ============================================
                            End Banner Section
                ============================================ */}
            {/* ============================================
                            Start Section1
                ============================================ */}
            <section className="uaweb_section1">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row1">
                        <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol1">
                            <div className="uaweb_contentsection_content lg-ltr">
                                <h1 className="lg-ltr uaweb_contentsection_titleleft">
                                    Intuitive & User-friendly Websites
                                </h1>
                                <p className="lg-ltr uaweb_contentsection_textleft">
                                    Create your compelling Digital web experience with us to meet your user, business, industry, and vertical needs and let users fall in love with your brand
                            </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-ltr uaweb_contentsection_getitnowbtnleft">
                                            Get it now
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                        <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="uaweb_contentsection_imgbody">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/img1.svg" />
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section1
                ============================================ */}
            {/* ============================================
                            Start Section2
                ============================================ */}
            <section className="uaweb_section2">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row2">
                        <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <div className="uaweb_contentsection_imgbody">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/img2.svg" />
                            </div>
                        </Col>
                        <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol2">
                            <div className="uaweb_contentsection_content lg-rtl">
                                <h1 className="lg-rtl uaweb_contentsection_titleright">
                                    Dynamic Websites for seamless experience
                                </h1>
                                <p className="lg-rtl uaweb_contentsection_textright">
                                    We follow a holistic approach and proven methodologies to build dynamic websites with all the desired features and seamless experience, leveraging next-generation technologies
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-rtl uaweb_contentsection_getitnowbtnleft">
                                            Get it now
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section2
                ============================================ */}
            {/* ============================================
                            Start Section3
                ============================================ */}
            <section className="uaweb_section3">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row3">
                        <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol3">
                            <div className="uaweb_contentsection_content lg-ltr">
                                <h1 className="lg-ltr uaweb_contentsection_titleleft">
                                    Boost your Business with UPapp Websites
                                </h1>
                                <p className="lg-ltr uaweb_contentsection_textleft">
                                    Enabling businesses to drive revenue, increase engagement and optimize productivity
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-ltr uaweb_contentsection_getitnowbtnleft">
                                            Get it now
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                        <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="uaweb_contentsection_imgbody">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/img3.svg" />
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section3
                ============================================ */}
            {/* ============================================
                            Start Section4
                ============================================ */}
            <section className="uaweb_section4">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row4">
                        <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <div className="uaweb_contentsection_imgbody">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/img4.svg" />
                            </div>
                        </Col>
                        <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol4">
                            <div className="uaweb_contentsection_content lg-rtl">
                                <h1 className="lg-rtl uaweb_contentsection_titleright">
                                    SEO friendly website for better visibility in search engine
                                </h1>
                                <p className="lg-rtl uaweb_contentsection_textright">
                                    Enabling businesses to get noticed by their customers and thus increase in page visits and transaction
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-rtl uaweb_contentsection_getitnowbtnleft">
                                            Get it now
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section4
                ============================================ */}
            {/* ============================================
                            Start Section5
                ============================================ */}
            <section className="uaweb_section5">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row5">
                        <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol5">
                            <div className="uaweb_contentsection_content lg-ltr">
                                <h1 className="lg-ltr uaweb_contentsection_titleleft">
                                    To sustain in the mobile-first era, equip your business with a mobile responsive website.
                                </h1>
                                <p className="lg-ltr uaweb_contentsection_textleft">
                                    To sustain in the mobile-first era, equip your business with a mobile responsive website. Deploying fluid websites for any size of business with high complexity in matter of weeks, maintaining top-notch quality
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-ltr uaweb_contentsection_getitnowbtnleft">
                                            Get it now
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                        <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="uaweb_contentsection_imgbody">
                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/img5.svg" />
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section5
                ============================================ */}
            {/* ============================================
                            Start Section6
                ============================================ */}
            <section className="uaweb_section6">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row6">
                        <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <div className="uaweb_contentsection_imgbody">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/img6.svg" />
                            </div>
                        </Col>
                        <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol6">
                            <div className="uaweb_contentsection_content lg-rtl">
                                <h1 className="lg-rtl uaweb_contentsection_titleright">
                                    Website Support and Maintenance is a challenge that we handle with Ease
                                </h1>
                                <p className="lg-rtl uaweb_contentsection_textright">
                                    We help clients with regular maintenance, code reviews, website audits, upgrades and migrations, web server management, security updates and bug fixes
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-rtl uaweb_contentsection_getitnowbtnleft">
                                            Get it now
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section6
                ============================================ */}
            {/* ============================================
                            Start Section7
                ============================================ */}
            <section className="uaweb_section7 uaweb_gradBg">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row7">
                        <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <h1 className="uaweb_contentsection_title">
                                <span style={{ color: "#4FBFDD" }}>KEY</span> features
                            </h1>
                        </Col>
                        <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} className=" uaweb_contentsection_contentCol7">
                            <Row gutter={16} type="flex" justify="center">
                                {keyFeatures.map(e => {
                                    return (
                                        <Col xs={12} md={6} lg={4} className="uaweb_kf_tile">
                                            <div>
                                                <div className="uaweb_kf_tile_img">
                                                    <img src={e.icon} />
                                                </div>
                                                <p className="uaweb_kf_tile_name">{e.name}</p>
                                            </div>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section7
                ============================================ */}
            {/* ============================================
                            Start Section8
                ============================================ */}
            <section className="uaweb_section8">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row8">
                        <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <h1 className="uaweb_contentsection_title">
                                Just 5 Steps To Launch Your Dream Business Online
                            </h1>
                        </Col>
                        <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol8">
                            <Row gutter={16} type="flex" justify="center">
                                {keySteps.map(e => {
                                    return (
                                        <Col xs={20} md={6} lg={4} className="uaweb_ks_tile">
                                            <div className="uaweb_ks_tile_inner">
                                                <div className="uaweb_ks_tile_img">
                                                    <img src={e.icon} />
                                                </div>
                                                <p className="uaweb_ks_tile_name">{e.name}</p>
                                                <div className="uaweb_ks_tile_hidden">
                                                    <p>Content</p>
                                                </div>
                                            </div>
                                            <div className="uaweb_ks_tile_num">
                                                <h1 className="uaweb_contentsection_title" style={{marginBottom:"0"}}>{e.num}</h1>
                                            </div>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section8
                ============================================ */}
            {/* ============================================
                            Start Section9
                ============================================ */}
            <section className="uaweb_section9 uaweb_gradBg">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="uaweb_contentsection_row9">
                        <Col lg={{ span: 8, order: 1 }} md={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }} className="uaweb_contentsection_contentCol9">
                            <h1 className="uaweb_contentsection_title">
                                Not another Website Builder, We craft unique Websites
                            </h1>
                        </Col>
                        <Col lg={{ span: 24, order: 2 }} md={{ span: 18, order: 2 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol9 uaWeb_s9_desktop">
                            <Row type="flex">
                                <Col lg={{ span: 8, order: 1 }} md={{ span: 8, order: 1 }} className="uaweb_contentsection_contentColInner9">
                                    <h1 style={{ color: "#00C2E1" }}>Properties</h1>
                                </Col>
                                <Col lg={{ span: 8, order: 1 }} md={{ span: 8, order: 1 }} className="uaweb_contentsection_contentColInner9">
                                    <h1>Website Builders​</h1>
                                </Col>
                                <Col lg={{ span: 8, order: 1 }} md={{ span: 8, order: 1 }} className="uaweb_contentsection_contentColInner9">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/logo.png" />
                                    <h1>UPapp Websites</h1>
                                </Col>
                            </Row>
                            <Row type="flex" className="uaweb_contentsection_contentColInner9_tableData">
                                <Col lg={{ span: 8, order: 1 }} md={{ span: 8, order: 1 }} className="uaweb_contentsection_contentColInner9">
                                    <Row type="flex" className="uaweb_table_Col1">
                                        <Col lg={{ span: 24 }} className="uaweb_tableProperties">
                                            <h1>Appearance</h1>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_tableProperties">
                                            <h1>Scalability​</h1>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_tableProperties">
                                            <h1>Mobile Responsiveness​</h1>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_tableProperties">
                                            <h1>SEO Friendliness</h1>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_tableProperties">
                                            <h1>Flexibility</h1>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={{ span: 8, order: 1 }} md={{ span: 8, order: 1 }} className="uaweb_contentsection_contentColInner9">
                                    <Row type="flex" className="uaweb_table_Col2">
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col2Inner">
                                            <p>Cookie-Cutter Website Template lacks professional look & feel</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col2Inner">
                                            <p>Limited set of features with zero room for customization, hampering the scalability of business​</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col2Inner">
                                            <p>Websites are optimized to display correctly on mobile devices, not fully mobile responsive​</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col2Inner">
                                            <p>Not at all SEO friendly with limitations on SEO best practices implementation​</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col2Inner">
                                            <p>If not satisfied with service provider, website can't be reused and needs to be dumped</p>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={{ span: 8, order: 1 }} md={{ span: 8, order: 1 }} className="uaweb_contentsection_contentColInner9">
                                    <Row type="flex" className="uaweb_table_Col3">
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col3Inner">
                                            <p>Highly Customizable Web Layouts makes businesses stand out of the competition​</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col3Inner">
                                            <p>Highly scalable and Easy to customize so that the website can evolve with evolving business needs.</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col3Inner">
                                            <p>Fully mobile responsive websites built with fluid layouts, media queries and scripts​</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col3Inner">
                                            <p>SEO friendly websites that has potential to rank higher on Google search list​</p>
                                        </Col>
                                        <Col lg={{ span: 24 }} className="uaweb_table_Col3Inner">
                                            <p>We design and develop a masterpiece and let you be the owner of the website with complete flexibility</p>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={{ span: 18, order: 2 }} sm={{ span: 18, order: 2 }} xs={{ span: 24, order: 2 }} className="uaweb_contentsection_contentCol9 uaWeb_s9_mobile">
                            <Row>
                                <Col lg={{ span: 18, order: 1 }} md={{ span: 24, order: 1 }} className="uaWeb_s9_mobile_heading">
                                    <h1>Website Builders​</h1>
                                </Col>
                                <Col md={{ span: 15, order: 2 }} sm={{ span: 24, order: 2 }} className="uaweb_contentsection_contentColInner9">
                                    <Collapse accordion
                                        bordered={false}
                                        defaultActiveKey={['1']}
                                        className="site-collapse-custom-collapse"
                                    >
                                        {websiteBuilders.map(index => {
                                            return (
                                                <Panel className="uaf_cp-stagesmobile-tabs site-collapse-custom-panel" header={index.title} key={index.Key}>
                                                    <p>{index.desc}</p>
                                                </Panel>
                                            );
                                        })}
                                    </Collapse>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg={{ span: 18, order: 1 }} md={{ span: 24, order: 1 }} className="uaWeb_s9_mobile_heading">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/website/logo.png" />
                                    <h1>UPapp Websites</h1>
                                </Col>
                                <Col md={{ span: 15, order: 2 }} sm={{ span: 24, order: 2 }} className="uaweb_contentsection_contentColInner9">
                                    <Collapse accordion
                                        bordered={false}
                                        defaultActiveKey={['1']}
                                        className="site-collapse-custom-collapse"
                                    >
                                        {Upappwebsite.map(index => {
                                            return (
                                                <Panel className="uaf_cp-stagesmobile-tabs site-collapse-custom-panel" header={index.title} key={index.Key}>
                                                    <p>{index.desc}</p>
                                                </Panel>
                                            );
                                        })}
                                    </Collapse>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section9
                ============================================ */}
            {/* ============================================
                            Start Section10
                ============================================ */}
            <section className="uaweb_section10">
                <WebsitePagePricing />
            </section>
            {/* ============================================
                            End Section10
                ============================================ */}
        </div>
    );
};
UPappweb.propTypes = {};

export default UPappweb;
