import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import WebPagePricing from './WebPagePricing'
import { loadApplication } from "../../actions/application.action";
import { useTranslation } from 'react-i18next';
const Subscription = ({ direction }) => {
    const [state, setState] = useState({
        selectApplication: "Ecommerce"
    });

    const { t } = useTranslation();

    const [appDetails, setAppData] = useState({
        apps: [],
        currency: "",
        UAWSP: "",
        UAWPP: "",
        UAWBP: ""

    });
    const {
        country,
        currency,
        UAWSP,
        UAWPP,
        UAWBP,

    } = appDetails;


    useEffect(() => {

        setAppData({
            ...appDetails,

            country: sessionStorage.getItem("country"),
            currency: sessionStorage.getItem("currency_sign"),
            // Website
            UAWSP: Math.round(
                349 * sessionStorage.getItem("currency_rate")
            ),
            UAWPP: Math.round(
                699 * sessionStorage.getItem("currency_rate")
            ),
            UAWBP: Math.round(
                999 * sessionStorage.getItem("currency_rate")
            ),
        });

    }, [sessionStorage.getItem("currency")]);


    return (

        <Fragment>
            <div style={{ margin: "40px auto 20px auto" }}>
                <WebPagePricing
                    currency={currency}
                    UAWSP={UAWSP}
                    UAWPP={UAWPP}
                    UAWBP={UAWBP}
                />
            </div>
        </Fragment>
    );
};

Subscription.propTypes = {
    user: PropTypes.object.isRequired,
    loadApplication: PropTypes.func.isRequired,
    Applications: PropTypes.array.isRequired,
    loading: PropTypes.bool
};

const mapStateToProps = state => ({
    user: state.auth.user,
    Applications: state.application.Applications,
    loading: state.application.loading
});
export default connect(mapStateToProps, {
    loadApplication
    // getLocation
})(Subscription);
