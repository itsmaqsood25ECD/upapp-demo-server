/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Button,
  Typography,
  Row,
  Col,
  Icon,
  Carousel,
  List,
  Spin,
  Input,
  Modal,
  Form,
  Tabs
} from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import ReactGA from "react-ga";
import ReCAPTCHA from "react-google-recaptcha";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import SmartInput from "react-phone-number-input/smart-input";
import { Link } from "react-router-dom";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";
import SSsliderFacility from "./single page/SSsliderFacility";

// images
import AndroidIosAppIncluded from "../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";
import FacilityManagementApp from "../../assets/img/FacilityManagementFinal/facility_management_app.svg";
import ContractManagement from "../../assets/img/FacilityManagementFinal/contract_management_large.svg";
import HelpDeskManagement from "../../assets/img/FacilityManagementFinal/helpdesk_management_large.svg";
import MaintenanceManagement from "../../assets/img/FacilityManagementFinal/maintennace_management_large.svg";
import PropertyManagement from "../../assets/img/FacilityManagementFinal/property_management_large.svg";

// end of images

// Icons

import RentalManagementIcon from "../../assets/img/FacilityManagementFinal/rental_management.svg";
import ContractManagementIcon from "../../assets/img/FacilityManagementFinal/Contract_Management.svg";
import PropertyManagementIcon from "../../assets/img/FacilityManagementFinal/property_management.svg";
import MaintenanceManagementIcon from "../../assets/img/FacilityManagementFinal/maintenance_management.svg";
import HelpDeskManagementIcon from "../../assets/img/FacilityManagementFinal/help_desk_management.svg";

// Icons Ended

import PackagePriceVerticleFacility from "./PackagePriceVerticleFacility";
import { loadApplication } from "../../actions/application.action";
import { setAlert } from "../../actions/alert.action.jsx";
import { Helmet } from "react-helmet";
import IndustriesWeServe from "./IndustriesWeServe";
import HomePageBanners from "./HomePageBanners";

const { TabPane } = Tabs;
const { TextArea } = Input;
const { Title, Text } = Typography;
let filteredApps;
const recaptchaRef = React.createRef();

const FacilityManagement = ({
  loadApplication,
  Applications,
  loading,
  direction,
  setAlert
}) => {

  const { t } = useTranslation();

  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",

    // EcommcercenewBDSPM: '',
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    country: "",
    // Setup Charge
    MSC: ""
  });
  const [formData, setFormData] = useState({});

  const [state, setState] = useState({
    visible: false,
    modalVisible: false,
    userlocation: "",
    HumanVarification: false
  });

  const {
    apps,
    selectApplication,
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    MSC
  } = appDetails;

  let ecommApps, bookingApps;

  const Facility_Screenshot = [
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Splash+Screen%402x.png",
      name: "Splash Screen"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Sign+In%402x.png",
      name: "Sign In Screen"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Sign+Up%402x.png",
      name: "Sign Up Screen"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Splash+Screen+%E2%80%93+4%402x.png",
      name: "Properties Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Home%402x.png",
      name: "Home Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Maintenance+Request%402x.png",
      name: "Maintenance Request"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Menu%402x.png",
      name: "Menu Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Notification%402x.png",
      name: "Notification Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Property+page%402x.png",
      name: "Payment History"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Registered+Req%402x.png",
      name: "Registered Request"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Registered+Req%402x.png",
      name: "Rent Properties"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Tenancy+Contracts%402x.png",
      name: "Tenancy Contracts"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Tenancy+Documents%402x.png",
      name: "Tenancy Documents"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Edit+Profile+page%402x.png",
      name: "Edit Profile"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Directory%402x.png",
      name: "Directory Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Contact+Us%402x.png",
      name: "Contact Us"
    }
  ];

  const Maintainer_Screenshot = [
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Splash+Screen%402x.png",
      name: "Splash Screen"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Sign+In%402x.png",
      name: "Sign In Screen"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Sign+Up%402x.png",
      name: "Sign Up Screen"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/User+End/Splash+Screen+%E2%80%93+4%402x.png",
      name: "Properties Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/AddComment%402x.png",
      name: "Add Comments"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/AddDescription%402x.png",
      name: "Add Description"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/MyRequestsCompleted%402x.png",
      name: "Completed Request"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/Edit+Profile+page%402x.png",
      name: "Edit Profile"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/MyRequestPending%402x.png",
      name: "My Request Pending"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/MyRequestsToday%402x.png",
      name: "My Request Today"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/NewRequests%402x.png",
      name: "New Request Page"
    },
    {
      img:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Screens/FacilityScreens/Maintainer/App+Settings%402x.png",
      name: "Setting Page"
    }
  ];

  const keyFeatures = [
    {
      icon: HelpDeskManagementIcon,
      title: `${t("FacilityPage.HelpDeskManagement")}`
    },
    {
      icon: MaintenanceManagementIcon,
      title: `${t("FacilityPage.MaintenanceManagement")}`
    },
    {
      icon: PropertyManagementIcon,
      title: `${t("FacilityPage.PropertyManagement")}`
    },
    {
      icon: ContractManagementIcon,
      title: `${t("FacilityPage.ContractManagement")}`
    },
    {
      icon: RentalManagementIcon,
      title: `${t("FacilityPage.RentalManagement")}`
    }
  ];

  useEffect(() => {
    if (sessionStorage.getItem("currency") === "KWD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "KWD",
        // Mobile
        newMobileBDSPM: Math.round(12),
        newMobileADSPM: Math.round(9),
        newMobileBDSPY: Math.round(12),
        newMobileADSPY: Math.round(6),
        MSC: Math.round(306.16)
      });
    } else if (sessionStorage.getItem("currency") === "AED") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "AED",
        // Mobile
        newMobileBDSPM: Math.round(146),
        newMobileADSPM: Math.round(110),
        newMobileBDSPY: Math.round(146),
        newMobileADSPY: Math.round(73),
        // setup charge
        MSC: Math.round(3669.33)
      });
    } else if (sessionStorage.getItem("currency") === "SGD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "SGD",
        // Mobile
        newMobileBDSPM: Math.round(54),
        newMobileADSPM: Math.round(41),
        newMobileBDSPY: Math.round(54),
        newMobileADSPY: Math.round(27),
        MSC: Math.round(1400.94)
      });
    } else if (sessionStorage.getItem("currency") === "OMR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "OMR",
        // Mobile
        newMobileBDSPM: Math.round(16),
        newMobileADSPM: Math.round(12),
        newMobileBDSPY: Math.round(16),
        newMobileADSPY: Math.round(6),
        MSC: Math.round(384.63)
      });
    } else if (sessionStorage.getItem("currency") === "QAR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "QAR",
        // Mobile
        newMobileBDSPM: Math.round(145),
        newMobileADSPM: Math.round(109),
        newMobileBDSPY: Math.round(145),
        newMobileADSPY: Math.round(72),
        MSC: Math.round(3637.36)
      });
    } else if (sessionStorage.getItem("currency") === "BHD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "BD",

        // Mobile
        newMobileBDSPM: Math.round(15),
        newMobileADSPM: Math.round(11),
        newMobileBDSPY: Math.round(15),
        newMobileADSPY: Math.round(8),
        MSC: Math.round(376.62)
      });
    } else if (sessionStorage.getItem("currency") === "MYR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "MYR",
        // Mobile
        newMobileBDSPM: Math.round(165),
        newMobileADSPM: Math.round(123),
        newMobileBDSPY: Math.round(165),
        newMobileADSPY: Math.round(82),
        MSC: Math.round(4223.17)
      });
    } else if (sessionStorage.getItem("currency") === "LKR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "LKR",

        // Mobile
        newMobileBDSPM: Math.round(7224),
        newMobileADSPM: Math.round(5413),
        newMobileBDSPY: Math.round(7224),
        newMobileADSPY: Math.round(3603),
        MSC: Math.round(90623.4)
      });
    } else if (sessionStorage.getItem("currency") === "EGP") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "EGP",

        // Mobile
        newMobileBDSPM: Math.round(643),
        newMobileADSPM: Math.round(482),
        newMobileBDSPY: Math.round(643),
        newMobileADSPY: Math.round(321),
        MSC: Math.round(7799.05)
      });
    } else if (sessionStorage.getItem("currency") === "SAR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "SAR",
        // Mobile
        newMobileBDSPM: Math.round(150),
        newMobileADSPM: Math.round(112),
        newMobileBDSPY: Math.round(150),
        newMobileADSPY: Math.round(75),
        MSC: Math.round(3747.66)
      });
    } else if (sessionStorage.getItem("currency") === "JOD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "JOD",

        // Mobile
        newMobileBDSPM: Math.round(28),
        newMobileADSPM: Math.round(21),
        newMobileBDSPY: Math.round(28),
        newMobileADSPY: Math.round(14.11),
        MSC: Math.round(708.29)
      });
    } else if (sessionStorage.getItem("currency") === "KYD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "KYD",
        // Mobile
        newMobileBDSPM: Math.round(33.25),
        newMobileADSPM: Math.round(24.91),
        newMobileBDSPY: Math.round(33.25),
        newMobileADSPY: Math.round(16.58),
        MSC: Math.round(830.76)
      });
    } else if (sessionStorage.getItem("currency") === "ZAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "ZAR",
        // Mobile
        newMobileBDSPM: Math.round(604.16),
        newMobileADSPM: Math.round(452.74),
        newMobileBDSPY: Math.round(604.16),
        newMobileADSPY: Math.round(301.33),
        MSC: Math.round(7570.99)
      });
    } else if (sessionStorage.getItem("currency") === "USD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "USD",
        // Mobile
        newMobileBDSPM: 39.9,
        newMobileADSPM: 29.9,
        newMobileBDSPY: 39.9,
        newMobileADSPY: 19.9,
        MSC: Math.round(500)
      });
    } else {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: sessionStorage.getItem("currency_sign"),

        // Mobile
        newMobileBDSPM: Math.round(
          39.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPM: Math.round(
          29.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileBDSPY: Math.round(
          39.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPY: Math.round(
          19.9 * sessionStorage.getItem("currency_rate")
        ),
        MSC: Math.round(500 * sessionStorage.getItem("currency_rate"))
      });
    }
  }, [Applications, country, currency]);

  return Applications === null || loading ? (
    <Fragment>
      <Spin></Spin>
    </Fragment>
  ) : (
      <div>
        <ScrollTop />
        <Helmet>
          <title>UPapp factory - Buy Ready Made Apps</title>
          <meta
            name="Readymade eCommerce Mobile Apps for Android and iOS Platforms | UPapp factory"
            content="Build native mobile ecommerce app (android & iOS) for your eCommerce store. Readymade ecommerce app on all platforms. we help you by developing a feature-rich mobile app solution depends on your business requirements."
          />
        </Helmet>
        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "100px auto 180px auto"
          }}
        >
          <Row type="flex">
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo" style={{}}>
                    {t("FacilityPage.taglineTitle")}
                  </Title>
                  <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">{t("General.GetStarted")}</Button>
                  </Link>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("FacilityPage.taglineTitle")}
                  </Title>
                  <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">{t("General.GetStarted")}</Button>
                  </Link>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px", marginTop: "-50px" }}
                  src={FacilityManagementApp}
                  alt="Facility Management Application"
                />
              </div>
            </Col>
          </Row>
        </section>
        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0 auto 80px auto",
            textAlign: "center"
          }}
        >
          <p style={{ fontSize: "45px", marginBottom: "30px" }}>
            {t("FacilityPage.keyFeatures")}
          </p>
          <Row type="flex" justify="space-around" align="middle">
            {keyFeatures.map(index => {
              return (
                <Col xs={12} sm={6} lg={4}>
                  <div
                    style={{
                      width: "100%",
                      maxWidth: "180px",
                      height: "130px",
                      margin: "30px auto"
                    }}
                  >
                    <img
                      style={{ width: "100px", height: "100px" }}
                      src={index.icon}
                      alt=""
                    />
                    <p style={{ marginTop: "15px" }}> {index.title}</p>
                  </div>
                </Col>
              );
            })}
          </Row>
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            margin: "80px auto 0px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <Row type="flex" gutter={30}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                {direction === DIRECTIONS.LTR && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points lg-rtl"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl">
                      {t("ecommercePage.layer4.title1")}
                    </Title>
                    <Title
                      level={1}
                      className="text-demo-sub-text faclity-sub-text lg-rtl lg-float-right"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      {t("ecommercePage.layer4.desc")}
                    </Title>

                    <Link to="/factory">
                      <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">{t("General.GetStarted")}</Button>
                    </Link>
                  </div>
                </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points lg-rtl lg-p-rtl-15 "
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl">
                      {t("ecommercePage.layer4.title1")}
                    </Title>
                    <Title
                      level={1}
                      className="text-demo-sub-text faclity-sub-text lg-rtl "
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      {t("ecommercePage.layer4.desc")}
                    </Title>

                    <Link to="/factory">
                      <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">{t("General.GetStarted")}</Button>
                    </Link>
                  </div>
                </Fragment>
                }
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={AndroidIosAppIncluded}
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            margin: "0px auto 60px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <p
              style={{
                fontSize: "45px",
                marginTop: "100px",
                marginBottom: "80px"
              }}
            >
              {t("FacilityPage.Manageyourfacilitieseffectively")}
            </p>
            <Row type="flex" justify="center" align="middle">
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                {direction === DIRECTIONS.LTR && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-ltr" style={{}}>
                      {t("FacilityPage.MaintenanceManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text lg-ltr"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul className="m-dir-ltr lg-ltr facility-lists">
                        <li>
                          <p>
                            {" "}
                            {t("FacilityPage.Layer1.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer1.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {" "}
                            {t("FacilityPage.Layer1.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer1.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer1.5")}
                          </p>
                        </li>
                        <li>
                          {" "}
                          <p>
                            {t("FacilityPage.Layer1.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {" "}
                            {t("FacilityPage.Layer1.7")}
                          </p>
                        </li>
                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-left"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points "
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl" style={{}}>
                      {t("FacilityPage.MaintenanceManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text lg-rtl"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul className="m-dir-rtl lg-rtl facility-lists">
                        <li>
                          <p>
                            {" "}
                            {t("FacilityPage.Layer1.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer1.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {" "}
                            {t("FacilityPage.Layer1.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer1.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer1.5")}
                          </p>
                        </li>
                        <li>
                          {" "}
                          <p>
                            {t("FacilityPage.Layer1.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {" "}
                            {t("FacilityPage.Layer1.7")}
                          </p>
                        </li>
                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn main lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }

              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={MaintenanceManagement}
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            margin: "0px auto 60px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <Row type="flex" justify="center" align="middle" gutter={30}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                {direction === DIRECTIONS.LTR && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points lg-rtl"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl" style={{}}>
                      {t("FacilityPage.ContractManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text lg-rtl lg-float-right"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul
                        dir="rtl"
                        className="m-dir-ltr lg-rtl facility-lists facility-lists-right"
                      >

                        <li>
                          <p>
                            {t("FacilityPage.Layer2.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.5")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.7")}
                          </p>
                        </li>

                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points lg-rtl "
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl" style={{}}>
                      {t("FacilityPage.ContractManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text lg-right lg-float-right"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul
                        dir="ltr"
                        className="m-dir-rtl lg-rtl facility-lists facility-lists-right"
                      >
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.5")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer2.7")}
                          </p>
                        </li>

                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-float"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={ContractManagement}
                    alt="Contract Management App"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            margin: "0px auto 60px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <Row type="flex" justify="center" align="middle">
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                {direction === DIRECTIONS.LTR && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-ltr" style={{}}>
                      {t("FacilityPage.HelpDeskManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text lg-ltr"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul className="m-dir-ltr lg-ltr facility-lists">
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.5")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.7")}
                          </p>
                        </li>

                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-left"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl" style={{}}>
                      {t("FacilityPage.HelpDeskManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text lg-rtl"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul className="m-dir-rtl lg-rtl facility-lists">

                        <li>
                          <p>
                            {t("FacilityPage.Layer3.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.5")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer3.7")}
                          </p>
                        </li>

                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={HelpDeskManagement}
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>
        <section
          className="UAF-container-fluid"
          style={{
            margin: "0px auto 60px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <Row type="flex" justify="center" align="middle" gutter={30}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                {direction === DIRECTIONS.LTR && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points lg-rtl"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl" style={{}}>
                      {t("FacilityPage.PropertyManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text  lg-rtl lg-float-right"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul
                        dir="rtl"
                        className="lg-rtl lg-float-right m-dir-ltr  facility-lists"
                      >

                        <li>
                          <p>
                            {t("FacilityPage.Layer3.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.5")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.7")}
                          </p>
                        </li>

                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                  <div
                    className="tagline-heading tagline-body sub-points lg-rtl"
                    style={{ padding: "auto 15px" }}
                  >
                    <Title level={1} className="texty-demo lg-rtl" style={{}}>
                      {t("FacilityPage.PropertyManagement")}
                      <br />
                    </Title>
                    <p
                      className="text-demo-sub-text faclity-sub-text  lg-rtl lg-float-right"
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: 400,
                        maxWidth: "100%"
                      }}
                    >
                      <ul
                        dir="rtl"
                        className="lg-rtl lg-float-right m-dir-rtl  facility-lists"
                      >

                        <li>
                          <p>
                            {t("FacilityPage.Layer4.1")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.2")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.3")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.4")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.5")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.6")}
                          </p>
                        </li>
                        <li>
                          <p>
                            {t("FacilityPage.Layer4.7")}
                          </p>
                        </li>

                      </ul>
                    </p>

                    <Link to="/factory/booking">
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    </Link>
                  </div>
                </Fragment>
                }
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={PropertyManagement}
                    alt="Contract Management App"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <div className="single-app-heading-text">
          <br />
          {t("FacilityPage.HowCustomer")}
        </div>
        <SSsliderFacility screenshots={Facility_Screenshot} />
        <br />
        <br />
        <div className="single-app-heading-text">
          <br />
          {t("FacilityPage.HowMaintainer")}
        </div>
        <SSsliderFacility screenshots={Maintainer_Screenshot} />

        <section className="" style={{ marginTop: "80px", marginBottom: "80px" }}>
          <PackagePriceVerticleFacility
            currency={currency}
            MBDSPM={newMobileBDSPM}
            MADSPM={newMobileADSPM}
            MBDSPY={newMobileBDSPY}
            MADSPY={newMobileADSPY}
            duration="Mo"
            PackType="Annually"
            MSC={MSC}
          />
        </section>
      </div>
    );
};

const HomeRFC = Form.create({ name: "Request_for_customize" })(
  FacilityManagement
);

HomeRFC.propTypes = {
  loadApplication: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});

export default connect(mapStateToProps, {
  loadApplication,
  setAlert
})(withDirection(HomeRFC));
