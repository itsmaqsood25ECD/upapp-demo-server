import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import PackagePriceVerticleEcommerce from "../Pages/PackagePriceVerticleEcommerce";
import BookingAppFeatureList from "./single page/BookingAppFeatureList";
import EcommerceAppFeatureList from "./single page/EcommerceAppFeatureList";
import Walkthrugh from "../Pages/Walkthrugh";
import { Affix, Typography, Row, Col, Select, Button, } from "antd";
import { Link } from "react-router-dom";
import ScrollTop from "../ScrollTop";
import Hero from "../hero";
import { loadApplication } from "../../actions/application.action";
import { Helmet } from "react-helmet";

import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import DirectionProvider from 'react-with-direction/dist/DirectionProvider';

import { useTranslation } from 'react-i18next';
import PackagePriceVerticleBooking from "./PackagePriceVerticleBooking"
import i18next from 'i18next';
import PackagePriceVerticleRestaurant from "./PackagePriceVerticleRestaurant"
import MultivendorPrice from './PackagePriceVerticleMarketPlace';
import BotHomePricing from './BotHomePricing'

const { Option, OptGroup } = Select;
const { Title, Text } = Typography;
let filteredApps;

const Subscription = ({ direction, user }) => {
  const [state, setState] = useState({
    selectApplication: "Ecommerce"
  });

  const { t } = useTranslation();

  const [appDetails, setAppData] = useState({
    apps: [],
    currency: "",
    // Booking
    bookingMobileBDSPM: "",
    bookingMobileADSPM: "",
    bookingMobileBDSPY: "",
    bookingMobileADSPY: "",
    bookingWebsiteBDSPM: "",
    bookingWebsiteADSPM: "",
    bookingWebsiteBDSPY: "",
    bookingWebsiteADSPY: "",
    bookingWebAppBDSPM: "",
    bookingWebAppADSPM: "",
    bookingWebAppBDSPY: "",
    bookingWebAppADSPY: "",
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    newWebsiteBDSPM: "",
    newWebsiteADSPM: "",
    newWebsiteBDSPY: "",
    newWebsiteADSPY: "",
    newWebAppBDSPM: "",
    newWebAppADSPM: "",
    newWebAppBDSPY: "",
    newWebAppADSPY: "",
    country: "",
    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: "",
    // setup charge
    WSC: "",
    MSC: "",
    WMSC: "",

    // ----- restaurant ------- //

    Res_newADSPM: "",
    Res_newBDSPY: "",
    Res_newADSPY: "",
    Res_newMobileBDSPM: "",
    Res_newMobileADSPM: "",
    Res_newMobileBDSPY: "",
    Res_newMobileADSPY: "",
    Res_MSC: "",
    // Plan B
    Res_MFPP: "",

   //----- Multivendor ----//
   Mul_MFPP:""

  });
  const {
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    newWebsiteBDSPM,
    newWebsiteADSPM,
    newWebsiteBDSPY,
    newWebsiteADSPY,
    newWebAppBDSPM,
    newWebAppADSPM,
    newWebAppBDSPY,
    newWebAppADSPY,
    WFPP,
    WAFPP,
    MFPP,
    // setup charge
    WSC,
    MSC,
    WMSC,
    //Restaurnat 
    Res_newMobileBDSPM,
    Res_newMobileADSPM,
    Res_newMobileBDSPY,
    Res_newMobileADSPY,
    Res_MSC,
    Res_MFPP,
    // multi vendor
    Mul_MFPP,

  } = appDetails;

  const { selectApplication } = state;

  useEffect(() => {

    setAppData({
      ...appDetails,
      country: sessionStorage.getItem("country"),
      currency: sessionStorage.getItem("currency_sign"),
      // Website
      newWebsiteBDSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPM: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteBDSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      // Web App
      newWebAppBDSPM: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppBDSPY: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      // Mobile
      newMobileBDSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileBDSPY: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPY: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      //  Plan B

      WFPP: Math.round(59.9 * sessionStorage.getItem("currency_rate")),
      MFPP: Math.round(89.9 * sessionStorage.getItem("currency_rate")),
      WAFPP: Math.round(119.9 * sessionStorage.getItem("currency_rate")),
      // setup charge
      WSC: Math.round(500 * sessionStorage.getItem("currency_rate")),
      WMSC: Math.round(1200 * sessionStorage.getItem("currency_rate")),
      MSC: Math.round(800 * sessionStorage.getItem("currency_rate")),

      // Restaurant App

      // Mobile
      Res_newMobileBDSPM: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileADSPM: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileBDSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_MSC: Math.round(49.9 * sessionStorage.getItem("currency_rate")),
      //  Plan B
      Res_MFPP: Math.round(49.9 * sessionStorage.getItem("currency_rate")),
      // Multivendor 
      Mul_MFPP: Math.round(4900 * sessionStorage.getItem("currency_rate")),
    });

  }, [sessionStorage.getItem("currency")]);

  const handleSelect = value => {
    setState({
      ...state,
      selectApplication: value
    });
  };

  return (

    <Fragment>
      {/* { console.log("Selected Application Type: return :-->", selectApplication) } */}
      <ScrollTop />
      <Helmet>
        <title>Pricing - Mobile Apps Ready Pricing</title>
        <meta
          name="Find the simple and affordable pricing for your mobile apps on monthly and yearly basis."
          content="mobile apps pricing, low price mobile apps development, cheap mobile apps development"
        />
      </Helmet>
      <Hero Title={`${t("header.Pricing")}`} />
      <div style={{ background: '#F4F7F9', minHeight: '50px', padding: "20px 0px", textAlign: "center" }}>
        <Row type="flex" justify="center">
          <Col xs={24} lg={8}>
            <Text>Choose App</Text>
            <Select
              defaultValue={`Ecommerce`}
              style={{ width: 300, margin: '5px 20px 5px 20px' }}
              onChange={handleSelect}
            >
              <Option key="ecomm" value="Ecommerce">
               eCommerce Single Vendor
								</Option>
                <Option key="mul-ecomm" value="Mul-Ecommerce">
                eCommerce Multi Vendor
								</Option>

              <Option key='restaurant' value="restaurant">
                Restaurant Application
                </Option>
                <Option key='bot' value="upapp-Bot">
                UPapp Bot
                </Option>
            </Select>
          </Col>
        </Row>
      </div>

      <div style={{ margin: "40px auto 20px auto" }}>
        {selectApplication == "Ecommerce" ? (
          <DirectionProvider direction={direction}>
<PackagePriceVerticleEcommerce
            currency={currency}
            MBDSPM={newMobileBDSPM}
            MADSPM={newMobileADSPM}
            MBDSPY={newMobileBDSPY}
            MADSPY={newMobileADSPY}
            WBDSPM={newWebsiteBDSPM}
            WADSPM={newWebsiteADSPM}
            WBDSPY={newWebsiteBDSPY}
            WADSPY={newWebsiteADSPY}
            WABDSPM={newWebAppBDSPM}
            WAADSPM={newWebAppADSPM}
            WABDSPY={newWebAppBDSPY}
            WAADSPY={newWebAppADSPY}
            WFPP={WFPP}
            WAFPP={WAFPP}
            MFPP={MFPP}
            USER={user}
            duration="Mo"
            SLUG={filteredApps && filteredApps.fields && filteredApps.fields.slug}
            ID={filteredApps && filteredApps.fields && filteredApps.UID}
            PackType="Annually"
            //setup charge
            WSC={WSC}
            MSC={MSC}
            WMSC={WMSC}
          />
          </DirectionProvider>) : (null)}

        {selectApplication == "Booking" ? (
          <PackagePriceVerticleBooking
            // currency="OMR"
            currency={currency}
            MBDSPM={newMobileBDSPM}
            MADSPM={newMobileADSPM}
            MBDSPY={newMobileBDSPY}
            MADSPY={newMobileADSPY}
            WBDSPM={newWebsiteBDSPM}
            WADSPM={newWebsiteADSPM}
            WBDSPY={newWebsiteBDSPY}
            WADSPY={newWebsiteADSPY}
            WABDSPM={newWebAppBDSPM}
            WAADSPM={newWebAppADSPM}
            WABDSPY={newWebAppBDSPY}
            WAADSPY={newWebAppADSPY}
            WFPP={WFPP}
            WAFPP={WAFPP}
            MFPP={MFPP}
            WSC={WSC}
            MSC={MSC}
            WMSC={WMSC}
            duration="Mo"
            PackType="Annually"
          />
        ) : (null)}

        {selectApplication == "restaurant" ? (
          <PackagePriceVerticleRestaurant
            currency={currency}
            MBDSPM={Res_newMobileBDSPM}
            MADSPM={Res_newMobileADSPM}
            MBDSPY={Res_newMobileBDSPY}
            MADSPY={Res_newMobileADSPY}
            duration="Mo"
            PackType="Annually"
            MSC={Res_MSC}
            MFPP={Res_MFPP}
          />
        ) : (null)}

{selectApplication == "Mul-Ecommerce" ? (
          <MultivendorPrice
          currency={currency}
          duration="Mo"
          PackType="Annually"
          MFPP={Mul_MFPP}
          />
        ) : (null)}


{selectApplication === "upapp-Bot" ? (
          <BotHomePricing />
        ) : (null)}

      </div>
      <Row
        justify="center"
        gutter={16}
        style={{
          paddingBottom: "50px",
          paddingTop: "10px"
        }}
      >
        {selectApplication == "Ecommerce" ? (
          <EcommerceAppFeatureList />
        ) : (
            null
          )}
        {selectApplication == "Booking" ? (
          <BookingAppFeatureList />
        ) : (
            null
          )}
        {selectApplication == "restaurant" ? (
          null
        ) : (
            null
          )}
      </Row>
      <Row
        gutter={16}
        type="flex"
        justify="center"
        style={{
          paddingBottom: "50px",
          marginLeft: "0px",
          marginRight: "0px",
          textAlign: "center"
        }}
      >
      </Row>
    </Fragment>
  );
};

Subscription.propTypes = {
  user: PropTypes.object.isRequired,
  loadApplication: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  user: state.auth.user,
  Applications: state.application.Applications,
  loading: state.application.loading
});
export default connect(mapStateToProps, {
  loadApplication
  // getLocation
})(withDirection(Subscription));
