import React, { Component, Fragment } from 'react'
import { Menu } from 'antd';


export class SideMenu extends Component {

    componentWillMount() {

    }

    render() {
        console.log(this.props.Category);
        const CAT = this.props.Category;

        const INDUSTRIES = CAT.filter(cat => {
            return (
                cat.type === "Industry"
            );
        });
        const CATEGORY = CAT.filter(cat => {
            return (
                cat.type === "Category"
            );
        });

        const TYPE = CAT.filter(cat => {
            return (
                cat.type === "Type"
            );
        });

        // console.log(INDUSTRIES);
        // console.log(CATEGORY);
        // console.log(TYPE);


        return (
            <Fragment>
                <Menu
                    style={{ width: 256 }}
                    defaultSelectedKeys={['All']}
                    mode="inline"
                    onSelect={selectedKeys => console.log(selectedKeys)}
                // onClick={handleSelect}
                >  <Menu.Item key="All">All</Menu.Item>
                    <Menu.ItemGroup key="TYPE" title="APP TYPE">
                    {TYPE.map((data, i) => {
                            return (
                            <Menu.Item key={data._id}>{data.name}</Menu.Item>
                            );
                        })}
                    </Menu.ItemGroup>
                    <Menu.ItemGroup key="industries" title="INDUSTRIES">
                    {INDUSTRIES.map((data, i) => {
                            return (
                                 <Menu.Item key={data._id}>{data.name}</Menu.Item>                            );
                        })}
                    </Menu.ItemGroup>
                    <Menu.ItemGroup key="Category" title="CATEGORIES">
                    {CATEGORY.map((data, i) => {
                            return (
                                
                                        <Menu.Item key={data._id}>{data.name}</Menu.Item>
                                       
                            );
                        })}
                    </Menu.ItemGroup>
                </Menu>
            </Fragment>
        )
    }
}



export default SideMenu;
