/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor, List, Divider, } from "antd";
import CurrencyFormat from "react-currency-format";
import "../../assets/css/packagePrice.css";
import { withTranslation } from 'react-i18next';

const { Title } = Typography;
const { Link } = Anchor;


export class PackagePriceVerticleFacility extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //plan B
            MFPP: this.props.MFPP,
        };
    }


    componentWillReceiveProps(nextApp) {
        this.setState({
            // Plan B
            MFPP: nextApp.MFPP,
        });
    }


    render() {
        const { t } = this.props;
        const currency = this.props.currency;
        const MSC = this.props.MSC;

        const featuresList = [
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.1")}`,
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.2")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.3")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.4")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.5")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.6")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.7")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.8")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.9")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.10")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.11")}`
            },
            {
                title: `${t("MarketPlacePage.keyfunction.Icons.12")}`
            }
        ]

        return (

            <div className="package-bg">
                <div style={{ textAlign: "center" }}>
                    <Title level={3}>{t("restaurentApp.FeaturesIncluded")}</Title>
                </div>
                <Row
                    gutter={16}
                    type="flex"
                    justify="center"
                    align="middle"
                    style={{ margin: "0px 0px", border: "0px solid #eee" }}
                >
                    <Col lg={24} xs={24} className="">
                        <div
                            className="V-Price-Switcher"
                            style={{
                                textAlign: "center",
                                marginTop: "15px",
                                marginBottom: "15px"
                            }}
                        >
                        </div>
                    </Col>
                    <Col xs={24}>
                        <Row gutter={16}>
                            <div className="uaf_pricingVertical_outer">
                                <div className="priceing-card-horizontal" style={{ paddingTop: "60px" }}>
                                    <List
                                        grid={{
                                            gutter: 16,
                                            xs: 1,
                                            sm: 2,
                                            md: 4,
                                            lg: 4,
                                            xl: 4,
                                            xxl: 3,
                                        }}
                                        dataSource={featuresList}
                                        renderItem={item => (
                                            <List.Item style={{ fontSize: "14px" }}>
                                                <Icon style={{ color: "green", fontSize: "20px" }} type="check" />  {item.title}
                                            </List.Item>
                                        )}
                                    />
                                    <Divider style={{ fontSize: "25px", marginTop: "35px", marginBottom: "20px" }} >{t('pricing.ourPricing')}</Divider>
                                    <p
                                        className="text-demo-sub-text lg-ltr"
                                        style={{
                                            fontSize: "17px",
                                            lineHeight: "20px",
                                            fontWeight: 500,
                                            maxWidth: "100%",
                                            textAlign: 'center',
                                            marginBottom: '25px'
                                        }}
                                    >
                                        {/* *{t('MarketPlacePage.startsat')} $4,900 */}
                                        {/* <span className="PriceNum2">
                                            <span className="currency">{currency}</span>
                                            <CurrencyFormat
                                                value={Number(this.state.MFPP)}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                            />
                                            <sup className="sup-star"></sup>
                                            <span></span>
                                            <span className="per-month-tag"></span>
                                        </span> */}
                                        <div>
                                            <p>For Special Pricing with free hosting & technical Support
Contact Sales</p>
                                        </div>
                                    </p>
                                    <div style={{ textAlign: "center", width: "100%", marginBottom: "30px", marginTop: "20px" }}>
                                        <Anchor style={{ margin: "0 auto" }}>
                                            <Link
                                                href="/contactUs#GetInTouchForm"
                                                title={
                                                    <Button className="UAFprimaryButton1" type="primary">{t('General.ContactSales')}</Button>
                                                }
                                            ></Link>
                                        </Anchor>
                                    </div>

                                </div>
                            </div>
                        </Row>
                    </Col>
                </Row>
            </div>

        );
    }
}

export default withTranslation()(PackagePriceVerticleFacility);
