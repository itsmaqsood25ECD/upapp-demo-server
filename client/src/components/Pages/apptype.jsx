import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import ScrollTop from "../ScrollTop";
import { Helmet } from "react-helmet";
import { loadApplication } from "../../actions/application.action";
import {
  List,
  Typography,
  Card,
  Layout,
  Input,
  Row,
  Col,
  Menu,
  Spin
} from "antd";
import Title from "antd/lib/typography/Title";

const { Text } = Typography;
const { Meta } = Card;

const AppType = ({ match, Applications }) => {
  console.log(match.params.industry, Applications);
  const [applicationData, setApplicationData] = useState({
    apps: []
  });
  const { apps } = applicationData;

  if (Applications === null) {
    console.log("if");
    loadApplication();
  }
  useEffect(() => {
    if (Applications === null) {
      console.log("if");
      loadApplication();
    }
    console.log(Applications);
    setApplicationData({
      ...applicationData,
      apps:
        Applications &&
        Applications.filter(arg => {
          return (
            arg.fields.type.toLowerCase() === match.params.type.toLowerCase()
          );
        })
    });
  }, [match.params.type, Applications]);
  if (Applications === null) {
    console.log("if");
    loadApplication();
  }
  return (
    <Fragment>
      <ScrollTop />
      <Helmet>
        <meta charSet="utf-8" />
        <title>{match.params.type} Website and App - UPappfactory</title>
        <h1>{match.params.type} Website and App </h1>
        {match.params.type === "ecommerce" ? (
          <meta
            name="Get your e-commerce mobile application for your business within 7 - 12 working days. Pre-made mobile application for e-commerce business. Launch your online shop immediately and start selling online."
            content="Get your e-commerce mobile application for your business within 7 - 12 working days. Pre-made mobile application for e-commerce business. Launch your online shop immediately and start selling online."
          />
        ) : (
          <meta
            name="Looking for mobile application development company for your online appointment booking app. How about a readymade app for your online applointment booking."
            content="Looking for mobile application development company for your online appointment booking app. How about a readymade app for your online applointment booking."
          />
        )}
      </Helmet>
      <div
        style={{
          fontSize: "30px",
          textAlign: "center",
          marginTop: "150px"
        }}
      >
        <Title level={1} style={{ fontSize: "30px", fontWeight: "500" }}>
          {match.params.type} website and app
        </Title>
      </div>

      <div style={{ maxWidth: "1280px", margin: "0 auto" }}>
        <Row style={{ marginTop: "30px" }}>
          <Col xs={24} sm={24} lg={24}>
            <Row className="store-table">
              <List
                loading={!apps}
                grid={{
                  gutter: 10,
                  xs: 1,
                  sm: 2,
                  md: 2,
                  lg: 3,
                  xl: 3,
                  xxl: 3
                }}
                // pagination={{
                // 	onChange: page => {},
                // 	pageSize: 20,
                // 	position: 'both',
                // 	size: 'small'
                // }}
                dataSource={apps && apps}
                footer={<div />}
                renderItem={item => (
                  <List.Item>
                    <Card
                      hoverable
                      className="store-card"
                      cover={
                        <Link
                          to={`/readymade-apps/${item.fields.slug}/${item.UID}`}
                        >
                          <img
                            alt="example"
                            src={item.fields.images[0].thumb.url}
                          />
                        </Link>
                      }
                    >
                      <Meta
                        title={item.fields.name}
                        description={item.fields.desc}
                      />
                    </Card>
                  </List.Item>
                )}
              />
            </Row>
            <Row>
              <Col
                xs={24}
                style={{ marginBottom: "30px", marginTop: "30px" }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

AppType.propTypes = {
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};
const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});
export default connect(mapStateToProps, null)(AppType);
