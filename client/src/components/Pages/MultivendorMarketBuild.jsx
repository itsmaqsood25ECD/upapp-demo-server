import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';

// industry we searve Icons

import bakery from "../../assets/img/marketplace page/bakery.png" ;
import beauty from "../../assets/img/marketplace page/beauty.png" ;
import electronics from "../../assets/img/marketplace page/electronics.png" ;
import fashion from "../../assets/img/marketplace page/fashion.png" ;
import flowers from "../../assets/img/marketplace page/flowers.png" ;
import furnitures from "../../assets/img/marketplace page/furnitures.png" ;
import grocery from "../../assets/img/marketplace page/grocery.png" ;
import jewellery from "../../assets/img/marketplace page/jewellery.png" ;
import kitchen from "../../assets/img/marketplace page/kitchen.png" ;
import sports from "../../assets/img/marketplace page/sports.png" ;
import stationery from "../../assets/img/marketplace page/stationery.png" ;
import toys from "../../assets/img/marketplace page/toys.png" ;



function MultivendorMarketBuild(props) {

  const { t } = useTranslation();

  const multivendorbuild = [
    {
      icon: grocery,
      name: `${t('MarketPlacePage.build.Icons.Grocery')}`,
    //   link: "/factory/healthcare"
    },
    ,
    {
      icon: fashion,
      name: `${t('MarketPlacePage.build.Icons.Fashion')}`,
    //   link: "/factory/service"
    },
    {
      icon: beauty,
      name: `${t('MarketPlacePage.build.Icons.Beauty')}`,
    //   link: "/factory"
    },
    {
      icon: jewellery,
      name: `${t('MarketPlacePage.build.Icons.Jewellery')}`,
    //   link: "/factory/beauty"
    },
    {
      icon: furnitures,
      name: `${t('MarketPlacePage.build.Icons.Furniture')}`,
    //   link: "/factory/retail"
    },
    {
      icon: kitchen,
      name: `${t('MarketPlacePage.build.Icons.Kitchen')}`,
    //   link: "/factory"
    },
    {
      icon: electronics,
      name: `${t('MarketPlacePage.build.Icons.Electronics')}`,
    //   link: "/factory/retail"
    },
    {
      icon: stationery,
      name: `${t('MarketPlacePage.build.Icons.Stationaries')}`,
    //   link: "/factory/retail"
    },
    {
      icon: sports,
      name: `${t('MarketPlacePage.build.Icons.Sporting')}`,
    //   link: "/facility-management-application"
    },
    {
      icon: bakery,
      name: `${t('MarketPlacePage.build.Icons.Bakery')}`,
    //   link: "/factory/sports"
    },
    {
      icon: flowers,
      name: `${t('MarketPlacePage.build.Icons.Flowers')}`,
    //   link: "/factory/sports"
    },
    {
      icon: toys,
      name: `${t('MarketPlacePage.build.Icons.Toys')}`,
    //   link: "/factory/sports"
    }
  ];

  return (
    <div style={{ textAlign: "center" }}>
      <Row gutter={16} type="flex" justify="center">
        {multivendorbuild.map(icon => {
          return (
            <Col xs={12} md={6} lg={4}>
              <Link to={icon.link}>
                <div className="multivendorBuild-card" style={{ marginBottom: "15px" }}>
                  <img
                    style={{ width: "80px", height: "80px", padding:"8px" }}
                    src={icon.icon}
                    alt={icon.name}
                  />
                  <p style={{ marginTop: "15px" }}>{icon.name}</p>
                </div>
              </Link>
            </Col>
          );
        })}
      </Row>
    </div>
  );
}

MultivendorMarketBuild.propTypes = {};

export default MultivendorMarketBuild;
