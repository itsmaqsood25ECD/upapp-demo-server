import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import ScrollTop from "../ScrollTop";
import { Form, Icon, Input, Button, Typography, Row, Col, message } from "antd";
import { forgotPassword } from "../../actions/auth.action";
import PropTypes from "prop-types";
import { useTranslation } from 'react-i18next';
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import i18next from 'i18next';

const { Title } = Typography;

const ForgotPassword = ({ forgotPassword, isAuthenticated, direction }) => {
	const [formData, setFormData] = useState({
		email: "",
		disableSubmit: false
	});
	const { email, disableSubmit } = formData;
	const { t } = useTranslation();
	const onChange = e =>
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});

	const onSubmit = e => {
		e.preventDefault();
		setFormData({
			...formData,
			disableSubmit: true
		});
		message.loading("please wait..", 3.5);
		forgotPassword(email);
	};

	// Redirect to msg page
	if (isAuthenticated) {
		return <Redirect to="/forgot-password/token" />;
	}
	return (
		<Fragment>
			<ScrollTop />
			<div
				className="my-login-form"
				style={{
					background: "#FAFFFF",
					marginTop: "100px",
					marginBottom: "100px"
				}}
			>
				<Row>
					<Col xs={24}>
						<div style={{ textAlign: "center", margin: "10px auto 50px auto" }}>
							<Title>
								<Icon type="lock" /> {t('auth.forgotPassword')}
							</Title>
						</div>
					</Col>
					<Col xs={24}>
						<Form onSubmit={e => onSubmit(e)} className="login-form">
							<Form.Item>
								<Input
									placeholder={t('auth.email')}
									name="email"
									value={email}
									onChange={e => onChange(e)}
									prefix={
										<Icon
											type="mail"
											style={{
												color: "rgba(0,0,0,.25)"
											}}
										/>
									}
								/>
							</Form.Item>
							<Form.Item>
								<Button
									type="primary"
									htmlType="submit"
									className="login-form-button"
									disabled={disableSubmit}
								>
									{t('auth.submit')}
								</Button>
							</Form.Item>
						</Form>
					</Col>
				</Row>
			</div>
		</Fragment>
	);
};

ForgotPassword.propTypes = {
	forgotPassword: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool,
	direction: withDirectionPropTypes.direction
};

const mapStateToProps = state => ({
	isAuthenticated: state.auth.isAuthenticated
});
export default connect(
	mapStateToProps,
	{
		forgotPassword
	}
)(withDirection(ForgotPassword));
