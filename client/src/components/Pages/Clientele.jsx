import React from "react";
import { Typography, Row, Col, Icon } from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";
import '../../assets/css/clientelePagePCImage.css'
import { Scrollbars } from 'react-custom-scrollbars';

const { Title, Text } = Typography;

const Screens = {
  BurgerGarage: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/BurgerGarage/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/BurgerGarage/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/BurgerGarage/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/BurgerGarage/4.png"
  ],
  // DarNijood: [
  //   "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/DarNijood/1.png",
  //   "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/DarNijood/2.png",
  //   "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/DarNijood/3.png",
  //   "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/DarNijood/4.png"
  // ],
  Fursahh: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Fursah/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Fursah/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Fursah/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Fursah/4.png"
  ],
  ScanZone: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/ScanZone/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/ScanZone/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/ScanZone/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/ScanZone/4.png"
  ],
  SiteInspection: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/SiteInspection/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/SiteInspection/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/SiteInspection/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/SiteInspection/4.png"
  ],
  SalamProperties: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Salam+properties/loginScreen.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Salam+properties/Facilities.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Salam+properties/Menupage.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Salam+properties/Settings.png"
  ],
  TuitionMela: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/TuitionMela/HomeScreen.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/TuitionMela/AllCourses.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/TuitionMela/AvailableTutor.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/TuitionMela/tutorProfile.png"
  ],
  Comex: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Comex/SplashScreen.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Comex/TimelineScreen.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Comex/ViewPhotos.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Comex/ProfileSettings.png"
  ],
  Alhilal: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Al-hilal/homeScreen.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Al-hilal/category.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Al-hilal/ProductsPage.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Al-hilal/cart.png"
  ],
  CrownePlaza: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/crowneplaza/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/crowneplaza/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/crowneplaza/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/crowneplaza/4.png"
  ],
  GreenLeaf: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/greenleaf/greenleaf1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/greenleaf/greenleaf2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/greenleaf/greenleaf3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/greenleaf/greenleaf4.png"
  ],
  FitStopGo: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/fitstopgo/fitstopgo1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/fitstopgo/fitstopgo2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/fitstopgo/fitstopgo3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/fitstopgo/fitstopgo4.png"
  ],
  DaralAseel: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/daraalaseel/daraalaseel1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/daraalaseel/daraalaseel2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/daraalaseel/daraalaseel3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/daraalaseel/daraalaseel4.png",
  ],
  TheModProject: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/themodproject/themodproject1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/themodproject/themodproject2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/themodproject/themodproject3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/themodproject/themodproject4.png",
  ],
  SpiceOnClick: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/spiceonclick/spiceonclick1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/spiceonclick/spiceonclick2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/spiceonclick/spiceonclick3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/spiceonclick/spiceonclick4.png",
  ],
  IDStore: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/idstore/idstore1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/idstore/idstore2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/idstore/idstore3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/idstore/idstore4.png",
  ],
  Farmasi: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/farmasi/farmasi1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/farmasi/farmasi2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/farmasi/farmasi3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/farmasi/farmasi4.png",
  ],
  LensBox: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/lensmore/lensmore1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/lensmore/lensmore2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/lensmore/lensmore3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/lensmore/lensmore4.png"
  ],
  EmilyPets: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/emilypets/emilypets1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/emilypets/emilypets2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/emilypets/emilypets3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/emilypets/emilypets4.png",
  ],
  Mom: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/mom/mom1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/mom/mom2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/mom/mom3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/mom/mom4.png",
  ],
  MoonMart: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/moonmart/moonmart1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/moonmart/moonmart2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/moonmart/moonmart3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/moonmart/moonmart4.png",
  ],
  Aadyah: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/aadyah/aadyah1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/aadyah/aadyah2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/aadyah/aadyah3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/aadyah/aadyah4.png",
  ],
  Euphoria: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/euphoria/euphoria1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/euphoria/euphoria2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/euphoria/euphoria3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/euphoria/euphoria4.png",
  ],
  HNProfessionalMakeup: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/hnprofessional/hnprofessional1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/hnprofessional/hnprofessional2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/hnprofessional/hnprofessional3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/hnprofessional/hnprofessional4.png",
  ],
  PlatinumInteriors: [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/platinuminteriors/platinuminteriors1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/platinuminteriors/platinuminteriors2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/platinuminteriors/platinuminteriors3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/platinuminteriors/platinuminteriors4.png",
  ],
  ManisCafe:[
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/maniscafe/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/maniscafe/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/maniscafe/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/maniscafe/4.png"
  ],
  Tradesup:[
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Tradesup/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Tradesup/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Tradesup/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/Tradesup/4.png"
  ],
  Bindera:[
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/bindera/1.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/bindera/2.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/bindera/3.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/bindera/4.png"
  ]
};

const logo = [
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/ScanZone.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Fursahh_Logo_Final.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Holer-al-salmy.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/buger_garage.png",
  // "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/DarNijoodLogo.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/crowneplaza.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/SalamPropertiesLogo.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/TuitionMelaLogo.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Comex+3%402x.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Al-Hilaal-final-logo.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/sunbeleza.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/petbery.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/greenleaf/greenleaf.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/fitstopgo/fitstopgo.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/daraalaseel/daraalaseel.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/themodproject/themodproject.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/spiceonclick/spiceonclick.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/idstore/idstore.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/farmasi/farmasi.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/lensmore/lensmore.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/emilypets/emilypets.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/mom/mom.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/moonmart/moonmart.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/aadyah/aadyah.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/euphoria/euphoria.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/hnprofessional/hnprofessional.png",
  "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/platinuminteriors/platinuminteriors.png",
  "https://manisoman.net/assets/media/manis.png",
  "https://bindera.om/wp-content/uploads/2020/07/LogoV2-011.png",
  "https://tradesup.co/static/media/logo.f1618b2b.svg"
];
const Clientele = props => {
  return (
    <div style={{ marginTop: "80px" }}>
      <ScrollTop />
      {console.log(Screens)}
      <section>
        <section className="Clientele_main_outer_section">
          <Title level={1} className="clientele_Page_mainHeading">
            <span className="blue">Ideas </span>that we<br />TURN into reality
          </Title>
          <p className="clientele_Page_mainHeading_content">
            We strive to work towards our client's success, a few cases are
          </p>


          {/* Burger Garage */}
          <Row type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img style={{ width: "120px" }} src={logo[3]} />
                <p>Burger Garage</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />
                  Oman
                </p>

                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.burgergarage" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/burger-garage/id1509157367" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="http://burgergarageoman.com/" target="_blank"><Icon type="dashboard" theme="filled" /> <br /> CMS</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.BurgerGarage.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Fursahh */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[1]} />
                <p>Fursahh</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.app.fursahh&hl=en_IN" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/fursahh-%D9%81%D8%B1%D8%B5%D8%A9/id1478949351" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="http://fursahh.com/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="http://fursahh.com/admin/" target="_blank"><Icon type="dashboard" theme="filled" /> <br /> CMS</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Fursahh.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Scanzone */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[0]} />
                <p>ScanZone</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/South_Africa.svg.png" />{" "}
                  South Africa
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.scanzone" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/scanzone/id1493353070" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.ScanZone.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Site Inspection */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[2]} />
                <p>Site Inspection</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.site_inspection" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/site-inspection/id1513798449" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://siteinspection.upappfactory.app/" target="_blank"><Icon type="dashboard" theme="filled" /> <br /> CMS</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.SiteInspection.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Salam Properties */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[5]} />
                <p>Salam Properties</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.app.salamsquare&hl=en_IN" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/salam-properties/id1474936406" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.SalamProperties.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Tuition Mela */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[6]} />
                <p>Tuition Mela</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_India.svg.webp" />{" "}
                  India
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.tuition_mela" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  {/* <div className="clientele_page_icon_Outer">
                    <a href=""><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div> */}
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://www.tuitionmela.com/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.TuitionMela.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Comex */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[7]} />
                <p>Comex</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href=""><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href=""><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Comex.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Al-Hilal */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[8]} style={{ width: "100px" }} />
                <p>AL-Hilal</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.alhilal" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/alhilal-gaming/id1493353451" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Alhilal.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Crowne Plaza */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[4]} style={{ width: "230px" }} />
                <p>Crowne Plaza</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.crowneplaza" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/cp-muscat-qurum/id1510546901" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.CrownePlaza.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Sunbeleza */}
          {/* <Row style={{ marginTop: "100px", marginBottom: "130px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[9]} style={{ width: "100px" }} />
                <p>Sunbeleza</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/malaysia.png" />{" "}
                  Malaysia
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href=""><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={16} style={{ marginRight: '20px' }} className="clientele_content_body">
              <div className="laptop">
                <div className="laptop__screen">
                  <div className="laptop_screen_body">
                    <Scrollbars
                      style={{
                        background: '#f6fffe',
                        height: 320,
                        padding: '10px',
                        borderRadius: '3px'
                      }}>
                      <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/website-screens/webview/Sunbeleza.png" alt="Screen" />
                    </Scrollbars>
                  </div>
                </div>
                <div class="laptop__bottom">
                  <div class="laptop__under"></div>
                </div>
                <div class="laptop__shadow"></div>
              </div>

              <div className="mobile">
                <div className="mobile__screen">
                  <div className="mobile_screen_body">
                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/website-screens/mobileview/Sunbeleza.png" alt="Screen" />
                  </div>
                </div>
                <div class="mobile__shadow"></div>
              </div>
            </Col>
          </Row> */}
          {/* Green Leaf India */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[11]} />
                <p>Green Leaf India</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_India.svg.webp" />{" "}
                  India
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.GreenLeaf" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/green-leaf-india/id1522262457" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.GreenLeaf.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Fit Stop Go */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[12]} />
                <p>Fit Stop Go</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.FitStopGo" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/fit-stop-go/id1522289058" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    {/* <a href="https://fitstopgo.com" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a> */}
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.FitStopGo.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Dar Al Aseel */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[13]} />
                <p>Dar Al Aseel</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.DarAlAseel" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/dar-al-aseel/id1520219883" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.DaralAseel.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* The Mod Project */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[14]} />
                <p>The Mod Project</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/malaysia.png" />{" "}
                  Malaysia
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.themodproject" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/the-mod-project/id1510788766" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://themodproject.my" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.TheModProject.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* SpiceOnClick */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[15]} />
                <p>SpiceOnClick</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/UAE.png" />{" "}
                  UAE
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.SpiceOnClick" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.SpiceOnClick.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* ID Store KW */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[16]} />
                <p>ID Store</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Kuwait.svg" />{" "}
                  Kuwait
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.IDStoreKW" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/id-store-kw/id1521482165" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.IDStore.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Farmasi */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[17]} />
                <p>Farmasi</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.farmasi" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/farmasi/id1510148738" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://farmasi-oman.com" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Farmasi.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Lens Box */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[18]} />
                <p>Lens Box</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/UAE.png" />{" "}
                  UAE
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.LensBox" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    {/* <a href="https://lensesbox.co" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a> */}
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.LensBox.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Emily Pets */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[19]} />
                <p>Emily Pets</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_India.svg.webp" />{" "}
                  India
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.Emilypet" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/emily-pets/id1517214860" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://emilypets.co" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.EmilyPets.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Smart Mom */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[20]} />
                <p>Smart Mom</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.Smartmom" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/the-smart-mom/id1517957613" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://smartmom.info/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Mom.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Moon Mart */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[21]} />
                <p>Moon Mart</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_India.svg.webp" />{" "}
                  India
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.MoonMart" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/online-moonmart/id1510557071" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://moonmart.net/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.MoonMart.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Aadyah */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[22]} />
                <p>Aadyah</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/srilanka.svg" />{" "}
                  Sri Lanka
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.Aadyasuper" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/aadyah-super/id1514910405" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://aadyahsuper.lk" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Aadyah.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Euphoria */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[23]} />
                <p>Euphoria</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.euphoria" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/euphoria-oman/id1510632954" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Euphoria.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* HN Professional Makeup */}
          {/* <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[24]} />
                <p>HN Professional Makeup</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.HNProfessionalMakeup" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/hn-professional-makeup/id1519993213" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.HNProfessionalMakeup.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row> */}
          {/* Platinum Interiors */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[25]} />
                <p>Platinum Interiors</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/UAE.png" />{" "}
                  UAE
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upappfactory.platinuminterior" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/platinum-interiors/id1519595488" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    {/* <a href="https://platinuminteriors.ae" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a> */}
                    <a><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.PlatinumInteriors.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Manis Cafe */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[26]} />
                <p>MANI'S CAFE</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upapp.maniscafe" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/us/app/manis-cafe/id1532942485" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.ManisCafe.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Trades Up */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[28]} />
                <p>TRADESUP</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://play.google.com/store/apps/details?id=com.upapp.tradesUP&hl=en_US&gl=US" target="_blank"><Icon type="android" theme="filled" /> <br /> Android <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://apps.apple.com/in/app/tradesup/id1533776215" target="_blank"><Icon type="apple" theme="filled" /> <br /> iOS <br /> Application</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <a href="https://tradesup.co/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Tradesup.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>
          {/* Bindera */}
          <Row style={{ marginTop: "100px" }} type="flex" justify="space-around" align="middle" gutter={30} >
            <Col xs={24} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[27]} />
                <p>BINDERA</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://bindera.om/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={18}>
              <div className="clientele-screenshots">
                {Screens.Bindera.map(bgs => (
                  <img className="client-screen" src={bgs} />
                ))}
              </div>
            </Col>
          </Row>

          {/* <Row style={{ marginTop: "100px", marginBottom: "130px" }} type="flex" justify="space-around" align="middle" gutter={30}>
            <Col xs={25} lg={6}>
              <div className="clientele-logo-name">
                <img src={logo[27]} style={{ width: "100px" }} />
                <p>BINDERA</p>
                <p className="country-name">
                  <img className="country-flag" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" />{" "}
                  Oman
                </p>
                <div className="clientele_pageIcon_outerMain">
                  <div className="clientele_page_icon_Outer">
                    <a href="https://bindera.om/" target="_blank"><Icon type="layout" theme="filled" /><br />Website</a>
                  </div>
                  <div className="clientele_page_icon_Outer">
                    <Icon type="dashboard" theme="filled" /> <br /> CMS
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} lg={16} style={{ marginRight: '20px' }} className="clientele_content_body">
              <div className="laptop">
                <div className="laptop__screen">
                  <div className="laptop_screen_body">
                    <Scrollbars
                      style={{
                        background: '#f6fffe',
                        height: 320,
                        padding: '10px',
                        borderRadius: '3px'
                      }}>
                      <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/website-screens/webview/bindera.webp" alt="Screen" />
                    </Scrollbars>
                  </div>
                </div>
                <div class="laptop__bottom">
                  <div class="laptop__under"></div>
                </div>
                <div class="laptop__shadow"></div>
              </div>

              <div className="mobile">
                <div className="mobile__screen">
                  <div className="mobile_screen_body">
                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/website-screens/mobileview/bindera.webp" alt="Screen" />
                  </div>
                </div>
                <div class="mobile__shadow"></div>
              </div>
            </Col>
          </Row> */}

        </section>
      </section>
    </div>
  );
};

Clientele.propTypes = {};

export default Clientele;
