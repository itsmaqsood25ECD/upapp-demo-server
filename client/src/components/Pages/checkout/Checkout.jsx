import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import ScrollTop from "../../ScrollTop";
import "../../../assets/css/scrollbottom.css";
import { Spin } from "antd";
import { CountryDropdown } from "react-country-region-selector";
import Hero from "../../hero";
import { getCouponCode, BankTransfer } from "../../../actions/checkout.action";
import { getApplication } from "../../../actions/application.action";
// import { getLocation } from "../../../actions/userlocation.action";
import { proceedToPay } from "../../../actions/checkout.action";
import {
  Card,
  Checkbox,
  Row,
  Col,
  Typography,
  Input,
  Button,
  Form,
  Radio,
  message,
  Icon
} from "antd";
import moment from "moment";
import "antd/dist/antd.css";
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import axios from "axios";

const { Title, Text } = Typography;
const InputGroup = Input.Group;

const CheckoutPage = ({
  match,
  user,
  getApplication,
  product,
  loading,
  getCouponCode,
  form: { getFieldDecorator, validateFieldsAndScroll },
  BankTransfer,
  proceedToPay
  // userlocation
}) => {
  const [appDetails, setAppData] = useState({
    disable: false,
    totalPrice: "",
    packType: "",
    defaultPrice: base64.decode(match.params.price),
    coupon_code: "",
    disablePayment: true,
    payClicked: false,
    discountPrice: "",
    redirectOnBankSuccess: false,
    mycouponApplied: false,
    disablecouponfield: false,
    redirectOnPaySuccess: false,
    redirectOnCheckoutSucces: false,
    countryHead: localStorage.getItem("country"),
    OrderId: ""
  });

  const {
    totalPrice,
    packType,
    coupon_code,
    disablePayment,
    discountPrice,
    redirectOnBankSuccess,
    payClicked,
    disable,
    mycouponApplied,
    disablecouponfield,
    redirectOnPaySuccess,
    redirectOnCheckoutSucces,
    OrderId,
    countryHead
  } = appDetails;
  useEffect(() => {
    getApplication(match.params.id);
    getCouponCode();
    if (countryHead == "SG") {
      setAppData({
        ...appDetails,
        disable: true,
        discountPrice: null,
        disablePayment: true,
        packType:
          !loading &&
          base64.decode(match.params.price) ===
            Math.round(product && product.fields && product.fields.month.adspm)
            ? "Month"
            : "Year",
        mycouponApplied: false,
        disablecouponfield: false,
        currencyState: "$",
        newBDSPM: Math.round(
          product && product.fields && product.fields.month.bdspm
        ),
        newADSPM: Math.round(
          product && product.fields && product.fields.month.adspm
        ),
        newBDSPY: Math.round(
          product && product.fields && product.fields.year.bdspy
        ),
        newADSPY: Math.round(
          product && product.fields && product.fields.year.adspy
        )
      });

      if (
        base64.decode(match.params.price) ==
        Math.round(product && product.fields && product.fields.month.adspm)
      ) {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: Math.round(base64.decode(match.params.price)),
          packType: "Month",
          defaultPrice: Math.round(base64.decode(match.params.price))
        });
      } else {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: Math.round(base64.decode(match.params.price) * 12),
          packType: "Year",
          defaultPrice: Math.round(base64.decode(match.params.price))
        });
      }
    } else if (countryHead == "OM") {
      setAppData({
        ...appDetails,
        disable: true,
        discountPrice: null,
        disablePayment: true,
        packType:
          !loading &&
          base64.decode(match.params.price) ===
            (product && product.fields && product.fields.month.adspm)
            ? "Month"
            : "Year",
        mycouponApplied: false,
        disablecouponfield: false,
        currencyState: "$",
        newBDSPM: product && product.fields && product.fields.month.bdspm,
        newADSPM: product && product.fields && product.fields.month.adspm,
        newBDSPY: product && product.fields && product.fields.year.bdspy,
        newADSPY: product && product.fields && product.fields.year.adspy
      });

      if (
        base64.decode(match.params.price) ==
        (product && product.fields && product.fields.month.adspm)
      ) {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: base64.decode(match.params.price),
          packType: "Month",
          defaultPrice: base64.decode(match.params.price)
        });
      } else {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: base64.decode(match.params.price) * 12,
          packType: "Year",
          defaultPrice: base64.decode(match.params.price)
        });
      }
    }
  }, []);

  const selectCountry = val => {
    setAppData({
      ...appDetails,
      country: val
    });
  };

  const handleChange = e => {
    if (countryHead == "OM") {
      if (
        e.target.value ===
        (product && product.fields && product.fields.month.adspm)
      ) {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: e.target.value,
          discountPrice: null,
          mycouponApplied: false,
          disablecouponfield: false,
          packType:
            e.target.value ===
            (product && product.fields && product.fields.month.adspm)
              ? "Month"
              : "Year"
        });
      } else {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: +e.target.value * 12,
          discountPrice: null,
          mycouponApplied: false,
          disablecouponfield: false,
          packType:
            e.target.value ===
            (product && product.fields && product.fields.month.adspm)
              ? "Month"
              : "Year"
        });
      }
    } else if (countryHead == "SG") {
      if (
        e.target.value ===
        Math.round(product && product.fields && product.fields.month.adspm)
      ) {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: e.target.value,
          discountPrice: null,
          mycouponApplied: false,
          disablecouponfield: false,
          packType:
            e.target.value ===
            Math.round(product && product.fields && product.fields.month.adspm)
              ? "Month"
              : "Year"
        });
      } else {
        setAppData({
          ...appDetails,
          currencyState: "$",
          totalPrice: +e.target.value * 12,
          discountPrice: null,
          mycouponApplied: false,
          disablecouponfield: false,
          packType:
            e.target.value ===
            Math.round(product && product.fields && product.fields.month.adspm)
              ? "Month"
              : "Year"
        });
      }
    }
  };

  const onChange = e => {
    setAppData({
      ...appDetails,
      [e.target.name]: e.target.value
    });
  };

  let WDmonthPrice;
  let WDyearPrice;
  let monthPrice;
  let yearPrice;
  let defaultPrice;
  let coupon_details;
  let currency;

  if (countryHead == "SG") {
    currency = "$";
    WDmonthPrice = Math.round(
      product && product.fields && product.fields.month.bdspm
    );
    WDyearPrice = Math.round(
      product && product.fields && product.fields.year.bdspy
    );
    monthPrice = Math.round(
      product && product.fields && product.fields.month.adspm
    );
    yearPrice = Math.round(
      product && product.fields && product.fields.year.adspy
    );

    defaultPrice =
      base64.decode(match.params.price) == monthPrice ? monthPrice : yearPrice;
  } else if (countryHead == "OM") {
    currency = "$";
    WDmonthPrice = Math.round(
      product && product.fields && product.fields.month.bdspm
    );
    WDyearPrice = Math.round(
      product && product.fields && product.fields.year.bdspy
    );
    monthPrice = Math.round(
      product && product.fields && product.fields.month.adspm
    );
    yearPrice = Math.round(
      product && product.fields && product.fields.year.adspy
    );
    defaultPrice =
      base64.decode(match.params.price) == monthPrice ? monthPrice : yearPrice;
  }

  const applyCouponCode = async () => {
    const cc = await getCouponCode(appDetails.coupon_code);
    coupon_details = cc;

    if (coupon_details) {
      setAppData({
        ...appDetails,
        couponCodeTitle: coupon_details.coupon_code,
        couponCodeDesc: coupon_details.coupon_description
      });

      if (coupon_details.general && coupon_details.general.coupon_expiry_date) {
        const user_coupon_used = user.coupon_used.filter(arg => {
          return arg.coupon_code === appDetails.coupon_code;
        });
        if (
          moment(+coupon_details.general.coupon_expiry_date).format("L") <
          moment().format("L")
        ) {
          message.error("Coupon Has Expired");
          getCouponCode();
          return;
        }
        if (
          coupon_details.usage_limits &&
          coupon_details.usage_limits.usage_limit_per_coupon
        ) {
          if (
            coupon_details.usage_limits.usage_limit_per_coupon ==
              coupon_details.usage_limits.usage_limit_per_coupon_used &&
            coupon_details.usage_limits.usage_limit_per_user ==
              user_coupon_used[0].coupon_used_number
          ) {
            message.error("coupon is not valid");
            getCouponCode();
            return;
          }
          if (
            coupon_details.usage_limits.usage_limit_per_user ==
              user_coupon_used &&
            user_coupon_used[0].coupon_used_number
          ) {
            message.error("You have exceeded the coupon usage");
            getCouponCode();
            return;
          }
        }
        if (
          coupon_details.general &&
          coupon_details.general.coupon_pack_type !== "None"
        ) {
          if (coupon_details.general.coupon_pack_type === packType) {
            if (coupon_details.general.discount_type === "Amount") {
              if (
                coupon_details.usage_restriction.exclude_category.includes(
                  product && product.fields && product.fields.category
                ) ||
                coupon_details.usage_restriction.exclude_products.includes(
                  product && product.fields && product.fields.category
                )
              )
                checkForExcludeCategory();
              deductCouponAmt();
            } else if (coupon_details.general.discount_type === "Percentage") {
              if (
                coupon_details.usage_restriction.exclude_category.includes(
                  product && product.fields && product.fields.category
                ) ||
                coupon_details.usage_restriction.exclude_products.includes(
                  product && product.fields && product.fields.category
                )
              )
                checkForExcludeCategory();
              deductCouponAmt();
            }
          }
        } else {
          if (
            coupon_details.usage_restriction.exclude_category.includes(
              product && product.fields && product.fields.category
            ) ||
            coupon_details.usage_restriction.exclude_products.includes(
              product && product.fields && product.fields.category
            )
          ) {
            checkForExcludeCategory();
          } else {
            deductCouponAmt();
          }
        }
      }
    }
  };

  const checkForExcludeCategory = () => {
    if (
      coupon_details.usage_restriction.exclude_category.includes(
        product && product.fields && product.fields.category
      )
    ) {
      message.error("coupon cannot be applied for this category");
      getCouponCode();
    } else {
      checkForExcludeProduct();
    }
  };

  const checkForExcludeProduct = () => {
    if (
      coupon_details.usage_restriction.exclude_products.includes(
        product && product.fields && product.fields.category
      )
    ) {
      message.error("coupon cannot be applied for this product");
      getCouponCode();
    }
  };

  const deductCouponAmt = () => {
    if (
      coupon_details.usage_restriction.min_spend ||
      coupon_details.usage_restriction.max_spend
    ) {
      if (
        (totalPrice > coupon_details.usage_restriction.min_spend ||
          totalPrice > coupon_details.usage_restriction.max_spend) &&
        coupon_details.general.discount_type === "Amount"
      ) {
        setAppData({
          ...appDetails,
          discountPrice: (
            +totalPrice - +coupon_details.general.coupon_amount
          ).toFixed(2),
          mycouponApplied: true,
          disablecouponfield: true
        });
        message.success("Coupon Applied");
      } else if (
        (totalPrice > coupon_details.usage_restriction.min_spend ||
          totalPrice > coupon_details.usage_restriction.max_spend) &&
        coupon_details.general.discount_type === "Percentage"
      ) {
        const coupon_amount = (
          coupon_details.general.coupon_amount / 100
        ).toFixed(2);
        setAppData({
          ...appDetails,
          discountPrice: (+totalPrice - +totalPrice * coupon_amount).toFixed(2),
          mycouponApplied: true,
          disablecouponfield: true
        });
        message.success("Coupon Applied");
      }
    } else if (+totalPrice < coupon_details.general.coupon_amount) {
      message.error("Coupon cannot be applied for this cart value");
      getCouponCode();
    } else {
      if (coupon_details.general.discount_type === "Amount") {
        setAppData({
          ...appDetails,
          discountPrice: (
            +totalPrice - coupon_details.general.coupon_amount
          ).toFixed(2),
          mycouponApplied: true,
          disablecouponfield: true
        });
        message.success("Coupon Applied");
      } else if (coupon_details.general.discount_type === "Percentage") {
        const coupon_amount = (
          coupon_details.general.coupon_amount / 100
        ).toFixed(2);
        setAppData({
          ...appDetails,
          mycouponApplied: true,
          disablecouponfield: true,
          discountPrice: (+totalPrice - +totalPrice * coupon_amount).toFixed(2)
        });
      }
    }
  };

  const myCouponSucess = () => {
    setAppData({
      ...appDetails,
      mycouponApplied: true,
      disablecouponfield: true
    });
  };

  const enableCouponFields = () => {
    if (packType === "Month") {
      setAppData({
        ...appDetails,
        mycouponApplied: false,
        disablecouponfield: false,
        discountPrice: monthPrice
      });
    } else if (packType === "Year") {
      setAppData({
        ...appDetails,
        mycouponApplied: false,
        disablecouponfield: false,
        discountPrice: yearPrice * 12
      });
    }
  };

  //ccavenue
  const checkoutToPay = () => {
    validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        setAppData({ ...appDetails, payClicked: true });
        const {
          firstName,
          lastName,
          phone,
          email,
          line1,
          line2,
          city,
          state,
          postal_code,
          country
        } = values;
        if (discountPrice) {
          appDetails.discountPrice = discountPrice.toString();
        }
        appDetails.appName = product && product.fields && product.fields.name;
        appDetails.category =
          product && product.fields && product.fields.category;
        appDetails.country = country;
        appDetails.firstName = firstName;
        appDetails.lastName = lastName;
        appDetails.email = email;
        appDetails.phone = phone;
        appDetails.line1 = line1;
        appDetails.line2 = line2;
        appDetails.city = city;
        appDetails.postal_code = postal_code;
        appDetails.state = state;
        const res = await proceedToPay(appDetails);
        if (res && res.Redirect) {
          setTimeout(() => {
            setAppData(
              {
                ...appDetails,
                redirectOnPaySuccess: true
              },
              5000
            );
          });
        }
      }
    });
  };

  async function handleToken(token, addresses) {
    if (discountPrice) {
      appDetails.discountPrice = discountPrice.toString();
    }
    appDetails.appName = product && product.fields && product.fields.name;
    appDetails.category = product && product.fields && product.fields.category;
    let response;
    message.loading("Please Wait...", 2);
    if (packType === "Month") {
      response = await axios.post("/pay_monthly", { token, appDetails });
    } else {
      response = await axios.post("/pay_yearly", { token, appDetails });
    }
    const { success } = response.data;
    if (success) {
      setAppData({
        ...appDetails,
        redirectOnCheckoutSucces: true,
        OrderId: response.data.result.UID
      });
      message.success("Success!");
    } else {
      message.error("Something went wrong");
    }
  }

  const bankTransfer = async () => {
    validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        setAppData({ ...appDetails, disablePayment: true });
        const {
          firstName,
          lastName,
          phone,
          email,
          line1,
          line2,
          city,
          state,
          postal_code,
          country
        } = values;
        if (discountPrice) {
          appDetails.discountPrice = discountPrice.toString();
        }
        appDetails.appName = product && product.fields && product.fields.name;
        appDetails.category =
          product && product.fields && product.fields.category;
        appDetails.country = country;
        appDetails.firstName = firstName;
        appDetails.lastName = lastName;
        appDetails.email = email;
        appDetails.phone = phone;
        appDetails.line1 = line1;
        appDetails.line2 = line2;
        appDetails.city = city;
        appDetails.postal_code = postal_code;
        appDetails.state = state;
        const hide = message.loading("Please Wait...", 7);
        const res = await BankTransfer(appDetails);
        if (res && res.status.state === "Pending") {
          setTimeout(hide, 500);
          setAppData({
            ...appDetails,
            redirectOnBankSuccess: true
          });
        }
      }
    });
  };

  if (redirectOnBankSuccess) {
    return <Redirect to="/bank-transfer-success" />;
  }

  if (redirectOnPaySuccess) {
    return <Redirect to="/PaymentRedirect" />;
  }

  if (redirectOnCheckoutSucces) {
    return <Redirect to={`/Paymentsuccess/${OrderId}`} />;
  }

  const handleContainerOnBottom = () => {
    console.log(
      "I am at bottom in optional container! " + Math.round(performance.now())
    );
    setAppData({
      ...appDetails,
      disable: false
    });
    // if (alertOnBottom) {
    //   alert('Bottom of this container hit! Too slow? Reduce "debounce" value in props')
    // }
  };

  const checkboxHandle = e => {
    if (e.target.checked) {
      setAppData({
        ...appDetails,
        disablePayment: false
      });
    } else {
      setAppData({
        ...appDetails,
        disablePayment: true
      });
    }
  };

  return loading && product === null ? (
    <Fragment>
      <div
        style={{
          width: "100Vw",
          height: "100Vh",
          background: "rgba(255,255,255,0.5)",
          textAlign: "center"
        }}
      >
        {/* <img style={{width:"150px", margin:"45Vh auto"}} src={loader} alt="loader" /> */}
        <Spin style={{ margin: "45Vh auto auto auto" }} size="large" />
      </div>
    </Fragment>
  ) : (
    <Fragment>
      <ScrollTop />

      <div>
        <Hero Title="Checkout" />
        <Row style={{ marginTop: "55px", marginBottom: "55px" }}>
          <Col xs={{ span: 24, push: 0 }} lg={{ span: 18, push: 3 }}>
            <Row>
              <Col xs={24} lg={12} style={{ padding: "10px 15px" }}>
                <Card
                  className="Select_plan"
                  title="Select Plan"
                  extra={<p />}
                  style={{}}
                >
                  <Radio.Group
                    onChange={handleChange}
                    defaultValue={defaultPrice}
                    // buttonStyle="solid"
                    className="plan-container"
                  >
                    <Radio.Button value={monthPrice}>
                      <Row
                        style={{ textAlign: "center", marginTop: "10px" }}
                        type="flex"
                        justify="space-around"
                        align="middle"
                      >
                        <Col span={24}>
                          <Text
                            style={{
                              fontWeight: "300",
                              fontSize: "16px",
                              color: "rgb(60, 60, 60)"
                            }}
                          >
                            Monthly
                          </Text>
                        </Col>

                        <Col span={24} className="radio-checkout-Price">
                          <Text
                            style={{
                              opacity: "0.3",
                              textDecoration: "line-through"
                            }}
                          >
                            <span>{currency}</span>
                            <CurrencyFormat
                              value={WDmonthPrice}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </Text>
                          <br />
                          <Text
                            style={{
                              fontSize: "30px",
                              fontWeight: "600",
                              color: "#00BCD4"
                            }}
                          >
                            <span className="Check-curr">{currency} </span>{" "}
                            <CurrencyFormat
                              value={monthPrice}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </Text>
                          <span style={{ color: "rgb(60, 60, 60)" }}>/Mo</span>
                        </Col>
                        {/* <Col span={24}>
													<span
														style={{
															fontSize: '18px',
															fontWeight: '600',
															color: 'rgb(60, 60, 60)'
														}}
													>
														{monthDP}% Off
													</span>
												</Col> */}
                      </Row>
                    </Radio.Button>
                    <Radio.Button value={yearPrice}>
                      <Row
                        style={{ textAlign: "center", marginTop: "10px" }}
                        type="flex"
                        justify="space-around"
                        align="middle"
                      >
                        <Col span={24}>
                          <Text
                            style={{
                              color: "rgb(60, 60, 60)",
                              fontSize: "16px"
                            }}
                          >
                            Yearly
                          </Text>
                        </Col>
                        <Col span={24} className="radio-checkout-Price">
                          <Text
                            style={{
                              opacity: "0.3",
                              textDecoration: "line-through"
                            }}
                          >
                            <span>{currency} </span>{" "}
                            <CurrencyFormat
                              value={WDyearPrice}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </Text>
                          <br />
                          <Text
                            style={{
                              fontSize: "30px",
                              fontWeight: "600",
                              color: "#00BCD4"
                            }}
                          >
                            <span className="Check-curr">{currency} </span>{" "}
                            <CurrencyFormat
                              value={yearPrice}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </Text>
                          <span style={{ color: "rgb(60, 60, 60)" }}>/Mo</span>
                        </Col>

                        {/* <Col span={24}>
													<span
														style={{
															fontSize: '18px',
															fontWeight: '600',
															color: 'rgb(60, 60, 60)'
														}}
													>
														{yearDP}% Off
													</span>
												</Col> */}
                      </Row>
                    </Radio.Button>
                  </Radio.Group>
                </Card>
                <br />

                <Card title="Billing Details" extra={<p />} style={{}}>
                  <Form layout="vertical">
                    <Row>
                      <Col xs={24} lg={12} style={{ padding: "0px 10px" }}>
                        <Form.Item>
                          {getFieldDecorator("firstName", {
                            rules: [
                              {
                                required: true,
                                message: "Please type your first name"
                              }
                            ],
                            initialValue: user && user.firstName
                          })(
                            <Input
                              placeholder="First Name"
                              name="firstName"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="user"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "0px 10px" }}>
                        <Form.Item>
                          {getFieldDecorator("lastName", {
                            rules: [
                              {
                                required: true,
                                message: "Please type your last name"
                              }
                            ],
                            initialValue: user && user.lastName
                          })(
                            <Input
                              placeholder="Last Name"
                              name="lastName"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="user"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={24} lg={12} style={{ padding: "0px 10px" }}>
                        <Form.Item>
                          {getFieldDecorator("email", {
                            rules: [
                              {
                                type: "email",
                                message: "The input is not valid E-mail!"
                              },
                              {
                                required: true,
                                message: "Please input your E-mail!"
                              }
                            ],
                            initialValue: user && user.email
                          })(
                            <Input
                              placeholder="Email"
                              name="email"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="mail"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "0px 10px" }}>
                        <Form.Item>
                          {getFieldDecorator("phone", {
                            rules: [
                              {
                                required: true,
                                message: "Please type Mobile"
                              }
                            ],
                            initialValue: user && user.phone
                          })(
                            <Input
                              placeholder="Mobile"
                              name="phone"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="mobile"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={24} lg={24} style={{ padding: "0px 10px" }}>
                        <Form.Item
                          label="Billing Address"
                          style={{ margin: 0 }}
                        />
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "0px 10px" }}>
                        <Form.Item label="" style={{ margin: 0 }}>
                          {getFieldDecorator("line1", {
                            rules: [
                              {
                                required: true,
                                message: "Please fill in your address"
                              }
                            ],
                            initialValue: user && user.line1
                          })(
                            <Input
                              placeholder="Line 1"
                              name="line1"
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "0px 10px" }}>
                        <Form.Item label="" style={{ margin: 0 }}>
                          {getFieldDecorator("line2", {
                            rules: [
                              {
                                required: true,
                                message: "Please fill in your address"
                              }
                            ],
                            initialValue: user && user.line2
                          })(
                            <Input
                              placeholder="Line 2"
                              name="line2"
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "5px 10px" }}>
                        <Form.Item label="" style={{ margin: 0 }}>
                          {getFieldDecorator("city", {
                            rules: [
                              {
                                required: true,
                                message: "Please fill in your city"
                              }
                            ],
                            initialValue: user && user.city
                          })(
                            <Input
                              placeholder="City"
                              name="city"
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "5px 10px" }}>
                        <Form.Item label="" style={{ margin: 0 }}>
                          {getFieldDecorator("state", {
                            rules: [
                              {
                                required: true,
                                message: "Please fill in your state"
                              }
                            ],
                            initialValue: user && user.state
                          })(
                            <Input
                              placeholder="State"
                              name="state"
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "5px 10px" }}>
                        <Form.Item label="" style={{ margin: 0 }}>
                          {getFieldDecorator("postal_code", {
                            rules: [
                              {
                                required: true,
                                message: "Please fill in your Postal Code"
                              }
                            ],
                            initialValue: user && user.postal_code
                          })(
                            <Input
                              placeholder="Postal Code"
                              name="postal_code"
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col xs={24} lg={12} style={{ padding: "5px 10px" }}>
                        <Form.Item label="" style={{ margin: 0 }}>
                          {getFieldDecorator("country", {
                            rules: [
                              {
                                required: true,
                                message: "Please fill in your country"
                              }
                            ],
                            initialValue: user && user.country
                          })(
                            <CountryDropdown
                              blacklist={[
                                "AF",
                                "AO",
                                "DJ",
                                "GQ",
                                "ER",
                                "GA",
                                "IR",
                                "KG",
                                "LY",
                                "MD",
                                "NP",
                                "NG",
                                "ST",
                                "SL",
                                "SD",
                                "SY",
                                "SR",
                                "TM",
                                "VE",
                                "ZW",
                                "IL"
                              ]}
                              name="country"
                              valueType="full"
                              value={user && user.country}
                              onChange={val => selectCountry(val)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form>
                </Card>
              </Col>
              <Col xs={24} lg={12} style={{ padding: "10px 15px" }}>
                {/* Cart Summery */}
                <Card
                  className="slim-card"
                  title="Order Summary"
                  extra={<Title level={4} />}
                  style={{ width: "100%" }}
                >
                  <Title level={4}>
                    <span
                      style={{
                        fontWeight: "300",
                        fontSize: "20px"
                      }}
                    >
                      {product && product.fields && product.fields.name}
                    </span>
                  </Title>
                  <hr style={{ opacity: "0.3" }} />
                  <Row type="flex" justify="space-around" align="middle">
                    <Col xs={24} lg={10} className="left-text sm-text-center">
                      <Text>Today's Charge</Text>
                    </Col>
                    <Col xs={24} lg={14} className="right-text sm-text-center">
                      <Title
                        style={{
                          fontSize: "30px",
                          color: "rgb(0, 188, 212)",
                          fontWeight: "300"
                        }}
                      >
                        <span>{currency}</span>{" "}
                        {discountPrice ? (
                          <CurrencyFormat
                            value={discountPrice}
                            displayType={"text"}
                            thousandSeparator={true}
                          />
                        ) : (
                          <CurrencyFormat
                            value={totalPrice}
                            displayType={"text"}
                            thousandSeparator={true}
                          />
                        )}
                      </Title>
                      <Text>{packType}ly</Text>
                    </Col>
                  </Row>
                  {packType === "Month" ? (
                    <div>
                      <hr style={{ opacity: "0.3", height: "1px" }} />
                      <Row type="flex" justify="space-around" align="middle">
                        <Col
                          xs={24}
                          lg={10}
                          className="left-text sm-text-center"
                        >
                          <Text>Recurring {packType}ly charge</Text>
                          <br />
                          <Text>Start next {packType}</Text>
                        </Col>
                        <Col
                          xs={24}
                          lg={14}
                          className="right-text sm-text-center"
                        >
                          <Title
                            level={4}
                            style={{
                              fontSize: "20px",
                              color: "rgb(0, 188, 212)",
                              fontWeight: "300"
                            }}
                          >
                            <span>{currency} </span>{" "}
                            {discountPrice ? (
                              <CurrencyFormat
                                value={discountPrice}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                            ) : (
                              <CurrencyFormat
                                value={totalPrice}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                            )}
                          </Title>
                          <Text>{packType}ly</Text>
                        </Col>
                      </Row>
                    </div>
                  ) : (
                    " "
                  )}
                </Card>
                {/* Cart Summery */}
                {/* Terms & Condition */}
                <br />
                {/* Cart Summery */}
                {/* Terms & Condition */}

                <Card
                  className="slim-card"
                  title="Terms & Condition"
                  extra={<Title level={4} />}
                  style={{ width: "100%" }}
                >
                  <div>
                    <div className="inner-scroll-example">
                      <h4 style={{ fontSize: "14px", fontWeight: "700" }}>
                        I agree that I have read all Terms & Conditions, Terms
                        of User & Privacy Policy on UP app factory website
                        <a
                          href="https://www.upappfactory.com/terms-service"
                          target="_blank"
                        >
                          (www.upappfactory.com)
                        </a>
                        . I also agree the following,
                      </h4>
                      <ol>
                        <li>
                          The delivery period committed to me by UPapp factory
                          of 7-12 business days will include the working days of
                          the country where I am based in and I have signed up
                          from. The business days commitment from UPapp factory
                          starts on the day I have supplied all the materials
                          required to complete my apps and admin panel (system).
                          <br />
                        </li>
                        <li>
                          The time frame does not include the time it would take
                          for the apps to be approved by app stores (Google Play
                          & iTunes) and the time frame committed to me by UPapp
                          factory will only include my apps being ready to go
                          live in the stores.
                          <br />
                        </li>
                        <li>
                          The plan that I am committed to along with the amount
                          being deducted from my card would start from now or
                          else I make the bank transfer to UPapp factory’s
                          corporate account.
                          <br />
                        </li>
                        <li>
                          The time frame does not include the time it would take
                          for the content to be live as it depends on me how
                          soon I provide the content to UPapp factory team to
                          ensure the content is live on my app.
                        </li>
                        <li>
                          I agree to have my training session within the
                          committed time frame of 7-12 working days.
                          <br />
                        </li>
                        <li>
                          The time frame does not include the time it would take
                          for the payment gateway to be live as it depends on me
                          confirming the documents to the payment gateway
                          company recommended by UP app factory.
                          <br />
                        </li>
                      </ol>
                    </div>
                    <Checkbox onChange={checkboxHandle} disabled={disable}>
                      I accept terms & Conditions
                    </Checkbox>
                  </div>
                </Card>
                <br />
                {/* <Addons></Addons> */}

                {/* End Terms & Condition */}
                {/* Coupon Code */}
                {disablecouponfield == false ? (
                  <InputGroup compact style={{ margin: "30px auto" }}>
                    <Input
                      disabled={disablecouponfield}
                      name="coupon_code"
                      value={coupon_code}
                      onChange={e => onChange(e)}
                      className="coupon-code-input-box"
                    />
                    <Button
                      type="primary"
                      disabled={disablecouponfield}
                      onClick={applyCouponCode}
                      className="coupon-code-apply-btn"
                    >
                      Apply Coupon
                    </Button>
                  </InputGroup>
                ) : (
                  ""
                )}

                {mycouponApplied == true ? (
                  <Card className="slim-card" style={{ width: "100%" }}>
                    {coupon_code}

                    <Button
                      onClick={enableCouponFields}
                      className="cancelCoupon"
                      size="small"
                      shape="circle"
                      icon="close"
                    />
                  </Card>
                ) : (
                  ""
                )}
                {/* Coupon Code */}

                {/* Proceed To Payment */}

                {/* <Button
								    
									type="primary"
									onClick={checkoutToPay}
									disabled={disablePayment}
									style={{ width: '100%', height: '50px' }}
								>
									PayPal
								</Button> */}

                <Button
                  type="primary"
                  onClick={bankTransfer}
                  disabled={disablePayment}
                  style={{ width: "100%", height: "50px", marginTop: "10px" }}
                >
                  Bank Transfer
                </Button>

                {/* <Button
										type="primary"
										onClick={checkoutToPay}
										disabled={payClicked}
										style={{ width: '100%', height: '50px', marginTop: '10px' }}
									>ccavenue
								</Button> */}
                {/* {countryHead === "SG" ? <StripeCheckout
										stripeKey="pk_live_A2VJciGTTF8M71aVAmNU1D1Q00llCnNX4Q"
										token={handleToken}
										amount={totalPrice * 100}
										name={product && product.fields && product.fields.name}
										billingAddress
									/> : ""} */}

                {/* Proceed To Payment */}
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

const Checkout = Form.create({ name: "checkout_page" })(CheckoutPage);
Checkout.propTypes = {
  getApplication: PropTypes.func.isRequired,
  // getLocation: PropTypes.func.isRequired,
  product: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  getCouponCode: PropTypes.func.isRequired,
  BankTransfer: PropTypes.func.isRequired,
  proceedToPay: PropTypes.func.isRequired,
  redirect: PropTypes.bool
};

const mapStateToProps = state => ({
  product: state.application.Application,
  user: state.auth.user,
  redirect: state.checkout.redirect,
  // userlocation: state.userlocation,
  loading: state.checkout.loading
});
export default connect(mapStateToProps, {
  getApplication,
  getCouponCode,
  BankTransfer,
  proceedToPay
  //  getLocation
})(Checkout);
