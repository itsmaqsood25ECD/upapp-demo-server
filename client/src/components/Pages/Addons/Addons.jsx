import React, { Component } from 'react'
import { Modal, Button, Checkbox, Row, Col } from 'antd';
const options = [
    { label: 'Apple', value: 'Apple' },
    { label: 'Pear', value: 'Pear' },
    { label: 'Orange', value: 'Orange' },
  ];

export default class Addons extends Component {
    state = { visible: false };

    showModal = () => {
        this.setState({
          visible: true,
        });
      };
    
      handleOk = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
      };
    
      handleCancel = e => {
        console.log(e);
        this.setState({
          visible: false,
        });
      };

      checkgrpChange = value => {
        console.log('checked = ', value);
        console.log('label', )

      }
      

    render() {
        return (
            <div>

<Button type="primary" 
style={{ width: '100%', height: '50px' }}
onClick={this.showModal}>
          Add One's
        </Button>
        <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
           <Checkbox.Group style={{ width: '100%' }} onChange={this.checkgrpChange}>
    <Row gutter={16}>
      <Col xs={24}>
        <Checkbox value="A" name="1">A</Checkbox>
      </Col>
      <Col xs={24}>
        <Checkbox value="B" name="2">B</Checkbox>
      </Col>
      <Col xs={24}>
        <Checkbox value="C" name="3">C</Checkbox>
      </Col>
      <Col xs={24}>
        <Checkbox value="D" name="4">D</Checkbox>
      </Col>
      <Col xs={24}>
        <Checkbox value="E" id="5">E</Checkbox>
      </Col>
    </Row>
  </Checkbox.Group>

           </Modal>
            </div>
        )
    }
}
