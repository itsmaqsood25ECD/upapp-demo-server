import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ScrollTop from '../ScrollTop';
import Hero from '../../components/hero';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import SmartInput from 'react-phone-number-input/smart-input';
import { Helmet } from 'react-helmet';
import { Button, Typography, Form, Icon, Input, Row, Col, Select, Collapse, Radio, Drawer} from 'antd';
import { contact } from '../../actions/contact.action';
import ReCAPTCHA from 'react-google-recaptcha';
import 'antd/dist/antd.css';
import GeneralQuery from '../../assets/img/ContactUS/customer-service.svg';
import TechSupport from '../../assets/img/ContactUS/tech-support.svg';
import { setAlert } from '../../actions/alert.action.jsx';
import GeneralQuestion from '../../assets/img/Faqs/GeneralQuestion.svg';
import ReachUs from '../../assets/img/Faqs/ReachUs.svg';
import Offerings from '../../assets/img/Faqs/Offerings.svg';
import Compatibility from '../../assets/img/Faqs/Compatibility.svg';
import Policies from '../../assets/img/Faqs/Policies.svg';
import Delivery from '../../assets/img/Faqs/Delivery.svg';
import Security from '../../assets/img/Faqs/Security.svg';
import Others from '../../assets/img/Faqs/Others.svg';
import ContactUsForm from '../Pages/ContactUsForm'
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';

const { Panel } = Collapse;
const { Text } = Typography;
// const text = (
// 	<p style={{ paddingLeft: 24 }}>
// 		A dog is a type of domesticated animal. Known for its loyalty and
// 		faithfulness, it can be found as a welcome guest in many households across
// 		the world.
// 	</p>
// );

let SelectedVar = '';
let FAQSelectedVar = '';
const recaptchaRef = React.createRef();


const ContactForm = ({
	contact,
	setAlert,
	form: { getFieldDecorator, validateFields, setFieldsValue },
	direction
}) => {
	const { t } = useTranslation();

	const { Title } = Typography;
	const { TextArea } = Input;
	const { Option } = Select;
	const [formData, setFormData] = useState({
		isSubmitted: false,
		SwitchSelected: '',
		HumanVarification: false,
		faqSwitchSelected:''
	});


	const [state, setState] = useState({
	   visible: false
	})

	const {visible} = state;

	const { country, SwitchSelected, HumanVarification, faqSwitchSelected } = formData;

	const showDrawer = () => {
		setState({
		  visible: true,
		});
	  };

	 const onClose = () => {
		setState({
		  visible: false,
		});
	  };

	const onCapchaChange = e => {
		// console.log(e);
		setFormData({
			...formData,
			HumanVarification: true
		});
	};

	const onExpired = () => {
		// console.log('expired');
		setFormData({
			...formData,
			HumanVarification: false
		});
	};

	const handleSwitchChange = e => {
		
		// console.log(e.target.value);
		SelectedVar = e.target.value;
		// console.log('SelectedVar', SelectedVar);
		setFormData({
			...formData,
			SwitchSelected: e.target.value,
			type: e.target.value
		});
		// console.log('SwitchSelected', SwitchSelected);
	};
	const FaqHandleChange = e => {
		showDrawer();
		// console.log(e.target.value);
		FAQSelectedVar = e.target.value;
		setFormData({
			...formData,
			faqSwitchSelected: e.target.value
		});
		// console.log('SwitchSelected', faqSwitchSelected);
	};

	const onChange = e => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};
	const onPhoneChange = e => {
		setFormData({
			...formData,
			mobile: e
		});
	};
	const handleChange = e => {
		setFormData({ ...formData, category: e });
	};

	const onExistSubmit = e => {
		e.preventDefault();
		validateFields((err, values) => {
			if (!HumanVarification) {
				// console.log('You are BOT');
				setAlert('Please Verify reCAPTCHA', 'danger');
			} else if (!err) {
				setFormData({
					...formData
				});
				contact(formData);
				setFormData({
					...formData,
					isSubmitted: true
				});
			}
		});
	};
	const onNewSubmit = e => {
		e.preventDefault();
		validateFields((err, values) => {
			if (!HumanVarification) {
				// console.log('You are BOT');
				setAlert('Please Verify reCAPTCHA', 'danger');
			} else if (!err) {
				setFormData({
					...formData
				});
				contact(formData);
				setFormData({
					...formData,
					isSubmitted: true
				});
			}
		});
	};

	// Redirect if contact form is submitted
	if (formData.isSubmitted) {
		return <Redirect to="/contactUs/call-back-confirmation" />;
	}

	return (
		<Fragment>
			<ScrollTop></ScrollTop>
			<Helmet>
				<title>Contact Us- Contact our Support team</title>
				<meta
					name="Get Location Details and Support form of upappfactory"
					content="UPappfactory Address Contact Number"
				/>
			</Helmet>

			<Hero Title={`${t("header.Support")}`}></Hero>
			{/* <div style={{ background: 'rgb(250, 255, 255)', paddingBottom: '150px' }}>
				<Row type="flex" justify="space-around" align="middle">
					<Col xs={24} lg={24}>

						<div className="callback-switch-container" >
							<Radio.Group
								onChange={handleSwitchChange}
								className="plan-container"
							>
								<Radio.Button value="NewUser">
									<div className="CallBack-switch">
										<img src={GeneralQuery} alt="genrealQuery" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.NewUser")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button value="ExistingUser">
									<div className="CallBack-switch">
										<img src={TechSupport} alt="genrealQuery" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.ExistingUser")}
										</span>
									</div>
								</Radio.Button>
							</Radio.Group>
						</div>

							<div className="support-bg" >
						

						
							{SelectedVar === 'NewUser' ? (
								<div className="contact-form-container">
									<Title
										level={4}
										style={{
											fontSize: '20px',
											textAlign: 'center',
											textTransform: 'capitalize',
											fontWeight: '300'
										}}
									>
										{t("Support.wedLove")}
									</Title>
									<Form onSubmit={e => onNewSubmit(e)} className="login-form">
										<Form.Item>
											{getFieldDecorator('name', {
												rules: [
													{
														required: true,
														message: `${t("General.Pleasetypeyourname")}`
													}
												]
											})(
												<Input
													placeholder={t("General.Pleasetypeyourname")}
													name="name"
													onChange={e => onChange(e)}
													prefix={
														<Icon
															type="user"
															style={{ color: 'rgba(0,0,0,.25)' }}
														/>
													}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator('email', {
												rules: [
													{ required: true, message: `${t("General.Pleasetypeyourname")}`}
												]
											})(
												<Input
													placeholder={t("General.Email")}
													name="email"
													onChange={e => onChange(e)}
													prefix={
														<Icon
															type="mail"
															style={{ color: 'rgba(0,0,0,.25)' }}
														/>
													}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator('mobile', {
												rules: [
													{
														required: true,
														message: `${t("General.PleaseentermobileNumber")}`
													}
												]
											})(
												<PhoneInput
													style={{ marginTop: '10px' }}
													placeholder={t("General.mobile")}
													name="mobile"
													inputComponent={SmartInput}
													onChange={e => onPhoneChange(e)}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator('description', {
												rules: [
													{
														required: true,
														message:`${t("General.PleasetypeMessage")}`
													}
												]
											})(
												<TextArea
													name={t("General.Message")}
													onChange={e => onChange(e)}
													placeholder="Message"
													autosize={{ minRows: 2, maxRows: 6 }}
												/>
											)}
										</Form.Item>
										<Form.Item>
											<ReCAPTCHA
												ref={recaptchaRef}
												onExpired={onExpired}
												sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
												onChange={e => onCapchaChange(e)}
											/>
										</Form.Item>
										<Form.Item style={{ textAlign: 'center' }}>
											<Button type="primary" htmlType="submit" className="UAFprimaryButton1">{t('General.Submit')}</Button>
										</Form.Item>
									</Form>
								</div>
							) : (
								' '
							)}

							{SelectedVar === 'ExistingUser' ? (
								<div className="contact-form-container">
									<Title
										level={4}
										style={{
											fontSize: '20px',
											textAlign: 'center',
											textTransform: 'capitalize',
											fontWeight: '300'
										}}
									>
										{t("Support.wedLove")}
									</Title>
									<Form onSubmit={e => onExistSubmit(e)} className="login-form">
										<Form.Item>
											{getFieldDecorator('name', {
												rules: [
													{
														required: true,
														message:`${t("General.Pleasetypeyourname")}`
													}
												]
											})(
												<Input
													placeholder={t("General.Pleasetypeyourname")}
													name="name"
													onChange={e => onChange(e)}
													prefix={
														<Icon
															type="user"
															style={{ color: 'rgba(0,0,0,.25)' }}
														/>
													}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator('email', {
												rules: [
													{ required: true, message: `${t("General.Pleasetypeyouremail")}` }
												]
											})(
												<Input
													placeholder={t("General.Email")}
													name="email"
													onChange={e => onChange(e)}
													prefix={
														<Icon
															type="mail"
															style={{ color: 'rgba(0,0,0,.25)' }}
														/>
													}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator('mobile', {
												rules: [
													{
														required: true,
														message: `${t("General.PleaseentermobileNumber")}`
													}
												]
											})(
												<PhoneInput
													style={{ marginTop: '10px' }}
													placeholder={t("General.mobile")}
													name="mobile"
													inputComponent={SmartInput}
													onChange={e => onPhoneChange(e)}
												/>
											)}
										</Form.Item>
										
										<Form.Item hasFeedback>
											{getFieldDecorator('category', {
												rules: [
													{ required: true, message:`${t("Support.SelectCategory")}` }
												]
											})(
												<Select
													placeholder={t("Support.SelectCategory")}
													onChange={e => handleChange(e)}
												>
													<Option value="Billing">
													{t("Support.Billing")}
														</Option>
													<Option value="upgrade">
													{t("Support.Upgrade")}
													</Option>
													<Option value="technicalsupport">
													{t("Support.TechnicalSupport")}
													</Option>
												</Select>
											)}
										</Form.Item>

										<Form.Item>
											{getFieldDecorator(
												'app_name',
												{}
											)(
												<Input
													placeholder={t("Support.AppName")}
													name="app_name"
													onChange={e => onChange(e)}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator(
												'admin_url',
												{}
											)(
												<Input
													placeholder={t("Support.AdminUrl")}
													name="admin_url"
													onChange={e => onChange(e)}
												/>
											)}
										</Form.Item>

										<Form.Item>
											{getFieldDecorator('description', {
												rules: [
													{
														required: true,
														message:`${t("General.PleasetypeMessage")}`
													}
												]
											})(
												<TextArea
													name="description"
													onChange={e => onChange(e)}
													placeholder={t("General.Message")}
													autosize={{ minRows: 2, maxRows: 6 }}
												/>
											)}
										</Form.Item>
										<Form.Item>
											<ReCAPTCHA
												ref={recaptchaRef}
												onExpired={onExpired}
												sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
												onChange={e => onCapchaChange(e)}
											/>
										</Form.Item>

										<Form.Item style={{ textAlign: 'center' }}>
											<Button type="primary" htmlType="submit" className="UAFprimaryButton1">{t('General.Submit')}</Button>
										</Form.Item>
									</Form>
								</div>
							) : (
								''
							)}

							{SelectedVar === '' ? (
								<div style={{ textAlign: 'center', fontSize: '25px' }}>
									{' '}
									{t("Support.PleaseSelect")}{' '}
								</div>
							) : (
								''
							)}
						</div>
						
					</Col>
				</Row>
			</div> */}
			<Row>
				<Col xs={24}>
					<div className="faqs-bg" style={{ paddingBottom: '50px' }}>
						<Row gutter={16}>
							<div
								style={{
									marginTop: '50px',
									marginBottom: '50px',
									textAlign: 'center',
									fontSize: '30px'
								}}
							>
								{t("Support.FAQs")}
							</div>

							<Col xs={24}>
							<div className="">
							<Radio.Group
								onChange={FaqHandleChange}
								className="plan-container faq-container"
							>
								<Radio.Button onClick={FaqHandleChange} value="GeneralQuestion">
									<div className="CallBack-switch">
										<img src={GeneralQuestion} alt="General Question" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.GeneralQuestion")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="ReachUs">
									<div className="CallBack-switch">
										<img src={ReachUs} alt="Reach Us" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.ReachUs")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="Offerings">
									<div className="CallBack-switch">
										<img src={Offerings} alt="Offerings" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
												{t("Support.Offerings")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="Compatibility">
									<div className="CallBack-switch">
										<img src={Compatibility} alt="Compatibility" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.Compatibility")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="PaymentsPolicies">
									<div className="CallBack-switch">
										<img src={Policies} alt="genrealQuery" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.Payments&Policies")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="Delivery">
									<div className="CallBack-switch">
										<img src={Delivery} alt="genrealQuery" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
										{t("Support.Delivery")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="Security">
									<div className="CallBack-switch">
										<img src={Security} alt="genrealQuery" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
											{t("Support.Security")}
										</span>
									</div>
								</Radio.Button>
								<Radio.Button onClick={FaqHandleChange} value="Others">
									<div className="CallBack-switch">
										<img src={Others} alt="genrealQuery" />
										<span
											style={{
												color: '#333',
												fontWeight: '600',
												display: 'block'
											}}
										>
										{t("Support.Others")}
										</span>
									</div>
								</Radio.Button>
								
							</Radio.Group>
						</div>
						
							</Col>
							
					</Row>
					</div>
					<Drawer
						title={t("Support.FAQs")}
						// width="650px"
						height="90%"
						placement="bottom"
						className="m-faq"
						closable={false}
						onClose={onClose}
						visible={visible}
						>
								
								{FAQSelectedVar === 'GeneralQuestion' ? <div>
					                <p className="Faq-question">{t("Support.Gq.1.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.2.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.3.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.4.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.5.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.5.a")}</p>
									<br/>
								
								</div> : ''}
					{FAQSelectedVar === 'ReachUs' ? <div>
									<p className="Faq-question">{t("Support.ru.1.q")}</p>
									<p className="Faq-ans">{t("Support.ru.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.ru.2.q")}</p>
									<p className="Faq-ans">{t("Support.ru.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.ru.3.q")}</p>
									<p className="Faq-ans">{t("Support.ru.3.a")}</p>
									<br/>
									</div> : ''}
					{FAQSelectedVar === 'Offerings' ? <div>
									<p className="Faq-question">{t("Support.of.1.q")}</p>
									<p className="Faq-ans">{t("Support.of.1.a")}.</p>
									<br/>
									<p className="Faq-question">{t("Support.of.2.q")}</p>
									<p className="Faq-ans">{t("Support.of.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.3.q")}</p>
									<p className="Faq-ans">{t("Support.of.3.q")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.4.q")}</p>
									<p className="Faq-ans">{t("Support.of.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.5.q")}</p>
									<p className="Faq-ans">{t("Support.of.5.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.6.q")}</p>
									<p className="Faq-ans">{t("Support.of.6.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.7.q")}</p>
									<p className="Faq-ans">{t("Support.of.7.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.8.q")}</p>
									<p className="Faq-ans">{t("Support.of.8.a")}</p>
									<br/>
									</div> : ''}
					{FAQSelectedVar === 'Compatibility' ? <div>
									<p className="Faq-question">{t("Support.com.1.q")}</p>
									<p className="Faq-ans">{t("Support.com.1.a")}</p>
									<br/> 
									<p className="Faq-question">{t("Support.com.2.q")}</p>
									<p className="Faq-ans">{t("Support.com.2.a")}</p>
									<br/> 
									<p className="Faq-question">{t("Support.com.3.q")}</p>
									<p className="Faq-ans">{t("Support.com.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.com.4.q")}</p>
									<p className="Faq-ans">{t("Support.com.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.com.5.q")}</p>
									<p className="Faq-ans">{t("Support.com.5.a")}</p>
									<br/>
									
								</div> : ''}
					{FAQSelectedVar === 'PaymentsPolicies' ? <div> 
									<p className="Faq-question">{t("Support.Pay.1.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.2.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.3.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.4.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.5.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.5.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.6.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.6.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.7.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.7.a")}</p>
									<br/></div> : ''}
					{FAQSelectedVar === 'Delivery' ? <div>
									<p className="Faq-question">{t("Support.Del.1.q")}</p>
									<p className="Faq-ans">{t("Support.Del.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Del.2.q")}</p>
									<p className="Faq-ans">{t("Support.Del.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Del.3.q")}</p>
									<p className="Faq-ans">{t("Support.Del.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Del.4.q")}</p>
									<p className="Faq-ans">{t("Support.Del.4.a")}</p>
									<br/>
									
					</div> : ''}
					{FAQSelectedVar === 'Security' ? <div> 
									<p className="Faq-question">{t("Support.Sec.1.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.1.q")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Sec.2.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Sec.3.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Sec.4.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.4.a")}</p>
									<br/>
					</div> : ''}
					{FAQSelectedVar === 'Others' ? <div> 
					<p className="Faq-question">{t("Support.oth.1.q")}</p>
									<p className="Faq-ans">{t("Support.oth.1.a")}</p>
									<br/> </div> : ''}
					
						</Drawer>
					
					<Drawer
						title={t("Support.FAQs")}
						width="650px"
						className="d-faq"
						placement="right"
						closable={false}
						onClose={onClose}
						visible={visible}
						>
						{direction === DIRECTIONS.LTR && <Fragment>

							{FAQSelectedVar === 'GeneralQuestion' ? <div>
					                <p className="Faq-question">{t("Support.Gq.1.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.2.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.3.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.4.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Gq.5.q")}</p>
									<p className="Faq-ans">{t("Support.Gq.5.a")}</p>
									<br/>
								
								</div> : ''}
					{FAQSelectedVar === 'ReachUs' ? <div>
									<p className="Faq-question">{t("Support.ru.1.q")}</p>
									<p className="Faq-ans">{t("Support.ru.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.ru.2.q")}</p>
									<p className="Faq-ans">{t("Support.ru.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.ru.3.q")}</p>
									<p className="Faq-ans">{t("Support.ru.3.a")}</p>
									<br/>
									</div> : ''}
					{FAQSelectedVar === 'Offerings' ? <div>
									<p className="Faq-question">{t("Support.of.1.q")}</p>
									<p className="Faq-ans">{t("Support.of.1.a")}.</p>
									<br/>
									<p className="Faq-question">{t("Support.of.2.q")}</p>
									<p className="Faq-ans">{t("Support.of.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.3.q")}</p>
									<p className="Faq-ans">{t("Support.of.3.q")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.4.q")}</p>
									<p className="Faq-ans">{t("Support.of.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.5.q")}</p>
									<p className="Faq-ans">{t("Support.of.5.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.6.q")}</p>
									<p className="Faq-ans">{t("Support.of.6.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.7.q")}</p>
									<p className="Faq-ans">{t("Support.of.7.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.of.8.q")}</p>
									<p className="Faq-ans">{t("Support.of.8.a")}</p>
									<br/>
									</div> : ''}
					{FAQSelectedVar === 'Compatibility' ? <div>
									<p className="Faq-question">{t("Support.com.1.q")}</p>
									<p className="Faq-ans">{t("Support.com.1.a")}</p>
									<br/> 
									<p className="Faq-question">{t("Support.com.2.q")}</p>
									<p className="Faq-ans">{t("Support.com.2.a")}</p>
									<br/> 
									<p className="Faq-question">{t("Support.com.3.q")}</p>
									<p className="Faq-ans">{t("Support.com.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.com.4.q")}</p>
									<p className="Faq-ans">{t("Support.com.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.com.5.q")}</p>
									<p className="Faq-ans">{t("Support.com.5.a")}</p>
									<br/>
									
								</div> : ''}
					{FAQSelectedVar === 'PaymentsPolicies' ? <div> 
									<p className="Faq-question">{t("Support.Pay.1.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.2.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.3.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.4.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.4.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.5.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.5.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.6.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.6.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Pay.7.q")}</p>
									<p className="Faq-ans">{t("Support.Pay.7.a")}</p>
									<br/></div> : ''}
					{FAQSelectedVar === 'Delivery' ? <div>
									<p className="Faq-question">{t("Support.Del.1.q")}</p>
									<p className="Faq-ans">{t("Support.Del.1.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Del.2.q")}</p>
									<p className="Faq-ans">{t("Support.Del.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Del.3.q")}</p>
									<p className="Faq-ans">{t("Support.Del.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Del.4.q")}</p>
									<p className="Faq-ans">{t("Support.Del.4.a")}</p>
									<br/>
									
					</div> : ''}
					{FAQSelectedVar === 'Security' ? <div> 
									<p className="Faq-question">{t("Support.Sec.1.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.1.q")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Sec.2.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.2.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Sec.3.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.3.a")}</p>
									<br/>
									<p className="Faq-question">{t("Support.Sec.4.q")}</p>
									<p className="Faq-ans">{t("Support.Sec.4.a")}</p>
									<br/>
					</div> : ''}
					{FAQSelectedVar === 'Others' ? <div> 
					<p className="Faq-question">{t("Support.oth.1.q")}</p>
									<p className="Faq-ans">{t("Support.oth.1.a")}</p>
									<br/> </div> : ''}
					
						</Fragment>
						}
						{direction === DIRECTIONS.RTL && <Fragment>
							<div className="lg-rtl">
							{FAQSelectedVar === 'GeneralQuestion' ? <div>
		<p className="Faq-question">{t("Support.Gq.1.q")}</p>
		<p className="Faq-ans">{t("Support.Gq.1.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Gq.2.q")}</p>
		<p className="Faq-ans">{t("Support.Gq.2.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Gq.3.q")}</p>
		<p className="Faq-ans">{t("Support.Gq.3.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Gq.4.q")}</p>
		<p className="Faq-ans">{t("Support.Gq.4.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Gq.5.q")}</p>
		<p className="Faq-ans">{t("Support.Gq.5.a")}</p>
		<br/>
	
	</div> : ''}
{FAQSelectedVar === 'ReachUs' ? <div>
		<p className="Faq-question">{t("Support.ru.1.q")}</p>
		<p className="Faq-ans">{t("Support.ru.1.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.ru.2.q")}</p>
		<p className="Faq-ans">{t("Support.ru.2.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.ru.3.q")}</p>
		<p className="Faq-ans">{t("Support.ru.3.a")}</p>
		<br/>
		</div> : ''}
{FAQSelectedVar === 'Offerings' ? <div>
		<p className="Faq-question">{t("Support.of.1.q")}</p>
		<p className="Faq-ans">{t("Support.of.1.a")}.</p>
		<br/>
		<p className="Faq-question">{t("Support.of.2.q")}</p>
		<p className="Faq-ans">{t("Support.of.2.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.of.3.q")}</p>
		<p className="Faq-ans">{t("Support.of.3.q")}</p>
		<br/>
		<p className="Faq-question">{t("Support.of.4.q")}</p>
		<p className="Faq-ans">{t("Support.of.4.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.of.5.q")}</p>
		<p className="Faq-ans">{t("Support.of.5.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.of.6.q")}</p>
		<p className="Faq-ans">{t("Support.of.6.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.of.7.q")}</p>
		<p className="Faq-ans">{t("Support.of.7.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.of.8.q")}</p>
		<p className="Faq-ans">{t("Support.of.8.a")}</p>
		<br/>
		</div> : ''}
{FAQSelectedVar === 'Compatibility' ? <div>
		<p className="Faq-question">{t("Support.com.1.q")}</p>
		<p className="Faq-ans">{t("Support.com.1.a")}</p>
		<br/> 
		<p className="Faq-question">{t("Support.com.2.q")}</p>
		<p className="Faq-ans">{t("Support.com.2.a")}</p>
		<br/> 
		<p className="Faq-question">{t("Support.com.3.q")}</p>
		<p className="Faq-ans">{t("Support.com.3.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.com.4.q")}</p>
		<p className="Faq-ans">{t("Support.com.4.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.com.5.q")}</p>
		<p className="Faq-ans">{t("Support.com.5.a")}</p>
		<br/>
		
	</div> : ''}
{FAQSelectedVar === 'PaymentsPolicies' ? <div> 
		<p className="Faq-question">{t("Support.Pay.1.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.1.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Pay.2.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.2.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Pay.3.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.3.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Pay.4.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.4.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Pay.5.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.5.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Pay.6.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.6.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Pay.7.q")}</p>
		<p className="Faq-ans">{t("Support.Pay.7.a")}</p>
		<br/></div> : ''}
{FAQSelectedVar === 'Delivery' ? <div>
		<p className="Faq-question">{t("Support.Del.1.q")}</p>
		<p className="Faq-ans">{t("Support.Del.1.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Del.2.q")}</p>
		<p className="Faq-ans">{t("Support.Del.2.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Del.3.q")}</p>
		<p className="Faq-ans">{t("Support.Del.3.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Del.4.q")}</p>
		<p className="Faq-ans">{t("Support.Del.4.a")}</p>
		<br/>
		
</div> : ''}
{FAQSelectedVar === 'Security' ? <div> 
		<p className="Faq-question">{t("Support.Sec.1.q")}</p>
		<p className="Faq-ans">{t("Support.Sec.1.q")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Sec.2.q")}</p>
		<p className="Faq-ans">{t("Support.Sec.2.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Sec.3.q")}</p>
		<p className="Faq-ans">{t("Support.Sec.3.a")}</p>
		<br/>
		<p className="Faq-question">{t("Support.Sec.4.q")}</p>
		<p className="Faq-ans">{t("Support.Sec.4.a")}</p>
		<br/>
</div> : ''}
{FAQSelectedVar === 'Others' ? <div> 
<p className="Faq-question">{t("Support.oth.1.q")}</p>
		<p className="Faq-ans">{t("Support.oth.1.a")}</p>
		<br/> </div> : ''}


							</div>
</Fragment>
}
						</Drawer>

				</Col>
			</Row>

			<Row type="flex">
				{/* <Col lg={8} xs={24}>
					<div className="dubai-contact-add-container ">
						<div className="dubai-add">
							<div>
							<Title
							style={{color:"#fff"}}
							 level={3}>Dubai | United Arab Emirates</Title>
							
							<p>
							Boulevard Plaza Tower 1, Sheikh Mohammed Bin Rashid Boulevard, Downtown Dubai U.A.E
							</p>
							<p>
							contactus@upapp.co
							</p>
							</div>
						</div>
					
					</div>
                   
				</Col>
				 */}
				{/* {country === 'OM' ? 	
				 <Col lg={24} xs={24}>
					<div className="oman-contact-add-container">
						<div className="dubai-add">
							<div>
							<Title
							style={{color:"#fff"}}
							 level={2}>Sultanate of Oman</Title>
							 
							<p style={{fontSize:"20px"}}>
							101 Salam Square South, Dohat Al Adab Street, Madinat Sultan Qaboos, Muscat
							</p>
							<p>
							Mobile -&nbsp; +968 2235 2207 &nbsp;&nbsp;&nbsp; Email - &nbsp;contactus@upapp.co
							</p>
							</div>
						</div>
					</div>
                </Col> :  */}
				<Col lg={8} xs={24}>
					<div className="oman-contact-add-container">
						<div className="dubai-add">
							<div style={{textShadow:"2px 2px 1px #000"}}>
							<Title
							style={{color:"#fff"}}
							 level={3}>Sultanate of Oman</Title>
							 
							<p style={{fontSize:"14px"}}>
							101 Salam Square South, Dohat Al Adab Street, Madinat Sultan Qaboos, Muscat
							</p>
							<p>
							Email - &nbsp;contactus@upapp.co
							&nbsp;&nbsp;&nbsp;&nbsp;
							Mobile -&nbsp; +968 95077555  
							
						
							</p>
							</div>
						</div>
					</div>
                </Col>
				<Col lg={8} xs={24}>
					<div className="singapore-contact-add-container">
						<div className="dubai-add">
							<div style={{textShadow:"2px 2px 1px #000"}}>
								<Title style={{ color: '#fff' }} level={3}>
									Singapore
								</Title>

								<p>
									531A Upper Cross Street, #04-95, Hong Lim Complex 051531
									Singapore
								</p>
								<p>Email - &nbsp;contactus@upapp.co
								&nbsp;&nbsp;&nbsp;&nbsp;
								  Phone Number -&nbsp; +65 6714 6696</p>
							</div>
						</div>
					</div>
				</Col>
			
				<Col lg={8} xs={24}>
					<div className="turkey-contact-add-container">
						<div className="dubai-add">
							<div style={{textShadow:"2px 2px 1px #000"}}>
								<Title style={{ color: '#fff' }} level={3}>
								 Turkey
								</Title>

								<p>
								Office No. 14 , 3rd Floor , Tatar Yapı , Loft Up 7.60 , 19-21 Kıble Sk. Ümraniye , Istanbul , Turkey
								</p>
								<p>Email - &nbsp;info@infinitech.com.tr
								&nbsp;&nbsp;&nbsp;&nbsp;
								  Phone Number -&nbsp; +90 216 7553619 </p>
							</div>
						</div>
					</div>
				</Col>
			
			</Row>
			{/* <a className="Whatsapp-chat-icon" href="https://calendly.com/maqsoodecomm25/request-for-demo" target="_blank">
 					<img src={whatsapp} style={{ height: "60px" }}></img>
 				</a> */}
				 <ContactUsForm />
		</Fragment>
	);
};

const Contact = Form.create({ name: 'normal_login' })(ContactForm);

Contact.propTypes = {
	contact: PropTypes.func.isRequired,
	setAlert: PropTypes.func.isRequired
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps, { contact, setAlert })(withDirection(Contact));
