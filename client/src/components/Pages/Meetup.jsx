import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { connect } from "react-redux";

// event
import { eventMeet, getEventMeet } from "../../actions/event.meet.action";

// event
import { Redirect } from "react-router-dom";
import moment from "moment";
import ReactGA from "react-ga";
import SrollTop from "../ScrollTop";
import {
  Icon,
  Button,
  Typography,
  Modal,
  Input,
  Row,
  Col,
  Form,
  Select,
  DatePicker,
  Menu
} from "antd";

const { Title, Text } = Typography;
const { Option, OptGroup } = Select;
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];
const recaptchaRef = React.createRef();

const Meetup = ({
  form: { getFieldDecorator, validateFields, setFieldsValue },
  eventMeet,
  getEventMeet,
  events
}) => {
  const [state, setState] = useState({});

  const [formData, setFormData] = useState({
    isSubmitted: false
  });

  useEffect(() => {
    getEventMeet();
    console.log(events);
  }, []);

  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSelect = e => {
    setFormData({ ...formData, company_size: e });
  };

  const onPhoneChange = e => {
    setFormData({ ...formData, mobile: e });
  };

  const selectCountry = val => {
    setFormData({ ...formData, country: val });
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (err) {
        console.log(err);
      } else if (!err) {
        setFormData({
          ...formData
        });
        eventMeet(formData);
        setFormData({
          ...formData,
          isSubmitted: true
        });
      }
    });
  };

  if (formData.isSubmitted) {
    return <Redirect to="/become-our-partner/sumbitted" />;
  }

  return (
    <Fragment>
      <Row type="flex" className="partner-section-bg">
        <Col lg={16} xs={24} style={{ paddingTop: "60px" }}>
          <div className="event-container">
            <p className="meetup-tag">
              {" "}
              <span style={{ color: "#4fbfdd" }}>Sri Lanka</span>{" "}
              Entrepreneurship Meetup
            </p>
            <p className="meetup-agenda-title">What we're about</p>
            <p className="agenda-desc">
              This is group for hustlers, hackers and hipsters to come together
              to discuss ideas and problems in entrepreneurship, and share their
              experience with others. It's a great platform to team up with
              fellow enthusiasts and lay the foundation to building a great
              company.
            </p>
            <hr style={{ border: "1px solid #ccc" }} />
            <Row type="flex" justify="space-around" align="middle">
              <Col xs={2} lg={2}>
                <Icon type="clock-circle" />
              </Col>
              <Col xs={22} lg={22}>
                Sat, 11 Jan, 9:30 am – 12:30 pm
              </Col>
            </Row>
            <Row type="flex" justify="space-around" align="middle">
              <Col xs={2} lg={2}>
                <Icon type="environment" />
              </Col>
              <Col xs={22} lg={22}>
                University of Moratuwa University of Moratuwa, Katubedda, Sri
                Lanka, 10400 Moratuwa
              </Col>
            </Row>
            <br></br>
            <br></br>
            <Row>
              <Col xs={24}>
                <div class="mapouter">
                  <div class="gmap_canvas">
                    <iframe
                      width="600"
                      height="360"
                      id="gmap_canvas"
                      src="https://maps.google.com/maps?q=colombo&t=&z=11&ie=UTF8&iwloc=&output=embed"
                      frameborder="0"
                      scrolling="no"
                      marginheight="0"
                      marginwidth="0"
                    ></iframe>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Col>
        <Col lg={8} xs={24} style={{ paddingTop: "60px" }}>
          <div className="Become-Our-Partner-form">
            <div
              class="logo"
              style={{ width: "100%", marginTop: "40px", marginLeft: "0px" }}
            ></div>
            <Form className="partnership-form">
              <Row gutter={16}>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your name"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Name"
                        name="name"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your email"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Email"
                        name="email"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>

                <Col xs={24} lg={24}>
                  <Form.Item hasFeedback>
                    {getFieldDecorator("designation", {
                      rules: [
                        {
                          required: true,
                          message: "Please enter your designation"
                        }
                      ]
                    })(
                      <Input
                        placeholder="designation"
                        name="designation"
                        onChange={e => onChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("mobile", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your mobile number!"
                        }
                      ]
                    })(
                      <PhoneInput
                        style={{ marginTop: "10px" }}
                        name="mobile"
                        inputComponent={SmartInput}
                        placeholder="Enter phone number"
                        onChange={e => onPhoneChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("country", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your country"
                        }
                      ]
                    })(
                      <CountryDropdown
                        blacklist={[
                          "AF",
                          "AO",
                          "DJ",
                          "GQ",
                          "ER",
                          "GA",
                          "IR",
                          "KG",
                          "LY",
                          "MD",
                          "NP",
                          "ST",
                          "SL",
                          "SD",
                          "SY",
                          "SR",
                          "TM",
                          "VE",
                          "ZW",
                          "IL"
                        ]}
                        name="country"
                        valueType="full"
                        style={{ marginTop: "12px" }}
                        onChange={val => selectCountry(val)}
                      />
                    )}
                  </Form.Item>
                </Col>

                <Col xs={24} lg={24}>
                  <Form.Item hasFeedback>
                    {getFieldDecorator("city", {
                      rules: [
                        {
                          required: true,
                          message: "Please enter your city"
                        }
                      ]
                    })(
                      <Input
                        placeholder="city"
                        name="city"
                        onChange={e => onChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item hasFeedback>
                    {getFieldDecorator("description", {
                      rules: [
                        {
                          required: true,
                          message: "Please enter why you want to join "
                        }
                      ]
                    })(
                      <Input
                        placeholder="Why you want to Join ?"
                        name="description"
                        onChange={e => onChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <div style={{ textAlign: "center" }}>
                <Button
                  style={{
                    width: "100%",
                    height: "40px",
                    fontSize: "18px"
                  }}
                  type="primary"
                  onClick={onSubmit}
                >
                  Join Now
                </Button>
              </div>
            </Form>
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

const EMT = Form.create({ name: "Event_meetup" })(Meetup);
EMT.propTypes = {
  eventMeet: PropTypes.func.isRequired,
  events: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  events: state.eventMeet.events
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, { eventMeet, getEventMeet })(EMT);
