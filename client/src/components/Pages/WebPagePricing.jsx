import React, { Component } from "react";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor } from "antd";
import CurrencyFormat from "react-currency-format";
import close from '../../assets/img/Products/website/close.svg'
import check from '../../assets/img/Products/website/check.svg'
// import "../../assets/css/webPagePrice.css";
import { withTranslation } from 'react-i18next';
const { TabPane } = Tabs;
const { Title } = Typography;
const { Link } = Anchor;
class WebPagePricing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UAWSP: this.props.UAWSP,
            UAWPP: this.props.UAWPP,
            UAWBP: this.props.UAWBP,
            currency: this.props.currency,
        };
    }
    componentWillReceiveProps(nextApp) {
        this.setState({
            UAWSP: nextApp.UAWSP,
            UAWPP: nextApp.UAWPP,
            UAWBP: nextApp.UAWBP,
            currency: nextApp.currency
        });
    }
    render() {
        return (
            <div className="UAF-container-fluid">
                <Row type="flex" className="uaweb_contentsection_row10">
                    <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                        <h1 className="botHome_s10_title_full">
                            Boost your Business
                        </h1>
                        <p className="botHome_contentsection_text_full">No commitment necessary<br />Change or cancel your Subscription Anytime</p>
                    </Col>
                    <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_pricing_mainOuter">
                        <Row gutter={16}>
                            <Col xs={24} md={8} lg={8} className="uaweb_pricing_verical_col">
                                <div className="pack-card Basic">
                                    <div className="Pack-identifier Basic">
                                        <p>
                                            <span>standard</span><br />
                                            <p >Great for small Business</p>
                                        </p>
                                    </div>
                                    <div className="uaweb_pricingText_content">
                                        <Row>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>100% Responsive & Mobile Friendly</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Pages including Home page<br /><b>(up to 5 pages)</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Contact form​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Photo gallery​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Dynamic Website</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>FREE Technical Maintenance<br /><b>(1 month)</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Hosting Disk Space<br /><b>( 3GB )</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Unlimited Bandwidth<br /><b>( 5 emails )​</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Additional Pages $35/ Page</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Domain Cost Free Up to $10​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" alt=""/>
                                                <p>Career form</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>Enquiry form</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>Blog set up Free​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>FREE SEO Friendly URL​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>Graphics & Banner​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>SEO Configuration​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>1st Year Free Professional Hosting Plan​</p>
                                            </Col>
                                        </Row>
                                        <hr className="uaweb_pricingText_ColLine" />
                                    </div>
                                    <div className="s10_currency_outer">
                                        <Title level={2} class="light-text s10_currency_section">&nbsp;
                                        <span className="PriceNum">
                                                <sup className="currency s10_currency">{this.state.currency}</sup>
                                                <CurrencyFormat value={Number(this.state.UAWSP)} displayType={"text"} thousandSeparator={true} />
                                                <sub className="per-month-tag s10_monthTag">/mo</sub>
                                            </span>
                                        </Title>
                                    </div>
                                    <div>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" className="s10_monthTag_tile_btn" title={
                                                <Button size={"large"} type="primary" className="botPricing_btn">
                                                    Get Started
                                                </Button>} />
                                        </Anchor>
                                    </div>
                                    <div className="uaweb_pricing_bottomline">
                                        <p>Pay Upfront</p>
                                        <b>NO SETUP COST</b>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} md={8} lg={8} className="uaweb_pricing_verical_col">
                                <div className="pack-card ultra" style={{ marginTop: "-10px", padding: "40px 20px" }}>
                                    <div className="Pack-identifier ultra">
                                        <p>
                                            <span>Professional</span><br />
                                            <p>Most Popular</p>
                                        </p>
                                    </div>
                                    <div className="uaweb_pricingText_content">
                                        <Row>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>100% Responsive & Mobile Friendly</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Pages including Home page<br /><b>(up to 5 pages)</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Contact form​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Photo gallery​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Dynamic Website</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>FREE Technical Maintenance<br /><b>(1 month)</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Hosting Disk Space<br /><b>( 3GB )</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Unlimited Bandwidth<br /><b>( 5 emails )​</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Additional Pages $35/ Page</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Domain Cost Free Up to $10​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Career form</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>Enquiry form</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>Blog set up Free​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>FREE SEO Friendly URL​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>Graphics & Banner​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>SEO Configuration​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={close} classname="uaweb_pricing_icon" />
                                                <p>1st Year Free Professional Hosting Plan​</p>
                                            </Col>
                                        </Row>
                                        <hr className="uaweb_pricingText_ColLine" />
                                    </div>
                                    <div className="s10_currency_outer">
                                        <Title level={2} class="light-text s10_currency_section">&nbsp;
                                        <span className="PriceNum">
                                                <sup className="currency s10_currency">{this.state.currency}</sup>
                                                <CurrencyFormat value={Number(this.state.UAWPP)} displayType={"text"} thousandSeparator={true} />
                                                <sub className="per-month-tag s10_monthTag">/mo</sub>
                                            </span>
                                        </Title>
                                    </div>
                                    <div>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" className="s10_monthTag_tile_btn" title={
                                                <Button size={"large"} type="primary" className="botPricing_btn">
                                                    Get Started
                                                </Button>} />
                                        </Anchor>
                                    </div>
                                    <div className="uaweb_pricing_bottomline">
                                        <p>Pay Upfront</p>
                                        <b>NO SETUP COST</b>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} md={8} lg={8} className="uaweb_pricing_verical_col">
                                <div className="pack-card Premium">
                                    <div className="Pack-identifier Premium">
                                        <p>
                                            <span>enterprise</span><br />
                                            <p>For Large Organisations</p>
                                        </p>
                                    </div>
                                    <div className="uaweb_pricingText_content">
                                        <Row>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>100% Responsive & Mobile Friendly</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Pages including Home page<br /><b>(up to 5 pages)</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Contact form​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Photo gallery​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Dynamic Website</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>FREE Technical Maintenance<br /><b>(1 month)</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Hosting Disk Space<br /><b>( 3GB )</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Unlimited Bandwidth<br /><b>( 5 emails )​</b></p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Additional Pages $35/ Page</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Domain Cost Free Up to $10​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Career form</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Enquiry form</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Blog set up Free​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>FREE SEO Friendly URL​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>Graphics & Banner​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>SEO Configuration​</p>
                                            </Col>
                                            <Col xs={24} md={24} lg={24} className="uaweb_pricingText_Col">
                                                <img src={check} classname="uaweb_pricing_icon" />
                                                <p>1st Year Free Professional Hosting Plan​</p>
                                            </Col>
                                        </Row>
                                        <hr className="uaweb_pricingText_ColLine" />
                                    </div>
                                    <div className="s10_currency_outer">
                                        <Title level={2} class="light-text s10_currency_section">&nbsp;
                                        <span className="PriceNum">
                                                <sup className="currency s10_currency">{this.state.currency}</sup>
                                                <CurrencyFormat value={Number(this.state.UAWBP)} displayType={"text"} thousandSeparator={true} />
                                                <sub className="per-month-tag s10_monthTag">/mo</sub>
                                            </span>
                                        </Title>
                                    </div>
                                    <div>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" className="s10_monthTag_tile_btn" title={
                                                <Button size={"large"} type="primary" className="botPricing_btn">
                                                    Get Started
                                                </Button>} />
                                        </Anchor>
                                    </div>
                                    <div className="uaweb_pricing_bottomline">
                                        <p>Pay Upfront</p>
                                        <b>NO SETUP COST</b>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default withTranslation()(WebPagePricing);