import React, { Component } from "react";
import { Row, Col, Typography, Divider } from "antd";
import "./invoice.css";
import AppLogo from "../../../assets/img/brand/upapp-line.png";

const { Title, Text } = Typography;

export default class printInv extends Component {
	render() {
		return (
			<div class="Invoice-Container">
				<div className="app-logo">
					<img src={AppLogo} alt="UP APP FACTORY"></img>
				</div>
				<Row type="flex" justify="space-around" align="middle">
					<Col xs={24} lg={12} className="print-col-50">
						<Col xs={24}>
							<Title level={4}>
								{this.props.Fname} {this.props.Lname}
							</Title>
						</Col>
					</Col>
					<Col xs={24} lg={12} className="print-col-50">
						<div style={{ textAlign: "right" }}>
							<Title level={4} className="Title-label">
								Date:
							</Title>{" "}
							<Text>{this.props.date}</Text>
						</div>
						{/* <div style={{ textAlign: "right" }}>
							<Title level={4} className="Title-label">
								Reciept ID:
							</Title>{" "}
							<Text>{this.props.id}</Text>
						</div> */}
						<div style={{ textAlign: "right" }}>
							<Title level={4} className="Title-label">
								Order ID:
							</Title>{" "}
							<Text>{this.props.id}</Text>
						</div>{" "}
					</Col>
				</Row>
				<Divider type="horizontal" />
				<Row type="flex" justify="space-around">
					<Col xs={24} lg={12} className="print-col-50">
						<div style={{ textAlign: "left" }} className="table-label">
							<Text level={4}>AppName</Text>
						</div>
						<div style={{ textAlign: "left" }} className="table-label">
							<Text level={4}>subscription type</Text>
						</div>
						<div style={{ textAlign: "left" }} className="table-label">
							<Text level={4}>Price</Text>
						</div>
						<div style={{ textAlign: "left" }} className="table-label">
							<Text level={4}>Coupon Applied</Text>
						</div>
						<div style={{ textAlign: "left" }} className="table-label">
							<Text level={4}>discount</Text>
						</div>
					</Col>
					<Col xs={24} lg={12} className="print-col-50">
						<div style={{ textAlign: "right" }} className="table-text">
							<Text>{this.props.applicationName}</Text>
						</div>
						<div style={{ textAlign: "right" }} className="table-text">
							<Text>{this.props.type}</Text>
						</div>
						<div style={{ textAlign: "right" }} className="table-text">
							<Text>{this.props.totalPrice}</Text>
						</div>
						<div style={{ textAlign: "right" }} className="table-text">
							<Text>{this.props.coupon || "NA"}</Text>
						</div>
						<div style={{ textAlign: "right" }} className="table-text">
							<Text>{this.props.totalPrice - this.props.discount || "NA"}</Text>
						</div>
					</Col>
				</Row>
				<Divider type="horizontal" style={{ margin: "3px auto" }} />
				<Row type="flex" justify="space-around">
					<Col xs={24} lg={12} className="print-col-50">
						<div style={{ textAlign: "left" }} className="table-label">
							<Text level={4}>Total</Text>
						</div>
					</Col>
					<Col xs={24} lg={12} className="print-col-50">
						<div style={{ textAlign: "right" }} className="table-label">
							<Text>{this.props.discount || this.props.totalPrice}</Text>
						</div>
					</Col>
				</Row>
			</div>
		);
	}
}
