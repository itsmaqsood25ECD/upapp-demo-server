import React, { Fragment, useState, useEffect } from "react";
import ScrollTop from "../../ScrollTop";
import { update } from "../../../actions/auth.action";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hero from "../../../components/hero";
import { CountryDropdown } from 'react-country-region-selector';
import {
	Row,
	Col,
	Input,
	Form,
	Typography,
	Icon,
	Button,
	Select,
	Card,
	Spin
} from "antd";
import "antd/dist/antd.css";

const { Title } = Typography;
const { Option } = Select;
const Edit = ({ user, loading, update, edit }) => {
	const [formData, setFormData] = useState({
		firstName: "",
		lastName: "",
		age: "",
		email: "",
		gender: "",
		phone: "",
		line1: "",
		line2: "",
		state: "",
		city: "",
		country: "",
		postal_code: ""
	});

	useEffect(() => {
		setFormData({
			firstName: loading || !user.firstName ? "" : user.firstName,
			lastName: loading || !user.lastName ? "" : user.lastName,
			age: loading || !user.age ? "" : user.age,
			email: loading || !user.email ? "" : user.email,
			gender: loading || !user.gender ? "" : user.gender,
			phone: loading || !user.phone ? "" : user.phone,
			line1: loading || !user.line1 ? "" : user.line1,
			line2: loading || !user.line2 ? "" : user.line2,
			state: loading || !user.state ? "" : user.state,
			city: loading || !user.city ? "" : user.city,
			country: loading || !user.country ? "" : user.country,
			postal_code: loading || !user.postal_code ? "" : user.postal_code
		});
	}, [loading]);

	const {
		firstName,
		lastName,
		age,
		email,
		gender,
		phone,
		line1,
		line2,
		state,
		city,
		country,
		postal_code
	} = formData;

	const onChange = e =>
		setFormData({ ...formData, [e.target.name]: e.target.value });

	const onSelect = e => {
		setFormData({ ...formData, gender: e });
	};

	const selectCountry = val => {
		setFormData({ ...formData, country: val });
	}

	const onSubmit = e => {
		e.preventDefault();
		update({
			firstName,
			lastName,
			age,
			email,
			gender,
			phone,
			line1,
			line2,
			state,
			city,
			country,
			postal_code
		});
	};
	if (edit) {
		return <Redirect to="/my-profile" />;
	}
	return loading && user === null ? (
		<Fragment>
			<Spin />
		</Fragment>
	) : (
		<Fragment>
			<ScrollTop />
			<Hero Title="Edit Profile" />
			<div
				className="contact-heading"
				style={{
					padding: "auto 15px"
				}}
			/>

			<Card
				title="Edit Profile"
				style={{
					marginBottom: "24px",
					marginLeft: "15px",
					marginRight: "15px"
				}}
			>
				<Form onSubmit={e => onSubmit(e)}>
					<Card title="General Details">
						<Row type="flex" justify="space-around">
							<Col lg={2} xs={24}>
								<Form.Item label="First Name" />
							</Col>
							<Col lg={6} xs={24}>
								<Input
									value={firstName}
									name="firstName"
									onChange={e => onChange(e)}
								/>
							</Col>
							<Col lg={2} xs={24}>
								<Form.Item label="Last Name" />
							</Col>
							<Col lg={6} xs={24}>
								<Input
									value={lastName}
									name="lastName"
									onChange={e => onChange(e)}
								/>
							</Col>
						</Row>
						<Row type="flex" justify="space-around">
							<Col lg={2} xs={24}>
								<Form.Item label="Age" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={age} name="age" onChange={e => onChange(e)} />
							</Col>
							<Col lg={2} xs={24}>
								<Form.Item label="Gender" />
							</Col>
							<Col lg={6} xs={24}>
								<Select
									value={gender}
									name={gender}
									onChange={e => onSelect(e)}
								>
									<Option value="male">Male</Option>
									<Option value="female">Female</Option>
								</Select>
							</Col>
						</Row>
						<Row type="flex" justify="space-around">
							<Col lg={2} xs={24}>
								<Form.Item label="E-Mail" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={email} name="email" onChange={e => onChange(e)} />
							</Col>
							<Col lg={2} xs={24}>
								<Form.Item label="Phone" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={phone} name="phone" onChange={e => onChange(e)} />
							</Col>
						</Row>
					</Card>
					<Card title="Address Details" style={{ marginTop: "24px" }}>
						<Row type="flex" justify="space-around">
							<Col lg={2} xs={24}>
								<Form.Item label="Line 1" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={line1} name="line1" onChange={e => onChange(e)} />
							</Col>
							<Col lg={2} xs={24}>
								<Form.Item label="Line 2" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={line2} name="line2" onChange={e => onChange(e)} />
							</Col>
						</Row>
						<Row type="flex" justify="space-around">
							<Col lg={2} xs={24}>
								<Form.Item label="City" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={city} name="city" onChange={e => onChange(e)} />
							</Col>
							<Col lg={2} xs={24}>
								<Form.Item label="State" />
							</Col>
							<Col lg={6} xs={24}>
								<Input value={state} name="state" onChange={e => onChange(e)} />
							</Col>
						</Row>
						<Row type="flex" justify="space-around">
							<Col lg={2} xs={24}>
								<Form.Item label="Postal Code" />
							</Col>
							<Col lg={6} xs={24}>
								<Input
									value={postal_code}
									name="postal_code"
									onChange={e => onChange(e)}
								/>
							</Col>
							<Col lg={2} xs={24}>
								<Form.Item label="Country" />
							</Col>
							<Col lg={6} xs={24}>
								{/* <Input
									value={country}
									name="country"
									onChange={e => onChange(e)}
								/> */}

							<CountryDropdown
							blacklist={['AF','AO','DJ','GQ','ER','GA','IR','KG','LY','MD','NP','NG', 'ST','SL','SD','SY','SR','TM','VE','ZW','IL']}
							name="country"
							valueType="short"
							className="edit-country-selector"
							value={country}
							onChange={(val) => selectCountry(val)} 
							/>


							</Col>
						</Row>
					</Card>
					<Row
						type="flex"
						justify="center"
						style={{ textAlign: "center", marginTop: "40px" }}
					>
						<Col xs={24} lg={4}>
							<Link to="/my-profile">
								<Button
									type="success"
									htmlType="submit"
									size={"Large"}
									style={{
										margin: "5px 15px",
										width: "180px",
										height: "40px",
										fontSize: "20px"
									}}
								>
									<Icon type="close" />
									Cancel
								</Button>
							</Link>
						</Col>
						<Col xs={24} lg={4}>
							<Button
								type="primary"
								htmlType="submit"
								size={"Large"}
								style={{
									margin: "5px 15px",
									width: "180px",
									height: "40px",
									fontSize: "20px"
								}}
							>
								<Icon type="save" />
								Save
							</Button>
						</Col>
					</Row>
				</Form>
			</Card>
		</Fragment>
	);
};

Edit.propTypes = {
	user: PropTypes.object.isRequired,
	update: PropTypes.func.isRequired,
	loading: PropTypes.bool,
	edit: PropTypes.bool
};

const mapStateToProps = state => ({
	user: state.auth.user,
	loading: state.auth.loading,
	edit: state.auth.edit
});
export default connect(
	mapStateToProps,
	{ update }
)(Edit);
