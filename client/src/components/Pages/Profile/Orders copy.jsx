import React, { Fragment, useEffect, useState } from 'react';
import { CSVLink } from 'react-csv';
import moment from 'moment';
import { Link } from 'react-router-dom';
import ScrollTop from '../../ScrollTop';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Button, Card, Icon, Modal, Tabs, Upload, Tooltip } from 'antd';
import {
	getUserOrders,
	cancelSubscription,
	upgradeSubscription,
	uploadReceipt
} from '../../../actions/order.action';
import Hero from '../../../components/hero';

const { TabPane } = Tabs;
const { confirm } = Modal;

const Orders = ({
	orders,
	loading,
	getUserOrders,
	cancelSubscription,
	upgradeSubscription,
	uploadReceipt
}) => {
	useEffect(() => {
		getUserOrders();
	
	}, []);

	console.log(orders)


	const [StateData, setStateData] = useState({
		loading: false,
		visible: false,
		previewVisible: false,
		previewImage: '',
		id: '',
		reciepts: [],
		recieptRecived: false
	});

	const { recieptRecived, previewVisible, previewImage } = StateData;

	const showModal = id => {
		setStateData({
			...StateData,
			visible: true,
			id: id
		});
	};
	const PendingOrderData = [];
	const CancelledOrderData = [];
	const ActiveOrderData = [];

	orders.map(order => {
		if (order.status && order.status.state === 'Pending') {
			const Ids = {
				id: order.UID,
				appName: order.appName,
				packType: order.packType,
				coupon_code: order.coupon_code || 'No Coupon Used',
				start_date: moment(order.start_date).format('MMMM Do YYYY'),
				total_price: order.discount_price || order.total_price,
				statusPurpose: order.status.purpose,
				state: order.status.state,
				status: order.status.purpose + ' ' + order.status.state,
				hasReciepts: order.reciepts.length == 0 ? false : true
			};
			PendingOrderData.push(Ids);
		} else if (
			order.status &&
			order.status.purpose === 'Cancellation' &&
			order.status.state === 'Approved'
		) {
			const Ids = {
				id: order.UID,
				appName: order.appName,
				packType: order.packType,
				coupon_code: order.coupon_code || 'No Coupon Used',
				start_date: moment(order.start_date).format('MMMM Do YYYY'),
				total_price: order.discount_price || order.total_price,
				statusPurpose: order.status.purpose,
				state: order.status.state,
				status: order.status.purpose + ' ' + order.status.state,
				reciepts: order.reciepts
			};
			CancelledOrderData.push(Ids);
		} else if (
			order.status &&
			order.status.state === 'Active') {
			const Ids = {
				id: order.UID,
				appName: order.appName,
				packType: order.packType,
				coupon_code: order.coupon_code || 'No Coupon Used',
				start_date: moment(order.start_date).format('MMMM Do YYYY'),
				total_price: order.discount_price || order.total_price,
				statusPurpose: order.status.purpose,
				state: order.status.state,
				status: order.status.purpose + ' ' + order.status.state
			};
			ActiveOrderData.push(Ids);
		}
	});

	const cancelOrder = id => {
		confirm({
			title: 'Cancel Subscription',
			content: 'Are you sure you want to cancel this subscription?',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				cancelSubscription(id);
				Modal.success({
					title: 'Cancel Subscription',
					content: `Your request to cancel subscription for order:${id} has been taken. Our team will get back to you soon`
				});
			},
			onCancel() { }
		});
	};

	// const upgradeOrder = id => {
	// 	confirm({
	// 		title: 'Upgrade Subscription',
	// 		content: 'Are you sure you want to upgrade this subscription to yearly?',
	// 		okText: 'Yes',
	// 		okType: 'danger',
	// 		cancelText: 'No',
	// 		onOk() {
	// 			upgradeSubscription(id);
	// 			Modal.success({
	// 				title: 'Upgrade Subscription',
	// 				content: `Please make sure to cancel the previous monthly subscriptions from your paypal account.Please wait while we transfer you to the paypal payments page`
	// 			});
	// 			// message.loading("Payment in progress..", 0);
	// 		},
	// 		onCancel() {
	// 			console.log('Cancel');
	// 		}
	// 	});
	// };

	const uploadOrder = id => {
		console.log(id);
		setStateData({
			...StateData,
			ID: id
		});
		console.log(StateData);
		showModal(id);
	};

	// const handleStateChanges = () =>{
	// 	setStateData({
	// 		...StateData,

	// 	})
	// }



	const Cancelledcolumns = [
		{
			title: 'Order ID',
			dataIndex: 'id',
			key: 'id'
		},
		{
			title: 'App Name',
			dataIndex: 'appName',
			key: 'appName'
		},
		{
			title: 'Subscription Type',
			dataIndex: 'packType',
			key: 'packType'
		},
		{
			title: 'Coupon Used',
			dataIndex: 'coupon_code',
			key: 'coupon_code'
		},
		{
			title: 'Date',
			dataIndex: 'start_date',
			key: 'start_date'
		},
		{
			title: 'Amount',
			dataIndex: 'total_price',
			key: 'total_price'
		},
		{
			title: 'Status',
			dataIndex: 'status',
			key: 'status'
		}
	];

	const Pendingcolumns = [
		{
			title: 'Order ID',
			dataIndex: 'id',
			key: 'id'
		},
		{
			title: 'App Name',
			dataIndex: 'appName',
			key: 'appName'
		},
		{
			title: 'Subscription Type',
			dataIndex: 'packType',
			key: 'packType'
		},
		{
			title: 'Coupon Used',
			dataIndex: 'coupon_code',
			key: 'coupon_code'
		},
		{
			title: 'Date',
			dataIndex: 'start_date',
			key: 'start_date'
		},
		{
			title: 'Amount',
			dataIndex: 'total_price',
			key: 'total_price'
		},
		{
			title: 'Status',
			dataIndex: 'status',
			key: 'status'
		},

		{
			title: 'Action',
			key: 'action',
			render: (text, order) => (
				<span>
					{order && order.statusPurpose !== 'Cancellation' ? (
						<Fragment>

							{order.hasReciepts === false ?
								<Tooltip title="Upload Receipt">
									<Button
										type="dashed"
										shape="circle"
										onClick={() => uploadOrder(order.id)}
									>
										<Icon type="cloud-upload" />
									</Button>&nbsp;</Tooltip> : <Tooltip title="Invoice Uploaded">
									<Button
										type="dashed"
										shape="circle"
									// onClick={() => uploadOrder(order.id)}
									>
										<Icon type="check" />
									</Button>&nbsp;</Tooltip>}


							<Tooltip title="Cancel Order">
								<Button
									type="dashed"
									shape="circle"
									onClick={() => cancelOrder(order.id)}
								>
									<Icon type="close" />
								</Button>
							</Tooltip>
						</Fragment>
					) : (
							''
						)}
				</span>
			)
		}
	];
	const ActiveColumns = [
		{
			title: 'Order ID',
			dataIndex: 'id',
			key: 'id'
		},
		{
			title: 'App Name',
			dataIndex: 'appName',
			key: 'appName'
		},
		{
			title: 'Subscription Type',
			dataIndex: 'packType',
			key: 'packType'
		},
		{
			title: 'Coupon Used',
			dataIndex: 'coupon_code',
			key: 'coupon_code'
		},
		{
			title: 'Date',
			dataIndex: 'start_date',
			key: 'start_date'
		},
		{
			title: 'Amount',
			dataIndex: 'total_price',
			key: 'total_price'
		},
		{
			title: 'Status',
			dataIndex: 'state',
			key: 'state'
		},

		{
			title: 'Action',
			key: 'action',
			render: (text, order) => (
				<span>
					{/* {order && order.packType === 'Month' ? (
						<Tooltip title="Upgrade Package">
							<Button
								type="dashed"
								shape="circle"
								// onClick={() => upgradeOrder(order.id)}
							>
								<Icon type="arrow-up" />
							</Button>
						</Tooltip>
					) : (
							''
						)} */}
					&nbsp;
					<Link to={`/my-orders/${order.id}`}>
						<Tooltip title="Invoice">
							<Button type="dashed" shape="circle">
								<Icon type="eye" />
							</Button>
						</Tooltip>
					</Link>
					&nbsp;
					<Tooltip title="Cancel Order">
						<Button
							type="dashed"
							shape="circle"
							onClick={() => cancelOrder(order.id)}
						>
							<Icon type="close" />
						</Button>
					</Tooltip>
				</span>
			)
		}
	];

	const headers = [
		{ label: 'order ID', key: 'id' },
		{ label: 'Subscription Type', key: 'packType' },
		{ label: 'App Name', key: 'appName' },
		{ label: 'Placed At', key: 'start_date' },
		{ label: 'Amount', key: 'total_price' }
		// { label: "status", Key: "status"}
	];

	const uploadButton = (
		<div>
			<Icon type={loading ? 'loading' : 'plus'} />
			<div className="ant-upload-text">Upload</div>
		</div>
	);
	// screenshot Upload
	async function getBase64SS(file) {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => resolve(reader.result);
			reader.onerror = error => reject(error);
		});
	}
	const handleSSCancel = () =>
		setStateData({ ...StateData, previewVisible: false });
	const handleRecieptChange = async ({ fileList }) => {
		setStateData({
			...StateData,
			reciepts: fileList.map(arg => {
				return {
					name: arg.originFileObj.name,
					uid: arg.originFileObj.uid,
					url: arg.response && arg.response.locationArray[0],
					status: arg.status,
					originFileObj: {
						name: arg.originFileObj.name,
						uid: arg.originFileObj.uid,
						url: arg.response && arg.response.locationArray[0]
					}
				};
			})
		});
	};
	const handlePreview = async file => {
		if (!file.url && !file.preview) {
			file.preview = await getBase64SS(file.originFileObj);
		}
		setStateData({
			...StateData,
			previewImage: file.url || file.preview,
			previewVisible: true
		});
	};

	const handleSubmit = () => {
		uploadReceipt(StateData);
		Modal.success({
			title: 'Upload Success',
			content: (
				<div>
					Reciept was uploaded successfully.
					<br />
					Our Team will verify the payment and get back to you as soon as
					possible.
					<br />
					Thank you for your patience.
				</div>
			)
		});
		setStateData({
			...StateData,
			visible: false,
			recieptRecived: !(recieptRecived)
		});
	};
	const handleCancel = () => {
		setStateData({
			...StateData,
			visible: false
		});
	};


	return loading && orders === null ? (
		<Fragment>Spinner</Fragment>
	) : (
			<Fragment>
				<ScrollTop></ScrollTop>
				<Hero Title="My Orders" />
				<Tabs defaultActiveKey="1" className="Order-list-tabs">
					<TabPane tab={<span>Active Orders</span>} key="1">
						<Card
							title="Active Orders"
							className="order-table-card"
							extra={
								<CSVLink data={ActiveOrderData} headers={headers}>
									<Button type="dashed">Export CSV</Button>
								</CSVLink>
							}
							style={{
								marginTop: '24px',
								marginBottom: '24px',
								marginLeft: '15px',
								marginRight: '15px'
							}}
						>
							<Table columns={ActiveColumns} dataSource={ActiveOrderData} />
						</Card>
					</TabPane>
					<TabPane tab={<span>Pending Orders</span>} key="2">
						<Card
							title="Pending Orders"
							className="order-table-card"
							extra={
								<CSVLink data={PendingOrderData} headers={headers}>
									<Button type="dashed">Export CSV</Button>
								</CSVLink>
							}
							style={{
								marginTop: '24px',
								marginBottom: '24px',
								marginLeft: '15px',
								marginRight: '15px'
							}}
						>
							<Table columns={Pendingcolumns} dataSource={PendingOrderData} />
						</Card>
					</TabPane>
					<TabPane tab={<span>Cancelled Orders</span>} key="3">
						<Card
							title="Cancelled Orders"
							className="order-table-card"
							extra={
								<CSVLink data={CancelledOrderData} headers={headers}>
									<Button type="dashed">Export CSV</Button>
								</CSVLink>
							}
							style={{
								marginTop: '24px',
								marginBottom: '24px',
								marginLeft: '15px',
								marginRight: '15px'
							}}
						>
							<Table columns={Cancelledcolumns} dataSource={CancelledOrderData} />
						</Card>
					</TabPane>
				</Tabs>

				{/* Modal */}

				<Modal
					visible={StateData.visible}
					title="Reciept Upload"

					onCancel={handleCancel}
					footer={[
						<Button key="back" onClick={handleCancel}>
							Cancel
					</Button>,
						<Button key="submit" type="primary" onClick={handleSubmit}>
							Submit
					</Button>
					]}
				>
					<Upload
						action="http://localhost:5000/upload/reciept"
						listType="picture-card"
						reciept={StateData.reciept}
						onPreview={handlePreview}
						onChange={handleRecieptChange}
						multiple
						name="reciept"
					>
						{StateData.reciept && StateData.reciept >= 1 ? null : uploadButton}
						{}
					</Upload>
					<Modal visible={previewVisible} footer={null} onCancel={handleSSCancel}>
						<img alt="example" style={{ width: '100%' }} src={previewImage} />
					</Modal>
				</Modal>

				{/* Modal */}
			</Fragment>
		);
};

Orders.propTypes = {
	getUserOrders: PropTypes.func.isRequired,
	orders: PropTypes.object.isRequired,
	loading: PropTypes.bool,
	cancelSubscription: PropTypes.func.isRequired,
	upgradeSubscription: PropTypes.func.isRequired,
	uploadReceipt: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
	orders: state.orders.orders,
	loading: state.orders.loading,
});

export default connect(
	mapStateToProps,
	{ getUserOrders, cancelSubscription, upgradeSubscription, uploadReceipt }
)(Orders);
