import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import ReCAPTCHA from "react-google-recaptcha";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { CountryDropdown } from "react-country-region-selector";
import { Redirect } from "react-router-dom";
import { loadApplication } from "../../actions/application.action";
import {Layout,Row,Col,Typography,Select,Skeleton,Modal,Button,Icon,Form,Input} from "antd";
import Logo from "../../assets/img/brand/logo icon.png";
import { requestDemo } from "../../actions/contact.action";
import { setAlert } from "../../actions/alert.action.jsx";
import PropTypes from "prop-types";
import ReactPlayer from "react-player";
import VideoBanner from "../../assets/img/video-banner.png";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';


const { Footer } = Layout;
const { Title, Text } = Typography;
const recaptchaRef = React.createRef();

const FooterComp = ({
  requestDemo,
  auth: { loading, user },
  setAlert,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  loadApplication,
  Applications,
  direction
}) => {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { Option } = Select;
  const [formData, setFormData] = useState({
    isSubmitted: false,
    SwitchSelected: "",
    HumanVarification: false,
    faqSwitchSelected: ""
  });

  const {
    country,
    SwitchSelected,
    HumanVarification,
    faqSwitchSelected
  } = formData;



  const { t } = useTranslation();


  const [state, setState] = useState({
    visible: false,
    PlayerMute: false,
  });

  const {
    Language
  } = state;

  useEffect(() => {
    loadApplication();

    // console.log("Applications ............", Applications);

    if (localStorage.getItem('lng')) {
      setState({
        ...state,
        Language: localStorage.getItem('lng')
      })
    } else {
      localStorage.setItem('lng', 'EN');
      setState({
        ...state,
        Language: 'EN'
      })
    }

    if (localStorage.getItem('lng') === 'AR') {
      // layoutDirection = 'rtl';
      i18next.changeLanguage('AR')
    } else if (localStorage.getItem('lng') === 'EN') {
      // layoutDirection = 'ltr';
      i18next.changeLanguage('EN')
    }

  }, []);


  // language change


  const LanguageChanged = (lang) => {
    console.log(lang);
    localStorage.setItem('lng', lang)

    setState({
      ...state,
      Language: localStorage.getItem('lng')
    })
    window.location.reload()
  }


  // Google reCAPCHA

  const onCapchaChange = e => {
    console.log(e);
    setFormData({
      ...formData,
      HumanVarification: true
    });
  };

  const onExpired = () => {
    console.log("expired");
    setFormData({
      ...formData,
      HumanVarification: false
    });
  };

  // End Of Google reCAPCHA

  // Forms

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };
  const handleChange = e => {
    setFormData({ ...formData, category: e });
  };
  const handleAppChange = e => {
    setFormData({ ...formData, app_name: e });
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!HumanVarification) {
        console.log("You are BOT");
        setAlert("Please Verify reCAPTCHA", "danger");
      } else if (!err) {
        setFormData({
          ...formData
        });
        requestDemo(formData);
        setFormData({
          ...formData,
          isSubmitted: true
        });
      }
    });
  };

  // Redirect if contact form is submitted
  if (formData.isSubmitted) {
    return <Redirect to="/contactUs/call-back-confirmation" />;
  }

  // End Of Forms

  // Video Modal

  const showModal = () => {
    setState({
      ...state,
      PlayerMute: false,
      visible: true
    });
  };

  const handleOk = e => {
    console.log(e);
    setState({
      ...state,
      visible: false
    });
  };

  const handleCancel = e => {
    console.log(e);
    setState({
      ...state,
      visible: false,
      PlayerMute: true
    });
  };

  // End Of Video Modal

  const generalFooter = (
    <Fragment>
      <section
        style={{
          background: "#333",
          paddingTop: "80px"
        }}
      >
        <p
          style={{
            fontSize: "35px",
            textAlign: "center",
            margin: "0px auto 60px auto",
            color: "#fff"
          }}
        >
          {t('Footer.GetinTouchwithus')}
        </p>

        <div
          id="GetInTouchForm"
          className="video-footer-container"
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            boxShadow: "0px 0px 10px 5px #17171717"
          }}
        >
          <Row
            // gutter={16}
            style={{ marginLeft: "0px", marginRight: "0px" }}
            type="flex"
            justify="center"
            align="middle"
          >
            <Col lg={12} md={24} className="tab-d-none" style={{ background: "#53defd" }}>
              <div>
                <Button
                  className="video-play-button"
                  onClick={showModal}
                  type="primary"
                  shape="circle"
                  icon="caret-right"
                  size="large"
                />
                <img
                  width="100%"
                  height="100%"
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Others/paper+Plane.png"
                  alt=""
                />
              </div>

              <Modal
                title=""
                className="video-modal"
                visible={state.visible}
                onOk={handleOk}
                footer={null}
                onCancel={handleCancel}
              >
                <ReactPlayer
                  width="100%"
                  height="450px"
                  muted={state.PlayerMute}
                  url="https://www.youtube.com/watch?v=vGPzpVhyO4s"
                  className="react-player"
                  playing
                  light="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Others/VideoBannerNew.png"
                />
              </Modal>
            </Col>
            <Col lg={12} md={24}>
              <div>
                <Form onSubmit={e => onSubmit(e)} className="getin-touch-form">
                  <Row gutter={16}>
                    {direction === DIRECTIONS.LTR && <Fragment>

                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("name", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.Pleasetypeyourname')}`
                              }
                            ]
                          })(
                            <Input
                              placeholder={`${t('General.Name')}`}
                              name="name"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="user"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("email", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.Pleasetypeyouremail')}`
                              }
                            ]
                          })(
                            <Input
                              placeholder={`${t('General.Email')}`}
                              name="email"
                              type="email"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="mail"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("country", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.Pleasetypeyourcountry')}`
                              }
                            ]
                          })(
                            <CountryDropdown
                              blacklist={[
                                "AF",
                                "AO",
                                "DJ",
                                "GQ",
                                "ER",
                                "GA",
                                "IR",
                                "KG",
                                "LY",
                                "MD",
                                "NP",
                                "ST",
                                "SL",
                                "SD",
                                "SY",
                                "SR",
                                "TM",
                                "VE",
                                "ZW",
                                "IL"
                              ]}
                              name="country"
                              valueType="full"
                              defaultOptionLabel={`${t('General.Country')}`}
                              style={{ marginTop: "12px", height: "32px" }}
                              onChange={val => selectCountry(val)}
                            />
                          )}
                        </Form.Item>{" "}
                      </Col>
                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("mobile", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.PleaseentermobileNumber')}`
                              }
                            ]
                          })(
                            <PhoneInput
                              style={{ marginTop: "10px" }}
                              placeholder={`${t('General.PleaseentermobileNumber')}`}
                              name="mobile"
                              inputComponent={SmartInput}
                              onChange={e => onPhoneChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("interested_in", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.PleaseselectProduct')}`
                              }
                            ]
                          })(
                            <Select
                              placeholder={`${t('General.product')}`}
                              onChange={e => handleChange(e)}
                            >
                              <Option value="singleVendor">
                                {t('header.ecommerce')}
                              </Option>
                              <Option value="multiVendor">
                                {t('header.multivendor')}
                              </Option>
                              {/* <Option value="booking">
                            {t('Footer.BookingApps')}
                              </Option> */}
                              {/* <Option value="Facility">
                                {t('Footer.FacilityManagementApps')}
                              </Option> */}
                              {/* <Option value="Facility">
                                {t('Footer.FacilityManagementApps')}
                              </Option> */}
                              <Option value="Restaurant">
                                {t('Footer.RestaurantApp')}
                              </Option>
                              <Option value="UPappBot">
                                {t('Footer.UPappbot')}
                              </Option>
                            </Select>
                          )}
                        </Form.Item>
                      </Col>
                      {/* <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("app_name", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.PleaseSelectApp')}`
                              }
                            ]
                          })(
                            <Select
                              placeholder={`${t('General.PleaseSelectApp')}`}
                              onChange={e => handleAppChange(e)}
                            >

                              {Applications &&
                                Applications.map(app => {
                                  if (app.fields.type === 'Ecommerce') {
                                    return (
                                      <Option value={app.fields.name}>
                                        {app.fields.name}
                                      </Option>
                                    );
                                  }

                                })}
                              <Option value="Other">Others</Option>
                            </Select>
                          )}
                        </Form.Item>
                      </Col> */}
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("business_name", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.BusinessName')}`
                              }
                            ]
                          })(
                            <Input
                              placeholder={`${t('General.BusinessName')}`}
                              name={'business_name'}
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("description", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.AboutYourBusiness')}`
                              }
                            ]
                          })(
                            <TextArea
                              name="description"
                              onChange={e => onChange(e)}
                              placeholder={`${t('General.AboutYourBusiness')}`}
                              autosize={{ minRows: 2, maxRows: 6 }}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          <ReCAPTCHA
                            ref={recaptchaRef}
                            onExpired={onExpired}
                            sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
                            onChange={e => onCapchaChange(e)}
                          />
                        </Form.Item>
                      </Col>

                    </Fragment>
                    }
                    {direction === DIRECTIONS.RTL && <Fragment>


                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("email", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.Pleasetypeyouremail')}`
                              }
                            ]
                          })(
                            <Input
                              placeholder={`${t('General.Email')}`}
                              name="email"
                              type="email"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="mail"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("name", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.Pleasetypeyourname')}`
                              }
                            ]
                          })(
                            <Input
                              placeholder={`${t('General.Name')}`}
                              name="name"
                              onChange={e => onChange(e)}
                              prefix={
                                <Icon
                                  type="user"
                                  style={{ color: "rgba(0,0,0,.25)" }}
                                />
                              }
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("mobile", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.PleaseentermobileNumber')}`
                              }
                            ]
                          })(
                            <PhoneInput
                              style={{ marginTop: "10px" }}
                              placeholder={`${t('General.PleaseentermobileNumber')}`}
                              name="mobile"
                              inputComponent={SmartInput}
                              onChange={e => onPhoneChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={12} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("country", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.Pleasetypeyourcountry')}`
                              }
                            ]
                          })(
                            <CountryDropdown
                              blacklist={[
                                "AF",
                                "AO",
                                "DJ",
                                "GQ",
                                "ER",
                                "GA",
                                "IR",
                                "KG",
                                "LY",
                                "MD",
                                "NP",
                                "ST",
                                "SL",
                                "SD",
                                "SY",
                                "SR",
                                "TM",
                                "VE",
                                "ZW",
                                "IL"
                              ]}
                              name="country"
                              valueType="full"
                              defaultOptionLabel={`${t('General.Country')}`}
                              style={{ marginTop: "12px", height: "32px" }}
                              onChange={val => selectCountry(val)}
                            />
                          )}
                        </Form.Item>{" "}
                      </Col>
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("app_name", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.PleaseSelectApp')}`
                              }
                            ]
                          })(
                            <Select
                              placeholder={`${t('General.PleaseSelectApp')}`}
                              onChange={e => handleAppChange(e)}
                            >
                              {Applications &&
                                Applications.map(app => {
                                  return (
                                    <Option value={app.fields.name}>
                                      {app.fields.name}
                                    </Option>
                                  );
                                })}
                              <Option value="Other">Others</Option>
                            </Select>
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("interested_in", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.PleaseselectProduct')}`
                              }
                            ]
                          })(
                            <Select
                              placeholder={`${t('General.product')}`}
                              onChange={e => handleChange(e)}
                            >
                              {/* <Option value="ecommerce">
                                {t('Footer.eCommerceApps')}
                              </Option> */}
                              <Option value="singleVendor">
                                {t('header.ecommerce')}
                              </Option>
                              <Option value="multiVendor">
                                {t('header.multivendor')}
                              </Option>
                              {/* <Option value="booking">
                                  {t('Footer.BookingApps')}
                                  </Option> */}
                              {/* <Option value="Facility">
                                {t('Footer.FacilityManagementApps')}
                              </Option> */}
                              <Option value="Restaurant">
                                {t('Footer.RestaurantApp')}
                              </Option>
                              <Option value="UPappBot">
                                {t('Footer.UPappbot')}
                              </Option>
                            </Select>
                          )}
                        </Form.Item>
                      </Col>

                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("description", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.AboutYourBusiness')}`
                              }
                            ]
                          })(
                            <TextArea
                              name="description"
                              onChange={e => onChange(e)}
                              placeholder={`${t('General.AboutYourBusiness')}`}
                              autosize={{ minRows: 2, maxRows: 6 }}
                            />
                          )}
                        </Form.Item>
                      </Col>
                      <Col lg={24} xs={24}>
                        <Form.Item>
                          {getFieldDecorator("business_name", {
                            rules: [
                              {
                                required: true,
                                message: `${t('General.BusinessName')}`
                              }
                            ]
                          })(
                            <Input
                              placeholder={`${t('General.BusinessName')}`}
                              name={`${t('General.BusinessName')}`}
                              onChange={e => onChange(e)}
                            />
                          )}
                        </Form.Item>
                      </Col>

                      <Col lg={24} xs={24}>
                        <Form.Item>
                          <ReCAPTCHA
                            ref={recaptchaRef}
                            onExpired={onExpired}
                            sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
                            onChange={e => onCapchaChange(e)}
                          />
                        </Form.Item>
                      </Col>

                    </Fragment>
                    }
                    <Col lg={24} xs={24}>
                      {direction === DIRECTIONS.LTR && <Fragment>
                        <Form.Item style={{ textAlign: "left" }}>
                          <Button type="primary" htmlType="submit" className="UAFprimaryButton1">{t('General.Submit')}</Button>
                        </Form.Item>
                      </Fragment>
                      }
                      {direction === DIRECTIONS.RTL && <Fragment>
                        <Form.Item style={{ textAlign: "right", width: "100%", float: "right" }}>
                          <Button type="primary" htmlType="submit" className="UAFprimaryButton1">{t('General.Submit')}</Button>
                        </Form.Item>
                      </Fragment>
                      }
                    </Col>
                  </Row>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </section>
    </Fragment>
  );
  const adminFooter = (
    <Footer className="admin-footer">
      <Row gutter={16}>
        <Col xs={24} lg={12} style={{ textAlign: "right" }}>
          <Link to="/cms/admin/dashboard">upappfactory</Link> ©2020 all rights
          are reserved
        </Col>
      </Row>
    </Footer>
  );
  return loading && user === null ? (
    <Fragment>
      <Skeleton
        title={true}
        paragraph={true}
        loading={loading}
        rows="1"
        active
      ></Skeleton>
    </Fragment>
  ) : (
      <Fragment>
        {user && user.role === "admin" || user && user.role == "agency" ? adminFooter : generalFooter}
      </Fragment>
    );
};

const ContactUsForm = Form.create({ name: "Get_in_Touch" })(FooterComp);

ContactUsForm.prototypes = {
  loadApplication: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  auth: PropTypes.object.isRequired,
  requestDemo: PropTypes.func.isRequired,
  setAlert: PropTypes.func.isRequired,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  auth: state.auth
});

export default connect(mapStateToProps, { requestDemo, loadApplication })(
  withDirection(ContactUsForm)
);
