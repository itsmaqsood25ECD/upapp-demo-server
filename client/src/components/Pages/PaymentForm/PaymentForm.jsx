import React from 'react'
import PropTypes from 'prop-types'
import {
    Form,
    Input,
    Tooltip,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    AutoComplete,
} from antd;

const PaymentForm = ({form: { getFieldDecorator, validateFields, setFieldsValue }} ) => {

    const onSubmit = () =>{

    }

    
    return (
        <div>
            	<Form onSubmit={e => onSubmit(e)} className="login-form">
								<Form.Item>
									{getFieldDecorator("encRequest", {
										rules: [
											{ required: true, message: "Please input encRequest" }
										]
									})(
										<Input
											onChange={e => onChange(e)}
											name="encRequest"
											
											placeholder="EmaencRequest"
										/>
									)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator("accessCode", {
										rules: [
											{ required: true, message: "Please input your accessCode!" }
										]
									})(
										<Input
											onChange={e => onChange(e)}
											type="text"
											name="accessCode"
											prefix={
												
											}
											placeholder="accessCode"
										/>
									)}
								</Form.Item>
								<Form.Item>
								
									<Button
										type="primary"
										htmlType="submit"
										className="login-form-button"
									>
									Submit
								</Button>
								</Form.Item>
							</Form>
						
        </div>
    )
}

const CheckoutPaymentForm = Form.create({ name: "Payment_Form" })(PaymentForm);

CheckoutPaymentForm.propTypes = {

}

export default CheckoutPaymentForm;
