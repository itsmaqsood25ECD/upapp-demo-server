/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor } from "antd";
import CurrencyFormat from "react-currency-format";
import "../../assets/css/packagePrice.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';
const { TabPane } = Tabs;
const { Title } = Typography;
const { Link } = Anchor;
class BotPriceVertical extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ProfessionalPrice: this.props.ProfessionalPrice,
            BusinessPrice: this.props.BusinessPrice,
            currency: this.props.currency,
        };
    }

    componentWillReceiveProps(nextApp) {
        this.setState({
            ProfessionalPrice: nextApp.ProfessionalPrice,
            BusinessPrice: nextApp.BusinessPrice,
            currency: nextApp.currency
        });
    }


    render() {
        const { t } = this.props;

        return (
            <div className="UAF-container-fluid">
                <Row type="flex" className="botHome_contentsection_row12">
                    <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                        <h1 className="botHome_s10_title_full">
                            {t('pricing.Boost')}
                        </h1>
                        <p className="botHome_contentsection_text_full">
                            {t('pricing.noCommitment')}
                            <br />
                            {t('pricing.changeCancel')}
                        </p>
                    </Col>
                    <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_pricing_mainOuter">
                        <Row gutter={16}>
                            <Col xs={24} md={8} lg={8}>
                                <div className="pack-card Basic">
                                    <div className="Pack-identifier Basic">
                                        <p>
                                            <span>{t('pricing.basic')}</span>
                                        </p>
                                    </div>
                                    <div className="package-img-cont">
                                        <img className="package-img botHome_s10_PricingIcon" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/pr1.svg" />
                                    </div>
                                    <div className="botHome_s10_PricingTextSection">
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text1" >{t('bot.pricing.basic.line1')}</p>
                                        <hr className="s10_pricing_text_line" />
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text2" >{t('bot.pricing.basic.line2')}</p>
                                        <hr className="s10_pricing_text_line" />
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text3" >{t('bot.pricing.basic.line3')}<b>{t('bot.pricing.basic.line4')}</b></p>
                                    </div>
                                    <div className="s10_currency_outer">
                                        <Title level={2} class="light-text s10_currency_section">&nbsp;
                                        <span className="PriceNum">
                                                {/* <sup className="currency s10_currency">USD</sup> */}
                                                <sup className="currency s10_currency">{this.state.currency}</sup>
                                                <CurrencyFormat value={Number(this.state.ProfessionalPrice)} displayType={"text"} thousandSeparator={true} />
                                                <sub className="per-month-tag s10_monthTag">/mo</sub>
                                            </span>
                                        </Title>
                                    </div>
                                    <div>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" className="s10_monthTag_tile_btn" title={
                                                <Button size={"large"} type="primary" className="botPricing_btn">
                                                    {t('pricing.getStartedFree')}
                                                </Button>} />
                                        </Anchor>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} md={8} lg={8}>
                                <div className="pack-card ultra" style={{ marginTop: "-10px", padding: "40px 20px" }}>
                                    <div className="Pack-identifier ultra">
                                        <p>
                                            <span>{t('pricing.PROFESSIONAL')}</span>
                                        </p>
                                    </div>
                                    <div className="package-img-cont">
                                        <img className="package-img botHome_s10_PricingIcon" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/pr2.png" />
                                    </div>
                                    <div className="botHome_s10_PricingTextSection">
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text1" ><b>{t('bot.pricing.professional.line1')}</b><br />{t('bot.pricing.professional.line2')} {t('bot.pricing.professional.line3')}</p>
                                        <hr className="s10_pricing_text_line" />
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text2" ><b>{t('bot.pricing.professional.line4')}<br />{t('bot.pricing.professional.line5')}</b><br />{t('bot.pricing.professional.line6')}</p>
                                        <hr className="s10_pricing_text_line" />
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text3" >{t('bot.pricing.professional.line7')}<b>{t('bot.pricing.professional.line8')}</b></p>
                                    </div>
                                    <div className="s10_currency_outer">
                                        <Title level={2} class="light-text s10_currency_section">&nbsp;
                                        <span className="PriceNum">
                                                <sup className="currency s10_currency">{this.state.currency}</sup>
                                                <CurrencyFormat value={Number(this.state.BusinessPrice)} displayType={"text"} thousandSeparator={true} />
                                                <sub className="per-month-tag s10_monthTag">/mo</sub>
                                            </span>
                                        </Title>
                                    </div>
                                    <div>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" className="s10_monthTag_tile_btn" title={
                                                <Button size={"large"} type="primary" className="botPricing_btn">
                                                    {t('pricing.getStartedFree')}
                                                </Button>} />
                                        </Anchor>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} md={8} lg={8}>
                                <div className="pack-card Premium">
                                    <div className="Pack-identifier Premium">
                                        <p>
                                            <span>{t('pricing.ADVANCED')}</span>
                                        </p>
                                    </div>
                                    <div className="package-img-cont">
                                        <img className="package-img botHome_s10_PricingIcon" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/pr3.svg" />
                                    </div>
                                    <div className="botHome_s10_PricingTextSection">
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text1" >{t('bot.pricing.advance.line1')}</p>
                                        <hr className="s10_pricing_text_line" />
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text2" ><b>{t('bot.pricing.advance.line2')}<br />{t('bot.pricing.advance.line3')}</b></p>
                                        <hr className="s10_pricing_text_line" />
                                        <p className="botHome_s10_PricingTs_text s10_PricingTs_text3" >{t('bot.pricing.advance.line4')}</p>
                                    </div>
                                    <div className="s10_currency_outer">
                                        <Title level={2} class="light-text s10_currency_section">&nbsp;
                                        <span className="PriceNum">
                                                <sup className="currency s10_currency">{this.state.currency}</sup>
                                                {t('pricing.custom')}<CurrencyFormat value="" displayType={"text"} thousandSeparator={true} />
                                                <sub className="per-month-tag s10_monthTag">/mo</sub>
                                            </span>
                                        </Title>
                                    </div>
                                    <div>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" className="s10_monthTag_tile_btn" title={
                                                <Button size={"large"} type="primary" className="botPricing_btn">
                                                    {t('pricing.talkToUs')}
                                                </Button>} />
                                        </Anchor>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withTranslation()(BotPriceVertical);
