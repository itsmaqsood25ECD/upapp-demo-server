import React, { useState, useEffect, Fragment } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "antd/dist/antd.css";
import { Link, Redirect } from "react-router-dom";
import { List, Avatar, Button, Skeleton, Card, Icon } from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import ScrollTop from "../../ScrollTop";



let count;
const { Meta } = Card;

const WebsiteThemes = ({match, user}) => {

  const { t } = useTranslation();

  const [state, setState] = useState({
    initLoading: true,
    loading: false,
    data: [],
    list: []
  })

  const { initLoading, loading, list } = state;


useEffect(() => {
console.log(match)
    setState({
      initLoading: false,
      data: Newdata.filter(item =>{
        return match && match.params.themeID == item.id;
      }),
      list: []
    });
  }, [])

  const Newdata = [
    {
      title: `${t("ecommercePage.Demo")}1`,
      id:"EC01",
      url: "https://ecomdemo3.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-3.png"
    },
    {
      title: `${t("ecommercePage.Demo")}2`,
      id:"EC02",
      url: "https://ecomdemo10.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-10.png"
    },
    {
      title: `${t("ecommercePage.Demo")}3`,
      id:"EC03",
      url: "https://ecomdemo17.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-17.png"
    },
    {
      title: `${t("ecommercePage.Demo")}4`,
      id:"EC04",
      url: "https://ecomdemo21.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-21.png"
    },
    {
      title: `${t("ecommercePage.Demo")}5`,
      id:"EC05",
      url: "https://ecomdemo23.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-23.png"
    },
  ];


  if (user === null) {
    return <Redirect to="/login"></Redirect>;
  } return (
   <Fragment>
     <ScrollTop>

     </ScrollTop>
<div style={{border:"2px Solid #000",borderLeft:"none",borderRight:"none",background:"#000",marginTop:"85px",height:"50px",padding:"5px"}}>
<Link to={`/eCommerce-Single-Vendor-Platform/${match.params.Package}/${match.params.Plan}`}>
<Button type="primary" style={{background:'none',border:'none',float:"left",fontSize:"16px",color:"#d9d9d9"}}>
<Icon type="left" />
            Back
  </Button>
  </Link>
<Link to={`/eCommerce-Single-Vendor-Platform/${match.params.Package}/${match.params.Plan}/${match.params.themeID}/Checkout`}>
<Button type="primary" style={{background:'none',border:'none',float:"right",fontSize:"16px",color:"#d9d9d9"}}>
            Proceed To Checkout
            <Icon type="right" />
  </Button>
  </Link>
</div>
<div style={{ maxWidth: "1760px", margin: "0 auto" }}>
{console.log(state.data)}
<iframe className="holds-the-iframe" style={{width:"100%",height:"100Vh"}} src={state.data[0] && state.data[0].url} title={state.data[0] && state.data[0].title}></iframe>
</div>
   </Fragment>
  );
}

WebsiteThemes.prototypes = {
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps, {})(WebsiteThemes);
