import React, { Fragment, useState } from "react";
import { Icon, Card, Button, Typography, Row, Col, List, Spin, Input, Form, Anchor } from "antd";
import "antd/dist/antd.css";
import "./css/consultancy.css";
// import "./css/custom.css";
import "./css/main.css";
// import "./css/home.css";
// import "./css/homeNew.css";
import { Route } from "react-router-dom";
import CountUp from 'react-countup'
import ReactPlayer from "react-player";
// import { HashLink as Link } from 'react-router-hash-link';
import { NavLink } from "react-router-dom";
import moment from 'moment';
import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";

// img
import heroImg from "./img/hero.png"
import img1 from "./img/img1.png"
import img2 from "./img/img2.png"
import img3 from "./img/img3.png"
import img4 from "./img/img4.png"
import img5 from "./img/img5.png"
import img6 from "./img/img6.png"
import img7 from "./img/img7.png"
import img8 from "./img/img8.png"
import img91 from "./img/img91.png"
import img92 from "./img/img92.png"
import img10 from "./img/img10.png"
import img11 from "./img/img11.png"
import icon from "./img/icon.png"
import cs1 from "./img/cs1.png"
import cs2 from "./img/cs2.png"
import cs3 from "./img/cs3.png"
import cs4 from "./img/cs4.png"
import cs5 from "./img/cs5.png"
import cs6 from "./img/cs6.png"
import blg1 from "./img/blg1.png"
import blg2 from "./img/blg2.png"
import blg3 from "./img/blg3.png"
import cntctbg from "./img/cntctbg.png"
// Component Import
// import ConsultancyAboutUs from './aboutUs'
// import ConsultancyWhyUs from './whyUs'
// import ConsultancyConsultant from './consultant'
// import ConsultancyPricing from './pricing'
import ConsultancyContactUs from './contactUs'

const { Meta } = Card;
const Consultancy = (
  getRecentBlog,
  recentBlog,) => {
  function NextArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-next-btn"
        shape="circle"
      >
        <Icon type="arrow-right" />
      </Button>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-prev-btn"
        shape="circle"
      >
        <Icon type="arrow-left" />
      </Button>
    );
  }

  var settings = {
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    dots: false,
    // infinite: true,
    centerMode: true,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          // infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <React.Fragment>
      <div className="uaf_cp">
        {/* ===================
                    Start Landing  
                ===================  */}
        <div className="uaf_std_hero" style={{ marginTop: "0px" }}>
          <div className="UAF-container-fluid">
            <Row type="flex" justify="space-around" align="middle">
              <Col lg={{ span: 12, order: 1 }} className="alignSelfCenter">
                <h1 className="uaf_hero_title1">studio</h1>
                <hr className="uaf_hero_line1" />
                <div className="uaf_hero_content">
                  <h3 className="uaf_hero_content_title">Have an idea?<br />We have the tools you need.</h3>
                  <p className="uaf_hero_content_text">Entrepreneurs start with an idea, think it through, and after endless brain-racking, come up with a plan. In the world of digital media that keeps ever evolving, you need a team that can think a complete execution for and through customization, bring your plan to life.</p>
                </div>
              </Col>
              <Col lg={{ span: 12, order: 2 }}>
                <img src={heroImg} className="heroImage" />
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Landing  
                ===================  */}
        {/* ===================
                    Start button section  
                ===================  */}
        <div className="uaf_std_btn_row">
          <div className="">
            <Row type="flex" align="middle">
              <Col lg={{ span: 4, order: 1 }} className="alignSelfCenter uaf_std_btnrow_btnCol">
                <a href="#ideation">
                  Ideation
                </a>
              </Col>
              <Col lg={{ span: 4, order: 2 }} className="alignSelfCenter uaf_std_btnrow_btnCol">
                <a href="#brainstorm">
                  Brainstorm
                </a>
              </Col>
              <Col lg={{ span: 4, order: 3 }} className="alignSelfCenter uaf_std_btnrow_btnCol">
                <a href="#productDesign">
                  Product Design
                </a>
              </Col>
              <Col lg={{ span: 4, order: 4 }} className="alignSelfCenter uaf_std_btnrow_btnCol">
                <a href="#development">
                  Development
                </a>
              </Col>
              <Col lg={{ span: 4, order: 5 }} className="alignSelfCenter uaf_std_btnrow_btnCol">
                <a href="#testing">
                  Testing
                </a>
              </Col>
              <Col lg={{ span: 4, order: 6 }} className="alignSelfCenter uaf_std_btnrow_btnCol">
                <a href="#launch">
                  Launch
                </a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End button section  
                ===================  */}
        {/* ===================
                    Start Section1  
                ===================  */}
        <div className="uaf_std_section1" id="ideation">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Ideation and Concept visualisation
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">Every entrepreneur believes wholly in their idea. Using years of experience, we help entrepreneurs flesh out their ideas by integrating product understanding and user experience across digital touch-points.</p>
              </Col>
              <Col lg={{ span: 16, order: 2 }} className="uaf_std_section_imgmain">
                <img src={img1} />
              </Col>
              <Col lg={{ span: 24, order: 3 }} className="uaf_std_section_cntnt">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 10, order: 1 }} className="uaf_std_section_left">
                    <h3>Kickstart your idea, with our experts</h3>
                    <p>Entrepreneurs start with an idea, think it through, and after endless brain-racking, come up with a plan. In the world of digital media that keeps ever evolving, you need a team that can think a complete execution for and through customization, bring your plan to life.</p>
                    <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
                  </Col>
                  <Col lg={{ span: 14, order: 2 }} className="uaf_std_section_right">
                    <img src={img2} />
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Conceptualisation</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>UX Research</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 3 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Possible Outcomes</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 4 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Expert Review</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="uaf_std_case_study">
            <Row type="flex" align="middle">
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_img" >
                <img src={cs1} />
              </Col>
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_cntnt">
                <h3>Case Study of Burger Garage</h3>
                <hr />
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section1  
                ===================  */}
        {/* ===================
                    Start Section2  
                ===================  */}
        <div className="uaf_std_section2" id="brainstorm">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Brainstorm
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">Your opinion and our group of experts, are here to make a lively outcome of your concept into a successful reality guiding throughout the process</p>
              </Col>
              <Col lg={{ span: 16, order: 2 }} className="uaf_std_section_imgmain">
                <img src={img3} />
              </Col>
              <Col lg={{ span: 24, order: 3 }} className="uaf_std_section_cntnt">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 10, order: 1 }} className="uaf_std_section_left">
                    <h3>Metalize your concept into picture</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                    <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
                  </Col>
                  <Col lg={{ span: 14, order: 2 }} className="uaf_std_section_right">
                    <img src={img4} />
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Generation</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Combination</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 3 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Evaluation</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 4 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Validation</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="uaf_std_case_study">
            <Row type="flex" align="middle">
              <Col lg={{ span: 12, order: 2 }} className="uaf_std_case_study_img" >
                <img src={cs2} />
              </Col>
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_cntnt">
                <h3>Case Study of Crowne Plaza</h3>
                <hr />
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section2  
                ===================  */}
        {/* ===================
                    Start Section3  
                ===================  */}
        <div className="uaf_std_section3" id="productDesign">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Product Design
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">Craft delightful user experiences for your digital products. Solve real problems and improve your vital business metrics through beautiful interfaces</p>
              </Col>
              <Col lg={{ span: 24, order: 2 }} className="uaf_std_section_imgmain">
                <img src={img5} style={{ maxHeight: "650px" }} />
              </Col>
              <Col lg={{ span: 24, order: 3 }} className="uaf_std_section_cntnt">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 10, order: 2 }} className="uaf_std_section_left" style={{ padding: "0", alignSelf: "flex-end", paddingRight: "5rem" }}>
                    <h3>The right design for the best<br />User Experience</h3>
                    <p>Today's entrepreneurs and business leaders know the days of designing in a vacuum are long gone. Organizations who don’t listen to users are bound to lose to competitors who do. Gain an edge, generate ideas and evaluate your product usability. Or you can let our team's experienced UI and UX experts do the work while you soak up insights in real-time.</p>
                    <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
                  </Col>
                  <Col lg={{ span: 14, order: 1 }} className="uaf_std_section_right">
                    <img src={img6} style={{ maxHeight: "500px", marginRight: "80px", float: "right" }} />
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>UI/UX consulting</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Web & Mobile UI Design</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 3 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>User Feedback</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 4 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Testing</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="uaf_std_case_study">
            <Row type="flex" align="middle">
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_img" >
                <img src={cs3} />
              </Col>
              <Col lg={{ span: 12, order: 2 }} className="uaf_std_case_study_cntnt">
                <h3>Case Study of Crowne Plaza</h3>
                <hr />
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section3  
                ===================  */}
        {/* ===================
                    Start Section4  
                ===================  */}
        <div className="uaf_std_section4" id="development">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Development
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">We have one of the best experts in the field of Product development having expertise in various software for all kinds of product developement</p>
              </Col>
              <Col lg={{ span: 20, order: 2 }} className="uaf_std_section_imgmain">
                <img src={img7} />
              </Col>
              <Col lg={{ span: 24, order: 3 }} className="uaf_std_section_cntnt">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 10, order: 1 }} className="uaf_std_section_left">
                    <h3>Web development into action</h3>
                    <p>Today's entrepreneurs and business leaders know the days of designing in a vacuum are long gone. Organizations who don’t listen to users are bound to lose to competitors who do. Gain an edge, generate ideas and evaluate your product usability. Or you can let our team's experienced UI and UX experts do the work while you soak up insights in real-time.</p>
                    <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
                  </Col>
                  <Col lg={{ span: 14, order: 2 }} className="uaf_std_section_right">
                    <img src={img8} />
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>React</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Node</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 3 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Android</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 4 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>CMS</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="uaf_std_case_study">
            <Row type="flex" align="middle">
              <Col lg={{ span: 12, order: 2 }} className="uaf_std_case_study_img" >
                <img src={cs4} />
              </Col>
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_cntnt">
                <h3>Case Study of Platinum Interiors</h3>
                <hr />
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section4  
                ===================  */}

        {/* ===================
                    Start Section5  
                ===================  */}
        <div className="uaf_std_section5" id="testing">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Testing
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">testing is the process of testing different aspects of user experience to determine the best way for a website and its elements to interact with its audience.<br /><br />It’s not dissimilar to running a brick-and-mortar store. You want to know which aisles attract the most customers, which shelf heights move the most product, and what types of signage convince customers to convert.</p>
              </Col>
              <Col lg={{ span: 24, order: 2 }} className="uaf_std_section_cntnt">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 10, order: 2 }} className="uaf_std_section_left" style={{ padding: "0" }} >
                    <Row>
                      <Col lg={{ span: 24, order: 1 }} className="uaf_std_section5_cntnt">
                        <h3>User testing and Software testing</h3>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.</p>
                        <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
                      </Col>
                      <Col lg={{ span: 24, order: 1 }} className="uaf_std_section5_cntnt">
                        <img src={img91} />
                      </Col>
                    </Row>
                  </Col>
                  <Col lg={{ span: 14, order: 1 }} className="uaf_std_section_right">
                    <img src={img92} style={{ maxHeight: "500px", marginRight: "80px", float: "right" }} />
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 16, order: 3 }} className="uaf_std_section_imgmain" style={{ paddingTop: "7rem" }}>
                <img src={img10} />
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="space-around">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Modern user testing</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Voice of customer</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 3 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Availability</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 4 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Aesthetics</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="uaf_std_case_study">
            <Row type="flex" align="middle">
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_img" >
                <img src={cs5} />
              </Col>
              <Col lg={{ span: 12, order: 2 }} className="uaf_std_case_study_cntnt">
                <h3>Case study of Fursahh</h3>
                <hr />
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section5  
                ===================  */}
        {/* ===================
                    Start Section6  
                ===================  */}
        <div className="uaf_std_section6" id="launch">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Launch
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">The Launch of the product is the most essential part of any project, includes the final stages of the whole experience, UPapp is a step forward by handling even the post launch bugs and other details are taken care of to give you a complete seamless process.</p>
              </Col>
              <Col lg={{ span: 20, order: 2 }} className="uaf_std_section_imgmain">
                <img src={img11} />
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="center">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Launch</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section_tile">
                    <div className="uaf_std_section_tile_img">
                      <img src={icon} />
                    </div>
                    <h3>Post-Launch</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="uaf_std_case_study">
            <Row type="flex" align="middle">
              <Col lg={{ span: 12, order: 2 }} className="uaf_std_case_study_img" >
                <img src={cs6} />
              </Col>
              <Col lg={{ span: 12, order: 1 }} className="uaf_std_case_study_cntnt">
                <h3>Case Study of Scan Zone</h3>
                <hr />
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est.</p>
                <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >Learn more <span>&#62;</span></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section6  
                ===================  */}
        {/* ===================
                    Start Section7  
                ===================  */}
        <div className="uaf_std_section7">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  Create a Killer Digital Presence with UPapp
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">Creating a product is a start but finding your spot in the market and sustaining business takes way more planning and strategic approach. UPapp team understands your needs, concerns, ideas and vision; then puts it to test.</p>
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row type="flex" justify="left">
                  <Col lg={{ span: 6, order: 1 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>1</h1>
                    </div>
                    <h3>Commercial Viability</h3>
                    <p>After understanding exactly what your business needs are, we put the strategy to test & ensure the results are been driven.</p>
                  </Col>
                  <Col lg={{ span: 6, order: 2 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>2</h1>
                    </div>
                    <h3>Sound Technology</h3>
                    <p>Our development team adds its values set to the strategy, helping us with expertise in Woo Commerce, .Net, React Native etc.</p>
                  </Col>
                  <Col lg={{ span: 6, order: 3 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>3</h1>
                    </div>
                    <h3>Customer Experience</h3>
                    <p>Customer experience is kept at the forefront and our team ensures that every touchpoint is pleasant for the customer.</p>
                  </Col>
                  <Col lg={{ span: 6, order: 4 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>4</h1>
                    </div>
                    <h3>Internet of Things</h3>
                    <p>Every platform, every lead form, and everything banner ad click will push the customer into an integrated, sales-driven system. The idea is, to create an opportunity at every click.</p>
                  </Col>
                  <Col lg={{ span: 6, order: 5 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>5</h1>
                    </div>
                    <h3>Process Automation</h3>
                    <p>Your online presence should have a personal feel. So, that your company doesn’t turn out lazy we automate most of your manual works, data collection, analysis, form fillings, notification etc.</p>
                  </Col>
                  <Col lg={{ span: 6, order: 6 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>6</h1>
                    </div>
                    <h3>AI & Machine Learning</h3>
                    <p>Customer service is of the highest priority. We aim to give your customers human experience with the brand, and so we ensure that every touchpoint delivers impeccable UI/UX.</p>
                  </Col>
                  <Col lg={{ span: 12, order: 7 }} className="uaf_std_section7_tile">
                    <div className="uaf_std_section_tile_No">
                      <h1>7</h1>
                    </div>
                    <h3>End to End Digital Transformation Consultancy</h3>
                    <p>If you already have a digital presence but are not happy with the results, let's join hands & turn your present structure into a well-oiled sales machine.</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section7  
                ===================  */}
        {/* ===================
                    Start Section8  
                ===================  */}
        <div className="uaf_std_section8">
          <div className="UAF-container-fluid">
            <h1>Let's Connect Together</h1>
            <a href="#">Learn more</a>
          </div>
        </div>
        {/* ===================
                    End Section8  
                ===================  */}
        {/* ===================
                    Start Section9  
                ===================  */}
        <div className="uaf_std_section9">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                  From the Blog and Insights
                </h3>
                <hr className="uaf_std_section_header_line" />
              </Col>
              <Col lg={{ span: 24, order: 4 }} className="uaf_std_section_tile_outer">
                <Row gutter={16} type="flex" justify="space-around" align="middle">
                  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 24 }} xl={{ span: 24 }}>
                    <Row gutter={16}>
                      {/* <Slider {...settings} style={{ padding: "20px 50px" }}>
                        {recentBlog && recentBlog.map((blog) =>
                          <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                            <NavLink to={`/blog/${blog.slug}`}>
                              <Card className="uaf_bp_card"
                                cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blog.featured_image} />}
                              >
                                <Meta description={<div>
                                  <p className="uaf_bp_card-desc"><span className="uaf_bp_card-desc-date">{moment(blog.date_addedd).format('ll')} | by</span><a href="javascript:void(0)" > {blog.author}</a></p>
                                  <p className="uaf_bp_card-desc-heading">{blog.title}</p>
                                </div>} />
                              </Card>
                            </NavLink>
                          </Col>
                        )
                        }
                      </Slider> */}
                      <Slider {...settings} style={{ padding: "20px 50px" }}>
                        <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                          <NavLink>
                            <Card className="uaf_bp_card uaf_std_card"
                              cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blg1} />}
                            >
                              <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">How much does an eCommerce App cost</p>
                                <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">June 24 2020, by </span>Harris Aslam</p>
                              </div>} />
                            </Card>
                          </NavLink>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                          <NavLink>
                            <Card className="uaf_bp_card uaf_std_card"
                              cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blg2} />}
                            >
                              <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">Would you like to see your eCommerce venture net</p>
                                <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">June 24 2020, by </span>Harris Aslam</p>
                              </div>} />
                            </Card>
                          </NavLink>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                          <NavLink>
                            <Card className="uaf_bp_card uaf_std_card"
                              cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blg3} />}
                            >
                              <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">11 Reasons how a mobile App can boost eCommerce</p>
                                <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">June 24 2020, by </span>Harris Aslam</p>
                              </div>} />
                            </Card>
                          </NavLink>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                          <NavLink>
                            <Card className="uaf_bp_card uaf_std_card"
                              cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blg1} />}
                            >
                              <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">How much does an eCommerce App cost</p>
                                <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">June 24 2020, by </span>Harris Aslam</p>
                              </div>} />
                            </Card>
                          </NavLink>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                          <NavLink>
                            <Card className="uaf_bp_card uaf_std_card"
                              cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blg2} />}
                            >
                              <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">Would you like to see your eCommerce venture net</p>
                                <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">June 24 2020, by </span>Harris Aslam</p>
                              </div>} />
                            </Card>
                          </NavLink>
                        </Col>
                        <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                          <NavLink>
                            <Card className="uaf_bp_card uaf_std_card"
                              cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blg3} />}
                            >
                              <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">11 Reasons how a mobile App can boost eCommerce</p>
                                <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">June 24 2020, by </span>Harris Aslam</p>
                              </div>} />
                            </Card>
                          </NavLink>
                        </Col>
                      </Slider>
                    </Row>
                  </Col>
                </Row>
                <a href="#"><h3>View More ></h3></a>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section9  
                ===================  */}
        {/* ===================
                    Start Section  
                ===================  */}
        <div className="uaf_std_section10">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 18, order: 1 }} className="uaf_std_section_header" style={{ margin: "0 auto" }}>
                <h3 style={{lineHeight:"1.2"}}>
                  Want to know more<br />Get in Touch
                </h3>
                <hr className="uaf_std_section_header_line" />
              </Col>
            </Row>
            <ConsultancyContactUs />
          </div>
        </div>
        {/* ===================
                    End Section  
                ===================  */}
        {/* ===================
                    Start About us  
                ===================  */}
        {/* <ConsultancyAboutUs /> */}
        {/* ===================
                    End About us  
                ===================  */}

        {/*  */}
      </div>
    </React.Fragment >
  );
};

export default Consultancy;
