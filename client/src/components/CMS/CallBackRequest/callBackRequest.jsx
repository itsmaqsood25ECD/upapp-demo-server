import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Allcontact } from "../../../actions/admin/request.management.action";
import { Table, Button, Input, Icon,Row ,Col, Modal, Descriptions } from "antd";
import PropTypes from "prop-types";
import { CSVLink } from "react-csv";
import { Card } from "antd";


let searchInput = React.createRef();
const CallBackRequest = ({ Allcontact, allcontact }) => {
  const [Data, setData] = useState({
    filteredInfo: null,
    sortedInfo: null,
    selectedRowKeys: [],
    data:[],
    visible:false,
    details:'',
  });

  const[xy, SetXy] = useState(0);

  let { filteredInfo, sortedInfo, data, visible, details } = Data;
  // const data = [];

  const filterCallBackData = () => {
    setData({
      ...Data,
      data: allcontact &&
      allcontact.filter((allcontact, index) => {
       return allcontact.type === "query"
      })

    })
  }

  useEffect(() => {
    Allcontact();
    filterCallBackData();
    console.log(data)
  }, [xy]);

const searchDataHandle = e => {
    console.log(e.target.value)
      if(e.target.value === ""){
        setData({
          ...Data,
          data: allcontact && allcontact.filter((allcontact, index) => {
           return allcontact.type === "query"
          })
      
        })
      } else {
        const searchData =  data && data.filter(s => {
          return (s.ticket_no.toString().toLowerCase().includes(
            (e.target.value).toLocaleLowerCase()
             )) || (s.name.toString().toLowerCase().includes(
              (e.target.value).toLocaleLowerCase()
               )) || (s.email.toString().toLowerCase().includes(
                (e.target.value).toLocaleLowerCase()
                 ))
         })
         setData({
          ...Data,
          data:searchData
        })
      }
}

console.log(data)


//  INFO AREA /////////////////////////////////

const MoreDetails = (text, record) => {
  showModal(text, record);
};

const showModal = (text, record) => {
  setData({
    ...Data,
    visible: true,
    details: record,

  });
};

const handleOk = e => {
  setData({
    ...Data,
    visible: false
  });
};

const handleCancel = e => {
  setData({
    ...Data,
    visible: false
  });
};

// END INFO AREA /////////////////////////////////


  // End Search column

  const handleChange = (pagination, filters, sorter) => {
    setData({
      ...Data,
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  sortedInfo = sortedInfo || {};
  filteredInfo = filteredInfo || {};
  const columns = [
    {
      title: "ID",
      width: 150,
      dataIndex: "UID",
      key: 0,
      sorter: (a, b) => a.UID.length - b.UID.length,
      sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order,
      render: (UID) => (
        <div  style={{width:"160px", wordWrap: 'break-word', wordBreak: 'break-word' }}>
          {UID}
        </div>
      ),
    },
    {
      title: "Ticket No",
      // width: 150,
      dataIndex: "ticket_no",
      key: 1,
      sorter: (a, b) => a.ticket_no.length - b.ticket_no.length,
      sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
    },
    {
      title: "name",
      dataIndex: "name",
      // width: 100,
      key: 2,
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
    },
    {
      title: "Email",
      dataIndex: "email",
      // width: 100,
      key: 3,
      sorter: (a, b) => a.email.length - b.email.length,
      sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
    },
    {
      title: "Mobile No",
      dataIndex: "mobile",
      // width: 50,
      key: 4,
      sorter: (a, b) => a.mobile.length - b.mobile.length,
      sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
    },
    {
			title: "Action",
			dataIndex: "description",
			// fixed: "right",
			key: 5,
			// width: 120,

			render: (text, record)  => (
				<div style={{textAlign:"center",width:"100%"}}>
					<Button
						type="dashed"
						size="small"
						onClick={() => MoreDetails(text, record)}
					>
						<Icon type="info-circle" />
					</Button>
          </div>
      )
    },
  ];
 

  return (
    <Fragment>
    
      <Card
            title="CallBack Requests"
            extra={
              <CSVLink data={data} filename={"call-back-request.csv"}>
                <Button type="dashed">Download CSV</Button>
              </CSVLink>
            }
          >
           <Input style={{width:"100%", maxWidth:"300px"}} onChange={searchDataHandle} placeholder="Search by ID, Ticket No, Name, Email"  />
                          <br/>
                          <br/>
            <Table
              // rowSelection={rowSelection}
              columns={columns}
              dataSource={data}
              onChange={handleChange}
              pagination={{ pageSizeOptions: ['10', '20', '30', '40'] }}
              // scroll={{ x: 750 }}
              // bordered
              // size="small" 
            />
          </Card>
          <Modal
				title="Details"
				visible={Data.visible}
				onOk={handleOk}
				onCancel={handleCancel}
			>

        <Descriptions
            title=""
            size="small"
            bordered
            column={{ xxl: 4, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          >
					<Descriptions.Item label="UID">
          {details.UID}
					</Descriptions.Item>
					<Descriptions.Item label="Name">
          {details.name}
					</Descriptions.Item>
          <Descriptions.Item label="Email">
          {details.email}
					</Descriptions.Item>
          <Descriptions.Item label="Business Name">
          {details.business_name}
					</Descriptions.Item>
          <Descriptions.Item label="Mobile">
          {details.mobile}
					</Descriptions.Item>
          <Descriptions.Item label="Budget">
          {details.budget}
					</Descriptions.Item>
          <Descriptions.Item label="Languages">
          {details && details.languages.map(lang =>{
            return lang + ","
          })}
					</Descriptions.Item>
          <Descriptions.Item label="Business Description">
          {details.description}
					</Descriptions.Item>
    </Descriptions>
        {console.log(details)}
      </Modal>
        
    </Fragment>
  );
};

CallBackRequest.propTypes = {
  Allcontact: PropTypes.func.isRequired,
  allcontact: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  allcontact: state.requestManagement.requests
});

export default connect(mapStateToProps, { Allcontact })(CallBackRequest);
