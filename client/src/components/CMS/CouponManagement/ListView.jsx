import React, { Component, Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import {
	Modal,
	Table,
	Button,
	Input,
	Icon,
	Tag,
	Divider,
	Row,
	Descriptions
} from "antd";
import { Link } from "react-router-dom";
import moment from "moment";
import PropTypes from "prop-types";
import {
	getCoupons,
	getCouponCode,
	deleteCouponCode
} from "../../../actions/admin/coupons.action";

let searchInput = React.createRef();

const CouponManagement = ({
	allcoupons: { allcoupons, loading },
	getCoupons,
	coupon_details,
	getCouponCode,
	deleteCouponCode
}) => {
	useEffect(() => {
		getCoupons();
	}, [loading]);

	const [Data, setData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: "",
		coupon_code: "",
		visible: false
	});

	let { filteredInfo, sortedInfo, selectedRowKeys } = Data;

	const data = [];

	allcoupons.map((allcoupons, index) => {
		let Ids = {
			key: allcoupons._id,
			code: allcoupons.coupon_code,
			couponType: allcoupons.general.discount_type,
			CouponAmount: allcoupons.general.discount_type == 'Amount' ? allcoupons.general.coupon_amount + ' OMR' : allcoupons.general.coupon_amount+'%',
			ExpiryDate: moment(allcoupons.general.coupon_expiry_date).format(
				"MMMM Do YYYY"
			),
			usageLimitperCoupon: allcoupons.usage_limits.usage_limit_per_coupon,
			usageLimitPerUser: allcoupons.usage_limits.usage_limit_per_coupon,
			allowedEmails: allcoupons.usage_restriction.allowed_emails,
			excludeCategory: allcoupons.usage_restriction.exclude_category,
			excludeProducts: allcoupons.usage_restriction.exclude_products,
			includeCategory: allcoupons.usage_restriction.include_category,
			maxSpend: allcoupons.max_spend,
			minSpend: allcoupons.min_spend,
			productCategory: allcoupons.product_category,
			products: allcoupons.products
		};
		data.push(Ids);
	});

	//  INFO AREA /////////////////////////////////

	const applyCouponCode = code => {
		showModal();
		getCouponCode(code);
	};

	const deleteCode = code => {
		deleteCouponCode(code);
		window.location.reload();
	};
	const showModal = () => {
		setData({
			visible: true
		});
	};

	const handleOk = e => {
		setData({
			visible: false
		});
	};

	const handleCancel = e => {
		setData({
			visible: false
		});
	};

	// END INFO AREA /////////////////////////////////

	//  Selecting rows
	const onSelectChange = selectedRowKeys => {
		setData({ selectedRowKeys });
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		selections: [
			{
				key: "all-data",
				text: "Select All Data",
				onSelect: () => {
					setData({
						selectedRowKeys: [...Array(46).keys()] // 0...45
					});
				}
			},
			{
				key: "odd",
				text: "Select Odd Row",
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return false;
						}
						return true;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			},
			{
				key: "even",
				text: "Select Even Row",
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return true;
						}
						return false;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			}
		]
	};

	//   End Selecting Rows

	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={node => {
						searchInput = node;
					}}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={e =>
						setSelectedKeys(e.target.value ? [e.target.value] : [])
					}
					onPressEnter={() => handleSearch(selectedKeys, confirm)}
					style={{ width: 188, marginBottom: 8, display: "block" }}
				/>
				<Button
					type="primary"
					onClick={() => handleSearch(selectedKeys, confirm)}
					icon="search"
					size="small"
					style={{ width: 90, marginRight: 8 }}
				>
					Search
				</Button>
				<Button
					onClick={() => handleReset(clearFilters)}
					size="small"
					style={{ width: 90 }}
				>
					Reset
				</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setData({ searchText: selectedKeys[0] });
	};

	const handleReset = clearFilters => {
		clearFilters();
		setData({ searchText: "" });
	};

	// End Search column

	const handleChange = (pagination, filters, sorter) => {
		setData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};

	const columns = [
		{
			title: "Code",
			// width: 100,
			dataIndex: "code",
			key: 1,
			...getColumnSearchProps("code"),
			sorter: (a, b) => a.code.length - b.code.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		},
		{
			title: "Coupon Type",
			dataIndex: "couponType",
			// width: 50,
			key: 2,
			...getColumnSearchProps("couponType"),
			sorter: (a, b) => a.couponType.length - b.couponType.length,
			sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
		},
		{
			title: "Coupon Amount",
			dataIndex: "CouponAmount",
			// width: 100,
			key: 3,
			...getColumnSearchProps("CouponAmount"),
			sorter: (a, b) => a.CouponAmount.length - b.CouponAmount.length,
			sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
		},
		{
			title: "Expiry Date",
			dataIndex: "ExpiryDate",
			key: 4,
			// width: 100,
			...getColumnSearchProps("ExpiryDate"),
			sorter: (a, b) => a.ExpiryDate.length - b.ExpiryDate.length,
			sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
		},
		{
			title: "Action",
			dataIndex: "code",
			// fixed: "right",
			key: 5,
			// width: 120,

			render: code => (
				<div style={{textAlign:"center",width:"100%"}}>
					<Button
						type="dashed"
						size="small"
						onClick={() => applyCouponCode(code)}
					>
						<Icon type="info-circle" />
					</Button>
					&nbsp;
					<Link to={`/cms/admin/edit-coupon/${code}`}>
						<Button type="dashed" size="small">
							<Icon type="edit" />
						</Button>
					</Link>
					&nbsp;
					<Button type="dashed" size="small" onClick={() => deleteCode(code)}>
						<Icon type="delete" />
					</Button>
				</div>
			)
		}
	];

	return (
		<Fragment>
			<Row>
				<Link to="/cms/admin/coupon-management/new-coupon">
					<Button
						type="primary"
						size={"large"}
						style={{ float: "right", marginTop: "10px" }}
					>
						ADD NEW
					</Button>
				</Link>
			</Row>
			<Divider orientation="left">Coupons Details</Divider>
			<Table
				rowSelection={rowSelection}
				columns={columns}
				dataSource={data}
				onChange={handleChange}
				// scroll={{ x: 1350 }}
				bordered
				size="small"
			/>

			<Modal
				title="Coupon Details"
				visible={Data.visible}
				onOk={handleOk}
				onCancel={handleCancel}
			>
				<Descriptions
					title=""
					size="small"
					bordered
					column={{ xxl: 4, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
				>
					<Descriptions.Item label="Coupon Code">
						{coupon_details && coupon_details.coupon_code}
					</Descriptions.Item>
					<Descriptions.Item label="Coupon Description">
						{coupon_details && coupon_details.coupon_description}
					</Descriptions.Item>
					<Descriptions.Item label="Exclude Product">
						{coupon_details && coupon_details.usage_restriction.allowed_emails}
					</Descriptions.Item>
					<Descriptions.Item label="Created At">
						{moment(coupon_details && coupon_details.createdAt).format(
							"MMMM Do YYYY"
						)}
					</Descriptions.Item>
					<Descriptions.Item label="Coupon Amount">
						{coupon_details && coupon_details.general.coupon_amount}
					</Descriptions.Item>
					<Descriptions.Item label="Discount Type">
						{coupon_details && coupon_details.general.discount_type}
					</Descriptions.Item>
					<Descriptions.Item label="Expiry Date">
						{moment(
							coupon_details && coupon_details.general.coupon_expiry_date
						).format("MMMM Do YYYY")}
					</Descriptions.Item>
					<Descriptions.Item label="Min Spend">
						{coupon_details && coupon_details.usage_restriction.min_spend}
					</Descriptions.Item>
					<Descriptions.Item label="Max Spend">
						{coupon_details && coupon_details.usage_restriction.max_spend}
					</Descriptions.Item>
					<Descriptions.Item label="Include Product">
						{coupon_details &&
							coupon_details.usage_restriction.products.map((cat, index) => {
								return <Tag>{cat}&nbsp;</Tag>;
							})}
					</Descriptions.Item>
					<Descriptions.Item label="Exclude Product">
						{coupon_details &&
							coupon_details.usage_restriction.exclude_products.map(
								(cat, index) => {
									return <Tag>{cat}&nbsp;</Tag>;
								}
							)}
					</Descriptions.Item>
					<Descriptions.Item label="Include Category">
						{coupon_details &&
							coupon_details.usage_restriction.include_category.map(
								(cat, index) => {
									return <Tag>{cat}&nbsp;</Tag>;
								}
							)}
					</Descriptions.Item>
					<Descriptions.Item label="Exclude Category">
						{coupon_details &&
							coupon_details.usage_restriction.exclude_category.map(
								(cat, index) => {
									return <Tag>{cat}&nbsp;</Tag>;
								}
							)}
					</Descriptions.Item>
				</Descriptions>
			</Modal>
		</Fragment>
	);
};

CouponManagement.propTypes = {
	getCoupons: PropTypes.func.isRequired,
	allcoupons: PropTypes.object.isRequired,
	getCouponCode: PropTypes.func.isRequired,
	coupon_details: PropTypes.object.isRequired,
	loading: PropTypes.bool,
	deleteCouponCode: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
	allcoupons: state.allcoupons,
	loading: state.loading,
	coupon_details: state.checkout.coupon_details
});
export default connect(
	mapStateToProps,
	{ getCoupons, getCouponCode, deleteCouponCode }
)(CouponManagement);
