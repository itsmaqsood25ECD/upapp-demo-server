import React, { Component, Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import {
	Modal,
	Table,
	Button,
	Input,
	Icon,
	Tag,
	Divider,
	Row,
	Descriptions,
	Card
} from "antd";
import { Link } from "react-router-dom";
import moment from "moment";
import PropTypes from "prop-types";
import {getsubscribe} from "../../../actions/subscribe.action";
import { CSVLink } from "react-csv";

let searchInput = React.createRef();

const SubscriptionManagement = ({
	getsubscribe,
	subscription,
	loading
}) => {
	useEffect(() => {
		getsubscribe();
		console.log("subscription >", subscription)
	}, [loading]);

	const [formData, setFormData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: "",
		coupon_code: "",
		visible: false,
		subscription:[]
	});

	let { filteredInfo, sortedInfo, selectedRowKeys } = useState;

	const data = [];

	subscription &&
	subscription.map(arg => {
			let Ids = {
				id: arg._id,
				UID:arg.UID,
				email: arg.email,
			};
			data.push(Ids);
		});
//  Selecting rows

const onSelectChange = selectedRowKeys => {
	setFormData({ selectedRowKeys });
};
const rowSelection = {
	selectedRowKeys,
	onChange: onSelectChange,
	hideDefaultSelections: true,
	selections: [
		{
			key: "all-data",
			text: "Select All Data",
			onSelect: () => {
				setFormData({
					selectedRowKeys: [...Array(46).keys()] // 0...45
				});
			}
		},
		{
			key: "odd",
			text: "Select Odd Row",
			onSelect: changableRowKeys => {
				let newSelectedRowKeys = [];
				newSelectedRowKeys = changableRowKeys.filter((key, index) => {
					if (index % 2 !== 0) {
						return false;
					}
					return true;
				});
				setFormData({
					selectedRowKeys: newSelectedRowKeys
				});
			}
		},
		{
			key: "even",
			text: "Select Even Row",
			onSelect: changableRowKeys => {
				let newSelectedRowKeys = [];
				newSelectedRowKeys = changableRowKeys.filter((key, index) => {
					if (index % 2 !== 0) {
						return true;
					}
					return false;
				});
				setFormData({
					selectedRowKeys: newSelectedRowKeys
				});
			}
		}
	]
};

	//   End Selecting Rows
	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
				<div style={{ padding: 8 }}>
					<Input
						ref={node => {
							searchInput = node;
						}}
						placeholder={`Search ${dataIndex}`}
						value={selectedKeys[0]}
						onChange={e =>
							setSelectedKeys(e.target.value ? [e.target.value] : [])
						}
						onPressEnter={() => handleSearch(selectedKeys, confirm)}
						style={{
							width: 188,
							marginBottom: 8,
							display: "block"
						}}
					/>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm)}
						icon="search"
						size="small"
						style={{
							width: 90,
							marginRight: 8
						}}
					>
						Search
				</Button>
					<Button
						onClick={() => handleReset(clearFilters)}
						size="small"
						style={{ width: 90 }}
					>
						Reset
				</Button>
				</div>
			),
		filterIcon: filtered => (
			<Icon
				type="search"
				style={{
					color: filtered ? "#1890ff" : undefined
				}}
			/>
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});
	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setFormData({ searchText: selectedKeys[0] });
	};
	const handleReset = clearFilters => {
		clearFilters();
		setFormData({ searchText: "" });
	};

	// End Search column

	const handleChange = (pagination, filters, sorter) => {
		setFormData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};


	const columns = [
		{
			title: "ID",
			dataIndex: "UID",
			key: 1,
			...getColumnSearchProps("name"),
			sorter: (a, b) => a.UID.length - b.UID.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		},
		{
			title: "Email ID",

			dataIndex: "email",
			key: 2,
			...getColumnSearchProps("type"),
			sorter: (a, b) => a.email.length - b.email.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		}
	];




	return (
		<Fragment>
			<Card
            title="Subscribed Users"
            extra={
              <CSVLink data={data} filename={"Subscribed-users.csv"}>
                <Button type="dashed">Download CSV</Button>
              </CSVLink>
            }
          >
<Table
				rowSelection={rowSelection}
				columns={columns}
				dataSource={data}
				onChange={handleChange}
				// scroll={{ x: 1350 }}
				bordered
				size="small"
			/>
		  </Card>
			

			
		</Fragment>
	);
};

SubscriptionManagement.propTypes = {
	getsubscribe: PropTypes.func.isRequired,
	subscription: PropTypes.array.isRequired,
	// loading: PropTypes.bool
};

const mapStateToProps = state => ({
	loading: state.loading,
	subscription: state.subscribe.subscription
});
export default connect(
	mapStateToProps,
	{ getsubscribe }
)(SubscriptionManagement);
