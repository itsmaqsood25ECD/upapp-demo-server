import React, { Fragment, useState, useEffect } from "react";
import { Row, Col, Input, Form, Button, Select, Tabs, Upload, Icon, message } from "antd";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import CKEditor from "@ckeditor/ckeditor5-react";
// import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import CKEditor from "react-ckeditor-component";
import { getBlog, UpdateBlog } from '../../../actions/admin/blog.management.action'
import axios from 'axios'
import PropTypes from "prop-types";
import reqwest from 'reqwest';

import { EditorState, ContentState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const { Option } = Select;

const EditForm = ({ match: { params: { id } }, getBlog, Blog, UpdateBlog, loading, form: { getFieldDecorator, validateFields, setFieldsValue } }) => {
  const [formData, setFormData] = useState({
    id: "",
    content: "",
    title: "",
    author: "",
    featureImage: "",
    fileList: [],
    uploading: false,
    uploadDisable: false,
    editorState: EditorState.createEmpty(),
    uploaded: false
  });
  const { author, uploading, fileList, content, featureImage, title, uploaded, editorState, uploadDisable } = formData;

  useEffect(() => {
    getBlogById()
  }, [loading]);

  const getBlogById = async () => {
    try {
      const response = await getBlog(id)
      const blocksFromHtml = htmlToDraft(response.content);
      const { contentBlocks, entityMap } = blocksFromHtml;
      const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
      setFormData({
        ...formData,
        id: id,
        title: response && response.title,
        author: response && response.author,
        featureImage: response && response.featured_image,
        content: response && response.content,
        editorState: EditorState.createWithContent(contentState),
        fileList: []
      })
    } catch (error) {
      console.log(error)
    }
  }
  console.log(id, "id");
  console.log("recent Blogs", Blog, loading)

  const handleUpload = async () => {

    setFormData({
      ...formData,
      uploading: true,
    });

    try {
      const data = new FormData();
      fileList.forEach(file => {
        data.append('BlogFeatureImg', file);
      });
      const response = await axios.post('https://upappfactory.com/upload/blogFeatured', data)
      setFormData({
        ...formData,
        featureImage: response.data.location,
        uploading: false,
        uploadDisable: true,
      })
      message.success('Featured Image Added Successfully.');

    } catch (error) {
      console.log(error, "error")
      message.success('Featured Image upload Failed.');
    }
  };

  // const updateContent = newContent => {
  //   setFormData({
  //     ...formData,
  //     content: newContent
  //   });
  // };

  const onChangeEditor = evt => {
    // var newContent = evt.editor.getData();

    setFormData({
      ...formData,
      content: draftToHtml(convertToRaw(evt.getCurrentContent())),
      editorState: evt
    });
  };

  // const onBlur = evt => {
  //   console.log("onBlur event called with event info: ", evt);
  // };

  // const afterPaste = evt => {
  //   console.log("afterPaste event called with event info: ", evt);
  // };


  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        UpdateBlog(formData)
        setFormData({
          uploaded: true
        })
      }
    })
  };

  if (uploaded) {
    return <Redirect to="/cms/admin/blog-list"></Redirect>;
  }

  const props = {
    onRemove: file => {
      setFormData(formData => {
        const index = formData.fileList.indexOf(file);
        const newFileList = formData.fileList.slice();
        newFileList.splice(index, 1);
        return {
          ...formData,
          fileList: newFileList,
          uploadDisable: false,
        };
      });
    },
    beforeUpload: file => {
      const isLt2M = file.size / 1024 / 1024 < 0.350;
      if (!isLt2M) {
        message.error('Image must smaller than 300KB!');
      } else {
      setFormData(formData => ({
        ...formData,
        fileList: [...formData.fileList, file],
      }));
      return false;
    }},
    fileList,
  };

  return (
    <Fragment>
      <Form
        // className="vacency-add-form"
        layout="vertical"
        onSubmit={e => onSubmit(e)}
      >
        <Row gutter={16} type="flex" justify="space-around">
          <Col xs={24}>
            <Row gutter={16} type="flex" justify="space-around">
              <Col xs={24}>
              <div style={{ marginBottom: "10px" }}>Featured Image (Please upload Less than 300kb File)</div>
                {featureImage ? (<div style={{ height: "150px", width: "150px", objectFit: "cover", position: "relative", marginBottom: "20px" }}>
                  <img src={featureImage} style={{ height: "150px", width: "150px", objectFit: "cover" }} />
                  <Icon onClick={() => setFormData({
                    ...formData,
                    featureImage: ""
                  })} type="close-circle" theme="twoTone" twoToneColor="#eb2f96" style={{ fontSize: '25px', position: "absolute", right: "-8px", top: "-10px" }} />
                </div>) :
                  <Form.Item>
                    <div>
                      <Upload {...props}>
                        <Button>
                          <Icon type="upload" /> Select File
                    </Button>
                      </Upload>
                      <Button
                        type="primary"
                        onClick={handleUpload}
                        disabled={uploadDisable}
                        loading={uploading}
                        style={{ marginTop: 16 }}
                      >
                        {uploading ? 'Uploading' : 'Start Upload'}
                      </Button>
                    </div>
                  </Form.Item>
                }
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Title</div>
                <Form.Item>
                  {getFieldDecorator("title", {
                    rules: [
                      { required: true, message: "Please Enter Blog Title" }
                    ],
                    initialValue: `${title}`,
                  })(
                    <Input
                      placeholder="Blog Title"
                      onChange={e => setFormData({
                        ...formData,
                        title: e.target.value
                      })}
                      name="title"
                      value={title}
                    />)}
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Blog</div>
                <Form.Item>
                  {/* {getFieldDecorator("content", {
                    rules: [
                      { required: true, message: "Please Enter Blog Content" }
                    ]
                  })( */}
                    <Editor
                      editorState={editorState}
                      toolbarClassName="toolbarClassName"
                      wrapperClassName="wrapperClassName"
                      editorClassName="editorClassName"
                      onEditorStateChange={onChangeEditor}
                    />
                  {/* )} */}


                  {/* <CKEditor
                    activeClass="p10"
                    content={content}
                    events={{
                      // "blur": onBlur,
                      // "afterPaste": afterPaste,
                      "change": onChangeEditor
                    }}
                  /> */}

                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Author</div>
                <Form.Item>
                  {getFieldDecorator("author", {
                    rules: [
                      { required: true, message: "Please Enter Author Name" }
                    ],
                    initialValue: `${author}`,
                  })(
                    <Input
                      placeholder="Enter Author Name"
                      onChange={e => setFormData({
                        ...formData,
                        author: e.target.value
                      })}
                      value={author}
                    />
                  )}
                </Form.Item>
              </Col>
            </Row>

          </Col>
        </Row>
        <div className="vacency-submit-btn" style={{ textAlign: "center" }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ margin: "10px", width: "100px" }}
            disabled={!uploadDisable && featureImage == ""}
          >
            UPDATE
          </Button>
          <Link
            to="/cms/admin/blog-list"
            style={{ margin: "10px", width: "100px" }}
          >
            <Button type="primary" htmlType="submit">
              CANCEL
            </Button>
          </Link>
        </div>
      </Form>
    </Fragment>
  );
};

const NormalEdit = Form.create({ name: "normal_Edit" })(EditForm);

NormalEdit.propTypes = {
  getBlog: PropTypes.func.isRequired,
  UpdateBlog: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  Blog: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  Blog: state.BlogManagement.Blog,
  loading: state.BlogManagement.loading
  // recentBlog: state.BlogManagement.recentBlog
})

export default connect(mapStateToProps, { getBlog, UpdateBlog })(NormalEdit);
