import React, { Fragment, useState } from "react";
import { Row, Col, Input, Form, Button, Select, Tabs } from "antd";
import { createJob } from "../../../actions/admin/career.management.action";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import CKEditor from "@ckeditor/ckeditor5-react";
// import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import CKEditor from "react-ckeditor-component";

import PropTypes from "prop-types";

const { Option } = Select;
const { TabPane } = Tabs;

const Create = ({ createJob }) => {
  const [formData, setFormData] = useState({
    content: "content",
    job_location: "",
    job_locationAR: "",
    job_title: "",
    job_titleAR: "",
    job_vacancy: "",
    job_vacancyAR: "",
    job_exp: "",
    job_expAR: "",
    uploaded: false
  });
  const { uploaded } = formData;
  const handleLocationSelect = val => {
    setFormData({
      ...formData,
      job_location: val
    });
  };

  const updateContent = newContent => {
    setFormData({
      ...formData,
      content: newContent
    });
  };

  const onChangeEditor = evt => {
    console.log("onChange fired with event info: ", evt);
    var newContent = evt.editor.getData();
    setFormData({
      ...formData,
      content: newContent
    });
  };

  const onBlur = evt => {
    console.log("onBlur event called with event info: ", evt);
  };

  const afterPaste = evt => {
    console.log("afterPaste event called with event info: ", evt);
  };

  const handleLocationSelecAR = val => {
    setFormData({
      ...formData,
      job_locationAR: val
    });
  };

  const onChange = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    createJob(formData);
    setFormData({
      ...formData,
      uploaded: true
    });
  };

  if (uploaded) {
    return <Redirect to="/cms/admin/career-management"></Redirect>;
  }

  return (
    <Fragment>
      <Form
        className="vacency-add-form"
        layout="vertical"
        onSubmit={e => onSubmit(e)}
      >
        <Row gutter={16} type="flex" justify="space-around">
          <Col xs={24}>
            <Tabs defaultActiveKey="English">
              <TabPane tab="English" key="English">
                <Row gutter={16} type="flex" justify="space-around">
                  <Col xs={24}>
                    <Form.Item>
                      <Select
                        defaultValue="Location"
                        onChange={handleLocationSelect}
                      >
                        <Option key="1" value="INDIA">
                          India
                        </Option>
                        <Option key="2" value="SINGAPORE">
                          Singapore
                        </Option>
                        <Option key="3" value="OMAN">
                          Oman
                        </Option>
                        <Option key="4" value="SRILANKA">
                          Srilanka
                        </Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <div style={{ marginBottom: "10px" }}>Job Title</div>
                    <Form.Item>
                      <Input
                        placeholder="Job Title"
                        onChange={e => onChange(e)}
                        name="job_title"
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <div style={{ marginBottom: "10px" }}>Job Vacancy</div>
                    <Form.Item>
                      <Input
                        placeholder="Job Vacancy"
                        onChange={e => onChange(e)}
                        name="job_vacancy"
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <div style={{ marginBottom: "10px" }}>Job Exp</div>
                    <Form.Item>
                      <Input
                        placeholder="Job Exp"
                        onChange={e => onChange(e)}
                        name="job_exp"
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    {/* <CKEditor
                      activeClass="p10"
                      content={formData.content}
                      events={{
                        blur: onBlur,
                        afterPaste: afterPaste,
                        change: onChangeEditor
                      }}
                    /> */}
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="Arabic" key="Arabic">
                <Row gutter={16} type="flex" justify="space-around">
                  <Col xs={24}>
                    <Form.Item>
                      <Select
                        defaultValue="Location"
                        onChange={handleLocationSelecAR}
                      >
                        <Option key="1" value="الهند">
                          India
                        </Option>
                        <Option key="2" value="سنغافورة">
                          Singapore
                        </Option>
                        <Option key="3" value="سلطنة عمان">
                          Oman
                        </Option>
                        <Option key="4" value="سريلانكا">
                          Srilanka
                        </Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <div style={{ marginBottom: "10px" }}>Job Title</div>
                    <Form.Item>
                      <Input
                        placeholder="Job Title"
                        onChange={e => onChange(e)}
                        name="job_titleAR"
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <div style={{ marginBottom: "10px" }}>Job Vacancy</div>
                    <Form.Item>
                      <Input
                        placeholder="Job Vacancy"
                        onChange={e => onChange(e)}
                        name="job_vacancyAR"
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24}>
                    <div style={{ marginBottom: "10px" }}>Job Exp</div>
                    <Form.Item>
                      <Input
                        placeholder="Job Exp"
                        onChange={e => onChange(e)}
                        name="job_expAR"
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </TabPane>
            </Tabs>
          </Col>
        </Row>
        <div className="vacency-submit-btn" style={{ textAlign: "center" }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ margin: "10px", width: "100px" }}
          >
            ADD
          </Button>
          <Link
            to="/cms/admin/career-management"
            style={{ margin: "10px", width: "100px" }}
          >
            <Button type="primary" htmlType="submit">
              CANCEL
            </Button>
          </Link>
        </div>
      </Form>
    </Fragment>
  );
};

Create.propTypes = {
  createJob: PropTypes.func.isRequired
};
export default connect(null, { createJob })(Create);
