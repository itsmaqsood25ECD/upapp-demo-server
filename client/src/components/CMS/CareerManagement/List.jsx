import React, { Fragment, useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { Row, Button, Table, Input, Icon, Spin } from "antd";
import PropTypes from "prop-types";
import { getAllJobs } from "../../../actions/admin/career.management.action";

let searchInput = React.createRef();
const List = ({ getAllJobs, Jobs }) => {
  const [Data, setData] = useState({
    filteredInfo: null,
    sortedInfo: null,
    selectedRowKeys: [],
    searchText: "",
    visible: false
  });

  let { filteredInfo, sortedInfo, selectedRowKeys } = Data;
  const data = [];

  useEffect(() => {
    getAllJobs();
  }, []);
  console.log(Jobs);

  Jobs &&
    Jobs.map((job, index) => {
      let Ids = {
        job_title: job && job.en.job_title,
        job_location: job && job.en.job_location,
        job_vacancy: job && job.en.job_vacancy,
        id: job._id
      };
      // console.log("IDs", Ids);
      data.push(Ids);
    });
  console.log("DATA >>> ", data);

  //  Selecting rows
  const onSelectChange = selectedRowKeys => {
    setData({ selectedRowKeys });
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    hideDefaultSelections: true,
    selections: [
      {
        key: "all-data",
        text: "Select All Data",
        onSelect: () => {
          setData({
            selectedRowKeys: [...Array(46).keys()] // 0...45
          });
        }
      },
      {
        key: "odd",
        text: "Select Odd Row",
        onSelect: changableRowKeys => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setData({ selectedRowKeys: newSelectedRowKeys });
        }
      },
      {
        key: "even",
        text: "Select Even Row",
        onSelect: changableRowKeys => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setData({ selectedRowKeys: newSelectedRowKeys });
        }
      }
    ]
  };

  //   End Selecting Rows

  // Search column

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.select());
      }
    }
  });

  const handleSearch = (selectedKeys, confirm) => {
    confirm();
    setData({ searchText: selectedKeys[0] });
  };

  const handleReset = clearFilters => {
    clearFilters();
    setData({ searchText: "" });
  };

  // End Search column

  const handleChange = (pagination, filters, sorter) => {
    setData({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  sortedInfo = sortedInfo || {};
  filteredInfo = filteredInfo || {};

  const columns = [
    {
      title: "Job Title",
      width: 150,
      dataIndex: "job_title",
      key: 1,
      ...getColumnSearchProps("job_title"),
      sorter: (a, b) => a.job_title.length - b.job_title.length,
      sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
    },
    {
      title: "Job Location",
      dataIndex: "job_title",
      width: 100,
      key: 2,
      sorter: (a, b) => a.job_title.length - b.job_title.length,
      sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
    },
    {
      title: "available Vacancy",
      dataIndex: "job_vacancy",
      width: 100,
      key: 3,
      sorter: (a, b) => a.job_vacancy.length - b.job_vacancy.length,
      sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
    },
    {
      title: "Action",
      dataIndex: "id",
      // fixed: "right",
      key: 6,
      width: 60,

      render: id => (
        <span>
          {/* <Link to={`/cms/admin/app-management/edit/${id}`}> */}
          <Button type="dashed" size="small">
            <Icon type="edit" />
          </Button>
          {/* </Link> */}
          &nbsp; &nbsp;
          <Button
            type="dashed"
            size="small"
            onClick={() => console.log("delted")}
          >
            <Icon type="delete" />
          </Button>
        </span>
      )
    }
  ];

  return (
    <Fragment>
      <Row>
        <Link to="/cms/admin/career-management/new-job">
          <Button
            type="primary"
            size={"large"}
            style={{
              float: "right",
              marginTop: "10px"
            }}
          >
            ADD NEW
          </Button>
        </Link>
      </Row>

      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data}
        onChange={handleChange}
        // scroll={{ x: 750 }}
        bordered
        size="small"
      />
    </Fragment>
  );
};

List.propTypes = {
  getAllJobs: PropTypes.func.isRequired,
  Jobs: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  Jobs: state.careersManagement.Jobs
});

export default connect(mapStateToProps, {
  getAllJobs
})(List);
