import React, { Fragment, useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";
import { Button } from "antd";
import "../../../Pages/Profile/invoice.css";
import Printinv from "../../../Pages/Profile/printInv";
import ReactToPrint from "react-to-print";
import { getAllOrders } from "../../../../actions/admin/orders.management.action";

const Invoice = ({
	match,
	allorders: { allorders, loading },
	getAllOrders
}) => {
	useEffect(() => {
		getAllOrders();
	}, [loading]);

	const NewData = [];

	allorders.map(order => {
		if ((order.OrderID || order.PlanID) === match.params.invoice) {
			let Ids = {
				id: order.OrderID || order.PlanID,
				appName: order.appName,
				packType: order.packType,
				coupon_code: order.coupon_code,
				fname: order.payer.payer_info.first_name,
				lname: order.payer.payer_info.last_name,
				start_date: moment(order.start_date).format("MMMM Do YYYY"),
				total_price: order.total_price,
				discount_price: order.discount_price
			};
			NewData.push(Ids);
		}
	});

	const componentRef = useRef();

	return loading && allorders === null ? (
		<Fragment>Spinner</Fragment>
	) : (
		<Fragment style={{ marginTop: "-70px !important" }}>
			<Printinv
				Fname={NewData[0] && NewData[0].fname}
				Lname={NewData[0] && NewData[0].lname}
				applicationName={NewData[0] && NewData[0].appName}
				date={NewData[0] && NewData[0].start_date}
				id={NewData[0] && NewData[0].id}
				type={NewData[0] && NewData[0].packType}
				coupon={NewData[0] && NewData[0].coupon_code}
				totalPrice={Number(NewData[0] && NewData[0].total_price)}
				discount={Number(NewData[0] && NewData[0].discount_price)}
				ref={componentRef}
			/>

			<ReactToPrint
				trigger={() => (
					<div style={{ textAlign: "center", margin: "0px auto 20px auto" }}>
						<Button size="large" type="primary">
							Print this out!
						</Button>
					</div>
				)}
				content={() => componentRef.current}
			/>
		</Fragment>
	);
};

Invoice.propTypes = {
	getAllOrders: PropTypes.func.isRequired,
	allorders: PropTypes.object.isRequired,
	loading: PropTypes.bool
};

const mapStateToProps = state => ({
	allorders: state.allorders,
	loading: state.loading
});
export default connect(
	mapStateToProps,
	{ getAllOrders }
)(Invoice);
