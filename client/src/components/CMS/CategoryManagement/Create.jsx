import React, { Fragment, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, Input, Form, Button, Select, Tabs } from 'antd';
import PropTypes from 'prop-types';
import { createCategory } from '../../../actions/admin/category.management.action';

const { Option } = Select;
const { TabPane } = Tabs;


const Create = ({ createCategory }) => {
	const [formData, setFormData] = useState({
		name: '',
		type: '',
		uploaded: false
	});
	const { uploaded } = formData;
	const onChange = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};
	const handleSelectChange = val => {
		setFormData({
			...formData,
			type: val
		});
	};
	const onSubmit = e => {
		e.preventDefault();
		createCategory(formData);
		setFormData({
			...formData,
			uploaded: true
		});
	};
	if (uploaded) {
		return <Redirect to="/cms/admin/category-management"></Redirect>;
	}
	return (
		<Fragment>
			<Form layout="vertical" onSubmit={e => onSubmit(e)}>
				<Row gutter={16} type="flex" justify="space-around">
					<Col xs={24}>
						<Form.Item>
							<Select defaultValue="Select Type" onChange={handleSelectChange}>
								<Option key="1" value="Type">
									Type
								</Option>
								<Option key="2" value="Category">
									Category
								</Option>
								<Option key="3" value="Industry">
									Industry
								</Option>
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24}>
						<Tabs defaultActiveKey="English">
							<TabPane tab="English" key="English">
								<div style={{ marginBottom: '10px' }}>Name</div>
								<Form.Item>
									<Input
										placeholder="Name"
										onChange={e => onChange(e)}
										name="name"
									/>
								</Form.Item>    </TabPane>
							<TabPane tab="Arabic" key="Arabic">
								<div style={{ marginBottom: '10px' }}>Name Arabic</div>
								<Form.Item>
									<Input
										placeholder="Name in Arabic"
										onChange={e => onChange(e)}
										name="nameAR"
									/>
								</Form.Item>    </TabPane>
						</Tabs>
					</Col>
				</Row>
				<Button type="primary" htmlType="submit">
					ADD
				</Button>
				<Link to="/cms/admin/category-management">
					<Button type="primary" htmlType="submit">
						CANCEL
					</Button>
				</Link>
			</Form>
		</Fragment>
	);
};

Create.propTypes = {
	createCategory: PropTypes.func.isRequired
};
const mapStateToProps = state => ({});
export default connect(
	mapStateToProps,
	{ createCategory }
)(Create);
