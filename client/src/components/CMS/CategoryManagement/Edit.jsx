import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, Input, Form, Tabs, Button, Select } from 'antd';
import {
	GetCategoryById,
	updateCategory
} from '../../../actions/admin/category.management.action';

const { Option } = Select;
const { TabPane } = Tabs;

const Edit = ({ GetCategoryById, match, updateCategory }) => {
	const [formData, setFormData] = useState({
		name: '',
		id: '',
		type: '',
		nameAR: '',
		isUploaded: false
	});
	const { name, type, nameAR, isUploaded } = formData;
	useEffect(() => {
		fetch();
	}, []);

	async function fetch() {
		const res = await GetCategoryById(match.params.id);
		console.log("props", res)
		setFormData({
			...formData,
			name: res.name,
			type: res && res.type,
			id: res._id,
			nameAR: res && res.nameAR,
			isUploaded: false,
		});
	}

	const handleSelectChange = val => {
		setFormData({
			...formData,
			type: val
		});
	};
	const onChange = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};
	const onSubmit = e => {
		e.preventDefault();
		updateCategory(formData);
		setFormData({
			...formData,
			isUploaded: true
		});
	};

	if (isUploaded) {
		return <Redirect to="/cms/admin/category-management"></Redirect>;
	}


	return (
		<Fragment>
			<p>Category</p>
			<Form layout="vertical" onSubmit={e => onSubmit(e)}>
				<Row gutter={16} type="flex" justify="space-around">
					<Col xs={24}>
						<Form.Item>
							<Select value={type} defaultValue={type} onChange={handleSelectChange}>
								<Option key="Type" value="Type">
									Type
								</Option>
								<Option key="Category" value="Category">
									Category
								</Option>
								<Option key="Industry" value="Industry">
									Industry
								</Option>
							</Select>
						</Form.Item>
					</Col>
					{/* 					
					<Col xs={24}>
						<div style={{ marginBottom: '10px' }}>Name</div>
						<Form.Item>
							<Input
								placeholder="Name"
								onChange={e => onChange(e)}
								name="name"
								value={name}
							/>
						</Form.Item>
					</Col>
				 */}
					<Col xs={24}>
						<Tabs defaultActiveKey="English">
							<TabPane tab="English" key="English">
								<div style={{ marginBottom: '10px' }}>Name</div>
								<Form.Item>
									<Input
										value={name}
										placeholder="Name"
										onChange={e => onChange(e)}
										name="name"
									/>
								</Form.Item>    </TabPane>
							<TabPane tab="Arabic" key="Arabic">
								<div style={{ marginBottom: '10px' }}>Name Arabic</div>
								<Form.Item>
									<Input
										value={nameAR}
										placeholder="Name in Arabic"
										onChange={e => onChange(e)}
										name="nameAR"
									/>
								</Form.Item>    </TabPane>
						</Tabs>
					</Col>

				</Row>
				<Button type="primary" htmlType="submit">
					UPDATE
				</Button>
				<Link to="/cms/admin/category-management">
					<Button type="primary" htmlType="submit">
						CANCEL
					</Button>
				</Link>
			</Form>
		</Fragment>
	);
};

Edit.propTypes = {
	GetCategoryById: PropTypes.func.isRequired,
	updateCategory: PropTypes.func.isRequired
};

export default connect(
	null,
	{
		GetCategoryById,
		updateCategory
	}
)(Edit);
