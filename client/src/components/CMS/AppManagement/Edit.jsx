import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import PropTypes from 'prop-types';

import {
	Row,
	Col,
	Input,
	Form,
	Button,
	Select,
	Upload,
	Icon,
	message,
	Modal,
	Switch,
	Tabs,
	Divider
} from 'antd';
import {
	GetApplicationById,
	updateApplication
} from '../../../actions/admin/application.management.action';
import { GetCategory } from '../../../actions/admin/category.management.action';

const { TextArea } = Input;
const { TabPane } = Tabs;

// ///////////////////////////

function getBase64(img, callback) {
	const reader = new FileReader();
	reader.addEventListener('load', () => callback(reader.result));
	reader.readAsDataURL(img);
}

function beforeUpload(file) {
	const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
	if (!isJpgOrPng) {
		message.error('You can only upload JPG/PNG file!');
	}
	const isLt2M = file.size / 1024 / 1024 < 2;
	if (!isLt2M) {
		message.error('Image must smaller than 2MB!');
	}
	return isJpgOrPng && isLt2M;
}

const { Option } = Select;

const EditAppManagement = ({
	GetApplicationById,
	GetCategory,
	match,
	updateApplication,
	Application,
	loading,
	Category
}) => {
	const [formData, setFormData] = useState({
		type: '',
		industry: '',
		category: '',
		id: '',
		thumbImg: '',
		name: '',
		desc: '',
		featureImg: '',
		screenshots: [],
		previewImage: '',
		previewVisible: false,
		isUploaded: false,
		floading: false,
		tloading: false,
		featured: false,
		categories: [],
		nameAR: "",
		descAR: "",
		tloading: false,
		floading: false,
		screenshotsEdit: []

	});
	const {
		desc,
		name,
		categories,
		isUploaded,
		category,
		type,
		industry,
		nameAR,
		descAR,
		featureImg,
		thumbImg,
		tloading,
		floading,
		screenshots,
		previewVisible,
		previewImage,
		screenshotsEdit
	} = formData;

	useEffect(() => {
		GetCategory();
		getApplicationData();
	}, []);

	async function getApplicationData() {
		const Application = await GetApplicationById(match.params.id);
		console.log(Application, "edit")
		setFormData({
			...formData,
			categories:
				Category &&
				Category.map(arg => {
					return arg;
				}),
			id: match.params.id,
			name: Application && Application.fields.name,
			desc: Application && Application.fields.desc,
			nameAR: Application && Application.fields.ar && Application.fields.ar.name,
			descAR: Application && Application.fields.ar && Application.fields.ar.desc,
			category: Application && Application.fields.category,
			type: Application && Application.fields.type,
			industry: Application && Application.fields.industry,
			featureImg: Application && Application.fields.images && Application.fields.images[0] && Application.fields.images[0].featured && Application.fields.images[0].featured.url,
			thumbImg: Application && Application.fields.images && Application.fields.images[0] && Application.fields.images[0].thumb && Application.fields.images[0].thumb.url,
			screenshots: Application && Application.fields.images && Application.fields.images[0] && Application.fields.images[0].screenshots && Application.fields.images[0].screenshots.map((arg, index) => {
				return arg;
			})
		});
	}
	const categoriesList = [];
	const typeList = [];
	const industryList = [];
	for (let i = 0; i <= categories.length - 1; i++) {
		if (categories[i].type === 'Category') {
			categoriesList.push(
				<Option key={categories[i]._id} value={categories[i].name}>
					{categories[i].name}
				</Option>
			);
		}
	}
	for (let i = 0; i <= categories.length - 1; i++) {
		if (categories[i].type === 'Type') {
			typeList.push(
				<Option key={categories[i]._id} value={categories[i].name}>
					{categories[i].name}
				</Option>
			);
		}
	}
	for (let i = 0; i <= categories.length - 1; i++) {
		if (categories[i].type === 'Industry') {
			industryList.push(
				<Option key={categories[i]._id} value={categories[i].name}>
					{categories[i].name}
				</Option>
			);
		}
	}

	const onSubmit = e => {
		e.preventDefault();
		// let screens = screenshots.concat(screenshotsEdit)
		// setFormData({ ...formData, screenshots: screens })
		console.log(formData)
		updateApplication(formData);
		setFormData({
			...formData,
			isUploaded: true
		});
	};
	const onChange = e => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		});
	};

	function handleTypeChange(value) {
		setFormData({ ...formData, type: value });
	}
	function handleCategoryChange(value) {
		setFormData({ ...formData, category: value });
	}
	function handleIndustryChange(value) {
		setFormData({ ...formData, industry: value });
	}

	// thumbnail Image Upload
	const thumbhandleChange = info => {
		if (info.file.status === 'uploading') {
			setFormData({
				...formData,
				tloading: true
			});
			return;
		}
		if (info.file.status === 'done') {
			// Get this url from response in real world.
			console.log(info.file.response.location);
			setFormData({
				...formData,
				thumbImg: info.file.response.location,
				tloading: false
			});
		}
	};
	const thumbUploadButton = (
		<div>
			<Icon type={tloading ? 'loading' : 'plus'} />
			<div className="ant-upload-text">Upload</div>
		</div>
	);


	// thumbnail Image Upload
	const featurehandleChange = info => {
		if (info.file.status === 'uploading') {
			setFormData({ ...formData, floading: true });
			return;
		}
		if (info.file.status === 'done') {
			// Get this url from response in real world.
			setFormData({
				...formData,
				featureImg: info.file.response.location,
				floading: false
			});
		}
	};

	const featureUploadButton = (
		<div>
			<Icon type={floading ? 'loading' : 'plus'} />
			<div className="ant-upload-text">Upload</div>
		</div>
	);

	// screenshot Upload
	async function getBase64SS(file) {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = () => resolve(reader.result);
			reader.onerror = error => reject(error);
		});
	}
	const handleSSCancel = () =>
		setFormData({ ...formData, previewVisible: false });


	const handlePreview = async file => {
		if (!file.url && !file.preview) {
			file.preview = await getBase64SS(file.originFileObj);
		}
		setFormData({
			...formData,
			previewImage: file.url || file.preview,
			previewVisible: true
		});
	};

	const handleSSChange = async ({ fileList }) => {
		setFormData({
			...formData,
			screenshotsEdit: fileList.map(arg => {
				return {
					name: arg.originFileObj.name,
					uid: arg.originFileObj.uid,
					url: arg.response && arg.response.locationArray[0],
				};
			})
		});
	};
	const uploadButton = (
		<div>
			<Icon type="plus" />
			<div className="ant-upload-text">Upload</div>
		</div>
	);

	if (isUploaded) {
		return <Redirect to="/cms/admin/app-management"></Redirect>;
	}
	return (
		<Fragment>
			<Form layout="vertical" onSubmit={e => onSubmit(e)}>
				<Row gutter={16} type="flex" justify="space-around">
					<Col xs={24}>
						<div style={{ marginBottom: '10px' }}>Choose Type</div>
						<Form.Item>
							<Select
								defaultValue="Select App Type"
								onChange={handleTypeChange}
								value={type}
							>
								{typeList}
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24}>
						<div style={{ marginBottom: '10px' }}>Choose Category</div>
						<Form.Item>
							<Select
								defaultValue="Select App Category"
								onChange={handleCategoryChange}
								value={category}
							>
								{categoriesList}
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24}>
						<div style={{ marginBottom: '10px' }}>Choose Industry</div>
						<Form.Item>
							<Select
								defaultValue="Select App Industry"
								onChange={handleIndustryChange}
								value={industry}
							>
								{industryList}
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24} lg={5}>
						<div style={{ marginBottom: '10px' }}>Thumbnail Image</div>
						{thumbImg ? < div style={{ height: "150px", width: "150px", objectFit: "cover", position: "relative", marginBottom: "20px" }}>
							<img src={thumbImg} style={{ height: "150px", width: "150px", objectFit: "cover" }} />
							<Icon onClick={() => setFormData({
								...formData,
								thumbImg: ""
							})} type="close-circle" theme="twoTone" twoToneColor="#eb2f96" style={{ fontSize: '25px', position: "absolute", right: "-8px", top: "-10px" }} />
						</div> : <Upload
							name="thumbImg"
							listType="picture-card"
							className="avatar-uploader"
							showUploadList={false}
							action="https://upappfactory.com/upload/thumbImg"
							beforeUpload={beforeUpload}
							onChange={thumbhandleChange}
						>
								{formData.thumbImg ? (
									<img
										src={`${formData.thumbImg}`}
										alt="featured"
										style={{ width: '100%' }}
									/>
								) : (
										thumbUploadButton
									)}
							</Upload>}
					</Col>
					<Col xs={24} lg={5}>
						<div style={{ marginBottom: '10px' }}>Featured Image</div>
						{featureImg ? < div style={{ height: "150px", width: "150px", objectFit: "cover", position: "relative", marginBottom: "20px" }}>
							<img src={featureImg} style={{ height: "150px", width: "150px", objectFit: "cover" }} />
							<Icon onClick={() => setFormData({
								...formData,
								featureImg: ""
							})} type="close-circle" theme="twoTone" twoToneColor="#eb2f96" style={{ fontSize: '25px', position: "absolute", right: "-8px", top: "-10px" }} />
						</div> :
							<Upload
								name="featureImg"
								listType="picture-card"
								className="avatar-uploader"
								showUploadList={false}
								action="https://upappfactory.com/upload/featureImg"
								beforeUpload={beforeUpload}
								onChange={featurehandleChange}
							>
								{formData.featureImg ? (
									<img
										src={`${formData.featureImg}`}
										alt="avatar"
										style={{ width: '100%' }}
									/>
								) : (
										featureUploadButton
									)}
							</Upload>}
					</Col>
					{console.log((screenshots && screenshots.length <= 49) || (screenshots == undefined), "length")}
					<Col xs={24} lg={14}>
						<div style={{ marginBottom: '10px' }}>Screenshots</div>
						{screenshots && screenshots.map(arg => < div style={{ height: "150px", width: "150px", objectFit: "cover", position: "relative", marginBottom: "20px" }}>
							<img src={arg.url} style={{ height: "150px", width: "150px", objectFit: "cover" }} />
							<Icon onClick={() => setFormData({ ...formData, screenshots: screenshots.filter(img => img.uid != arg.uid) })} type="close-circle" theme="twoTone" twoToneColor="#eb2f96" style={{ fontSize: '25px', position: "absolute", right: "-8px", top: "-10px" }} />
						</div>)}
						{((screenshots && screenshots.length <= 49) || (screenshots == undefined)) &&
							<div className="clearfix">
								<Upload
									action="https://upappfactory.com/upload/screenshots"
									listType="picture-card"
									screenshots={formData.screenshots}
									onPreview={handlePreview}
									onChange={handleSSChange}
									multiple
									name="screenshots"
								>
									{formData.screenshots && formData.screenshots.length >= 50
										? null
										: uploadButton}
									{}
								</Upload>
								<Modal
									visible={previewVisible}
									footer={null}
									onCancel={handleSSCancel}
								>
									<img
										alt="example"
										style={{ width: '100%' }}
										src={previewImage}
									/>
								</Modal>
							</div>
						}
					</Col>
					<Divider dashed />
					<Tabs defaultActiveKey="English">
						<TabPane tab="English" key="English">
							<Col xs={24}>
								<div style={{ marginBottom: '10px' }}>Application Name</div>
								<Form.Item>
									<Input
										placeholder="app name"
										onChange={e => onChange(e)}
										name="name"
										value={name}
									/>
								</Form.Item>
							</Col>
							<Col xs={24}>
								<div style={{ marginBottom: '10px' }}>Application Description</div>
								<Form.Item>
									<TextArea
										placeholder="description"
										onChange={e => onChange(e)}
										name="desc"
										value={desc}
										autosize={{ minRows: 3, maxRows: 5 }}
									/>
								</Form.Item>
							</Col>
						</TabPane>
						<TabPane tab="Arabic" key="2">
							<Col xs={24}>
								<div style={{ marginBottom: '10px' }}>Application Name</div>
								<Form.Item>
									<Input
										placeholder="app name"
										onChange={e => onChange(e)}
										name="nameAR"
										value={nameAR}
									/>
								</Form.Item>
							</Col>
							<Col xs={24}>
								<div style={{ marginBottom: '10px' }}>Application Description</div>
								<Form.Item>
									<TextArea
										placeholder="description"
										onChange={e => onChange(e)}
										name="descAR"
										value={descAR}
										autosize={{ minRows: 3, maxRows: 5 }}
									/>
								</Form.Item>
							</Col>
						</TabPane>
					</Tabs>
					<Col xs={24} style={{ margin: '30px auto' }}>
						<Row gutter={16} type="flex" justify="center">
							<Col lg={4} xs={24}>
								<Button type="primary" htmlType="submit" block>
									UPDATE
								</Button>
							</Col>
							<Col lg={4} xs={24}>
								<Link to="/cms/admin/app-management">
									<Button type="primary" htmlType="submit" block>
										CANCEL
									</Button>
								</Link>
							</Col>
							&nbsp;&nbsp;
						</Row>
					</Col>
				</Row>
			</Form>
		</Fragment >
	);
};

const Edit = Form.create({ name: 'edit_form_item' })(EditAppManagement);
Edit.propTypes = {
	GetApplicationById: PropTypes.func.isRequired,
	GetCategory: PropTypes.func.isRequired,
	updateApplication: PropTypes.func.isRequired,
	Application: PropTypes.object.isRequired,
	loading: PropTypes.bool,
	Category: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
	Application: state.appManagement.Application,
	loading: state.appManagement.loading,
	Category: state.categoryManagement.Category
});

export default connect(
	mapStateToProps,
	{ GetApplicationById, GetCategory, updateApplication }
)(Edit);
