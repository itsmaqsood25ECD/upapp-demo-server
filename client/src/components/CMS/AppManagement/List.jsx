import React, { Component, Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
	Modal,
	Table,
	Button,
	Input,
	Icon,
	Tag,
	Divider,
	Row,
	Spin,
	Descriptions
} from "antd";
import { GetApplication, DeleteApplication } from "../../../actions/admin/application.management.action";
import { GetCategory } from "../../../actions/admin/category.management.action";

let searchInput = React.createRef();
const { confirm } = Modal;

const List = ({ Applications, loading, GetApplication, refresh, DeleteApplication }) => {
	const [Data, setData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: "",
		visible: false
	});

	let { filteredInfo, sortedInfo, selectedRowKeys } = Data;
	const data = [];
	if (Applications === null) {
		GetApplication();
	}
	useEffect(() => {
		GetApplication();
		GetCategory();
	}, [refresh]);
	console.log("Application", Applications, refresh)
	Applications && Applications.map((app, index) => {
		let Ids = {
			id: app.UID,
			name: app.fields && app.fields.name,
			category: app.fields && app.fields.category,
			// month: app.fields && app.fields.month && app.fields.month.adspm + ' OMR',
			// year: app.fields && app.fields.year && app.fields.year.adspy + ' OMR',
			type: app.fields && app.fields.type,

		};
		// console.log("IDs", Ids);
		data.push(Ids);
	});
	console.log('DATA >>> ', data)
	//  Selecting rows
	const onSelectChange = selectedRowKeys => {
		setData({ selectedRowKeys });
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		selections: [
			{
				key: "all-data",
				text: "Select All Data",
				onSelect: () => {
					setData({
						selectedRowKeys: [...Array(46).keys()] // 0...45
					});
				}
			},
			{
				key: "odd",
				text: "Select Odd Row",
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return false;
						}
						return true;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			},
			{
				key: "even",
				text: "Select Even Row",
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return true;
						}
						return false;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			}
		]
	};

	//   End Selecting Rows

	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
				<div style={{ padding: 8 }}>
					<Input
						ref={node => {
							searchInput = node;
						}}
						placeholder={`Search ${dataIndex}`}
						value={selectedKeys[0]}
						onChange={e =>
							setSelectedKeys(e.target.value ? [e.target.value] : [])
						}
						onPressEnter={() => handleSearch(selectedKeys, confirm)}
						style={{ width: 188, marginBottom: 8, display: "block" }}
					/>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm)}
						icon="search"
						size="small"
						style={{ width: 90, marginRight: 8 }}
					>
						Search
				</Button>
					<Button
						onClick={() => handleReset(clearFilters)}
						size="small"
						style={{ width: 90 }}
					>
						Reset
				</Button>
				</div>
			),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setData({ searchText: selectedKeys[0] });
	};

	const handleReset = clearFilters => {
		clearFilters();
		setData({ searchText: "" });
	};

	// End Search column

	const handleChange = (pagination, filters, sorter) => {
		setData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};

	function showDeleteConfirm(id) {
		confirm({
			title: 'Are you sure delete this task?',
			content: 'Some descriptions',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				DeleteApplication(id)
			},
			onCancel() {
				console.log('Cancel');
			},
		});
	}

	const columns = [
		{
			title: "Name",
			width: 150,
			dataIndex: "name",
			key: 1,
			...getColumnSearchProps("name"),
			sorter: (a, b) => a.name.length - b.name.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		},
		{
			title: "Type",
			dataIndex: "type",
			width: 100,
			key: 2,
			sorter: (a, b) => a.type.length - b.type.length,
			sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
		},
		{
			title: "Category",
			dataIndex: "category",
			width: 100,
			key: 3,
			sorter: (a, b) => a.category.length - b.category.length,
			sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
		},
		// {
		// 	title: "Month",
		// 	dataIndex: "month",
		// 	width: 50,
		// 	key: 4,
		// 	...getColumnSearchProps("month"),
		// 	sorter: (a, b) => a.month.length - b.month.length,
		// 	sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
		// },
		// {
		// 	title: "Year",
		// 	dataIndex: "year",
		// 	key: 5,
		// 	width: 50,
		// 	...getColumnSearchProps("year"),
		// 	sorter: (a, b) => a.year.length - b.year.length,
		// 	sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order
		// },
		{
			title: "Action",
			dataIndex: "id",
			// fixed: "right",
			key: 6,
			width: 60,

			render: id => (
				<span>
					<Link to={`/cms/admin/app-management/edit/${id}`}>
						<Button type="dashed" size="small">
							<Icon type="edit" />
						</Button>
					</Link>
					&nbsp;
					&nbsp;
					<Button
						type="dashed"
						size="small"
						onClick={() => {
							showDeleteConfirm(id)
						}}
					>
						<Icon type="delete" />
					</Button>
				</span>
			)
		}
	];

	// console.log(Applications);
	return Applications === null || data === null || loading ? (
		<Fragment>
			<Spin></Spin>
		</Fragment>
	) : (
			<Fragment>
				<Row>
					<Link to="/cms/admin/app-management/new-app">
						<Button
							type="primary"
							size={"large"}
							style={{ float: "right", marginTop: "10px" }}
						>
							Add New Application
					</Button>
					</Link>
				</Row>
				<br />
				<Table
					rowSelection={rowSelection}
					columns={columns}
					dataSource={data}
					onChange={handleChange}
					// scroll={{ x: 750 }}
					bordered
					size="small"
				/>
			</Fragment>
		);
};

List.propTypes = {
	Applications: PropTypes.array.isRequired,
	loading: PropTypes.bool,
	GetApplication: PropTypes.func.isRequired,
	refresh: PropTypes.bool,
	DeleteApplication: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
	Applications: state.appManagement.Applications,
	loading: state.appManagement.loading,
	refresh: state.appManagement.refresh,
});
export default connect(
	mapStateToProps,
	{ GetApplication, DeleteApplication }
)(List);
