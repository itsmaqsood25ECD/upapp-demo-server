import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';
import ScrollTop from '../ScrollTop';

export default class CallBackConfirmation extends Component {
	constructor(props) {
		super(props);
		this.orderString = ``;
		this.title = `Oops.. Something Went Wrong`;
	}
	render() {
		return (
			<Fragment>
				<ScrollTop />
				<Result
					style={{ paddingTop: '150px',height:"100vH" }}
					status="error"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
