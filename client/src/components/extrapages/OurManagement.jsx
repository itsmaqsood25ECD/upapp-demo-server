import React, { Fragment } from "react";
import ScrollTop from "../ScrollTop";
import { Row, Col, Typography, Button, Icon } from "antd";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import withDirection, { DIRECTIONS } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import { Link } from "react-router-dom"

//Img
import Alsalmy from '../../assets/img/BOD/Alsalmy.jpg'
import Harris from '../../assets/img/BOD/Harris.jpg'
import HajiAli from '../../assets/img/BOD/hajiali.png'

const {Title} = Typography;



  const OurManagement = ({ }) => {

    const { t } = useTranslation();

    return (
     <Fragment>
         <ScrollTop />
         <div style={{background:"rgb(248 248 248)",width:"100%", minHeight:"100vH",paddingTop:"80px", textAlign:"center"}}>
             <div style={{marginTop:"50px",marginBottom:"50px"}}>
             <Title>
                Management
             </Title> 
            </div>    
            <div className="container-fluid">
                <Row type="flex">
                    <Col xs={24} lg={8} md={8}>
                      <div className="BOD-container">
                          <div>
                              <img className="BOD-img" src={Alsalmy} alt="Muhammad Al Salmy"/>
                          </div>
                          <div>
                              <p className="BOD-name" >
                                  Muhammad Al Salmy
                              </p>
                              <p className="BOD-position" >Chairman & Co-founder</p>
                                {/* <Button  onClick={(e) => {
                                e.preventDefault();
                                window.location.href='https://www.linkedin.com/in/muhammad-alsalmy-2b9bba2b/';
                                }} className="BOD-follow-btn" icon="linkedin" > Follow </Button>
                          */}

                         <a target="_blank" href="https://www.linkedin.com/in/muhammad-alsalmy-2b9bba2b/">
                               <Icon style={{fontSize:"25px", marginTop:"-10px", color: "#4d4d4d"}} type="linkedin" />
                               </a>
                         
                          </div>
                      </div>
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                    <div className="BOD-container">
                          <div>
                              <img className="BOD-img" src={Harris} alt="Muhammad Harris Aslam"/>
                          </div>
                          <div>
                              <p className="BOD-name" >
                                  Muhammad Harris Aslam
                              </p>
                              <p className="BOD-position" >CEO & Co-Founder</p>
                              {/* <Button  onClick={(e) => {
                                e.preventDefault();
                                window.location.href='https://www.linkedin.com/in/harisaslam/';
                                }} className="BOD-follow-btn" icon="linkedin" > Follow </Button> */}
                           
                           <a target="_blank" href="https://www.linkedin.com/in/harisaslam/">
                               <Icon style={{fontSize:"25px", marginTop:"-10px", color: "#4d4d4d"}} type="linkedin" />
                               </a>

                          </div>
                      </div>
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                    <div className="BOD-container">
                          <div>
                              <img className="BOD-img" src={HajiAli} alt="Haji Ali"/>
                          </div>
                          <div>
                              <p className="BOD-name">
                              Haji Ali
                              </p>
                              <p className="BOD-position" >COO & Co-founder</p>
                              {/* <Button  onClick={(e) => {
                                e.preventDefault();
                                window.location.href='https://www.linkedin.com/in/hajialipatel/';
                                }} className="BOD-follow-btn" icon="linkedin" >
                                Follow 
                               </Button> */}
                               <a target="_blank" href="https://www.linkedin.com/in/hajialipatel/">
                               <Icon style={{fontSize:"25px", marginTop:"-10px", color: "#4d4d4d"}} type="linkedin" />
                               </a>
                          
                          </div>
                      </div>
                    </Col>

                </Row>
            </div>        

         </div>
        
     </Fragment>
    );
}

const mapStateToProps = state => ({
 
});

export default connect(mapStateToProps, { })(withDirection(OurManagement));
