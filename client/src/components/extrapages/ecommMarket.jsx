/* eslint react/prop-types: 0 */
import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import ReactGA from "react-ga";
import SrollTop from "../ScrollTop";
import { Helmet } from "react-helmet";

import {
  Icon,
  Button,
  Typography,
  Modal,
  Input,
  Row,
  Col,
  Form,
  Select,
  DatePicker,
  Menu
} from "antd";
import { requestDemo } from "../../actions/contact.action";
import { loadApplication } from "../../actions/application.action";

const { Title } = Typography;
const { Option } = Select;

const DemoRequests = ({
  requestDemo,
  form: { getFieldDecorator, setFieldsValue, validateFieldsAndScroll },
  Applications
}) => {
  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: ""
  });

  useEffect(() => {
    if (Applications) {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          })
      });
    }
  }, [Applications]);
  const { apps } = appDetails;

  const handleSelect = value => {
    setFormData({ ...formData, interested_in: value });
  };

  const handleSelectCsize = value => {
    setFormData({ ...formData, csize: value });
  };

  const ecommApps =
    apps &&
    apps.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "ecommerce";
    });

  const bookingApps =
    apps &&
    apps.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "booking";
    });

  //  Request For Demo

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const [modal, setModal] = useState({
    visible: false
  });

  const [formData, setFormData] = useState({
    type: "demo"
  });
  const demoRequest = () => {
    showModal();
  };

  const showModal = () => {
    setModal({
      visible: true
    });
  };

  const handleOk = e => {
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        setModal({
          visible: false
        });
        console.log(formData);
        ReactGA.event({
          category: "Contact",
          label: "Request_Demo",
          action: "Request Demo Submit",
          value: 10
        });
        requestDemo(formData);
        Modal.success({
          title: "Demo Request Successful",
          content:
            "Your request to have a demo with UP app factory’s demo team is now received and our pre-sales consultant will be in touch with you soon."
        });
      }
    });
  };

  const handleCancel = e => {
    setModal({
      visible: false
    });
  };

  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };
  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onDateChange = e => {
    setFormData({ ...formData, prefered_date: e._d });
  };

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  // Request for Demo

  return (
    <Fragment>
      <SrollTop />
      <Helmet></Helmet>
      <div style={{ marginTop: "80px", minHeight: "580px", height: "100%" }}>
        <Row>
          <Col lg={16} xs={24} style={{ paddingTop: "150px" }}>
            <div
              className="s-feature-text-cont"
              style={{ paddingLeft: "80px", paddingRight: "150px" }}
            >
              <Title level={1} className="single-app-name">
                Custom Mobile App for your Business
              </Title>
              <Title level={4} className="single-app-description light-text">
                Ready2go E-commerce mobile application and customer appointment
                booking application for your business to launch online in just 7
                - 12 days. UPapp factory offers you the full-featured mobile app
                with a web-based backend login for almost any industry, that can
                be accessed on the go. And all that starts from *$29.9 per month
                as a launching benefit for the customer. Any Queries, let us get
                connected!
              </Title>
            </div>
          </Col>
          <Col lg={8} xs={24}>
            <Form
              style={{
                marginTop: "80px",
                boxShadow: "0px 0px 10px #ccc",
                padding: "40px 20px",
                marginLeft: "30px",
                marginRight: "30px",
                background: "#fff"
              }}
            >
              <Row gutter={16}>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your name"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Name"
                        name="name"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your email"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Email"
                        name="email"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                {/* <Col xs={24} lg={12}>
						<Form.Item>
							{getFieldDecorator('company', {
								rules: [
									{
										required: true,
										message: 'Please type your comapany name'
									}
								]
							})(
								<Input
									placeholder="Company"
									name="company"
									onChange={e => onChange(e)}
									prefix={
										<Icon type="bank" style={{ color: 'rgba(0,0,0,.25)' }} />
									}
								/>
							)}
						</Form.Item>
					</Col>
					<Col xs={24} lg={12}>
						<Form.Item>
							{getFieldDecorator('designation', {
								rules: [
									{
										required: true,
										message: 'Please type your designation'
									}
								]
							})(
								<Input
									placeholder="Designation"
									name="designation"
									onChange={e => onChange(e)}
									prefix={
										<Icon
											type="laptop"
											style={{ color: 'rgba(0,0,0,.25)' }}
										/>
									}
								/>
							)}
						</Form.Item>
					</Col>

					<Col xs={24} lg={12}>
						<Form.Item>
							{getFieldDecorator('csize', {
								rules: [
									{
										required: true,
										message: 'Please type your company size'
									}
								]
							})(
								<Select
									placeholder="Company Size"
									style={{ marginTop: '10px' }}
									onChange={handleSelectCsize}
								>
									<Option key="CompanySize" value="Company Size">
										Company Size
										</Option>
									<Option key="1to10" value="1 - 10 Employees">
										1 - 10 Employees
										</Option>
									<Option key="10to50" value="10 - 50 Employees">
										10 - 50 Employees
										</Option>
									<Option key="50to100" value="50 - 100 Employees">
										50 - 100 Employees
										</Option>
									<Option key="100more" value="100+ Employees">
										100+ Employees
										</Option>
								</Select>
							)}
						</Form.Item>
					</Col> */}
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("country", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your country"
                        }
                      ]
                    })(
                      <CountryDropdown
                        blacklist={[
                          "AF",
                          "AO",
                          "DJ",
                          "GQ",
                          "ER",
                          "GA",
                          "IR",
                          "KG",
                          "LY",
                          "MD",
                          "NP",
                          "NG",
                          "ST",
                          "SL",
                          "SD",
                          "SY",
                          "SR",
                          "TM",
                          "VE",
                          "ZW",
                          "IL"
                        ]}
                        name="country"
                        valueType="short"
                        style={{ marginTop: "12px" }}
                        onChange={val => selectCountry(val)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("mobile", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your mobile number!"
                        }
                      ]
                    })(
                      <PhoneInput
                        style={{ marginTop: "10px" }}
                        name="mobile"
                        inputComponent={SmartInput}
                        placeholder="Enter phone number"
                        onChange={e => onPhoneChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item hasFeedback>
                    {getFieldDecorator("interested_in", {
                      rules: [
                        {
                          required: true,
                          message:
                            "Please select the app you are interested in "
                        }
                      ]
                    })(
                      <Select
                        placeholder="Choose the Category you are intrested in"
                        style={{ marginTop: "10px" }}
                        onChange={handleSelect}
                      >
                        <Option key="ecommerce" value="eCommerce App">
                          eCommerce Application
                        </Option>
                        <Option key="booking" value="Booking App">
                          Booking Application
                        </Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                {/* <Col xs={24}>
							<div style={{ marginBottom: '10px' }}>Prefered Date</div>
							<Form.Item>
								{getFieldDecorator('prefered_date', {
									rules: [
										{
											required: true,
											message: 'Please choose your prefered date for demo'
										}
									]
								})(
									<DatePicker
										showTime
										onChange={e => onDateChange(e)}
										format={dateFormatList}
										disabledDate={disabledDate}
										name="prefered_date"
										style={{ width: '100%' }}
									/>
								)}
							</Form.Item>
						</Col>*/}
              </Row>

              <div style={{ textAlign: "center" }}>
                <Button className="UAFprimaryButton1" type="primary" onClick={handleOk} >Submit</Button>
              </div>
            </Form>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

const RFD = Form.create({ name: "Demo_Request" })(DemoRequests);
RFD.propTypes = {
  requestDemo: PropTypes.func.isRequired,
  loadApplication: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, { requestDemo, loadApplication })(RFD);
