import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';
import ScrollTop from '../../components/ScrollTop';

export default class PageNotFound extends Component {
  render() {
    return (
      <Fragment>
        <ScrollTop />
        <div style={{ marginTop:"100px" }}></div>
        <Result
          status="404"
          title="404"
          subTitle="Sorry, the page you visited does not exist."
          extra={
            <Link to="/">
              <Button type="primary">Back Home</Button>
            </Link>}

        />,
			</Fragment>
    );
  }
}
