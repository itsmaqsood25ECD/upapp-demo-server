import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import fullLogo from '../../../assets/img/brand/logo.png';
import './verificationlink.css';
import ScrollTop from '../../ScrollTop';

export default class verificationLink extends Component {
	render() {
		return (
			<div className="verifyAccount">
				<ScrollTop></ScrollTop>
				<div>
					<img src={fullLogo} alt="UP APP LOGO" style={{ maxWidth: '90px' }} />
				</div>
				<div class="msg">
					<p>
						Verification Link has been sent to your Email
						{/* <i class="email-id">{this.email}</i> */}
					</p>
					<p style={{ fontSize: '16px' }}>please verify your account</p>
				</div>
				<Link to="/login">
					<Button
						type="primary"
						size="large"
						style={{
							height: '50px',
							width: '100%',
							maxWidth: '380px',
							fontSize: '18px'
						}}
					>
						Login
					</Button>
				</Link>
			</div>
		);
	}
}
