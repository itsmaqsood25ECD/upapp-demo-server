import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';
import ScrollTop from '../ScrollTop';

export default class CallBackConfirmation extends Component {
	constructor(props) {
		super(props);
		this.orderString = `You have successfully submited your request for Demo, our sales team will reach you soon.`;
		this.title = `Demo Request Successfully Submitted`;
	}
	render() {
		return (
			<Fragment>
				<ScrollTop />
				<Result
					style={{ paddingTop: '150px' }}
					status="success"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
