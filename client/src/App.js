/* eslint react/prop-types: 0 */
import React, { Fragment, useEffect, useState } from "react";
import { Route, Switch } from "react-router-dom"
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Modal, Input, Typography, Checkbox } from 'antd'
import axios from "axios";
import HeaderComp from './components/Layout/Header'
import Sidebar from './components/Layout/sidebar'
import setAuthToken from './utils/setAuthToken'
import Alert from './components/Layout/Alert'
import * as EmailValidator from 'email-validator';
import { loadUser } from './actions/auth.action';
import { subscribe } from './actions/subscribe.action'
import { store } from './store/store'
import moment from "moment";
import QueueAnim from 'rc-queue-anim';
import FooterComp from './components/Layout/Footer'
import TempLangSwitch from './components/Layout/TempLangSwitch'
import Blog from './components/extrapages/Blog/Blogpage'
import SingleBlog from './components/extrapages/Blog/Blog'
import SingleBlogRedirect from './components/extrapages/Blog/reDirectBlog'
// RTL
import Subscription from "./components/Pages/Subscription";
import Store from "./components/Pages/Store";
import SingleApp from "./components/Pages/single page/app";
import Contact from "./components/Pages/Contact";
import View from "./components/Pages/Profile/View";
import Edit from "./components/Pages/Profile/Edit";
import Orders from "./components/Pages/Profile/Orders";
import ForgotPassword from "./components/Pages/ForgotPassword";
import Login from "./components/Pages/Auth/Login/Login";
import Register from "./components/Pages/Auth/Register/Register";
import Checkout from "./components/Pages/checkout/Checkout";
import Paymentsuccess from "./components/extrapages/Paymentsuccess";
import verificationLink from "./components/extrapages/Verificationlink/Verificationlink";
import confirmationLink from "./components/extrapages/Confirmationlink/Confirmationlink";
import resetPasswordLink from "./components/extrapages/Verificationlink/ResetPasswordLink";
import resetPasswordSuccess from "./components/extrapages/Verificationlink/ResetPasswordSuccess";
import resetPassword from "./components/Pages/ResetPassword";
import Invoice from "./components/Pages/Profile/invoice";
import Paymentcancelled from "./components/extrapages/Paymentfailed";
import Aboutus from "./components/extrapages/Aboutus";
import OurManagement from "./components/extrapages/OurManagement";
import PrivacyPolicy from "./components/extrapages/PrivacyPolicy";
import CacelationRefundPolicy from "./components/extrapages/CacelationRefundPolicy";
import TermsService from "./components/extrapages/TermsService";
import BankTransferSuccess from "./components/extrapages/BankTransferSuccess";
import AppType from "./components/Pages/apptype";
import ConfirmOrderSuccess from "./components/extrapages/ConfirmOrderSuccess";
import CallBackConfirmation from "./components/extrapages/CallBackConfirmation";
import ResumeSubmitted from "./components/extrapages/ResumeSubmitConfirmation";
import PaymentRedirect from "./components/Pages/PaymentForm";
import "./assets/css/animate.css";
import "./index.css";
import ApplicationRequest from "./components/Pages/ApplicationRequest";
import DemoRequests from "./components/Pages/DemoRequests";
import DemoRequestsSucess from "./components/extrapages/DemoRequestConfirmation";
import PartnerConfirmation from "./components/extrapages/PartnerConfirmationSucess";
import Faqs from "./components/extrapages/Faq";
import PageNotFound from "./components/extrapages/404NotFound";
import BecomeOurPartner from "./components/Pages/BecomeOurPartner";
import Careers from "./components/Pages/careers/careers";
import ApplyForJob from "./components/Pages/careers/applyJob";
import HomeNew from "./components/Pages/HomeNew";
import EcommerceLandingPage from "./components/Pages/SingleVendor/EcommerceLandingPage";
import ChooseWebsiteTheme from "./components/Pages/SingleVendor/ChooseWebsiteTheme";
import CheckoutFlow from "./components/Pages/checkout/CheckoutFlow"
import ThemeSelection from "./components/Pages/SingleVendor/ThemeSelection";
import BookingLandingPage from "./components/Pages/BookingLandingPage";
import MobileApp from "./components/MobileAppView/MobileAppPage";
import eventMeetup from "./components/Pages/Meetup";
import Clientele from "./components/Pages/Clientele";
import CheckoutJourney from "./components/Pages/CheckoutJourney/CheckoutJourney";
import FacilityManagement from "./components/Pages/FacilityManagement";
import RestaurantApp from "./components/Pages/RestaurentLandingPage";
import RestaurantAppDemo from "./components/Pages/RestaurentLandingPageDemo";
import UPappbot from './components/Pages/Bot/BotHome'
import UPappweb from './components/Pages/Webiste/UPappweb'
import Sucess from "./components/extrapages/Success";
import Failed from "./components/extrapages/Failed";
import Studio from "./components/Pages/Studio/index";
import StudioSuccess from "./components/Pages/Studio/success"
// RTL End
import DirectionProvider, { DIRECTIONS } from 'react-with-direction/dist/DirectionProvider';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import EcommerceMarketPlace from "./components/Pages/EcommerceMarketPlace";

let layoutDirection;
const { Text } = Typography;

setAuthToken()
if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const { Search } = Input;

if (localStorage.getItem('lng') === 'AR') {
  layoutDirection = 'rtl';
  // i18next.changeLanguage('AR')
} else if (localStorage.getItem('lng') === 'EN') {
  layoutDirection = 'ltr';
  // i18next.changeLanguage('EN')
} else {
  layoutDirection = 'ltr';
  // i18next.changeLanguage('EN')
}

// console.log(layoutDirection);

const App = ({ subscribe, direction }) => {
  const [state, setState] = useState({
    condition: false,
    modalVisible: false,
    seconds: 0,
    checked: false
  });
  const { t } = useTranslation();

  const { condition, modalVisible, checked } = state;



  useEffect(() => {
    store.dispatch(loadUser())

    const currentDate = moment().format('YYYY-MM-DD')

    // const futureMonth = moment().add(10, 's').format('YYYY-MM-DD,  h:mm:ss a');
    // console.log(currentDate >= localStorage.getItem('futureMonth'), 'date', localStorage.getItem('futureMonth'))

    if (currentDate >= localStorage.getItem('futureMonth')) {
      setState({ modalVisible: true })
      localStorage.setItem('ModalShows', false)
    } else {
      localStorage.getItem('ModalShows') === (false || null) ? (setState({ ...state, modalVisible: true })) : (setState({ ...state, modalVisible: false }))
    }



  }, [localStorage.getItem('ModalShows')])



  const onCheckChange = (e) => {
    const futureMonth = moment().add(1, 'M').format('YYYY-MM-DD');
    localStorage.setItem('futureMonth', futureMonth)
    // console.log('checked = ', e.target.checked);
    localStorage.setItem('ModalShows', e.target.checked);
    setState({ checked: e.target.checked })

  }

  const tick = () => {
    setState({ ...state, modalVisible: true })
  }

  const handleClick = () => {
    setState({
      condition: !condition
    })
  }




  const validate = (value) => {

    if (EmailValidator.validate(value)) {
      // console.log("email  ", value)
      subscribe(value);
      Modal.success({
        content: 'Thanks for submitting',
      });
      setState({ ...state, modalVisible: false })
    } else {
      // console.log("enter a valid Email id")
      Modal.error({
        title: 'Please Enter Valid Email',
        // content: 'some messages...some messages...',
      });
    }


  }


  return (
    <Fragment>

      {/* Ecommerce Consult Modal */}

      {/* <Modal style={{ overflow: "hidden" }}

        footer={null}
        centered

        className="entry-modal popupmodal"
        visible={modalVisible}
        onOk={() => setState({ ...state, modalVisible: false })}
        onCancel={() => setState({ ...state, modalVisible: false })}
      >
        {layoutDirection === 'ltr' && <Fragment>
          <div style={{ textAlign: "center", marginTop: '12.5%' }}>

            <Text style={{ color: "#333", fontSize: 20 }}>{t("subscriptionmodal.textline1")}</Text>
            <br></br>
            <br></br>
            <Text style={{ color: "#333", fontSize: 40, fontWeight: 800 }}>{t("subscriptionmodal.textline2")}<br></br>{t("subscriptionmodal.textline3")}</Text>
            <br></br>
            <br></br>
            <Text style={{ color: "#fff", fontSize: 18, marginTop: '30px' }}>
              {t("subscriptionmodal.byharisaslam")} <span style={{ fontSize: 12, color: "#333" }}>{t("subscriptionmodal.roleharisaslam")}</span>
            </Text>
            <br></br>
          </div>
          <Search
            className="inquiry-email"
            name="email"
            type="email"
            placeholder={t("subscriptionmodal.emailplaceholder")}
            enterButton={t("subscriptionmodal.buttonsubscribe")}
            size="large"
            onSearch={value => validate(value)}
          />
          <div>
            <Checkbox onChange={onCheckChange} checked={checked}>Don't Show Again</Checkbox>
          </div>
        </Fragment>}
        {layoutDirection === 'rtl' && <Fragment>
          <div style={{ textAlign: "center", marginTop: '12.5%' }}>

            <Text style={{ color: "#333", fontSize: 20 }}>{t("subscriptionmodal.textline1")}</Text>
            <br></br>
            <br></br>
            <Text style={{ color: "#333", fontSize: 40, fontWeight: 800 }}>{t("subscriptionmodal.textline2")}</Text>
            <br></br>
            <br></br>
            <Text style={{ color: "#fff", fontSize: 18, marginTop: '30px' }}>
              {t("subscriptionmodal.byharisaslam.1")}
            </Text>
            <Text style={{ fontSize: 12, color: "#333" }}>
              {t("subscriptionmodal.byharisaslam.2")}
            </Text>
            <br></br>
            <Text>
              <span style={{ fontSize: 12, color: "#333" }}>{t("subscriptionmodal.roleharisaslam")}</span>
            </Text>
            <br></br>
          </div>
          <Search
            className="inquiry-email"
            name="email"
            type="email"
            placeholder={t("subscriptionmodal.emailplaceholder")}
            enterButton={t("subscriptionmodal.buttonsubscribe")}
            size="large"
            onSearch={value => validate(value)}
          />
        </Fragment>}
      </Modal> */}


      {/* <Modal
footer={null}
          
          style={{ top:0}}
          width="100%"
          height="100vh"
          className="entry-modal"
          visible={modalVisible}
          onOk={() => setState({ ...state , modalVisible: false})}
          onCancel={() => setState({ ...state , modalVisible: false})}
        >
     <Search
     className="inquiry-email"
     type="email"
      placeholder="Email Address"
      enterButton="Subscribe"
      size="large"
      onSearch={value => console.log(value)}
    />
        </Modal> */}
      {/* 
        <Modal
       
          footer={null}
          centered
          width="50%"
      
          className="entry-modal-normal"
          visible={modalVisible}
          onOk={() => setState({ ...state , modalVisible: false})}
          onCancel={() => setState({ ...state , modalVisible: false})}
        >
          <div style={{textAlign:"center",marginTop:80}}>
          <Text style={{color:"#fff",fontSize:20}}>
          Subscribe and enjoy a free training on
          </Text>
          <br></br>
          <br></br>
          <Text style={{color:"#fff",fontSize:30,fontWeight:800}}>
          Setting up <br></br> a Profitable eCommerce Business
          </Text>
          <br></br>
          <br></br>
          <Text style={{color:"#fff",fontSize:18,marginTop:'30px'}}>
            By Haris Aslam, <span style={{fontSize:12}}> Director UPapp factory, USA Certified eCommerce Consultant</span>.
          </Text>
          <br></br>
          </div>
     <Search
     className="inquiry-email"
     name="email"
     type="email"
      placeholder="Email Address"
      enterButton="Subscribe"
      size="large"
      onSearch={value => validate(value)}
    />
        </Modal> */}

      {/* End of Ecommerce Consult Modal */}

      {/* <Snow /> */}

      {/* <Route exact path="/" component={ComingSoon} /> */}
      <DirectionProvider direction={layoutDirection}>
        <HeaderComp />
      </DirectionProvider>
      <Switch>


        <section className="container">

          <DirectionProvider direction={layoutDirection}>
            <Route path="/" component={HomeNew} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Alert />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Sidebar />
          </DirectionProvider>

          {/* <Router /> */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform" component={EcommerceLandingPage} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan" component={ChooseWebsiteTheme} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID" component={ThemeSelection} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID/Checkout" component={CheckoutFlow} exact />
          </DirectionProvider>


          <DirectionProvider direction={layoutDirection}>
            <Route path="/facility-management-application" component={FacilityManagement} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Multi-Vendor-Platform" component={EcommerceMarketPlace} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/restaurant-application" component={RestaurantApp} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/restaurant-application-demo" component={RestaurantAppDemo} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/bookingsolution" component={BookingLandingPage} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/Login" component={Login} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/SignUp" component={Register} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/factory" component={Store} exact />
          </DirectionProvider>
          <DirectionProvider>
          <Route path="/studio" component={Studio} exact />
          </DirectionProvider>
          <DirectionProvider>
          <Route path="/studio/success" component={StudioSuccess} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/factory/:type" component={Store} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/readymade-apps/:slug/:id" component={SingleApp} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/pricing" component={Subscription} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/readymade-apps/:slug/:id/checkout/:price" component={Checkout} />
          </DirectionProvider>

          {/* <Route path="/support" component={Contact} exact /> */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/contactUs" component={Contact} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/my-profile" component={View} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/edit-profile" component={Edit} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/my-orders" component={Orders} exact />
          </DirectionProvider>

          {/* <Route path="/my-payments" component={Payments} exact /> */}

          <DirectionProvider direction={layoutDirection}>
            <Route path="/Paymentsuccess/:OrderID" component={Paymentsuccess} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/Paymentcancelled/:OrderID" component={Paymentcancelled} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/verifyaccount" component={verificationLink} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/confirmation" component={confirmationLink} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/forgot-password" component={ForgotPassword} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/forgot-password/token" component={resetPasswordLink} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/reset-password/success" component={resetPasswordSuccess} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/reset-password/:token" component={resetPassword} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/my-orders/:invoice" component={Invoice} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/cms/admin/login" component={Login} />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/about-us" component={Aboutus} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/our-management" component={OurManagement} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/privacy-policy" component={PrivacyPolicy} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/cancellation-and-refund-policy" component={CacelationRefundPolicy} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/terms-service" component={TermsService} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/bank-transfer-success" component={BankTransferSuccess} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/confirm-order-success" component={ConfirmOrderSuccess} exact />
          </DirectionProvider>

          {/* <Route path="/industries/:industry" component={Industries} exact /> */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/type/:type" component={AppType} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/contactUs/call-back-confirmation" component={CallBackConfirmation} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/PaymentRedirect" component={PaymentRedirect} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/app-requests" component={ApplicationRequest} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/demo-requests" component={DemoRequests} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/faqs" component={Faqs} exact />
          </DirectionProvider>

          {/* <Route
            path="/mobile-application-development"
            component={EcommMarket}
            exact
          /> */}

          <DirectionProvider direction={layoutDirection}>
            <Route path="/demo-requests/success" component={DemoRequestsSucess} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/become-our-partner" component={BecomeOurPartner} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID/Checkout/success/:orderID" component={Sucess} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID/Checkout/failed" component={Failed} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/careers" component={Careers} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/career/apply/:id" component={ApplyForJob} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/career/apply/resume/sumbitted" component={ResumeSubmitted} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/become-our-partner/sumbitted" component={PartnerConfirmation} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/sri-lanka/meetup" component={eventMeetup} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/events/sumbitted" component={PartnerConfirmation} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/clientele" component={Clientele} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/mobileapp" component={MobileApp} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/journey" component={CheckoutJourney} exact />
          </DirectionProvider>

          {/* <DirectionProvider direction={layoutDirection}> */}
          <Route path="/langswitcher" component={TempLangSwitch} exact />
          {/* </DirectionProvider> */}

          {/* <DirectionProvider direction={layoutDirection}>
          <Route component={PageNotFound} />
          </DirectionProvider> */}

          {/* <DirectionProvider direction={layoutDirection}>
            <Route path="/blog" component={Blog} exact />
          </DirectionProvider> */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/blog" component={Blog} exact />
          </DirectionProvider>
          {/* <DirectionProvider direction={layoutDirection}>
            <Route path="/blog/:id" component={SingleBlogRedirect} exact/>
          </DirectionProvider> */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/blog/:id" component={SingleBlog} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/upappbot" component={UPappbot} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/upappweb" component={UPappweb} exact />
          </DirectionProvider>


        </section>
      </Switch>
      <DirectionProvider direction={layoutDirection}>
        <FooterComp />
      </DirectionProvider>

    </Fragment >
  )
}

App.propTypes = {
  subscribe: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, { subscribe })(App);