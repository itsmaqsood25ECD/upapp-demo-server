import React ,{Suspense} from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom'
import { PersistGate } from 'redux-persist/integration/react'
import {Spin} from 'antd'

//REDUX
import { store, persistor } from './store/store'
import { Provider } from "react-redux"
import App from './App.js';
import './index.css';

import createHistory from 'history/createBrowserHistory'
import ReactGA from 'react-ga';
import './i18n';


// Initialize google analytics page view tracking
const history = createHistory()
ReactGA.initialize('UA-151321184-1');
history.listen(location => {
	ReactGA.set({ page: location.pathname }); // Update the user's current page
	ReactGA.pageview(location.pathname); // Record a pageview for the given page
});


ReactDOM.render(
<Suspense fallback={(<Spin />)}>
<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<Router history={history}>
				<App></App>
			</Router>
		</PersistGate>
	</Provider>
	</Suspense>
	, document.getElementById('root'));

