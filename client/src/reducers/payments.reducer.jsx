import { GET_PAYMENTS } from "../actions/constants";

const initialState = {
	payments: [],
	loading: true
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case GET_PAYMENTS:
			return {
				...state,
				payments: payload,
				loading: false
			};
		default:
			return state;
	}
}
