import { COUNTRY_SUCCESS, COUNTRY_FAIL } from "../actions/constants";

const initialState = {
	loading: false
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case COUNTRY_SUCCESS:
			return {
				...state,
				...payload,
				loading: false
			};
		case COUNTRY_FAIL:
			return {
				...state,
				loading: false
			};
		default:
			return state;
	}
}
