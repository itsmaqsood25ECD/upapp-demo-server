import { EVENT_SUCCESS, EVENT_FAIL } from "../actions/constants";

const initialState = {
  // isSubmitted: null,
  loading: false,
  events: []
};

export default function(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case EVENT_SUCCESS:
      return {
        ...state,
        events: payload,
        loading: false
      };
    case EVENT_FAIL:
      return {
        ...state,
        // isSubmitted: false,
        loading: false
      };
    default:
      return state;
  }
}
