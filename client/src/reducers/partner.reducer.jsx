import { PARTNER_SUCCESS, PARTNER_FAIL } from "../actions/constants";

const initialState = {
  loading: false,
  Partner: []
};

export default function(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case PARTNER_SUCCESS:
      return {
        ...state,
        ...payload,
        // isSubmitted: true,
        loading: false
      };
    case PARTNER_FAIL:
      return {
        ...state,
        // isSubmitted: false,
        loading: false
      };
    default:
      return state;
  }
}
