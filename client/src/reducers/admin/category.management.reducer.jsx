import { LOAD_CATEGORY, CATEGORY_SUCCESS } from "../../actions/constants";

const initialState = {
	Category: null,
	loading: true,
	refresh: false
};

export default function (state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case LOAD_CATEGORY:
			return {
				...state,
				Category: payload.result,
				loading: false,
				refresh: false
			};
		case CATEGORY_SUCCESS:
			return {
				...state,
				refresh: true
			}
		default:
			return state;
	}
}
