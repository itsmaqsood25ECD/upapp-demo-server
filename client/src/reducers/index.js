import { combineReducers } from "redux";
import alert from "./alert.reducer.jsx"
import auth from './auth.reducer'
import contact from './contact.reducer'
import orders from './orders.reducer'
import payments from './payments.reducer'
import application from './application.reducer';
import checkout from './checkout.reducer';
import ordersManagement from './admin/orders.management.reducer';
import allcoupons from './admin/coupons.reducer';
import appManagement from './admin/application.management.reducer'
import categoryManagement from './admin/category.management.reducer';
import userlocation from './userlocation.reducer';
import usercountry from './usercountry.reducer';
import customerManagement from './admin/customer.management.reducer'
import requestManagement from './admin/request.management.reducer'
import careersManagement from './admin/careers.management.reducer'
import BlogManagement from './admin/blog.management.reducer'
import BecomePartner from './partner.reducer'
import eventMeet from './event.meet.reducer'
import subscribe from './subscribe.reducer'
import StudioContact from './studiocontact.reducer'
export default combineReducers({
    alert, customerManagement, careersManagement,
    auth, contact, orders, payments,
    application, checkout, ordersManagement,
    allcoupons, appManagement, categoryManagement, userlocation, usercountry,
    requestManagement, BecomePartner, eventMeet, subscribe, BlogManagement, StudioContact
});
