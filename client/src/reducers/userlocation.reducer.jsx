import { USER_LOCATION } from "../actions/constants";

const initialState = {
	userlocation: null,
	loading: true
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case USER_LOCATION:
			return {
				...state,
				userlocation: payload,
				loading: false
			};
		default:
			return state;
	}
}
