import { COUNTRY_SUCCESS, COUNTRY_FAIL } from './constants';

// Country Selector Submission
export const getCountry = ({
	countryCode
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({
		countryCode
	});
    console.log(body);
	try {
		// const res = await axios.post('/request-demo', body, config);
		dispatch({
			type: COUNTRY_SUCCESS,
			payload: body
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: COUNTRY_FAIL
		});
	}
};
