import axios from "axios";
import {
  LOAD_JOB_SUCCESS,
  LOAD_JOB_FAIL,
  GET_JOB_FETCH_SUCCESS,
  GET_JOB_FAIL
} from "../constants";

//POST category Data
export const createJob = ({
  job_title,
  job_titleAR,
  job_location,
  job_locationAR,
  job_vacancy,
  job_vacancyAR,
  job_exp,
  job_expAR
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    en: {
      job_title,
      job_location,
      job_vacancy,
      job_exp
    },
    ar: {
      job_titleAR,
      job_locationAR,
      job_vacancyAR,
      job_expAR
    }
  });
  console.log(body);
  try {
    await axios.post("/post-careers", body, config);
  } catch (error) {
    console.error(error);
  }
};

//  Get all Job Posting
export const getAllJobs = () => async dispatch => {
  try {
    const res = await axios.get("/get-careers");
    console.log(res.data);
    dispatch({
      type: LOAD_JOB_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: LOAD_JOB_FAIL,
      payload: error
    });
  }
};

// Get Job By Id
export const getJob = id => async dispatch => {
  console.log(id);
  try {
    const res = await axios.get(`/careers/${id}`);
    console.log(res.data);
    dispatch({
      type: GET_JOB_FETCH_SUCCESS,
      payload: res.data
    });
    console.log(res.data);
  } catch (error) {
    console.error(error);
    dispatch({
      type: GET_JOB_FAIL,
      payload: error
    });
  }
};

//POST category Data
export const PostResume = ({
  name,
  email,
  resume: { url },
  job_title,
  job_location,
  job_vacancy,
  job_exp
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    name,
    email,
    resume: {
      url
    },
    job_title,
    job_location,
    job_vacancy,
    job_exp
  });
  console.log(body);
  try {
    await axios.post("/apply_for_job", body, config);
  } catch (error) {
    console.error(error);
  }
};
