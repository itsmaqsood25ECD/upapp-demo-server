import axios from "axios";
import {
  GET_CONTACT,
  CONTACT_SUCCESS,
  CONTACT_FAIL,
  CUSTOM_APP_FAIL,
  CUSTOM_APP_SUCCESS,
  GET_IN_TOUCH_SUCCESS,
  GET_IN_TOUCH_FAIL
} from "./constants";

// Conatct Form Submission
export const contact = ({
  name,
  email,
  category,
  mobile,
  description,
  type,
  app_name,
  admin_url
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    name,
    email,
    mobile,
    category,
    description,
    type,
    app_name,
    admin_url
  });

  try {
    const res = await axios.post("/contact-us", body, config);
    dispatch({
      type: CONTACT_SUCCESS,
      payload: res.data
    });
    console.log(res.data);
  } catch (error) {
    console.error(error);
    dispatch({
      type: CONTACT_FAIL
    });
  }
};

// demo request Form Submission
export const requestDemo = ({
	name,
  email,
  country,
  mobile,
  category,
  description,
  business_name,
  app_name,
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({
		name,
		email,
		country,
		mobile,
		description,
		interested_in:category,
		business_name,
		app_name,
		type:"demo",
  });
  console.log(body);
  try {
    const res = await axios.post("/request-demo", body, config);
    dispatch({
      type: CONTACT_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: CONTACT_FAIL
    });
  }
};

// Request for customised app
export const requestForCustomisedApp = ({
  name,
  email,
  country,
  mobile,
  subject,
  description
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    name,
    email,
    country,
    mobile,
    subject,
    description,
    type: "appCustomizationRequest"
  });
  console.log(body);
  try {
    const res = await axios.post("/make_your_app", body, config);
    dispatch({
      type: CUSTOM_APP_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: CUSTOM_APP_FAIL
    });
  }
};

// Request for customised app
// export const GetInTouch = ({
//   name,
//   email,
//   country,
//   mobile,
//   description,
//   interested_in,
//   business_name,
//   app_name
// }) => async dispatch => {
//   const config = {
//     headers: {
//       "Content-Type": "application/json"
//     }
//   };
//   const body = JSON.stringify({
//     name,
//     email,
//     country,
//     mobile,
//     business_details: description,
//     type: "getIntouch",
//     interested_in,
//     business_name,
//     app_name
//   });
//   console.log(body);
//   try {
//     const res = await axios.post("/get_in_touch", body, config);
//     dispatch({
//       type: GET_IN_TOUCH_SUCCESS,
//       payload: res.data
//     });
//   } catch (error) {
//     console.error(error);
//     dispatch({
//       type: GET_IN_TOUCH_FAIL
//     });
//   }
// };
