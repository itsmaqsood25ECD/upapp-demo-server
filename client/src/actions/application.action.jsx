import axios from "axios";
import {
	GET_APPLICATION,
	APPLICATION_FAIL,
	LOAD_APPLICATION,
	USER_LOCATION,
	LOCATION_FAIL
} from "./constants";

// //Get Orders
export const getApplication = id => async dispatch => {
	try {
		const res = await axios.get(`/application-management/${id}`);
		dispatch({
			type: GET_APPLICATION,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
	}
};

//GET Application Data
export const loadApplication = () => async dispatch => {
	try {
		const res = await axios.get("/application-management");
		// return res.data;
		dispatch({
			type: LOAD_APPLICATION,
			payload: res.data
		});
	} catch (error) {
		dispatch({
			type: APPLICATION_FAIL
		});
	}
};


