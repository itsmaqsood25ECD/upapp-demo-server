const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const OrderCancellationSchema = new Schema({
    UID: {
        type: String
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    amt_to_be_charged: {
        type: Number,
    },
    amt_to_be_returned: {
        type: Number
    },
    cancelled_at: {
        type: Date,
        default: Date.now()
    },
    number_of_months_used_for: {
        type: Number,
        default: 1
    },
    order: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Order"
    },
    pending: {
        type: Boolean,
        default: true
    }

})

const Order_Cancellation = mongoose.model("Order_Cancellation", OrderCancellationSchema)
module.exports = Order_Cancellation