const mongoose = require('mongoose')
const moment = require('moment');

const Schema = mongoose.Schema;

const PaymentSchema = new Schema({
  UID: {
    type: String,
  },
  plan_type: {
    type: String
  },
  total_pice: {
    type: Number
  },
  plan_name: {
    type: String
  },
  pack_type: {
    type: String
  },
  payment_date: {
    type: String,
    default: moment().format()
  },
  order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Order"
  },
  invoice_url: {
    type: String
  },
  billing_info: {
    line1: {
      type: String
    },
    line2: {
      type: String
    },
    city: {
      type: String
    },
    country: {
      type: String
    },
    postal_code: {
      type: String
    },
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
})

const Payment = mongoose.model("Payment", PaymentSchema)
module.exports = Payment