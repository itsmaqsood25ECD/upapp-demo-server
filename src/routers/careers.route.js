const express = require('express')
const Careers = require('../models/careers.model')
const Candidate = require('../models/candidate.model')
const nodemailer = require('nodemailer')
const auth = require('../middleware/auth')


const router = new express.Router()
NODE_TLS_REJECT_UNAUTHORIZED = '0'

// Send the email 
const transporter = nodemailer.createTransport({
  host: 'smtp.zoho.com',
  port: 465,
  secure: true,
  auth: {
    user: 'careers@upappfactory.com',
    pass: 'Careers@98110669$@#'
  },
  tls: {
    rejectUnauthorized: false
  }
})


// Post New Job Posting
router.post("/post-careers", async (req, res) => {
  try {
    const careers = new Careers(req.body)
    if (!careers)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
    await careers.save()
    return res.status(200).send({ error: false, success: true, message: "Job posted successfully", result: careers })
  } catch (error) {
    console.error(error)
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
  }
})


// Get All Job posting
router.get("/get-careers", async (req, res) => {
  try {
    const careers = await Careers.find({})
    if (!careers)
      return res.status(404).send({ error: true, success: false, message: "Not Found", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Found Job Postings", result: careers })
  } catch (error) {
    console.error(error)
    return res.status(404).send({ error: true, success: false, message: "Not Found", result: error })
  }
})

// Get One Job by job title
router.get("/careers/:id", async (req, res) => {
  console.log(req.params.id)
  try {
    const career = await Careers.findById(req.params.id)
    if (!career)
      return res.status(404).send({ error: true, success: false, message: "Not Found", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Found Job", result: career })
  } catch (error) {
    console.error(error)
    return res.status(400).send({ error: true, success: false, message: "Bad request", result: error })
  }
})

// Update A Job Posting
router.patch('/careeres/:name', async (req, res) => {
  try {
    const career = await Careers.findOneAndUpdate({ job_title: req.params.name }, { $set: req.body }, { new: true })
    if (!career)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Update Successfull", result: career })
  } catch (error) {
    console.error(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

// Delete Job Posting
router.delete('/careers/:id', async (req, res) => {
  try {
    const career = await Careers.findById(req.params.id)
    if (!career)
      return res.status(404).send({ error: true, success: false, message: "Not found", result: "" })
    career.remove()
    return res.status(200).send({ error: false, success: true, message: "Deleted Successfully", result: career })
  } catch (error) {
    console.error(error)
    return res.status(500).send({ error: true, success: true, message: "Server error", result: error })
  }
})

// Apply for Job
router.post('/apply_for_job', async (req, res) => {
  try {
    const candidate = new Candidate(req.body)
    if (!candidate)
      return res.status(400).status({ error: true, success: false, message: "Bad Request", result: "" })
    const prevCandidate = await Candidate.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
    let counter, UID, UAF
    if (prevCandidate.length === 0) {
      counter = 1
    } else {
      counter = prevCandidate[0].UID.slice(8)
      counter = +counter + 1
    }
    UAF = "UAF-CAN-"
    if (counter < 10) {
      UID = `${UAF}0000000${counter}`
    } else if (counter < 100 && counter >= 10) {
      UID = `${UAF}0000000${counter}`
    } else if (counter < 1000 && counter >= 100) {
      UID = `${UAF}000000${counter}`
    } else if (counter < 10000 && counter >= 1000) {
      UID = `${UAF}00000${counter}`
    } else if (counter < 100000 && counter >= 10000) {
      UID = `${UAF}0000${counter}`
    } else if (counter < 1000000 && counter >= 100000) {
      UID = `${UAF}000${counter}`
    } else if (counter < 10000000 && counter >= 1000000) {
      UID = `${UAF}00${counter}`
    } else if (counter < 100000000 && counter >= 10000000) {
      UID = `${UAF}0${counter}`
    } else {
      UID = `${UAF}${counter}`
    }
    candidate.UID = UID
    const response = await candidate.save()
    console.log(response)
    const mailOptions = {
      from: 'careers@upappfactory.com',
      to: req.body.email,
      subject: `Resume Dropped Successfully`,
      bcc: "careeers@upappfactory.com",
      html: `<!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <title>
            Contact Us
          </title>
        </head>

        <body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
          <table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
            <tbody>
              <tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
                <td style="padding-left: 15px;">
                  <a href="">
                    <img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
                  </a>
                </td>
              </tr>
              <td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
                Dear ${candidate.name},
                <br /><br />
                Thank you for showing interest. We have recieved your CV and we will get back to you as soon as possible,
                <br /><br />
                Regards,
                <br />
                HR Team
                <br />
                UPappfactory
              </td>
              <tr style="text-align: center;background: #f7f7f7">
                <td style="padding:0px 20px;text-align: center;background: #f7f7f7">
                </td>
              </tr>
              <tr style="text-align: center;background: #f7f7f7">
                <td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
                  <p style="margin: 10px auto 0px auto;line-height: 0.8;">
                    <a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
                    |
                    <a style="text-decoration: none;" href="www.upappfactory.com">UPapp factory</a><br /><br />
                  </p>
                  <a href="https://www.facebook.com/Upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
                  <a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
                  <a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
                </td>
              </tr>
              <tr style="padding:20px;text-align: center;background: #f7f7f7">
                <td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
                  Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
                </td>
              </tr>
            </tbody>
          </table>
        </body>
      </html>`
    }
    await transporter.sendMail(mailOptions)
    console.log('saved successfully')
    return res.status(200).status({ error: false, success: true, message: "Saved Application successfully", result: candidate })
  } catch (error) {
    console.error(error)
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
  }
})

//  List All Applied jobs
router.get("/applied_jobs", async (req, res) => {
  try {
    const candidates = await Candidate.find({})
    if (!candidates)
      return res.status(404).send({ error: true, success: false, message: "Not Found", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Found all applied jobs", result: candidates })
  } catch (error) {
    console.error(error)
    return res.status(404).send({ error: true, success: false, message: "Not Found", result: error })
  }
})

module.exports = router