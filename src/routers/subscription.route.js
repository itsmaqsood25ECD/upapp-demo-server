const express = require('express')
const Subscription = require('../models/subscription.model.js')
const nodemailer = require('nodemailer')

const router = new express.Router()


// Send the email 
const transporter = nodemailer.createTransport({
  host: 'smtp.zoho.com',
  port: 465,
  secure: true,
  auth: {
    user: 'noreply@upappfactory.com',
    pass: 'Ecommdesk@123'
  },
  tls: {
    rejectUnauthorized: false
  }
})


// Subcribe
router.post('/subscription', async (req, res) => {
  try {
    const subscription = new Subscription(req.body)
    console.log(subscription)
    if (!subscription)
      return res.status(400).status({ error: true, success: false, message: "Bad Request", result: "" })
    const prevSubscription = await Subscription.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
    let counter, UID, UAF
    if (prevSubscription.length === 0) {
      counter = 1
    } else {
      counter = prevSubscription[0].UID.slice(8)
      counter = +counter + 1
    }
    UAF = "UAF-SUB-"
    if (counter < 10) {
      UID = `${UAF}0000000${counter}`
    } else if (counter < 100 && counter >= 10) {
      UID = `${UAF}0000000${counter}`
    } else if (counter < 1000 && counter >= 100) {
      UID = `${UAF}000000${counter}`
    } else if (counter < 10000 && counter >= 1000) {
      UID = `${UAF}00000${counter}`
    } else if (counter < 100000 && counter >= 10000) {
      UID = `${UAF}0000${counter}`
    } else if (counter < 1000000 && counter >= 100000) {
      UID = `${UAF}000${counter}`
    } else if (counter < 10000000 && counter >= 1000000) {
      UID = `${UAF}00${counter}`
    } else if (counter < 100000000 && counter >= 10000000) {
      UID = `${UAF}0${counter}`
    } else {
      UID = `${UAF}${counter}`
    }
    subscription.UID = UID
    await subscription.save()
    const mailOptions = {
      from: 'noreply@upappfactory.com',
      to: req.body.email,
      subject: `Subscribed to UPapp factory`,
      bcc: "aslam@upappfactory.com",
      html: `<!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <title>
            Contact Us
          </title>
        </head>

        <body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
          <table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
            <tbody>
              <tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
                <td style="padding-left: 15px;">
                  <a href="">
                    <img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
                  </a>
                </td>
              </tr>
              <td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
              Thank you for subscribing to our upcoming webinar on “Setting up a profitable eCommerce business” by Haris Aslam,
              <br /> 
              We will send you the details of the webinar with the link to connect in a few days.
              <br />
              For more information, you can always reach out to us. 
              <br /><br />

                Regards,
                <br />
                Digital content team @ UPapp factory
                <br />
                UPappfactory
              </td>
              <tr style="text-align: center;background: #f7f7f7">
                <td style="padding:0px 20px;text-align: center;background: #f7f7f7">
                </td>
              </tr>
              <tr style="text-align: center;background: #f7f7f7">
                <td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
                  <p style="margin: 10px auto 0px auto;line-height: 0.8;">
                    <a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
                    |
                    <a style="text-decoration: none;" href="www.upappfactory.com">UPapp factory</a><br /><br />
                  </p>
                  <a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
                  <a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
                  <a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
                </td>
              </tr>
              <tr style="padding:20px;text-align: center;background: #f7f7f7">
                <td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
                  Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
                </td>
              </tr>
            </tbody>
          </table>
        </body>
      </html>`
    }
    await transporter.sendMail(mailOptions)
    return res.status(200).send({ error: false, success: true, message: "Saved Subscription successfully", result: subscription })
  } catch (error) {
    console.log(error)
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
  }
})

// get ALl Subscription
router.get('/subscription', async (req, res) => {
  try {
    const subscription = await Subscription.find({})
    if (!subscription)
      return res.status(404).send({ error: true, success: false, message: "Not Found", result: {} })
    return res.status(200).send({ error: false, success: true, message: "Success", result: subscription })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

//Delete Subscription

router.delete('/subscription', async (req, res) => {
  try {
    const subscription = await Subscription.findOneAndDelete({ email: req.body.email })
    // const subscription = await Subscription.findByIdAndDelete(req.body.email)
    console.log(subscription)
    if (!subscription)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Successfully deleted", result: subscription })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server error", result: error })
  }
})


module.exports = router