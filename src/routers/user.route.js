const express = require('express')
const nodemailer = require('nodemailer')
const crypto = require('crypto')
const passswordValidator = require('password-validator')
const User = require('../models/user.model')
const auth = require('../middleware/auth')
const VerificationToken = require('../models/verification.model')


const router = new express.Router()
const passSchema = new passswordValidator();

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_USERNAME,
    pass: process.env.EMAIL_PASSWORD
  }
})

passSchema.is().min(7).has().uppercase().has().lowercase().has().digits().has().not().spaces()

// User signup route
router.post('/signup', async (req, res) => {
  const { firstName, lastName, email, password, gender, country, phone } = req.body
  if (!firstName || !lastName || !email || !password || !gender || !country || !phone)
    return res.status(400).send({ error: true, success: false, message: "Please fill in all the fields", result: {} })
  if (passSchema.validate(req.body.password)) {
    const prevUser = await User.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
    console.log(prevUser)
    let counter, UID
    if (prevUser.length === 0) {
      counter = 1
    } else {
      counter = prevUser[0].UID.slice(9)
      console.log(counter)
      counter = +counter + 1
    }
    if (counter < 10) {
      UID = `UAF-USER-0000000${counter}`
    } else if (counter < 100 && counter >= 10) {
      UID = `UAF-USER-0000000${counter}`
    } else if (counter < 1000 && counter >= 100) {
      UID = `UAF-USER-000000${counter}`
    } else if (counter < 10000 && counter >= 1000) {
      UID = `UAF-USER-00000${counter}`
    } else if (counter < 100000 && counter >= 10000) {
      UID = `UAF-USER-0000${counter}`
    } else if (counter < 1000000 && counter >= 100000) {
      UID = `UAF-USER-000${counter}`
    } else if (counter < 10000000 && counter >= 1000000) {
      UID = `UAF-USER-00${counter}`
    } else if (counter < 100000000 && counter >= 10000000) {
      UID = `UAF-USER-0${counter}`
    } else {
      UID = `UAF-USER-${counter}`
    }
    console.log(UID)
    const user = new User(req.body)
    user.UID = UID
    try {
      await user.save()
      // Create a verification token for this user
      const verificationToken = new VerificationToken({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') })

      // Save the verification token
      await verificationToken.save()
      //Send Mail
      const mailOptions = {
        from: process.env.EMAIL_USERNAME,
        to: user.email,
        bcc: "contactus@upapp.co",
        subject: 'Account Verification Token',
        html: `<!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="UTF-8" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
            />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no"
            />
            <title>Verification Email</title>
          </head>
          <body
            style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
            <table
              style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;">
              <tbody>
              <tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
              <td style="
           padding-left: 15px;
        ">
                <a href="">
                  <img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
                </a>
              </td>

            </tr>
                <tr>
                  <td
                    style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px; "
                  >
              Dear ${user.firstName} ${user.lastName},
              <br><br>
                   We thank you for signing up at UPappfactory and welcome aboard.
               <br><br>
                    To verify your email please 
                    <a href="https://www.upappfactory.com/confirmation/${verificationToken.token}">Click here</a>,

                    </td>
                  </tr>
      <tr >
                    <td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding:10px 20px;">
                    If you would like to speak with one of our representatives about how your business can be empowered by a mobile app solution along with the systems, please to do at <a href="mailto:contactus@upapp.co">contactus@upapp.co</a>.
                <br><br>
                You can also reach out to us through chat support available at <a href="https://www.upappfactory.com">www.upappfactory.com</a>.
                <br><br>
                Book your free demo <a href="https://www.upappfactory.com">here</a>
                <br><br>
              Regards,
              <br>
              Support Team
              <br>
              UPappfactory

                    </td>
            </tr>
            <tr style="text-align: center;background: #f7f7f7">

    								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
    									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
    										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
    										|
    										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
    									</p>
    									<a href="https://www.facebook.com/UpAppFactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
    									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
    									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
    								</td>

    							</tr>
    							<tr style="padding:20px;text-align: center;background: #f7f7f7">
    								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
    									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +65167146696
    								</td>
    							</tr>
          </table>

        </body>
        </html>`
      }
      const response = await transporter.sendMail(mailOptions)
      if (response.rejected.length === 0) {
        const token = await user.generateAuthToken()
        return res.status(201).send({ error: false, success: true, message: "User created Successfully", result: { user, token } })
      } else {
        user.remove()
        return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
      }
    } catch (e) {
      console.error(e)
      res.status(400).send({ error: true, success: false, message: "Bad request", result: e })
    }
  } else {
    res.status(400).send({ error: true, success: false, message: "Password requirement does not match", result: {} })
  }
})

//User verification confirmation route
router.get('/confirmation/:token', async (req, res) => {
  // Find a matching token
  try {
    const verificationToken = await VerificationToken.findOne({ token: req.params.token })
    if (!verificationToken)
      return res.status(400).send({ error: true, success: false, message: "We were unable to find a valid token. Your token my have expired.", result: {} });

    // If we found a token, find a matching user
    const user = await User.findOne({ _id: verificationToken._userId })
    if (!user)
      return res.status(400).send({ error: true, success: false, message: "We were unable to find a user for this token.", result: {} });
    if (user.isVerified)
      return res.status(400).send({ error: true, success: false, message: "This user has already been verified.", result: user });

    // Verify and save the user
    user.isVerified = true;
    await user.save()
    res.redirect(`${process.env.SERVER_NAME}/confirmation`)
  } catch (e) {
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: e })
  }
})

// User login route
router.post('/login', async (req, res) => {
  try {
    const response = await User.findByCredentials(req.body.email, req.body.password)
    if (response.error)
      return res.status(400).send(response)
    // Make sure the user has been verified
    if (!response.result.isVerified)
      return res.status(401).send({ error: true, success: false, message: 'Your account has not been verified.', result: {} });
    const token = await response.result.generateAuthToken()
    const user = response.result
    res.status(200).send({ error: false, success: true, message: "Login Successfull", result: { user, token } })
  } catch (e) {
    console.error(e)
    res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
  }
})

// User forgot password route
router.post('/forgot-password', async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email })
    if (!user)
      return res.status(400).send({ error: true, success: false, message: 'User does not exists.', result: {} });

    // Create a passsword reset token, save it, and send email
    const passwordResetToken = new VerificationToken({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });

    // Save the token
    await passwordResetToken.save()

    const mailOptions = {
      from: process.env.EMAIL_USERNAME,
      to: user.email,
      subject: 'Password Reset Token',
      // cc: "ali@upappfactory.com",
      html: `<!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <title>Forgot Password</title>
        </head>
        <body
          style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
          <table
            style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;">
            <tbody>
            <tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
            <td style="
         padding-left: 15px;
      ">
              <a href="">
                <img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
              </a>
            </td>
    
          </tr>
              <tr>
                <td
                  style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px 20px 0px 20px;">
                  Dear ${user.firstName} ${user.lastName}, <br /><br />
                  We understand that you have forgotten the password to your account
                  at UPapp factory, not to worry at all we’ve got your covered..
                  <br /><br />
                </td>
              </tr>
              <tr>
                <td
                  style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 10px 20px 20px 10px; text-align: center"
                  >
                  Simply click on the links here <br><br><a href='https://www.upappfactory.com/reset-password/${passwordResetToken.token}' style="
                    height: 50px;
                    color: #fff;
                    padding: 10px 20px;
                    font-family: sans-serif;
                    font-size: 20px;
                    text-decoration: none;
                    background: #2170e9;
                    position: relative;
                    width: 100%;
                    ">Reset Password</a> 
                </td>
              </tr>
              <tr>
                <td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding:0px 20px;">and provide with the new password that you’d like to use at UPapp factory. <br /><br />
                  If you are still facing issues, please reach out to us at
                  support@upappfactory.com or chat with one of our support
                  representatives at
                  <a href="www.upappfactory.com">UPapp factory.</a>
                  <br /><br />
                </td>
              </tr>
              <tr>
                <td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 0px 20px;">      
                  Regards,
                  <br />
                  Support Team
                  <br />
                  UPapp factory
                </td>
              </tr>
              <tr style="text-align: center;background: #f7f7f7">

										<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
											<p style="margin: 10px auto 0px auto;line-height: 0.8;">
												<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
												|
												<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
											</p>
											<a href="https://www.facebook.com/UpAppFactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
											<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
											<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
										</td>
						
									</tr>
									<tr style="padding:20px;text-align: center;background: #f7f7f7">
										<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
											Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +65167146696
										</td>
									</tr>
            </tbody>
          </table>
        </body>
      </html>`
    }
    await transporter.sendMail(mailOptions)
    res.status(200).send({ error: false, success: true, message: "Reset token has been mailed", result: passwordResetToken })
  } catch (e) {
    console.error(e)
    res.status(400).send({ error: true, success: true, message: "Bad Request", result: e })
  }
})

router.patch('/reset-password/:token', async (req, res) => {
  try {
    const passwordResetToken = await VerificationToken.findOne({ token: req.params.token })
    if (!passwordResetToken)
      return res.status(400).send({ error: true, success: false, message: "We were unable to find a valid token. Your token my have expired.", result: {} });
    // If we found a token, find a matching user
    const user = await User.findOne({ _id: passwordResetToken._userId })
    if (!user)
      return res.status(400).send({ error: true, success: false, message: 'We were unable to find a user for this token.', result: {} });
    if (passSchema.validate(req.body.password)) {
      user.password = req.body.password
      await user.save()
      return res.status(200).send({ error: false, success: true, message: "Password updated Successfully", result: user })
    } else {
      return res.status(400).send({ error: true, success: true, message: "Password requirement does not match", result: {} })
    }
  } catch (error) {
    console.log(error)
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: e })
  }
})

//User logout route
router.post('/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token
    })
    await req.user.save()
    return res.send({ error: false, success: true, message: "Successfully Logged out", result: { loggedOut: true } })
  } catch (e) {
    console.error(e)
    res.status(500).send({ error: true, success: false, message: "Server error", result: e })
  }
})

//Get user details route
router.get('/users/profile', auth, (req, res) => {
  res.send({ error: false, message: "user found", success: true, result: { user: req.user } })
})

//Update user route
router.put('/users/profile', auth, async (req, res) => {
  const updates = Object.keys(req.body)
  const allowedUpdates = ["firstName", "lastName", "email", "age", "gender", "country", "phone", "line1", "line2", "city", "state", "postal_code"]
  const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

  if (!isValidOperation) {
    return res.status(400).send({ error: true, success: false, message: 'Invalid Updates', result: {} })
  }

  try {
    updates.forEach((update) => req.user[update] = req.body[update])
    await req.user.save()

    return res.send({ error: false, success: true, message: "User updated successfully", result: req.body })
  } catch (e) {
    console.error(e)
    res.status(500).send({ error: true, success: false, message: "Server error", result: e })
  }
})

router.put('/user-role/:id', async (req, res) => {
  try {
    const user = await User.findOneAndUpdate({ UID: req.params.id }, { $set: req.body }, { new: true })
    if (!user)
      return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Update success", result: user })
  } catch (e) {
    console.error(e)
    res.status(500).send({ error: true, success: false, message: 'server error', result: e })
  }
})

// Delete user route
router.delete('/users/profile', auth, async (req, res) => {
  try {
    await req.user.remove()
    res.send({ error: false, success: true, message: "User deleted successfully", result: req.user })
  } catch (e) {
    console.error(e)
    res.status(500).send({ error: true, success: false, message: "server error", result: e })
  }
})

router.delete('/delete-user/:id', async (req, res) => {
  try {
       await User.findByIdAndDelete(id)
      res.send({ error: false, success: true, message: "User deleted successfully", result: req.user })
  } catch (e) {
      console.error(e)
      res.status(500).send({ error: true, success: false, message: "server error", result: e })
    }
})

// Get all customers
router.get('/get_all_users', async (req, res) => {
  try {
    const users = await User.find()
    if (!users || users.length == 0)
      return res.status(400).send({ error: true, success: false, message: "No user found", result: [] })
    return res.status(200).send({ error: false, success: true, message: "User Found", result: users })
  } catch (error) {
    console.error(error)
    return res.status(500).send({ error: true, success: false, message: "Server error", result: [] })
  }
})

// Get customer details
router.get('/get_customer_details/:id', async (req, res) => {
  try {
    const user = await User.findById(req.params.id)
    if (!user)
      return res.status(400).send({ error: true, success: false, message: "No User Found", result: {} })
    return res.status(200).send({ error: false, success: true, message: "User Found", result: user })

  } catch (error) {
    console.error(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})
// resend token for email confirmation
router.post('/resend', async (req, res) => {
  try {
    const user = User.findOne({ email: req.body.email })
    if (!user)
      return res.status(400).send({ error: true, success: true, message: 'User not found', result: {} });
    if (user.isVerified)
      return res.status(400).send({ error: true, success: false, message: 'This account has already been verified. Please log in.', result: user });

    // Create a verification token, save it, and send email
    const verificationToken = new VerificationToken({ _userId: user._id, token: crypto.randomBytes(16).toString('hex') });

    // Save the token
    await verificationToken.save()

    const mailOptions = {
      from: process.env.EMAIL_USERNAME,
      to: user.email,
      subject: 'Account Verification Token',
      // cc: 'ali@upappfactory.com',
      html: `<!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <title>Verification Email</title>
        </head>
        <body
          style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
          <table
            style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;">
            <tbody>
              <tr
                style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
                <td style="padding-left: 15px;">
                  <a href="www.upappfactory.com">
                    <img
                      style="width: 150px"
                      width="150"
                      src="https://file-uploads-reciepts.s3.us-east-2.amazonaws.com/logo-1569675410384.png"
                      alt=""
                    />
                  </a>
                </td>
                <td style="text-align: right;padding-right: 15px;">
                  <a href="https://www.facebook.com/UpAppFactory/">
                    <img
                      style="width: 25px" width="25"
                      src="http://icons.iconarchive.com/icons/danleech/simple/256/facebook-icon.png"/>
                  </a>
                  <a href="https://www.linkedin.com/company/upappfactory/">
                    <img
                      style="width: 25px" width="25"
                      src="http://www.clixmarketing.com/blog/wp-content/uploads/2013/06/linkedin-icon.png"/>
                  </a>
                  <a href="https://www.instagram.com/upappfactory/">
                      <img
                    style="width: 25px" width="25"
                    src="https://i0.wp.com/lilliputadventure.com/wp-content/uploads/2018/06/Instagram-Icon.png?fit=1200%2C1200&amp;w=640"/>
                  </a>
                </td>
              </tr>
              <tr>
                <td
                  style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;"
                >
            Dear ${user.firstName} ${user.lastName},
            <br><br>
                 We thank you for signing up at UPappfactory and welcome aboard.
             <br><br>
                 Please follow the link below to verify your email,
              <br><br>
              <a href='https://www.upappfactory.com/confirmation/${verificationToken.token}' style="
              height: 50px;
              color: #fff;
              font-family: sans-serif;
              font-size: 20px;
              padding: 2% 45%;
              text-decoration: none;
              background: #2170e9;
              position: relative;
              width: 100%;
              ">Verify</a>
              <br><br>
                  If you would like to speak with one of our representatives about how your business can be empowered by a mobile app solution along with the systems, please to do at <a href="mailto:">Sales Email</a>.
              <br><br>
              You can also reach out to us through chat support available at <a href="https://www.upappfactory.com">www.upappfactory.com</a>.
              <br><br>
              Book your free demo here, <a href="https://www.upappfactory.com">Link to be a demo</a>
              <br><br>
            Regards,
            <br>
            Support Team
            <br>
            UPappfactory
      
            </td>
          </tr>
          <tr>
            <td style="padding:20px;text-align: center;background: #f7f7f7">
              <a href="mailto:contactus@upapp.co" style="text-decoration: none;"
                >contactus@upapp.co</a
              >
              |
              <a style="text-decoration: none;" href="www.upappfactory.com"
                >UPappfactory</a
              ><br /><br />
              Address: Singapore (HQ) | 531A Upper Cross Street, #04-95, Hong Lim
              Complex 051531
            </td>
          </tr>
        </table>
          
      </body>
      </html>
`
    }
    await transporter.sendMail(mailOptions)
    res.status(200).send('A verification email has been sent to ' + user.email + '.')
  } catch (e) {
    res.status(500).send(e)
  }
})

// logout on all platforms
router.post('/logoutAll', auth, async (req, res) => {
  try {
    req.user.tokens = []
    await req.user.save()
    res.status(200).send({ error: false, success: true, message: "successfully logged out on all platforms", result: {} })
  } catch (e) {
    res.status(500).send({ error: true, success: false, message: "Server error", result: e })
  }
})



module.exports = router