const express = require('express')
const Checkout = require('../models/checkout.model')


const router = new express.Router()

router.post('/checkout', async (req, res) => {
  try {
    const checkout = new Checkout(req.body)
    if (!checkout)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
    await checkout.save()
    return res.status(200).send({ error: false, success: true, message: "Success", result: checkout })
  } catch (error) {
    console.error(error)
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
  }
})

router.get('/checkout', async (req, res) => {
  try {
    const checkout = await Checkout.find({})
    if (!checkout)
      return res.status(404).send({ error: true, success: false, message: "Not Found", result: "" })
    return res.status(200).status({ error: false, success: true, message: "Found Data", result: checkout })
  } catch (error) {
    console.error(error)
    return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
  }
})

module.exports = router