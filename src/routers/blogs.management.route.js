const express = require('express')
const Blogs = require('../models/blog.management.model')
const moment = require('moment')

const router = new express.Router()

// Post new Blog
router.post('/add-blog', async (req, res) => {
  console.log(req.body, "body")
  try {
    const prevBlog = await Blogs.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
    let counter, UID, UAF
    if (prevBlog.length === 0) {
      counter = 1
    } else {
      counter = prevBlog[0].UID.slice(9)
      counter = +counter + 1
    }
    UAF = "UAF-BLOG-"
    if (counter < 10) {
      UID = `${UAF}0000000${counter}`
    } else if (counter < 100 && counter >= 10) {
      UID = `${UAF}0000000${counter}`
    } else if (counter < 1000 && counter >= 100) {
      UID = `${UAF}000000${counter}`
    } else if (counter < 10000 && counter >= 1000) {
      UID = `${UAF}00000${counter}`
    } else if (counter < 100000 && counter >= 10000) {
      UID = `${UAF}0000${counter}`
    } else if (counter < 1000000 && counter >= 100000) {
      UID = `${UAF}000${counter}`
    } else if (counter < 10000000 && counter >= 1000000) {
      UID = `${UAF}00${counter}`
    } else if (counter < 100000000 && counter >= 10000000) {
      UID = `${UAF}0${counter}`
    } else {
      UID = `${UAF}${counter}`
    }
    const blog = new Blogs(req.body)
    console.log(blog)
    blog.UID = UID
    await blog.save()
    if (!blog)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Blog Created Successfully", result: blog })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

// Get All Blog
router.get('/get-blog', async (req, res) => {
  try {
    const blog = await Blogs.find({})
    if (!blog)
      return res.status(404).send({ error: true, success: false, message: 'No Blogs Found', result: "" })
    return res.status(200).send({ error: false, success: true, message: "Found data", result: blog })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

// get blog  by UID
router.get('/get-blog/:id', async (req, res) => {
  try {
    const blog = await Blogs.findOne({ slug: req.params.id })
    if (!blog)
      return res.status(404).send({ error: true, success: false, message: "No Blog Found", result: "" })
    return res.status(200).send({ error: false, success: false, message: "Found Blog", result: blog })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

// Update blog by UID

router.put('/update-blog/:id', async (req, res) => {
  try {
    req.body.date_modified = moment().format()
    const blog = await Blogs.findOneAndUpdate({ slug: req.params.id }, { $set: req.body }, { new: true })
    console.log(blog)
    if (!blog)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Update Successfull", result: blog })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

//  Delete Blog
router.delete('/delete-blog/:id', async (req, res) => {
  try {
    const blog = await Blogs.findOneAndDelete({ UID: req.params.id })
    if (!blog)
      return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Deleted successfully", result: blog })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
  }
})

//  Get Recent Blog
router.get('/get-recent-blog', async (req, res) => {
  try {
    const blog = await Blogs.find().sort({ _id: -1 }).limit(5)
    if (!blog)
      return res.status(404).send({ error: true, success: false, message: "Not Found", result: "" })
    return res.status(200).send({ error: false, success: true, message: "Found recent blog", result: blog })
  } catch (error) {
    console.log(error)
    return res.status(500).send({ error: true, sucuess: false, message: "Server error", result: error })
  }
})


module.exports = router