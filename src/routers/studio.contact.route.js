const express = require('express')
const StudioContact = require('../models/studio.contact.model')
const nodemailer = require('nodemailer')
const moment = require('moment')
const mysql = require('mysql');

let connection = mysql.createPool({
	host: '74.208.88.152',
	user: 'admin_crm',
	password: 'Admin@123',
	database: 'admin_crm'
});


const router = new express.Router()

// Send the email
const transporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: process.env.EMAIL_USERNAME,
		pass: process.env.EMAIL_PASSWORD
	}
})
// Send the email
const personalTransporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: 'harris@upapp.co',
		pass: 'Harris@1234^'
	}
})

// // Send the email
// const BusinessTransporter = nodemailer.createTransport({
// 	host: 'smtp.zoho.com',
// 	port: 465,
// 	secure: true,
// 	auth: {
// 		user: 'samual@upappfactory.com',
// 		pass: 'samual@1234'
// 	}
// })

const OmanTransporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: 'zach@upapp.co',
		pass: 'UPapp^123'
	}
})

// CRM 

connection.getConnection(function (err) {
	if (err) {
		console.error('error connecting: ' + err.stack);
		// connection = reconnect(connection)
		return;
	}

	console.log('My sql connected ');
});

function reconnect(connection) {
	console.log("\n New connection tentative...");

	//- Destroy the current connection variable
	if (connection) connection.destroy();

	//- Create a new one
	connection = mysql.createPool({
		host: '74.208.88.152',
		user: 'admin_crm',
		password: 'Admin@123',
		database: 'admin_crm'
	});

	//- Try to reconnect
	connection.getConnection(function (err) {
		if (err) {
			//- Try to connect every 2 seconds.
			setTimeout(reconnect(connection), 2000);
		} else {
			console.log("\n\t *** New connection established with the database. ***")
			console.log('connected as id ' + connection.threadId);
			return connection;
		}
	});
}

//- Error listener
connection.on('error', function (err) {
	console.log(err, "error")
	//- The server close the connection.
	if (err.code === "PROTOCOL_CONNECTION_LOST") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Connection in closing
	else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Fatal error : connection variable must be recreated
	else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Error because a connection is already being established
	else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
	}

	//- Anything else
	else {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

});

// 

// post contact form data
router.post('/studio-contact-us', async (req, res) => {
	try {
		const prevContact = await StudioContact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAS
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAS = "UAS-CON-"
		if (counter < 10) {
			UID = `${UAS}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAS}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAS}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAS}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAS}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAS}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAS}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAS}0${counter}`
		} else {
			UID = `${UAS}${counter}`
		}
		const contact = new StudioContact(req.body)
		contact.ticket_no = Date.now()
		contact.UID = UID
		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
		let mailOptions, notify

		const message1 = `\n*****	New Customer *****\n Message: ${contact.description}\n`
		//  name,email,country,mobile,interseted_in,UID 
		// name,source,stage_id,mobile,email,country,sales_rep,lead_score,due_date,next_followup,unsubcribed_at,token,created_at,updated_at,rating_status

		connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + contact.name + "','" + contact.email + "','" + contact.country + "','" + contact.mobile + "','" + UID + "','42','18','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + contact.description + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + contact.email + "')")

		mailOptions = {
			from: process.env.EMAIL_USERNAME,
			to: req.body.email,
			subject: `Greetings from UPapp!`,
			bcc: "aslam@upapp.co,",
			html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>
							Contact Us
						</title>
					</head>
	
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="padding-left: 15px;">
										<a href="">
											<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
								</tr>
								<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
									Dear ${contact.firstname},
									<br /><br />
									This is Harris, Business & Digital Transformation Consultant at UPapp studio. I will be your consultant & my team will soon arrange a digital session with you.<br/>
									Thank you for visiting UPapp studio’s website and submitting your request to connect. We are glad!<br/>
									With our HQ in Singapore. We are a team of 90+ digital transformation consultants, UI/UX experts, project leaders and developers.<br/>
									I understand that you’re looking at building an innovative digital product of yours and we are keen to explore the same with you.<br/>
									It would be great if we can set-up a call with you so that we can share ideas and give you some insightful information regarding your requirement.<br/>
									Appreciate if you can send an email and suggest a good time to connect. Please add the team on WhatsApp/Skype below in case that's easier for you.<br/>
									Looking forward to discussing the next sensational digital innovation that we will build together for you/your organization. 
									<br /><br />
									Regards,<br><br>
																			<div style="width:100%;display: -webkit-flex;display:-ms-flexbox;display:flex;display: flex;">
									<div style="width: 100px;float: left;flex-wrap: wrap;">
									<img style="width: 100px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo_icon.png">
								</div>
									<div style="
									float: left;
									color: #615f5f;
									">
																							<p style="font-size: 16px;margin-top: 0px">
																									<span style="font-size: 18px;font-weight: 800;color: #4fbfdd;">Harris A.</span><br><span style="
							color: #555;
					">Business & Digital Transformation Consultant</span><br><span style="
							font-weight: 700;
							color: #555;
					">UPapp PTE LTD.</span><br>
																									<a href="https://join.skype.com/invite/bmqeN8vvs6hD">Skype</a> | <span style="
		color: #4FBFDD;
		font-weight:800;
 
			">WhatsApp</span> +968 71547922 (Team) <br><span style="
					color: #4FBFDD;font-weight: 800;
			">W &nbsp;&nbsp;</span><a href="www.upappfactory.com">www.upappfactory.com</a><span style="
					color: #4FBFDD;
					font-weight: 800;
			">&nbsp;&nbsp; E &nbsp;&nbsp;</span><a href="mailto:harris@upapp.co">harris@upapp.co</a><br>
																									531A, Upper Cross St. #04-95, Hong Lim Complex 051531, Singapore. <span style="
					color: #4FBFDD;
				
					font-weight: 800;
			">&nbsp;&nbsp; L &nbsp;&nbsp;</span> +65 6714 6696<br>
	<a style="float: left;margin-left: 5px;" href="https://www.linkedin.com/company/upappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/linkedIN-circle.png"></a>
	<a style="float: left;margin-left: 5px;" href="https://www.instagram.com/upappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/insta-circle-icon.png"></a>
	<a style="float: left;margin-left: 5px;" href="https://www.facebook.com/UPappfactory/"><img style="width:22px" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Email-Signature/fb-icon-circle.png"></a>
																									</p>
																					</div>
																			</div>
																	</p>
															</td>

											</tr>
							</tbody>
			</table>
</body></html>`
		}
		notify = {
			from: process.env.EMAIL_USERNAME,
			to: "aslam@upapp.co",
			subject: `#${contact.UID} Call Back Request for ${contact.firstname}, ${moment().format('L')}`,
			bcc: "ali@upappfactory.com, maqsood@upapp.co",
			html: `<!DOCTYPE html>
					<html lang="en">
		
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>Call Back Request</title>
						<style>
							.bank td,
							th {
								border: 1px solid #dddddd;
								text-align: left;
								padding: 8px;
							}
		
							.bank tr:nth-child(even) {
								background-color: #dddddd;
							}
						</style>
					</head>
		
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="
									 padding-left: 15px;
								">
										<a href="">
											<img style="width: 150px" width="150"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
		
								</tr>
								<tr>
									<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
										Hi Sales Team,
										<br>
										We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
										<br>
										<br>
										<table class="bank" style="width:100%;border-spacing: 0px;border:none">
		
											<tr>
												<td>Ticket No</td>
												<td>${contact.UID}</td>
											</tr>
											<tr>
												<td>Name</td>
												<td>${contact.firstname} ${contact.lasttname}</td>
											</tr>
											<tr>
												<td>Email Address</td>
												<td>${contact.email}</td>
											</tr>
											<tr>
												<td>Country</td>
												<td>${contact.country}</td>
											</tr>
											<tr>
												<td>Budget</td>
												<td>${contact.budget}</td>
											</tr>
											<tr>
												<td>Message</td>
												<td>${contact.description}</td>
											</tr>
										</table>
		
										<br />
										Thanks!
										<br>
										<br />
										Regards,
										<br />
										UPapp factory
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
		
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
		
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
					</html>`
		}

		await transporter.sendMail(mailOptions)
		await transporter.sendMail(notify)
		res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
	} catch (e) {
		console.error(e)
		res.status(500).send({ error: true, success: false, message: "Server Error", result: e })
	}
})


router.get('/studio-contact-us', async (req, res) => {
	try {
		const contacts = await StudioContact.find({}).sort({ _id: -1 })
		if (!contacts)
			return res.status(404).send({ error: true, success: false, message: "No requests found", result: {} })
		return res.status(200).send({ error: false, success: true, message: "Request found", result: contacts })
	} catch (e) {
		res.status(500).send({ error: true, success: false, message: "server error", result: e })
	}
})



module.exports = router