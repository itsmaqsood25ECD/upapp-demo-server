const mongoose = require('mongoose')
let MONGO_URI = "mongodb+srv://ecommdesk:ecommdesk123@up-app-factory-c76aq.mongodb.net/Production?retryWrites=true&w=majority"
if (process.env.NODE_ENV === 'production')
  MONGO_URI = 'mongodb+srv://ecommdesk:ecommdesk123@up-app-factory-c76aq.mongodb.net/Production?retryWrites=true&w=majority'
mongoose.connect(MONGO_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
})


const db = mongoose.connection
db.on('error',
  console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('DB connection is successfull')
});
// mongodb+srv://ecommdesk:ecommdesk123@up-app-factory-c76aq.mongodb.net/Production?retryWrites=true&w=majority == prod
// mongodb+srv://ecommdesk:ecommdesk123@up-app-factory-c76aq.mongodb.net/Test?retryWrites=true&w=majority == dev